:r database
go
:r dumpdb
go

if exists (select * from sysobjects
                where name = "sp__revrole" )
        drop proc sp__revrole
go

create proc sp__revrole
as
begin
set nocount on
select "exec sp_role 'grant', " +
                rtrim(r.name)+","+
                suser_name(lr.suid)
        from master..sysloginroles lr, master..syssrvroles r
        where r.srid = lr.srid
   and   suser_name(lr.suid) != 'sa'

return
end
go

grant execute on sp__revrole to public
go
