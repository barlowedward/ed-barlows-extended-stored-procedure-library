USE sybsystemprocs
go
IF EXISTS(SELECT 1 FROM sysobjects WHERE type = 'P' AND name = 'sp__monusedtables')
BEGIN
  DROP PROCEDURE sp__monusedtables
END
go
CREATE PROCEDURE sp__monusedtables
(
	@OWNER VARCHAR(30) = NULL,
   @OBJNAME VARCHAR(30) = NULL
)
AS
------------------------------------------------------------------------------------------------
-- Vers|  Date  |      Who           | DA | Description
-------+--------+--------------------+----+-----------------------------------------------------
-- 1.0 |07/04/06|  Mich Talebzadeh   |    | Reports on table usage via MDA tables
-------+--------+--------------------+----+-----------------------------------------------------
BEGIN
   IF NOT EXISTS( SELECT 1
                    FROM sysobjects
                   WHERE type = 'U'
                     AND name = ISNULL(@OBJNAME, name)
                     AND USER_NAME(uid) = ISNULL(@OWNER, USER_NAME(uid)) )
   BEGIN
      PRINT "Table: %1!.%2! was not found", @OWNER, @OBJNAME
      RETURN -1
   END

  SELECT
			 "Owner" = ISNULL(@OWNER, USER_NAME(o.uid)),
			 "TableName" = ISNULL(@OBJNAME, o.name),
			 "LogicalReads" = m.LogicalReads,
			 "LockRequests" = m.LockRequests,
			 "Operations" = m.Operations,
			 "Selected" = m.OptSelectCount,
			 "WhenLastSelected" = m.LastOptSelectDate,
			 "Used" = m.UsedCount,
			 "WhenLastUsed" = m.LastUsedDate,
			 "Inserted" = m.RowsInserted,
			 "Updated" = m.RowsUpdated,
  			 "Deleted" = m.RowsDeleted
  INTO
      #temp
  from
       sysobjects o,
       master..monOpenObjectActivity m
  where
       object_name(m.ObjectID, m.DBID) = o.name
       and user_name(o.uid) = ISNULL(@OWNER, user_name(o.uid))
       and o.name = ISNULL(@OBJNAME, o.name)
       and o.type = 'U'
       and o.id = m.ObjectID
       and m.IndexID = 0
       and m.DBID = db_id()
       and object_name(m.ObjectID, m.DBID) not like 'sa_%'
       and object_name(m.ObjectID, m.DBID) not like '%__sa%'
       and object_name(m.ObjectID, m.DBID) not like 'rs_%'
       --and (m.RowsInserted > 0 or m.RowsUpdated > 0 or  m.RowsDeleted > 0)

  SELECT
      "Owner" = user_name(o.uid),
      "TableName" =  o.name,
      "Rows_in_table" = rowcnt(i.doampg)
  INTO
      #tab1
  FROM
       sysobjects o,
       sysindexes i,
       master.dbo.spt_values d,
       #temp t
  WHERE
       user_name(o.uid) = t.Owner
       and o.name = t.TableName
       and o.type = 'U'
       and i.id = o.id
       and d.number = 1
       and d.type = 'E'

  SELECT
       Owner,
       TableName,
       "RowTotal" = sum(Rows_in_table)
  INTO
       #tab2
  FROM
       #tab1
  GROUP BY
       Owner,
       TableName

  PRINT ""
  PRINT "Displaying table statistics"
  PRINT ""
  SELECT
      --t.Owner,
      t.TableName,
      "Rows" = b.RowTotal,
		t.Inserted,
		t.Updated,
		t.Deleted,
      t.LogicalReads,
      t.LockRequests
  FROM
       #temp t,
       #tab2 b
  WHERE
       t.Owner = b.Owner
       and t.TableName = b.TableName
       --and t.WhenLastSelected is NOT NULL
       --and t.WhenLastUsed is NOT NULL
       --and t.LockRequests = 0
  ORDER BY
        t.Operations
END
go
GRANT ALL ON sp__monusedtables TO PUBLIC
go
exit
