
#!/bin/sh
if [ -z "$1" -o -z "$2" ]
then
	echo "Usage $0 SERVER PASSWORD"
	exit
fi

SERVER=$1
PASSWORD=$2

isql -Usa -S$SERVER -P$PASSWORD -iinstall1.sql  -e
isql -Usa -P$PASSWORD -S$SERVER -i $SYBASE/$SYBASE_ASE/scripts/installmontables
isql -Usa -S$SERVER -P$PASSWORD -iinstall2.sql  -e
