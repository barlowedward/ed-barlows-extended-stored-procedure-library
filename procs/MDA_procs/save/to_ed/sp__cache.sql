use sybsystemprocs
go
IF EXISTS(SELECT 1 FROM sysobjects WHERE type = 'P' AND name = 'sp__cache')
BEGIN
  DROP PROCEDURE sp__cache
END
go
create procedure sp__cache with recompile
as
#----------------------------------------------------------------------------------------------
# Vers|  Date  |      Who           | DA | Description
#-----+--------+--------------------+----+-----------------------------------------------------
# 1.0 |07/04/06|  Mich Talebzadeh   |    | Reports on cache usage based on MDA tables
#-----+--------+--------------------+----+-----------------------------------------------------
begin
set nocount on
declare @time datetime
select @time = getdate()
select CacheName,
"AllocatedMB" = convert(char(6),sum(AllocatedKB)/1024)
into #cachesize
from master..monCachePool
group by CacheName
print ""
print "Data Cache status at %1!. Server up since %2!", @time, @@boottime
print ""
print "Notes -- During I/O completion processing when a physical I/O
needs to be placed into a buffer in a cache's pool, if the buffer at
the LRU is still dirty (changed but not persisted to disk), the engine
Stalls waiting for it. The engine cannot do anything until the I/O can
be processed by storing it in that buffer. This is a bad situation from
a performance perspective."
print ""
print "==> A higher 'hit ratio' is always desirable."
print "==> Watch for 'Stalls' column below. Ideally there should be nothing in this column."
print ""
select
        "Cache Name" = substring(m.CacheName+"("+t.AllocatedMB+"MB)",1,30),
        "Logical reads" = m.LogicalReads,
        "Physical Reads" = m.PhysicalReads,
        "Hit Ratio (%)" = convert(numeric(10,1),(1.0-(m.PhysicalReads*1.0/m.LogicalReads*1.0))*100.0),
        "Physical Writes" = m.PhysicalWrites,
        "Stalls" = m.Stalls
from master..monDataCache m,
     #cachesize t
where
     m.CacheName = t.CacheName
order by
     m.CacheName
print ""
select
        "Cache Name" = CacheName,
       "Pool Type" = IOBufferSize/1024,
       "Pool size/MB" = AllocatedKB/1024,
       "Used pages" = PagesTouched,
       --"Used pages/MB" = (PagesTouched * (@@maxpagesize/1024))/1024,
       "Pages read into buffer" = PagesRead,
       "Moved to hot area" = BuffersToMRU ,
       "fetch-and-discard" = BuffersToLRU,
       "Stalls" = Stalls
from master..monCachePool
end
go
grant exec on sp__cache to public
go
exit
