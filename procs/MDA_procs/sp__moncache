use sybsystemprocs
go
/* Procedure library copyright(c) 2004 by Edward M Barlow */

IF EXISTS (SELECT * FROM sysobjects
	   WHERE  name = "sp__moncache"
	   AND    type = "P")
   DROP PROC sp__moncache
go

CREATE PROC sp__moncache(
		@num_sec_delay int=NULL,
		@num_iter int=NULL,
		@dont_format char(1)=NULL )
AS
set nocount on
declare @delay char(8)

if @num_sec_delay is null
	select CacheName, PhysicalReads, PhysicalWrites, LogicalReads,
				HitRatio=convert(numeric(10,1),(1.0-(PhysicalReads*1.0/(LogicalReads+1)*1.0))*100.0)
	from master..monDataCache
else
begin
	select 	CacheName, PhysicalReads, PhysicalWrites, LogicalReads
	into #tmp
	from master..monDataCache

	if @num_sec_delay<10
		select @delay="00:00:0"+convert(char(1),@num_sec_delay)
	else
		select @delay="00:00:"+convert(char(2),@num_sec_delay)

	if @num_iter is null
		select @num_iter=100

	while @num_iter>0
	begin
		waitfor delay @delay

		select m.CacheName, PhysicalReads=m.PhysicalReads-t.PhysicalReads,
			PhysicalWrites=m.PhysicalWrites-t.PhysicalWrites,
			LogicalReads=m.LogicalReads-t.LogicalReads,
			HitRatio=convert(numeric(10,1),(1.0-(m.PhysicalReads*1.0/(m.LogicalReads+1)*1.0))*100.0)
		from master..monDataCache m,#tmp t
		where m.CacheName = t.CacheName

		delete #tmp

		insert 	#tmp
		select 	CacheName, PhysicalReads, PhysicalWrites, LogicalReads
		from 		master..monDataCache

		select @num_iter = @num_iter - 1
	end
end

return

go

GRANT EXECUTE ON sp__moncache  TO public
go
