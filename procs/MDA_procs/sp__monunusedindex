use sybsystemprocs
go
/* Procedure copyright(c) 1995 by Edward M Barlow */

/******************************************************************************
**
** Name        : sp__monunusedindex
**
** Created By  : Ed Barlow
**
******************************************************************************/

IF EXISTS (SELECT * FROM sysobjects
           WHERE  name = "sp__monunusedindex"
           AND    type = "P")
   DROP PROC sp__monunusedindex

go

create proc sp__monunusedindex( @dont_format char(1) = null, @no_print char(1) = null)
as
begin


select Dbname=db_name(DBID),Object=object_name(ObjectID,DBID),IndexID,IndexName=i.name, ObjectID
into #tmp
from master..monOpenObjectActivity a, sysobjects o, sysindexes i
where RowsInserted=0
and   a.ObjectID=o.id  and DBID=db_id()
and   o.id = i.id and a.IndexID=i.indid
and   o.type='U'
and   RowsUpdated=0
and   RowsDeleted=0
and   PhysicalReads=0
and   PhysicalWrites=0
and   PagesRead=0
and   object_name(ObjectID,DBID) not like 'sys%'
and   object_name(ObjectID,DBID) not like 'rs_%'
--and 	IndexID=0
--and   datediff(dd,LastOptSelectDate,"Jan  1 1900")=0
--and   datediff(dd,LastUsedDate,"Jan  1 1900")=0
--and   Operations=0

create table #indexlist (
   owner      char(30) not null,
   uid        smallint not null,
   name       char(30) not null,
   index_name char(30) not null,
   id         int not null,
   indexid    smallint not null,
   clust      char(1) null,
   allow_dup  char(1) null,
   ign_dup_key char(1) null,
   uniq       char(1) null,
   suspect    char(1) null,
   keylist    char(127) null,
   status     smallint not null,
   status2     smallint not null
)

   insert into   #indexlist
   select owner      = user_name(o.uid),
          o.uid,
          name       = o.name,
          index_name = i.name,
          id = i.id,
          indexid    = i.indid,
          clust      = convert(char(1),null),
          allow_dup  = convert(char(1),null),
          ign_dup_key  = convert(char(1),null),
          uniq       = convert(char(1),null),
          suspect    = convert(char(1),null),
          keylist    = convert(char(127),"N.A."),
          status      = status, status2=i.status2
   from   sysobjects o, sysindexes i, #tmp t
   where  i.id   = o.id
   and    i.id = ObjectID and i.indid=IndexID
   --and    o.type in ("U",@show_type)
   and      indid > 0

/* delete multiple rows */
delete #indexlist
from   #indexlist a, #indexlist b
where  a.indexid = 0
and    b.indexid != 0
and    a.name = b.name

update #indexlist
set    clust='Y'
where  indexid = 1
or			status&16=16
or  		status2&512 = 512

update #indexlist
set    uniq = 'Y'
where  status & 2 = 2

update #indexlist
set    ign_dup_key = 'Y'
where  status & 1 = 1

update #indexlist
set    allow_dup = 'Y'
where  status & 64 = 64

update #indexlist
set    suspect = 'Y'
where  status & 32768 = 32768
or     status2&8192= 8192

declare @count int
select  @count=1

while ( @count < 17 )   /* 16 appears to be the max number of indexes */
begin

   if @count=1
      update #indexlist
      set    keylist=index_col(name,indexid,1,uid)
      where  index_col(name,indexid,@count,uid) is not null
   else
      update #indexlist
      set    keylist=rtrim(keylist)+","+index_col(name,indexid,@count,uid)
      where  index_col(name,indexid,@count,uid) is not null

   if @@rowcount=0   break

   select @count=@count+1
end

update #indexlist
set    name=convert(char(30), rtrim(rtrim(substring(owner,1,15)) + "." +name))
where  owner!="dbo"

        if @no_print is null
        begin
        print "   INDEX KEY:     c = clustered            u = unique"
        print "                  a = allow dup row        s = suspect"
        print "                  i = ignore dup key "
        print ""
		end

   select "Name" = rtrim(name)+"."+index_name,
       c   = isnull(clust,""),
       u   = isnull(uniq,""),
       i   = isnull(ign_dup_key,""),
       a   = isnull(allow_dup,""),
       s   = isnull(suspect,""),
       "List of Index Keys"    = keylist
   from #indexlist
   order by owner,name,indexid

/*
select *
from #tmp
order by Dbname,Object,IndexID

drop Table #tmp
*/

end
go
