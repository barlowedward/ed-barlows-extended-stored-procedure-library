/* Procedure copyright(c) 1993-1995 by Simon Walker
********************************************************************************
** Modified by Tom Lu
** adding "Lock Scheme" to distinguish DOL tables from Allpages Lock tables
**
** Modified by Ludek Svozil, www.svozil-consulting.ch, April 2006
** Corrected "No_Clustered_Index" section for DOL tables
**
********************************************************************************
*/
:r database
go
:r dumpdb
go

if exists (select *
           from   sysobjects
           where  type = 'P'
           and    name = "sp__noindex")
begin
    drop procedure sp__noindex
end
go

create procedure sp__noindex ( @dont_format char(1) = null)
as
begin

    set nocount on

    select No_Indexes = o.name,
           "Rows" = rowcnt(i.doampg),
                Pages= data_pgs(o.id,i.doampg)
    from   sysobjects o, sysindexes i
    where  o.type = "U"
    and    o.id = i.id
    and    i.indid = 0
    and    o.id not in (select o.id
                        from   sysindexes i,
                               sysobjects o
                        where  o.id = i.id
                        and    o.type = "U"
                        and    i.indid > 0)
    order by Pages desc, rowcnt(i.doampg) desc


    -- APL tables first
    select No_Clustered_Index = o.name,
           "Rows" = rowcnt(i.doampg),
           Pages= data_pgs(o.id,i.doampg),
	   "Lock Scheme" = case when sysstat2 & 57344 = 16384 then "Datapages"
				when sysstat2 & 57344 = 32768 then "DataRows"
				else "Allpages"
				end
    from   sysindexes i,
           sysobjects o
    where  o.id = i.id
    and    o.type= "U"
    and    i.indid = 0
    and    sysstat2 & 57344 <> 16384 and sysstat2 & 57344 <> 32768  --APL

    UNION ALL -- add DOL tables with no clustered index
    select No_Clustered_Index = o.name,
           "Rows" = rowcnt(i.doampg),
           Pages= data_pgs(o.id,i.doampg),
	   "Lock Scheme" = case when sysstat2 & 57344 = 16384 then "Datapages"
				when sysstat2 & 57344 = 32768 then "DataRows"
				else "Allpages"
				end
    from   sysindexes i,
           sysobjects o
    where  o.id = i.id
    and    o.type= "U"
    and    i.indid = 0
    and   (sysstat2 & 57344 = 16384 OR sysstat2 & 57344 = 32768)
    and    not exists (
		select 'x' from sysindexes x
		where i.id = x.id
		and (status&16 = 16
		or status2&512 = 512)
	   )
   order by "Lock Scheme", Pages desc, rowcnt(i.doampg) desc

end
go

grant execute on sp__noindex to public
go
