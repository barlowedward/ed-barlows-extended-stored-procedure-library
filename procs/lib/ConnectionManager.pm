#Copyright (c) 2005-2006 by SQL Technologies.. All rights reserved.

package ConnectionManager;

use strict;
use vars qw($VERSION @ISA @EXPORT @EXPORT_OK);
use Exporter;
use strict;

use Repository;
use DBI;
use DBIFunc;
use Sys::Hostname;
use Net::Ping::External qw(ping);
use Do_Time;
use Tk;
use CommonFunc;
use Net::myFTP;

my($sybase_ok,$oracle_ok,$odbc_ok)=("FALSE","FALSE","FALSE");

$VERSION= 1.0;
my $opt_d;
my $sqltextref;
#my $global_data;
my @pre_sql_msgs;	# messages generated before sqltext is ready

@ISA      = ( 'Exporter' );

my(%connectionhash,%connectionstatus,%connectiondir);		# key is servername:type
my($unix_connection_type);
my(@av_drivers);
my(%avail_dsns);
my($dsn_list);

sub new {
	my( $class,%args) = @_;
	$opt_d = $args{-debug};
	$sqltextref = $args{-sqltext};
	#$global_data = $args{-global_data};
	my $self  = bless { %args }, $class;

	die "Must pass -statusmsg to connectionmanager\n"
		unless defined $args{-statusmsg};

	dbi_set_option( -print=>$args{-statusmsg} );
	$args{-statusmsg}->( "[connectmgr] Initializing Connection Manager\n" );
	$args{-statusmsg}->( "[connectmgr] Loading DBI Drivers\n" );
	@av_drivers=DBI->available_drivers();
	foreach my $dt (@av_drivers) {
		$args{-debugmsg}->( "[connectmgr] Testing Found Driver $dt\n" );

		if( $dt=~/ODBC/ ) {
			$odbc_ok="TRUE";
		} elsif ( $dt=~/Oracle/ ) {
			$oracle_ok="TRUE";
		} elsif(  $dt=~/Sybase/ ) {
			$sybase_ok="TRUE";
		} else {
			next;
		}
		eval {  DBI->data_sources($dt);	};

		if( $@ ) {
			$args{-debugmsg}->( "[connectmgr]   Errors for $dt : $@\n" );
			#print "ERRORS FOUND for $dt $@\n";
			if( $dt=~/ODBC/ ) {
				$odbc_ok="FALSE";
			} elsif ( $dt=~/Oracle/ ) {
				$oracle_ok="FALSE";
			} elsif(  $dt=~/Sybase/ ) {
				$sybase_ok="FALSE";
			}
		} else {
			$args{-debugmsg}->( "[connectmgr]   No Errors for $dt \n" );
			#print "OK $dt \n";
			foreach ( DBI->data_sources($dt)) {
				$dsn_list.= $_.",";
				#print "FOUND $_\n";
				s/DBI:$dt://i;
				s/server=//i;
				$avail_dsns{$_}=$dt;
			}
		}
	}
	
	if( is_nt() ) {
		$oracle_ok="TRUE";
		$sybase_ok="TRUE";
	}
		
	$args{-debugmsg}->( "[connectmgr]   Completed Driver Check \n" );
	$dsn_list =~ s/,$//;
	my($f)=$args{-global_data};
	#foreach (keys %avail_dsns) { print "$_ - $avail_dsns{$_} \n"; }
	my($hostn)=hostname();
	&$f(-class=>'available_dsns', -name=>$hostn, -value=>$dsn_list);
	&$f(-class=>'available_drivers', -name=>$hostn, -value=>\@av_drivers);
	$args{-debugmsg}->( "[connectmgr] Building Internal Connection Hashes\n" );
	foreach my $type ( main::get_server_types() ) {
		my($hptr)=main::get_svrlist_ptr($type);

		foreach (@$hptr ) {
			# this returns Ok, Fail, Unknown or undef
			my($newstate)=&$f(-class=>$type, -name=>$_, -arg=>"Last_Conn_State") || "Unknown";

			if( $newstate eq "Ok" ) {
				$newstate="Disconnected" if $type eq "sybase" or $type eq "sqlsvr" or $type eq "oracle" or $type eq "unix";
			} else {
				# if this is set then you have connected at some point in the past
				my($Last_Ok_Conn_Time) = &$f(-class=>$type, -name=>$_, -arg=>"Last_Ok_Conn_Time");
				#print "TYPE $type HOST $_ Last_Ok_Conn_Time=$Last_Ok_Conn_Time\n";
				$newstate="Never" unless defined $Last_Ok_Conn_Time;
			}
			$connectionstatus{$_.":".$type} = $newstate;
		}
	}
	$args{-statusmsg}->( "[connectmgr] Done Initializing Connection Manager\n" );

	return $self;
}

sub qs_get_dbi {
#	my(@av_drivers)=DBI->available_drivers();
#	my($dsn_list, %avail_dsns);
#	_debugmsg( "[func] Reading DBI Connections\n" );
#	foreach my $dt (@av_drivers) {
#		next unless $dt=~/ODBC/ or $dt=~/Oracle/ or $dt=~/Sybase/;
#		eval {  DBI->data_sources($dt);	};
#
#		if( $@ ) {
#			_statusmsg( "[func] ERRORS FOUND for $dt $@\n" );
#		} else {
#			_debugmsg( "[func]   Processing DSN Type $dt \n" );
#			foreach ( DBI->data_sources($dt)) {
#				$dsn_list.= $_.",";
#				# _debugmsg( "[func] FOUND $_\n" );
#				s/DBI:$dt://i;
#				s/server=//i;
#				$avail_dsns{$_}=$dt;
#			}
#		}
#	}
	return $dsn_list, %avail_dsns;
}

sub log_dbi_configuration {
	my($package)=@_;
	$package->log_sql( "log_dbi_configuration(): Testing For Available DBI Drivers\n");
  	$package->log_sql( "Available Drivers:",join(" ",@av_drivers),"\n");
  	#foreach (sort keys %avail_dsns) {
  	#	$package->log_sql( $_," ",$avail_dsns{$_},"\n" );
	#}

	if( $sybase_ok eq "TRUE" and ! defined $ENV{SYBASE} ){
		$sybase_ok="FALSE";
		$package->log_sql( "SYBASE IS NOT OK - NO SYBASE ENVIRONMENT VARIABLE\n"  );
	}

	$package->log_sql( "Sybase DBI Driver Loaded\n") 	if $sybase_ok 	eq "TRUE";
	$package->log_sql( "Oracle DBI Driver  Loaded\n") 	if $oracle_ok 	eq "TRUE";
	$package->log_sql( "ODBC DBI Driver  Loaded\n"  ) 	if $odbc_ok 	eq "TRUE";

	$package->log_sql( "WARNING: Sybase DBI Driver Not Installed\n") unless  $sybase_ok eq "TRUE";
	$package->log_sql( "WARNING: Oracle DBI Driver Not Installed\n") unless  $oracle_ok eq "TRUE";
	$package->log_sql( "WARNING: ODBC DBI Driver Not Installed\n"  ) unless  $odbc_ok   eq "TRUE";
}

# if pass -login in then dont create class info
sub connect {
	my($package)=shift;
	my(%args)=@_;
	#print "DBG DBG: connectionmanager->connect(".join(" ",@_).")\n";
	
	my($key)=$args{-name}.":".$args{-type};
	
	if( defined $connectionhash{$key} and defined $args{-reconnect} ) {		
		$package->{"-statusmsg"}->( "[connectmgr] connect($args{-name},$args{-type}) reconnect specified\n" );
		if( $args{-type} eq "unix" ) {
			my($c)=$connectionhash{$key}->reconnect(%args);
			$connectionhash{$key}=$c;
			return $c;
		}
		$package->disconnect(%args);
	}
	
	if( defined $connectionhash{$key} ){
		$package->{-statusmsg}->( "[connectmgr] connect($args{-name},$args{-type}) existing connection(status=". $connectionstatus{$key}. ")\n" );
		if( defined $args{-returnpwd} ) {
			${$args{-returnpwd}}=$connectiondir{$args{-name}};
		}
		return $connectionhash{$key};
	}
	$package->{-statusmsg}->( "[connectmgr] connect($args{-name},$args{-type})\n" ) unless $args{-quiet};
	my($f)=$$package{-global_data} || \&main::global_data;
	if( ! defined $args{-login} ) {
	   	&$f(-class=>$args{-type}, -name=>$args{-name},  -arg=>'Last_Conn_Time', -value=>time );
	   	&$f(-class=>$args{-type}, -name=>$args{-name},  -arg=>'Last_Conn_State', -value=>"Fail" );
	}

	my($login,$password);
	if( $args{-type} ne "win32servers" ) {	
		if( $args{-login} and $args{-password} and $args{-type} ) {
			$login	=	$args{-login};
			$password=	$args{-password};
		} elsif( defined $$package{-server_info} ) {
			if( $$package{-server_info}{$args{-name}.":".$args{-type}} ) {
				$login=$$package{-server_info}{$args{-name}.":".$args{-type}}{login};
				$password=$$package{-server_info}{$args{-name}.":".$args{-type}}{password};
			}
		}
			
		if( ! defined $login ) {
		    ($login,$password) = get_password( -name=>$args{-name}, -type=>$args{-type}, -debug=>$args{-debug} );
		}
	} 
	my($type)= $args{-type};

	if( lc($type) eq "sybase" and $sybase_ok eq "FALSE" ) {
		$package->{-statusmsg}->( "[connectmgr] failed - type=$type but dbi driver not ok \n" );
		$connectionstatus{$key}="Config Err";
		$connectionhash{$key}=undef;
		return undef,"sybase dbi driver bad";
	}

	if( lc($type) eq "sybase" and ! defined $ENV{SYBASE} ) {
		$package->{-statusmsg}->( "[connectmgr] failed - type=$type but SYBASE ENV variable not set\n" );
		$connectionstatus{$key}="Config Err";
		$connectionhash{$key}=undef;
		return undef,"no \$SYBASE environment variable";
	}

	if( lc($type) eq "oracle" and $oracle_ok  eq "FALSE" ) {
		$package->{-statusmsg}->( "[connectmgr] failed - type=$type but dbi driver not ok \n" );
		$connectionstatus{$key}="Config Err";
		$connectionhash{$key}=undef;
		return undef,"oracle dbi driver bad";
	}

	if( uc($type) eq "ODBC"   and $odbc_ok  eq "FALSE") {
		$package->{-statusmsg}->( "[connectmgr] failed - type=$type but dbi driver not ok \n" );
		$connectionstatus{$key}="Config Err";
		$connectionhash{$key}=undef;
		return undef,"Odbc dbi driver bad";
	}

	if( $args{-type} ne "win32servers" and (! defined $login or ! defined $password )) {
		$package->log_sql( "WARNING: Cant Find Login Or Password for $args{-name}\n");
		$connectionstatus{$key}="Invalid Login/Pass";
		$connectionhash{$key}=undef;
		#$package->{-statusmsg}->( "[connectmgr] WARNING: Cant Find Login Or Password for $args{-name}\n" );
		return undef,"Cant find login or password";
	}

	my($c);		# the conneciton
	if( $args{-type} eq "unix" ) {		# perform an ftp connection
		#my($method)=&$f(-class=>'install',-name=>'win32tounix') if is_nt();
		#$method=&$f(-class=>'install',-name=>'unixtounix')  unless is_nt();
		
		my($method);
		if( $args{-win32method} and is_nt() ) {
			$method=$args{-win32method};
		}elsif( $args{-unixmethod} and ! is_nt() ) {
			$method=$args{-unixmethod};
		} elsif( is_nt() ) {
			if( defined $$package{-server_info} ) {
			 	$method=$$package{-server_info}{$args{-name}.":unix"}{WIN32_COMM_METHOD};
			}			
		} else {
			if( defined $$package{-server_info} ) {
			 	$method=$$package{-server_info}{$args{-name}.":unix"}{UNIX_COMM_METHOD};
			}
		}
		
		if( ! $method ) {
				$connectionhash{ $key } = undef;
         	$connectionstatus{$key} = "Fail";
         	$package->{-statusmsg}->( "[connectmgr] Skipping $args{-name} - Method Not specified\n" );
         	$package->{-statusmsg}->( "[connectmgr] This probably indicates a configuration error\n" );
         	#if( defined $$package{-server_info} ) {
         	#	my(%x)=%{$$package{-server_info}{$args{-name}.":unix"}};
         		#foreach ( keys %x ) {
         		#	$package->{-statusmsg}->( "[connectmgr] DBG DBG KEY=$_ VAL=".$x{$_}."\n" );
         		#}
         	#}
				return undef,"Ignored GEM configuration error";
		}

		if( $method eq "NONE" ) {
				$connectionhash{ $key } = undef;
         	$connectionstatus{$key} = "Fail";
         	$package->{-statusmsg}->( "[connectmgr] Skipping $args{-name} - Method=NONE specified\n" );
				return undef,"Ignored per user directive";
		}
			
		$package->{-statusmsg}->( "[connectmgr] Using Net::myFTP method=$method to $args{-name}\n" );
		$c = Net::myFTP->new($args{-name}, 		
					PRINTFUNC=>$package->{-statusmsg}, 
					DEBUGFUNC=>$package->{-debugmsg}, 
					Debug=>$args{-debug}, 
					METHOD=>$method);
		if( ! $c )  {
			$connectionhash{ $key } = undef;
         $connectionstatus{$key} = "Fail";
         $package->{-statusmsg}->( "[connectmgr] WARNING: Cant create connection to $args{-name}\n" );
			return undef,"Cant create $method connection";
		}

		my($rc,$err)=$c->login($login,$password);
		if( ! $rc ) {
			$connectionhash{ $key } = undef;
         $connectionstatus{$key} = "Fail";
         $package->{-statusmsg}->( "[connectmgr] Cant $method login to $args{-name} : $err \n" );         
			return undef,"$method $err";
		}
		$connectiondir{$args{-name}} = $c->pwd();

		if( defined $args{-returnpwd} ) {
			${$args{-returnpwd}} = $connectiondir{$args{-name}};
		}
		$c->ascii();

		if( ! $c->cwd("/") ) {
			$connectionhash{ $key } = undef;
      	$connectionstatus{$key} = "Fail";
      	$package->{-statusmsg}->( "[connectmgr] WARNING: Cant directory list '/' on $args{-name}\n" );
			return undef,"Cant do directory listing of /";
		}
	} elsif( $args{-type} eq "win32servers" ) {
		# errr... i dunno... ping it???
		#$package->{-statusmsg}->( "[connectmgr] Pinging Win32 Server $args{-name}\n" );
		my($alive)=$package->_ping(%args);
		$package->{-statusmsg}->( "[connectmgr] Pinging Win32 Server $args{-name} returned $alive\n" );
		if( $alive ) {
			$connectionhash{ $key } = 1;
     		$connectionstatus{$key} = "Ok";
     		$c=1;
     	} else {
			$connectionhash{ $key } = undef;
   		$connectionstatus{$key} = "Fail";
   		$c=undef;
     	}
     	$package->{-statusmsg}->( "[connectmgr] done ping\n");
	} else {
      		my($ctype)=$type;
      		$ctype="ODBC" if is_nt();
      		$package->{-statusmsg}->( "[connectmgr] creating dbi connection to $args{-name}\n" ) unless $args{-quiet}; 		
      		
      		$c=dbi_connect(	-srv=>$args{-name},
      				-login=>$login,
      				-password=>$password,
      				-type=>$ctype,
      				-connection=>1,
      				-debug=>$args{-debug},
      				-quiet=>$args{-quiet} );

      		if( ! $c ) {
      			$package->{-statusmsg}->( "[connectmgr] connection failed\n" ) unless $args{-quiet};
      			$connectionhash{ $key } = undef;
         		$connectionstatus{$key} = "Fail";
         		if( defined $avail_dsns{$args{-name}} ) {
         			$package->log_sql("WARNING: Connect Failed to server=$args{-name}, typ=$args{-type}, login=$login\n","red");
         			return undef,"Connection Failed";
         			#$package->{-statusmsg}->( "[connectmgr] Unable to connect to server $args{-name} as login $login : dsn=$avail_dsns{$args{-name}}\n" );
         		} else {
         			if( is_nt() ) {
         				$package->log_sql("WARNING: Unable to Connect -  Server $args{-name} has No ODBC Settings\n","red");
         				return undef,"Unable to find ODBC settings for server";
         			} else {
         				$package->log_sql("WARNING: Unable to connect to server $args{-name} as login $login : no dsn available\n","red");
         				return undef,"Unable To Connect to $args{-name}...";
         			}
         		}
      		}

      		if( $args{-type} eq "sybase" or $args{-type} eq "sqlsvr" ) {
	      		# test connection to see if it really is an ase server...
					my(@d)=dbi_query(-connection=>$c, -db=>"master",
						-query=>"select name from sysobjects where name='sysobjects'");
					foreach (@d) {
						my($n)=dbi_decode_row($_);
						last if $n eq "sysobjects";
												
						if( $n=~/user api layer: external error: This routine cannot be called because another command struct/ ) {							
							$package->log_sql("WARNING: Unable to Connect -  Server $args{-name} is not an ASE or SQL server\n","red");
		         		return undef,"Unable to Connect -  Server $args{-name} is not an ASE or SQL server";
						}
						
						print "[ERROR] Woah... select returned $n!!!\n";
						print "[ERROR] this is part of ",join(",",dbi_decode_row($_)),"\n";
		
						$package->log_sql("WARNING: Unable to Connect -  Server $args{-name} is not an ASE or SQL server\n","red");
		         	return undef,"Unable to Connect -  Server $args{-name} Returned Abnormal Query Results";
					}
				}

   		dbi_set_mode("INLINE");
   	}

   	$connectionstatus{$key} = "Ok";

		if( ! defined $args{-login} ) {
			&$f(-class=>$args{-type}, -name=>$args{-name},  -arg=>'Last_Conn_State', -value=>"Ok" );
	   	&$f(-class=>$args{-type}, -name=>$args{-name},  -arg=>'Last_Ok_Conn_Time', -value=>time );
   	}

   	$connectionhash{ $key } = $c;
   	$package->log_sql("Successful Connection to server  $args{-name}\n");
   	$package->{-statusmsg}->( "[connectmgr] Successful Connection to server $args{-name}\n");
		return $c;
}

sub _ping {
	my($package)=shift;
	my(%args)=@_;

	my($key)=$args{-name}.":".$args{-type};
	if( $args{-type} eq "unix" or $args{-type} eq "win32servers" ) {
		my($p);
		$package->{-statusmsg}->( "[connectmgr] Pinging  $args{-name} (type=$args{-type}) With Timeout=>2\n" );
		my($alive);
		
		local $SIG{ALRM} = sub { die "CommandTimedout"; };
		eval {
			alarm(2);	
			$alive=ping(hostname=>$args{-name}, timeout=>2);
		  	alarm(0);
		};
		if( $@ ) {
			if( $@=~/CommandTimedout/ ) {
				_statusmsg( "[connectmgr] WOAH!!!  PING COMMAND TIMED OUT.\n" );			
			} else {
				_statusmsg( "[connectmgr] WOAH!!!  PING Command Returned $@\n" );
				alarm(0);
			}
		}   		
		
		#my($alive)=ping(hostname=>$args{-name}, timeout=>2);
		$package->{-debugmsg}->( "[connectmgr] Ping Returned $alive\n" ) if $package->{-debugmsg};
		if( $alive ) {
			$connectionhash{ $key } = 1;
   		$connectionstatus{$key} = "Ok";
   		return 1;
   	} else {
			$connectionhash{ $key } = undef;
   		$connectionstatus{$key} = "Fail";
   		return 0;
   	}

		#if( $p->ping($args{-name})) {
		#	print "successful ping of $args{-name}\n";
		#	$connectionstatus{$key}="Ping Ok";
		#	return 1;
		#} else {
		#	print "failed ping of $args{-name}\n";
		#	$connectionstatus{$key }="Ping Fail";
		#	return 0;
		#}
	}

	$package->{-statusmsg}->( "[connectmgr] Pinging Server $args{-name} (type=$args{-type})\n" );
	my($c,$errmsg)=$package->connect(%args);
		if( ! $c ) {
		$connectionstatus{ $key } = "Fail";
		return 0;
	}

	if( $args{-disconnect} ) {
		$connectionstatus{$key } = "Disconnected";
		$package->disconnect(%args);
	} else {
		$connectionstatus{$key} = "Ok";
	}
	return 1;
}

sub connection_status {
	my $package=shift;
	my(%args)=@_;
	my($rc)="Unknown";
	die "NO PACKAGE" unless defined $package;
	#print "DBG DBG connection_status $args{-name} type=$args{-type}\n";
	if( ! defined $args{-name} ) {
		die "Must pass -type to connection_status() if no name passed\n"
			unless defined $args{-type};

		my(%state_by_sys);
		my($srch)=":".$args{-type};
	#print "DBG DBG - printing keys for connectionstatus\n";
		foreach (keys %connectionstatus) {
	#print "DBG DBG connection_status - key=$_ state=$state_by_sys{$_}\n";
			next unless $_ =~ /$srch$/;
			my($sys,$typ)=split(/:/);
			$state_by_sys{$sys}=$connectionstatus{$_};
		}
		return %state_by_sys;
	} else {
		die "ConnectionManager::connection_status requires -type arg\n" unless defined $args{-type};
		my($key)=$args{-name}.":".$args{-type};
		if( defined $connectionstatus{$key} ){
			$rc= $connectionstatus{$key};
		} else {
			#my($f)=$$package{-global_data};
			#my($state) = global_data(-class=>$args{-type}, -name=>$args{-name}, -arg=>"state");
			#if( defined &$f(-class=>$args{-type}, -name=>$args{-name},  -arg=>'Last_Ok_Conn_Time' )){
			#	$rc= "Not Connected";
			#} else {
			#	$rc= "Never Connected";
			#}
			$rc="Unknown";
		}
		#$$package{-statusmsg}->("[ConnectionManger] returning connection_status($key}) => $rc \n");
		return $rc;
	}
}

sub decode_row {
	my($package,$row)=@_;
	return dbi_decode_row($row);
}

sub query {
	my($package)=shift;
	my(%args)=@_;
	$package->{-statusmsg}->(  "Returning : No Connection\n")
		if defined $args{-debug} and ! defined $args{-connection};
	return unless defined $args{-connection};
	$package->{-statusmsg}->(  "Returning : No Connection\n")
		if defined $args{-debug} and $args{-connection}==1;
	return if $args{-connection} == 1;

	my($str); foreach ( keys %args ) { $str.= $_."->".$args{$_}." " unless $_ eq "-connection" }
	$package->log_sql("executing ".$str);
	my(@rc)=dbi_query(%args);
	#print "DBG: QUERY COMPLETED - RETURNING $#rc\n";
	return @rc;
}

sub disconnect {
	my($package,%args)=@_;
	die "Must pass servername to ConnectionManager::Disconnect\n" 	unless defined $args{-name};
	die "Must pass type to ConnectionManager::Disconnect\n" 	unless defined $args{-type};
	my($key)=$args{-name}.":".$args{-type};
	
	return unless defined $connectionhash{$key};	# no point
	
	my($is_connected)="(Not Connected)" if defined $connectionhash{$key};
	
	if( $args{-type} eq "unix" ) {
		$connectionhash{$key}->close();
	} elsif( $args{-type} eq "win32servers" ) {
	} else {
		dbi_disconnect(-connection=>$connectionhash{$key});
	}
	delete $connectionhash{$key};

	$connectionstatus{$key}="Disconnected";

	$package->log_sql("disconnecting from $args{-name} $is_connected\n","blue");
}

sub log_sql {
	my($package,$msg,$tag)=@_;
	chomp $msg;
	#print $msg,"\n";
	if( defined $$sqltextref ) {
	   	if( defined $tag ) {
		   	$$sqltextref->insert('1.0',": $msg\n",$tag);
		} else {
		   	$$sqltextref->insert('1.0',": $msg\n");
		}
		$$sqltextref->insert('1.0',do_time(-fmt=>"yyyy.mm.dd hh:mi"),"blue");
	}
}

1;

__END__

=head1 NAME

ConnectionManager.pm - Persistent connections class

=head2 DESCRIPTION

This simple interface allows you to keep connections alive between
requests.  There is no timeout involved so its probably buggy.

=head2 FUNCTIONS

=over 4

=item * connect

=item * connection_status

 returns connection status:Ok/Disconnected/Fail/Never/Config Err/"Invalid Login/Pass"
 the saved values are stored in Last_Conn_State and Last_Ok_Conn_Time.  The Last_Conn_State
 values are Ok/Fail/Unknown

 if no -name : return hash of connection statuses for passed in type
 if    -name then return scalar

=item * decode_row

=item * disconnect(-name=>xxx, -type=>xxx)

=item * log_dbi_configuration

internal only

=item * log_sql

internal only

=item * new


=item * _ping

=item * query

=back
