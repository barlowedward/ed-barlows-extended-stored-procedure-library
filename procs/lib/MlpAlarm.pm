#
# MlpAlarm - Master Alarm Router Partners
#
# copyright (c) 2005-2006 by SQL Technologies.  All Rights Reserved.

package  MlpAlarm;

# For documentation on this file - type perldoc MlpAlarm.pm
#
# This file best viewed at tabstop=3
#
use   vars qw($VERSION @ISA @EXPORT @EXPORT_OK); use   strict;

my($DEVELOPER_IPADDR)="10.5.103.51";
my($DBGDBG);

# INSTALLATION INSTRUCTIONS
#    - edit the setup variables section below for the name and login/password for your database
#    - modify the BEGIN Block  to point to your sybase
#
# The next line contains the public login for the repository - this is a public account and it
# only has permissions on the appropriate tables as per the setup instructions for the database.
# Database installation notes are distributed in the monitoring/sql directory.
#
my($server,$login,$password,$db)=("DEVNULL","gemalarms","gemalarms","gemalarms");

#
# you should also add any necessary environment variables in the begin block.
#
BEGIN {
	if( -d "/export/home/sybase" and ! defined $ENV{SYBASE} ) {
		$ENV{SYBASE}="/export/home/sybase";
	} elsif( -d "/apps/sybase" and ! defined $ENV{SYBASE} ) {
		$ENV{SYBASE}="/apps/sybase";
	}
}
#################### END SETUP VARIABLES ################################

# my($SERVICE)=9998;
# my($SENDNETWORK)=";229.14.14.14;229.14.14.15";
# my($RECVNETWORK)=";229.14.14.15;229.14.14.14";
# my($DAEMON, $SUBJECT);

my(%mlp_batch_args,$job_started,$job_curstep,$job_curstepname);

use   	Carp;
use  	Do_Time;
use	CommonFunc;
use 	DBIFunc;
use 	File::Basename;
use 	Sys::Hostname;
my($this_hostname);

require  Exporter;
@ISA     = qw(Exporter);
@EXPORT  = qw(MlpEvent MlpTestInstall MlpHeartbeat MlpPerformance MlpGetCategory
	MlpGetScreens MlpRunScreen MlpGetContainer MlpGetDbh MlpGetReportCode MlpGetProgram MlpGetSeverity
	MlpManageData MlpGetOperator MlpEventUpdate MlpCleanup MlpGetAlarms MlpClearAlarms MlpBatchStep
	MlpSetBackupState MlpGetBackupState MlpSetPL MlpClearPL MlpSetVars MlpGetVars MlpSyncConfigFiles
	MlpMonitorStart MlpBatchStart MlpAgentStart MlpBatchRunning MlpBatchDone MlpAgentDone MlpBatchErr MlpReportArgs MlpSaveNotification);

$VERSION = '1.0';


#############################
# GLOBAL VARIABLES          #
#############################
my($i_am_initialized)="FALSE";
my($quiet);
my($monitor_program,$system,$debug);
my($server_type);
my(%condense_args);

##############################################################################
# METHOD ARGUMENTS                                                           #
# Set to 1 if Required, optional if 0.  key is func:value	                 #
# all method arguments must be in this array which is used for validation    #
##############################################################################

my(%OK_OPTS)= (
	"MlpEvent:-debug"			=> 0,
	"MlpEvent:-monitor_program"		=> 1,
	"MlpEvent:-system"			=> 1,
	"MlpEvent:-subsystem"			=> 0,
	"MlpEvent:-event_time"			=> 0,
	"MlpEvent:-severity"			=> 1,
	"MlpEvent:-event_id"			=> 0,
	"MlpEvent:-message_text"	  	=> 1,
	"MlpEvent:-message_value"		=> 0,
	"MlpEvent:-document_url"		=> 0,
	"MlpEvent:-condense"		=> 0,
	"MlpEvent:-nosave"		=> 0,
	"MlpEvent:-quiet"		=> 0,

	"MlpEventUpdate:-debug"			=> 0,
	"MlpEventUpdate:-monitor_program"		=> 1,
	"MlpEventUpdate:-system"			=> 1,
	"MlpEventUpdate:-subsystem"			=> 0,
	"MlpEventUpdate:-event_time"			=> 1,
	"MlpEventUpdate:-severity"			=> 0,
	"MlpEventUpdate:-event_id"			=> 0,
	"MlpEventUpdate:-message_text"	  	=> 1,
	"MlpEventUpdate:-message_value"		=> 0,
	"MlpEventUpdate:-document_url"		=> 0,
	"MlpEventUpdate:-quiet"			=> 0,

	"MlpHeartbeat:-debug"			=> 0,
	"MlpHeartbeat:-monitor_program"		=> 1,
	"MlpHeartbeat:-system"			=> 1,
	"MlpHeartbeat:-subsystem"		=> 0,
	"MlpHeartbeat:-state"			=> 1,
	"MlpHeartbeat:-message_text"		=> 0,
	"MlpHeartbeat:-document_url"		=> 0,
	"MlpHeartbeat:-batchjob"		=> 0,
	"MlpHeartbeat:-event_time"		=> 0,
	"MlpHeartbeat:-quiet"			=> 0,
	"MlpHeartbeat:-reset_status"		=> 0,

	"MlpPerformance:-debug"			=> 0,
	"MlpPerformance:-monitor_program"	=> 1,
	"MlpPerformance:-system"		=> 1,
	"MlpPerformance:-subsystem"		=> 0,
	"MlpPerformance:-value1"		=> 0,
	"MlpPerformance:-value2"		=> 0,
	"MlpPerformance:-value3"		=> 0,
	"MlpPerformance:-value4"		=> 0,
	"MlpPerformance:-value5"		=> 0,
	"MlpPerformance:-value6"		=> 0,
	"MlpPerformance:-value7"		=> 0,
	"MlpPerformance:-quiet"			=> 0,

	"MlpGetAlarms:-test"			=> 0,

	"MlpGetCategory:-user"			=> 0,
	"MlpGetCategory:-debug"			=> 0,

	"MlpGetBackupState:-system"		=> 0,
	"MlpGetBackupState:-production"	=> 0,
	"MlpGetBackupState:-orderby"		=> 0,
	"MlpGetBackupState:-debug"			=> 0,

	"MlpSetBackupState:-system"		=> 1,
	"MlpSetBackupState:-dbname"		=> 1,
	"MlpSetBackupState:-debug"		=> 0,
	"MlpSetBackupState:-last_fulldump_time"		=> 0,
        "MlpSetBackupState:-last_fulldump_file"		=> 0,
        "MlpSetBackupState:-last_fulldump_lsn"		=> 0,
        "MlpSetBackupState:-last_trandump_time"		=> 0,
        "MlpSetBackupState:-last_trandump_file"		=> 0,
        "MlpSetBackupState:-last_trandump_lsn"		=> 0,
        "MlpSetBackupState:-last_truncation_time"	=> 0,
        "MlpSetBackupState:-last_fullload_time"		=> 0,
        "MlpSetBackupState:-last_fullload_file"		=> 0,
        "MlpSetBackupState:-last_fullload_lsn"		=> 0,
        "MlpSetBackupState:-last_tranload_time"		=> 0,
        "MlpSetBackupState:-last_tranload_file"		=> 0,
        "MlpSetBackupState:-last_tranload_lsn"		=> 0,
        "MlpSetBackupState:-last_tranload_filetime"	=> 0,
        "MlpSetBackupState:-is_tran_truncated"	=> 0,
        "MlpSetBackupState:-is_db_usable"	=> 0,
        "MlpSetBackupState:-db_server_name"	=> 0,

        "MlpGetScreens:-user"			=> 0,
	"MlpGetScreens:-category"		=> 0,
	"MlpGetScreens:-debug"			=> 0,

	"MlpReportArgs:-reportname"		=> 1,
	"MlpReportArgs:-debug"			=> 0,

	"MlpRunScreen:-screen_name"		=> 1,
	"MlpRunScreen:-useheartbeattime"	=> 0,
	"MlpRunScreen:-container"		=> 0,
	"MlpRunScreen:-production"		=> 0,
	"MlpRunScreen:-severity"		=> 0,
	"MlpRunScreen:-system"			=> 0,
	"MlpRunScreen:-subsystem"		=> 0,
	"MlpRunScreen:-program"			=> 0,
	"MlpRunScreen:-showok"			=> 0,
	"MlpRunScreen:-html"			=> 0,
	"MlpRunScreen:-maxtimehours"			=> 0,
	"MlpRunScreen:-debug"			=> 0,
	"MlpRunScreen:-NL"			=> 0,

	"MlpGetContainer:-name"			=> 0,
	"MlpGetContainer:-type"			=> 0,
	"MlpGetContainer:-user"			=> 0,
	"MlpGetContainer:-debug"		=> 0,
	"MlpGetContainer:-screen_name"		=> 0,

	"MlpGetReportCode:-screen_name"		=> 1,
	"MlpGetReportCode:-user"		=> 0,

	"MlpCleanup:-days"			=> 1,
	"MlpCleanup:-debug"			=> 0,
	"MlpCleanup:-hbdays"			=> 0,
	"MlpCleanup:-evdays"			=> 0,

 	"MlpSaveNotification:-monitor_program"	=> 1,
	"MlpSaveNotification:-system"		=> 1,
	"MlpSaveNotification:-subsystem"	=> 0,
	"MlpSaveNotification:-severity"		=> 1,
	"MlpSaveNotification:-monitor_time"	=> 0,
	"MlpSaveNotification:-user_name" 	=> 1,
	"MlpSaveNotification:-notify_type" 	=> 1,
	"MlpSaveNotification:-header" 		=> 1,
	"MlpSaveNotification:-message" 		=> 1,

	"MlpMonitorStart:-monitor_program"=>	1,
	"MlpMonitorStart:-system"			=>	1,

	"MlpBatchStart:-monitor_program"=>	0,
	"MlpBatchStart:-name"			=>	0,
	"MlpBatchStart:-debug"			=>	0,
	"MlpBatchStart:-system"			=>	0,
	"MlpBatchStart:-subsystem"		=>	0,
	"MlpBatchStart:-message"		=>	0,
	"MlpBatchStart:-batchjob"		=>	0,
	"MlpBatchStart:-monitorjob"	=>	0,
	"MlpBatchStart:-reset_status"	=>	0,
	"MlpBatchStart:-stepnum"		=>	0,

	"MlpAgentStart:-monitor_program"	=>	1,
	"MlpAgentStart:-debug"			=>	0,
	"MlpAgentStart:-system"			=>	0,
	"MlpAgentStart:-subsystem"		=>	0,
	"MlpAgentStart:-message"		=>	0,

	"MlpBatchStep:-stepname"=>	1,
	"MlpBatchStep:-stepnum"=>	1,
	"MlpBatchStep:-message"=>	0,

	"MlpBatchRunning:-monitor_program"	=>	1,
	"MlpBatchRunning:-name"				=>	0,
	"MlpBatchRunning:-debug"			=>	0,
	"MlpBatchRunning:-system"				=>	1,
	"MlpBatchRunning:-subsystem"		=>	0,
	"MlpBatchRunning:-message"			=>	0,
	"MlpBatchRunning:-batchjob"			=>	0,
	"MlpBatchRunning:-stepnum"=>	0,
	"MlpBatchRunning:-monitorjob"=>	0,

	"MlpBatchDone:-monitor_program"		=>	1,
	"MlpBatchDone:-name"			=>	0,
	"MlpBatchDone:-batchjob"		=>	0,
	"MlpBatchDone:-state"			=>	0,
	"MlpBatchDone:-system"			=>	1,
	"MlpBatchDone:-debug"			=>	0,
	"MlpBatchDone:-subsystem"		=>	0,
	"MlpBatchDone:-message"			=>	0,
	"MlpBatchDone:-stepnum"			=>	0,
	"MlpBatchDone:-monitorjob"			=>	0,

	"MlpBatchErr:-monitor_program"=>	1,
	"MlpBatchErr:-name"				=>	0,
	"MlpBatchErr:-state"				=>	0,
	"MlpBatchErr:-system"			=>	1,
	"MlpBatchErr:-batchjob"			=>	0,
	"MlpBatchErr:-subsystem"		=>	0,
	"MlpBatchErr:-message"			=>	0,
	"MlpBatchErr:-stepnum"			=>	0,
	"MlpBatchErr:-monitorjob"			=>	0,
	"MlpBatchErr:-quiet"				=>	0
);

my($connection);

sub MlpSetVars {
	my(%args)=@_;
	$server		= $args{-server};
	$login		= $args{-login};
	$password	= $args{-password};
	$db		= $args{-db};
}
sub MlpGetVars {	return( $server, $login, $password , $db ); }

sub initialize {
	my(%args)=@_;
	print "Initializing Repository Connection\n" if defined $args{-debug};

	if( $server eq "" ) {
		# i dont know if this will work... but it might
		if( defined $main::CONFIG{ALARM_SVR} ) {
			print "oh my god... yes!!! i think this fixes the bug in configure timing!!!\n";
			print "the following variables were found:\n";
			print "SERVER\t$main::CONFIG{ALARM_SVR}\n";
			print "ALARM_DATABASE\t$main::CONFIG{ALARM_DATABASE}\n";
			print "ALARM_LOGIN\t$main::CONFIG{ALARM_LOGIN}\n";
			print "ALARM_PASSWORD\t$main::CONFIG{ALARM_PASSWORD}\n";
			print "ALARM_MAIL\t$main::CONFIG{ALARM_MAIL}\n";
			print "Contact Ed Barlow asap and tell him the good news!\n";
		}

		if( ! defined $quiet ) {
			print "******************************************************\n";
			print "Monitoring Error: GEM IS NOT INITIALIZED!!! \nIf You Are Running From The Configuration Utility, you need to save changes.\nIm sorry to exit here but GEM needs to be initialized.\nIf this is a legitimate error please contact GEM Support\n";
			print STDERR "Monitoring Error: GEM IS NOT INITIALIZED!!! \nIf You Are Running From The Configuration Utility, you need to save changes.\nIm sorry to exit here but GEM needs to be initialized.\nIf this is a legitimate error please contact GEM Support\n";
			print "******************************************************\n";
			exit(1);
		}
		$i_am_initialized = "FAILED";
		return 0;
	}

	if( defined $args{-monitor_program} ) {
		$monitor_program=$args{-monitor_program};
	} else {
		$monitor_program=basename($0);
	}

	$quiet=$args{-quiet} unless $quiet;

	$this_hostname=hostname();

	if( defined $args{-system} ) {
		$system=$args{-system};
	} else {
		$system=hostname();
	}

	if( is_nt() ) {
		$server_type="ODBC";
	} else {
		$server_type="Sybase";
	}

	# $subsystem=$args{-subsystem} if defined $args{-subsystem};
	$debug=$args{-debug} if defined $args{-debug};
	print "dbi_connect(-srv=>$server, -login=>$login, -password=>$password,-type=>$server_type, -debug=>$args{-debug}, -connection=>1, -die_on_error=>0)\n"
		if defined $args{-debug};

	my($rc) = dbi_connect(-srv=>$server, -login=>$login, -password=>$password,
		-type=>$server_type, -debug=>$args{-debug}, -connection=>1, -die_on_error=>0);
	if( ! $rc ) {
		print "dbi_connect() Failed\n" if defined $args{-debug};
		if( ! defined $quiet ) {
			print "******************************************************\n";
			print "Monitoring Error: Cant Connect To Server -  Marking Connection Failed\nUnable to connect to $server_type server $server as login $login\nHost=".hostname()."\nPERL=$^X \nSYBASE=$ENV{SYBASE}\nerror number=".$DBI::err."\nerrstr=".$DBI::errstr,"\n";
			print STDERR "Monitoring Error: Cant Connect To Server -  Marking Connection Failed\nUnable to connect to $server_type server $server as login $login\nHost=".hostname()."\nPERL=$^X \nSYBASE=$ENV{SYBASE}\nerror number=".$DBI::err."\nerrstr=".$DBI::errstr,"\n";
			print "******************************************************\n";
		}
		$i_am_initialized = "FAILED";
		return 0;
	};
	$connection=$rc;

	print "Connected as $login / $password - Trying to use $db\n" if defined $args{-debug};
	$rc=dbi_use_database($db,$connection);
	print "Use Db Failed to Use $db\n" if $rc == 0;
	return 0 if $rc == 0;

	# tib_init($SERVICE,$SENDNETWORK,$DAEMON,$0);
	print "looks good... Alarm System Initialization Successful\n" if defined $args{-debug};
	$i_am_initialized="TRUE";
	return 1;
}

sub validate_params
{
	my($func,%OPT)=@_;
	# check for bad keys
	foreach (keys %OPT) {
		next if defined $OK_OPTS{$func.":".$_};
		next if /-debug/;

		foreach my $x (keys %OPT) {
			print "validate error: arg $x => $OPT{$x} \n";
		}

		# did you mix up name=>x with -name=>x
		croak "ERROR: Incorrect $func Arguments - Change $_ argument to -$_\n"
			if defined $OK_OPTS{$func.":-".$_};

		croak "ERROR: Function $func Does Not Accept Parameter Named $_\n";
	}

	# check for missing required keys
	foreach (keys %OK_OPTS) {
		next unless /^$func:/;
		next unless $OK_OPTS{$_} == 1;
		$_=~s/$func://;
		next if defined $OPT{$_};
		croak "ERROR: Function $func Requires Parameter Named $_\n";
	}

	# log to a file if you are in debug mode
	if( $OPT{-debug} ) {
		my($logfile);
		if( is_nt() ) {
			$logfile="C:/MlpAlarm.log";
		} else {
			$logfile="/tmp/MlpAlarm.log";
		}
		my($rc)=open(OUT,">> $logfile");
		if( ! $rc ) {
			print OUT 	"========== $func ==========\n";
			foreach (keys %OPT ) { print OUT "\t$_\t$OPT{$_} \n"; }
			close(OUT);
		}
	}

	return %OPT;
}

sub MlpHeartbeat {
	my(%args1)=@_;
	initialize(%args1) unless $i_am_initialized eq "TRUE";
	if($i_am_initialized eq "FAILED" ) {
		print "Cant Save Heartbeat: Connection Was Marked Failed!\n(this should not abort your program)\n"
			unless defined $quiet;
		return 0;
	};
	if( defined $args1{-severity} and ! defined $args1{-state} ) {
		$args1{-state}=$args1{-severity};
		undef $args1{-severity};
	};
	$args1{-debug}=$debug unless  defined $args1{-debug};
	$args1{-state}=uc($args1{-state});
	$args1{-state}="OK" if defined $args1{-state} eq "INFORMATION"
			    or defined $args1{-state} eq "DEBUG";

	my(%args)= validate_params("MlpHeartbeat",%args1);

	if(   $args{-state} ne "EMERGENCY" and
		   $args{-state} ne "CRITICAL" and
		   $args{-state} ne "STARTED" and
		   $args{-state} ne "RUNNING" and
		   $args{-state} ne "NOT_RUN" and
		   $args{-state} ne "COMPLETED" and
		   $args{-state} ne "ALERT" and
		   $args{-state} ne "ERROR" and
		   $args{-state} ne "WARNING" and
		   $args{-state} ne "OK" ) {
		if( defined $args1{-batchjob} ) {
			warn "MlpHeartbeat Error : Invalid state {$args{-state}}\nValid states are: STARTED COMPLETED RUNNING NOT_RUN EMERGENCY CRITICAL ALERT ERROR WARNING OK INFORMATION DEBUG";
		} else {
			warn "MlpHeartbeat Error : Invalid state {$args{-state}}\nValid states are: EMERGENCY CRITICAL ALERT ERROR WARNING OK INFORMATION DEBUG";
		}
		return;
	}

	$args{-message_text} =~ s/\0/ /g if defined $args{-message_text};

	my($query);
	# handle -reset_status
	if( $args{-reset_status} ) {
		$query="UPDATE Heartbeat set state='NOT_RUN',message_text='".$args{-message_text}."' ".
   	" where monitor_program =".  dbi_quote(-text=>$args{-monitor_program}, -connection=>$connection).
   	" and system =".  dbi_quote(-text=>$args{-system}, -connection=>$connection);
   	$query.=" and subsystem =".  dbi_quote(-text=>$args{-subsystem}, -connection=>$connection) if defined $args{-subsystem};
		print $query if defined $args{-debug};
		foreach ( dbi_query(-db=>$db,-no_results=>1,-query=>$query, -debug=>$args{-debug}, -connection=>$connection, -die_on_error=>0 )) {
			my($txt)=join(" ",dbi_decode_row($_));
			next if $txt=~/^\s*$/;
			chomp $txt;
			print "Heartbeat(-reset_status) ($query):",$txt,"\n"
				unless defined $quiet;
		}
	}

	print "MlpHeartbeat(): " if defined $args{-debug};
	$query= "exec Heartbeat_proc
   \@monitor_program =".  dbi_quote(-text=>$args{-monitor_program}, -connection=>$connection).",
   \@system =".  dbi_quote(-text=>$args{-system}, -connection=>$connection).",
   \@subsystem =".  dbi_quote(-text=>$args{-subsystem}, -connection=>$connection).",
   \@state =".  dbi_quote(-text=>$args{-state}, -connection=>$connection).",
   \@message_text =".  dbi_quote(-text=>$args{-message_text}, -connection=>$connection).",
   \@batchjob =".  dbi_quote(-text=>$args{-batchjob}, -connection=>$connection).",
   \@document_url =".  dbi_quote(-text=>$args{-document_url}, -connection=>$connection)."\n";
	$query.=",\@heartbeat_time=".dbi_quote(-text=>$args{-event_time}, -connection=>$connection)."\n"
			if defined $args{-event_time};

	print $query if defined $args{-debug};

	my($ok)="TRUE";
	foreach ( dbi_query(-db=>$db,-query=>$query, -debug=>$args{-debug}, -connection=>$connection, -die_on_error=>0 )) {
		$ok="FALSE";
		my($txt)=join(" ",dbi_decode_row($_));
		next if $txt=~/^\s*$/;
		chomp $txt;
		print "Hearbeat Save ($query):",$txt,"\n"
			unless defined $quiet;
	}

	if($ok eq "FALSE") {
		my($out)="Save Of Heartbeat Failed\n";
		$out.= " err=".$DBI::err."\n" 			if defined $DBI::err;
		$out.= " errstr=".$DBI::errstr."\n" 	if defined $DBI::errstr;
		chomp $out;
		chomp $out;
		chomp $out;
		print $out,"\n"
			unless defined $quiet;
	}

	# tib_send($args{-monitor_program}.".".$args{-system}.".".$args{-subsystem},%args);

	if( defined $args{-debug} ) {
		print "Saved: ",$args{-state}," ",$args{-message_text},"\n"
	}
}

sub MlpReportArgs {
	my(%args1)=@_;
	initialize(%args1) unless $i_am_initialized eq "TRUE";
	if($i_am_initialized eq "FAILED" ) {
		print "Cant Get Report Args: Connection Was Marked Failed\n"
			unless defined $quiet;
		return 0;
	};
	my(%args)= validate_params("MlpReportArgs",%args1);
	my(%results);

	# standard Always On Args
	$results{submit}			=	"Run Report";
	$results{NONAVIGATION}			=	"TRUE";
	$results{radio_screen}			=	"All Reports";
	$results{shrinkfont}			=	"shrink font"
		unless $args{-reportname}=~/^GEM/;
	$results{filter_display}		=	"Details";
	$results{UseHeartbeatTime}		=	"1";

	# Args we exptect to be changed (these are default values);
	$results{filter_severity}		=	"Warnings";
	$results{filter_time}			=	"5 Days";
	$results{filter_container}		=	"All";
	# $results{debug}			=	"1";

	my($report_query)= "select reportname,keyname,value from ReportArgs where reportname='".$args{-reportname}."'";

	my($found)="FALSE";
	foreach ( dbi_query(-db=>$db,-query=>$report_query, -connection=>$connection,
				-die_on_error=>0, -debug=>$args{-debug} )) {
		my(@x) = dbi_decode_row($_);
		$found="TRUE";
		if( $x[1] eq "filter_severity" ) {
			$results{$x[1]}	= $x[2];
		} elsif( $x[1] eq "filter_time" ) {
			$results{$x[1]}	= $x[2];
		} elsif( $x[1] eq "filter_container" ) {
			$results{$x[1]}	= $x[2];
		} elsif( $x[1] eq "ProdChkBox" ) {
			$results{$x[1]} = $x[2] if $x[2] eq "YES";
		} else {
			$results{$x[1]} = $x[2];
		}
	}
	die "ERROR: Can Not Find Information With $report_query - invalid report\n"
		if $found eq "FALSE";

	if( defined $args{-debug} ) {
		foreach (keys %results) {
			print " results($_)=$results{$_} \n";
		}
	}

	return %results;
}

sub MlpClearPL {
	my(%args)=@_;

	initialize(%args) unless $i_am_initialized eq "TRUE";
	if($i_am_initialized eq "FAILED" ) {
		print "Cant Save Heartbeat: Connection Was Marked Failed\n"
			unless defined $quiet;
		return 0;
	};
	my($query)= "DELETE ProgramLookup";
	foreach ( dbi_query(-db=>$db, -query=>$query, -debug=>$args{-debug}, -connection=>$connection, -die_on_error=>0 )) {
		print "Returned: ",join(" ",dbi_decode_row($_)),"\n";
	}
}

sub MlpSetPL {
	my(%args)=@_;
	my($query)= "insert ProgramLookup values ('".
				$args{monitor_program}.
				"','".
				$args{use_system}.
				"','".
				$args{job_type}.
				"','".
				$args{rec_frequency_str}.
				"'".
				" )";
	foreach ( dbi_query(-db=>$db, -query=>$query, -debug=>$args{-debug}, -connection=>$connection, -die_on_error=>0 )) {
		print "Returned: ",join(" ",dbi_decode_row($_)),"\n";
	}
}

sub MlpPerformance {
	my(%args1)=@_;
	initialize(%args1) unless $i_am_initialized eq "TRUE";
	if($i_am_initialized eq "FAILED" ) {
		print "Cant Save Heartbeat: Connection Was Marked Failed\n"
			unless defined $quiet;
		return 0;
	};
	# $args1{-system}=$system
			# unless  defined $args1{-system};
	# $args1{-subsystem}=$subsystem
			# unless  defined $args1{-subsystem};
	# $args1{-monitor_program}=$monitor_program
			# unless  defined $args1{-monitor_program};
	$args1{-debug}=$debug
			unless  defined $args1{-debug};

	my(%args)= validate_params("MlpPerformance",%args1);

	my($query)= "exec Performance_proc
		\@monitor_program =".  dbi_quote(-text=>$args{-monitor_program}, -connection=>$connection).",
		\@system =".  dbi_quote(-text=>$args{-system}, -connection=>$connection).",
		\@subsystem =".  dbi_quote(-text=>$args{-subsystem}, -connection=>$connection).",
		\@val1 =".  $args{-value1}.",
		\@val2 =".  $args{-value2}.",
		\@val3 =".  $args{-value3}.",
		\@val4 =".  $args{-value4}.",
		\@val5 =".  $args{-value5}.",
		\@val6 =".  $args{-value6}.",
		\@val7 =".  $args{-value7}.",
		\@val8 =".  $args{-value8}.",
		\@val9 =".  $args{-value9}."\n";

	print $query if defined $args{-debug};

	my($ok)="TRUE";
	foreach ( dbi_query(-db=>$db, -query=>$query, -debug=>$args{-debug}, -connection=>$connection, -die_on_error=>0 )) {
		$ok="FALSE";
		print "Returned: ",join(" ",dbi_decode_row($_)),"\n"
			unless defined $quiet;
	}
	print "WARNING: Query Failed: $query\nerr=".$DBI::err." errstr=".$DBI::errstr,"\n" if $ok eq "FALSE" and ! defined $quiet;

	# tib_send($args{-monitor_program}.".".$args{-system}.".".$args{-subsystem},%args);
	print "Saved: $args{-severity} $args{-message_text}\n" if defined $args{-debug};
}

# UPDATE heartbeat - used for repeated messages
sub MlpEventUpdate {
	my(%args1)=@_;
	initialize(%args1) unless $i_am_initialized eq "TRUE";
	if($i_am_initialized eq "FAILED" ) {
		print "Cant Save Event: Connection Was Marked Failed\n"
			unless defined $quiet;
		return 0;
	};
	$args1{-debug}=$debug unless  defined $args1{-debug};

	my(%args)= validate_params("MlpEventUpdate",%args1);

	$args{-message_text} =~ s/\0/ /g;

	my($query)="UPDATE Event
		set message_text=".
			dbi_quote(-text=>$args{-message_text}, -connection=>$connection).
		" where monitor_program=".
			dbi_quote(-text=>$args{-monitor_program},-connection=>$connection).
   	" and system=".
		dbi_quote(-text=>$args{-system}, -connection=>$connection).
   	" and event_time=".
		dbi_quote(-text=>$args{-event_time}, -connection=>$connection);
		#$query.= " and batchjob is null";
		$query.= " and subsystem=".  dbi_quote(-text=>$args{-subsystem}, -connection=>$connection) if defined $args{-subsystem};

	print "DBG: QUERY=$query\n" if defined $args{-debug};
	my($ok)="TRUE";
	foreach ( dbi_query(-db=>$db, -query=>$query, -debug=>$args{-debug}, -connection=>$connection, -die_on_error=>0, -no_results=>1 )) {
		$ok="FALSE";
		print join(" ",dbi_decode_row($_)),"\n"
			unless defined $quiet;
	}
	print "WARNING: Query Failed: $query" if $ok eq "FALSE"
			and ! defined $quiet;
}

sub MlpTestInstall
{
	my(%args1)=@_;
	print "MlpTestInstall(): Running In Debug Mode ($i_am_initialized)\n" if defined $args1{-debug};

	return "Alarm System Has Not Been Configured"
		if $server eq "" or $login eq "" or $password eq "" or $db eq "";

	my($rc)=initialize(%args1) unless $i_am_initialized eq "TRUE";
	if( $rc==0 ) {
		return "Connect to $server may have failed\n" if ! defined $connection;
		return "Connect to $server ok but use $db may have failed\n";
	}

	if($i_am_initialized eq "FAILED" ) {
		return "Initialization Failed";
	}

	my(%objects) = (
Container		=> 'Missing',
Container_full		=> 'Missing',
Screen			=> 'Missing',
Severity		=> 'Missing',
Operator		=> 'Missing',
Heartbeat		=> 'Missing',
Event			=> 'Missing',
HeartbeatThresh		=> 'Missing',
clear_possible_alarms	=> 'Missing',
MessageLookup		=> 'Missing',
ValueConversion		=> 'Missing',
IgnoreList		=> 'Missing',
ProductionServers	=> 'Missing',
Heartbeat_proc		=> 'Missing',
Event_proc		=> 'Missing',
build_container		=> 'Missing',
get_alarm_version	=> 'Missing',
);
	foreach ( dbi_query(-db=>$db, -query=>"select name,type from sysobjects where type in ('U','P','TR')",
		-connection=>$connection, -die_on_error=>0 )) {
		my(@d)=dbi_decode_row($_);
		$objects{$d[0]} = "OK";
	}
	my($req_version)=2;

	if( ! defined $objects{Heartbeat} ) 		{ return "Alarm System Has Not Been Installed\n"; }
	if( ! defined $objects{get_alarm_version} ) 	{
		return "Alarm System Is not at v$req_version or later. Please Resinstall\n";
	}

	$rc="";
	foreach (keys %objects) { $rc.="Missing Object $_\n" if $objects{$_} ne "OK"; }
	return $rc if $rc ne "";

	my($version);
	foreach ( dbi_query(-db=>$db, -query=>"exec get_alarm_version",
		-connection=>$connection, -die_on_error=>0 )) {
		my(@d)=dbi_decode_row($_);
		$version=$d[0];
	}

	return "Alarm System Is At $version, not at v$req_version or later. Please Resinstall\n"
		unless $version>=$req_version;

	return "";
}

sub MlpEvent
{
	my(%args1)=@_;
	initialize(%args1) unless $i_am_initialized eq "TRUE";
	if($i_am_initialized eq "FAILED" ) {
		print "Cant Save Event: Connection Was Marked Failed\n"
			unless defined $quiet;
		return 0;
	};
	# $args1{-system}=$system
			# unless  defined $args1{-system};
	# $args1{-subsystem}=$subsystem
			# unless  defined $args1{-subsystem};
	# $args1{-monitor_program}=$monitor_program
			# unless  defined $args1{-monitor_program};
	$args1{-debug}=$debug
			unless  defined $args1{-debug};

	my(%args)= validate_params("MlpEvent",%args1);
	$args{-severity} = "INFORMATION" if $args{-severity} eq "OK";

	if( defined $args{-condense} ) {
		if( defined $args{-event_time} and
		$condense_args{-system} eq $args{-system} and
		$condense_args{-monitor_program} eq $args{-monitor_program} and
		$condense_args{-subsystem} eq $args{-subsystem} and
		$condense_args{-severity} eq $args{-severity} and
		$condense_args{-message_text} eq $args{-message_text}  ) {
			$condense_args{-count}=1 unless defined $condense_args{-count};
			$condense_args{-count}++;

			MlpEventUpdate(
				#-event_time => $args{-event_time},
				-system=>	$args{-system} ,
				-monitor_program=>	$args{-monitor_program} ,
				-subsystem=>	$args{-subsystem} ,
				-event_time=>	$condense_args{-event_time} ,
				-message_text=> "[message repeated $condense_args{-count} times] ".$args{-message_text}  );
			return;
		}
		foreach ( keys %args )  { $condense_args{$_} = $args{$_}; }
	}

	warn "Warning : Invalid severity  $args{-severity}\n"
		unless $args{-severity} eq "EMERGENCY" or
		       $args{-severity} eq "CRITICAL" or
		       $args{-severity} eq "ALERT" or
		       $args{-severity} eq "RUNNING" or
		       $args{-severity} eq "NOT_RUN" or
		       $args{-severity} eq "COMPLETED" or
		       $args{-severity} eq "STARTED" or
		       $args{-severity} eq "ERROR" or
		       $args{-severity} eq "WARNING" or
		       $args{-severity} eq "INFORMATION" or
		       $args{-severity} eq "DEBUG";

	my($query)="exec Event_proc ".
   		"\@monitor_program=".
		dbi_quote(-text=>$args{-monitor_program}, -connection=>$connection).",\n\t\t".
   		"\@monitor_time=";

	if( defined $args{-monitor_time}) {
		$query.=  dbi_quote(-text=>$args{-monitor_time}, -connection=>$connection).",\n\t\t";
	} else {
		$query.=  "null,\n\t\t";
	}

	my($txt)=substr($args{-message_text},0,130);
	if( $args{-nosave} ) { 	print "NOSAVE: txt=$txt\n"; }
	$txt=~s/\0/ /g;
	$txt=~s/\'//g;
	#$txt=~s/[\s\W]+$//;
	$txt=~s/\s+$//;
	#$txt="\'". $txt."\'";
	if( $args{-nosave} ) { 	print "NOSAVE: txt=$txt\n"; }

	$txt=dbi_quote(-text=>$txt, -connection=>$connection);
	if( $args{-nosave} ) { 	print "NOSAVE: txt=$txt\n"; }

	$query.=
   		"\@system=".
		dbi_quote(-text=>$args{-system}, -connection=>$connection).",\n\t\t".
   		"\@subsystem=".
		dbi_quote(-text=>$args{-subsystem}, -connection=>$connection).",\n\t\t".
   		"\@event_time=";

	if( defined $args{-event_time} ) {
		$query.=  dbi_quote(-text=>$args{-event_time}, -connection=>$connection).",\n\t\t";
	} else {
		$query.=  "null,\n\t\t";
	}

	$query.="\@severity=".
		dbi_quote(-text=>$args{-severity}, -connection=>$connection).",\n\t\t".
   		"\@event_id=".
		dbi_quote(-text=>$args{-event_id}, -connection=>$connection).",\n".
   		"\@message_text=".  $txt.",\n\t\t";
	#dbi_quote(-text=>$txt, -connection=>$connection).",\n\t\t";

	if( $args{-message_value} and $args{-message_value}=~/^\d+$/) {
		$query.= "\@message_value=".$args{-message_value}.",\n\t\t";
	} else {
		$query.= "\@message_value=null,\n\t\t";
	}
   	#"\@document_url=".
	#dbi_quote(-text=>$args{-document_url}, -connection=>$connection).",\n\t\t".
   	$query.="\@internal_state=".  "'I'\n";

   	#print $query;

	my($ok)="TRUE";
	if( $args{-nosave} ) {
		print "NOSAVE: db=$db query=$query\n";
	} else {
		foreach ( dbi_query(-db=>$db, -query=>$query, -debug=>$args{-debug}, -connection=>$connection, -die_on_error=>0, -no_results=>1 )) {
			$ok="FALSE";
			print join(" ",dbi_decode_row($_)),"\n"
				unless defined $quiet;
		}
	}

#	 THIS WHOLE BLOCK IS DEBUG TEXT
#	if( $ok eq "FALSE" ) {
#		open(DBG,">tmp.out");
#		print DBG $query;
#		print DBG "\n";
#
#	my($txt)=substr($args{-message_text},0,130);
#	$txt=~s/\0/ /;
#	#$txt=~s/\'//g;
#	#$txt=~s/[\s\W]+$//;
#	#$txt="\'". $txt."\'";
#	#$txt=dbi_quote(-text=>$txt, -connection=>$connection);
#
#		my(@d)=split("",$txt);
#		my($i)=0;
#		foreach (@d) {
#			print DBG $i,": $d[$i] => ",ord($d[$i]),"\n";
#			$i++;
#			last if $i>20;
#		}
#		print DBG $txt."\n";
#		close(DBG);
#		die "DONE";
#	}

	print "WARNING: Query Failed:\n$query" if $ok eq "FALSE"
			and ! defined $quiet;

	# tib_send($args{-monitor_program}.".".$args{-system}.".".$args{-subsystem},%args);
	print "Saved: ",$args{-severity}," ",$args{-message_text},"\n" if defined $args{-debug};
}

sub MlpGetCategory
{
	my(%args1)=@_;
	initialize(%args1) unless $i_am_initialized eq "TRUE";
	if($i_am_initialized eq "FAILED" ) {
		print "Cant Get Alarm Info: Connection Was Marked Failed\n";
		return 0;
	};
	my(%args)= validate_params("MlpGetCategory",%args1);
	my($query)= "SELECT distinct category from Screen\n";
	$query.=" where owner = \"".$args{-user}."\"" if defined $args{-user};
	print $query if defined $args{-debug};

	my(@rc);
	foreach ( dbi_query(-db=>$db,-query=>$query, -debug=>$args{-debug}, -connection=>$connection, -die_on_error=>0 )) {
		push @rc, dbi_decode_row($_);
	}
	return @rc;
}

sub MlpGetScreens
{
	my(%args1)=@_;
	initialize(%args1) unless $i_am_initialized eq "TRUE";
	if($i_am_initialized eq "FAILED" ) {
		print "Cant Get Alarm Info: Connection Was Marked Failed\n";
		return 0;
	};
	my(%args)= validate_params("MlpGetScreens",%args1);
	my($query)= "SELECT distinct screen_name from Screen where 1=1\n";
	$query.=" and owner = \"".$args{-user}."\"" if defined $args{-user};
	$query.=" and owner is null " unless defined $args{-user};
	$query.=" and category = \"".$args{-category}."\""
		if defined $args{-category};

	print $query if defined $args{-debug};
	my(@rc);
	foreach ( dbi_query(-db=>$db,-query=>$query, -debug=>$args{-debug}, -connection=>$connection, -die_on_error=>0 )) {
		push @rc, dbi_decode_row($_);
	}
	return @rc;
}

sub _warn {
	my( $html, $str )=@_;
	return $str unless $html;
	return "<font color=red>$str</font>";
}

# Run A Screen Returning Array of Arrays
# return $array_of_arrays and $!
sub MlpRunScreen
{
	my(%args1)=@_;
	initialize(%args1) unless $i_am_initialized eq "TRUE";
	if($i_am_initialized eq "FAILED" ) {
		print "Cant Get Alarm Info: Connection Was Marked Failed\n";
		return 0;
	};

	my(%args)= validate_params("MlpRunScreen",%args1);
	my($NL) = $args{-NL} || "\n";
	if( defined $args{-debug} ) {
		print "MlpRunScreen( ";
		foreach( keys %args ) {
			print " ".$_."=>".$args{$_}." " unless $_ eq "-NL";
		}
		print ")".$NL;
	}

	my(@outarray);	# array of arrays to return

	# INSTRUCTIONS FOR THIS SECTION
	#  return stuff in an array of pointers to arrays
	#  each element of the array is a row
	#  the first row is the header
	#  the header can have the following keywords in it
	#     *_LU - lookup, and in last row Both(=Add+Delete),Add,Delete,Update
	if( $args{-screen_name} eq "Delete By Monitor_Type") {
		my(@x)=qw(Monitor Type Batch Time Delete);
		push @outarray,\@x;
		my($report_query)=
"select monitor_program,convert(varchar,max(monitor_time),0),batchjob,'Heartbeat'
	from Heartbeat where internal_state!='D' /* and isnull(batchjob,'x') !=\"AGENT\" */ group by monitor_program
union
select monitor_program, convert(varchar,max(monitor_time),0),'','Event'
	from Event where internal_state!='D' group by monitor_program
	order by monitor_program";

		my($d_monitor_program,$d_monitor_time,$d_batch,$d_system);
		print $report_query,"\n" if defined $args{-debug};
		foreach ( dbi_query(-db=>$db,-query=>$report_query, -connection=>$connection, -die_on_error=>0 )) {
			($d_monitor_program,$d_monitor_time,$d_batch,$d_system)= dbi_decode_row($_);
			my(@x)=($d_monitor_program,$d_system,$d_batch,$d_monitor_time );
			push @outarray, \@x;
		}
		return \@outarray;
	} elsif( $args{-screen_name} eq "Delete By Monitor") {
		my(@x)=qw(Monitor Delete);
		push @outarray,\@x;
		my($report_query)=
"select distinct monitor_program from Heartbeat where internal_state!='D' and isnull(batchjob,'x') !=\"AGENT\" group by monitor_program
union
select monitor_program from Event where internal_state!='D' group by monitor_program
order by monitor_program";

		my($d_monitor_program);
		print $report_query,"\n" if defined $args{-debug};
		foreach ( dbi_query(-db=>$db,-query=>$report_query, -connection=>$connection, -die_on_error=>0 )) {
			($d_monitor_program)= dbi_decode_row($_);
			my(@x)=($d_monitor_program );
			push @outarray, \@x;
		}
		return \@outarray;
	} elsif( $args{-screen_name} eq "Containers" ) {
		my(@x)=qw(Container Type System);
		push @outarray,\@x;
		my($report_query)=
		"select 	name,
			Type=case
				when type='c' then 'Container'
				when type='s' then 'Server'
				else 'unknown'
			end,
			element
from Container order by name,type";
		print $report_query,"\n" if defined $args{-debug};
		foreach ( dbi_query(-db=>$db,-query=>$report_query, -connection=>$connection, -die_on_error=>0 )) {
			my(@x) = dbi_decode_row($_);
			push @outarray, \@x;
		}
		return \@outarray;
	} elsif( $args{-screen_name} eq "System->Container" ) {
		my(@x)=qw(Container System_LU Both);
		push @outarray,\@x;
		my($report_query)=
		"select 	name,
			-- Type=case
				-- when type='c' then 'Container'
				-- when type='s' then 'Server'
				-- else 'unknown'
			-- end,
			element
from ContainerOverride order by name,type";
		print $report_query,"\n" if defined $args{-debug};
		foreach ( dbi_query(-db=>$db,-query=>$report_query, -connection=>$connection, -die_on_error=>0 )) {
			my(@x) = dbi_decode_row($_);
			push @outarray, \@x;
		}
		return \@outarray;
	} elsif( $args{-screen_name} eq "Production" ) {
		my(@x)=qw(Server Chkbox_Is_Production Chkbox_Ignore Chkbox_Blackout Delete);
		push @outarray,\@x;

		my($report_query)="
select p.system,p.is_production,
	ignore=case 	when i.system is not null then 1 	else 0	end,
	blackout=case	when b.system is not null then 1 	else 0	end
from ProductionServers p, IgnoreList i, Blackout b
where p.system *= i.system
and   i.monitor_program is null
and   i.subsystem is null
and   p.system*=b.system
order by system";
		print $report_query,"\n" if defined $args{-debug};
		foreach ( dbi_query(-db=>$db,-query=>$report_query, -connection=>$connection, -die_on_error=>0 )) {
			my(@x) = dbi_decode_row($_);
			push @outarray, \@x;
		}
		return \@outarray;
	} elsif( $args{-screen_name} eq "Program->Container" ) {
		my(@x)=qw(Program_LU Container_LU Both);
		push @outarray,\@x;
		my($report_query)= "select 	monitor_program, container from ContainerMap order by monitor_program";
		print $report_query,"\n" if defined $args{-debug};
		foreach ( dbi_query(-db=>$db,-query=>$report_query, -connection=>$connection, -die_on_error=>0 )) {
			my(@x) = dbi_decode_row($_);
			push @outarray, \@x;
		}
		return \@outarray;
	} elsif( $args{-screen_name} eq "Delete By Row" ) {
		my(@x)=qw(Monitor System Type Rows Time Delete);
		push @outarray,\@x;
		my($report_query)=
"select monitor_program,system,dtype='Heartbeat',count(*),max(monitor_time) from Heartbeat where internal_state!='D'
	and isnull(batchjob,'x') !=\"AGENT\" group by monitor_program,system
union
select monitor_program,system,dtype='Event',count(*),max(monitor_time)     from Event where internal_state!='D' group by monitor_program,system
order by monitor_program,system,dtype";
		print $report_query,"\n" if defined $args{-debug};
		foreach ( dbi_query(-db=>$db,-query=>$report_query, -connection=>$connection, -die_on_error=>0 )) {
			my(@x) = dbi_decode_row($_);
			push @outarray, \@x;
		}
		return \@outarray;
	} elsif( $args{-screen_name} eq "Alarm History" ) {
		my(@x)=qw(Notify_User Notify_Time Alarm_Type Alarm_Subject Alarm_Message);
		push @outarray,\@x;
		my($report_query)=
		"select 	user_name,convert(varchar,notify_time,0),notify_type,subject,message
			from SentNotification order by notify_time desc";
		print $report_query,"\n" if defined $args{-debug};
		foreach ( dbi_query(-db=>$db,-query=>$report_query, -connection=>$connection, -die_on_error=>0 )) {
			my(@x) = dbi_decode_row($_);
			push @outarray, \@x;
		}
		return \@outarray;
	} elsif( $args{-screen_name} eq "Alarm Routing" ) {
		my(@x)=qw( User_LU Program_LU Container System Subsystem Min_Severity_LU Max_Severity_LU Use_Pager_LU Use_Email_LU Use_Netsend_LU Both);
		#
		push @outarray,\@x;
		my($report_query)=
		"select user_name,monitor_program,container,system,subsystem,min_severity,max_severity, use_pager,use_email,use_netsend
		from Notification order by user_name";
		print $report_query,"\n" if defined $args{-debug};
		foreach ( dbi_query(-db=>$db,-query=>$report_query, -connection=>$connection, -die_on_error=>0 )) {
			my(@x) = dbi_decode_row($_);
			push @outarray, \@x;
		}
		return \@outarray;
	} elsif( $args{-screen_name} eq "Filter Repeated Messages" ) {
		my(@x)=qw( User Program Container System Subsystem EmergencyMin FatalMin ErrorMin WarningMin Update);
		push @outarray,\@x;
		my($report_query)=
		"select user_name,monitor_program,container,system,subsystem,
			emergency_minutes, fatal_minutes, error_minutes, warning_minutes
		from Notification order by user_name";
		print $report_query,"\n" if defined $args{-debug};
		foreach ( dbi_query(-db=>$db,-query=>$report_query, -connection=>$connection, -die_on_error=>0 )) {
			my(@x) = dbi_decode_row($_);
			push @outarray, \@x;
		}
		return \@outarray;
	} elsif( $args{-screen_name} eq "Operator" ) {
		my(@x)=qw(User Enabled_LU Email Pager Netsend Both);
		push @outarray,\@x;
		my($report_query)=
		"select 	user_name, enabled, email, pager_email, netsend
			from Operator order by user_name";
		print $report_query,"\n" if defined $args{-debug};
		foreach ( dbi_query(-db=>$db,-query=>$report_query, -connection=>$connection, -die_on_error=>0 )) {
			my(@x) = dbi_decode_row($_);
			push @outarray, \@x;
		}
		return \@outarray;
	} elsif( $args{-screen_name} eq "Report Setup" ) {
		my(@x)=qw(Report_Name Min_Severity_LU Time_LU Container_LU Production_YN_LU Show_Program_YN_LU Program Title Both);
		push @outarray,\@x;
		my($report_query)=
		"select 	reportname, keyname, value
			from ReportArgs order by reportname";
		print $report_query,"\n" if defined $args{-debug};
		my(%time,%container,%severity,%production,%showprogram,%reporttitle,%progname);
		foreach ( dbi_query(-db=>$db,-query=>$report_query, -connection=>$connection, -die_on_error=>0 )) {
			my(@x) = dbi_decode_row($_);
			if( $x[1] eq "filter_severity" ) {
				$severity{$x[0]} = $x[2];
			} elsif( $x[1] eq "filter_time" ) {
				$time{$x[0]} = $x[2];
			} elsif( $x[1] eq "filter_container" ) {
				$container{$x[0]} = $x[2];
			} elsif( $x[1] eq "ProdChkBox" ) {
				$production{$x[0]} = $x[2];
			} elsif( $x[1] eq "progname" ) {
				$progname{$x[0]} = $x[2];
			} elsif( $x[1] eq "ShowProgram" ) {
				$showprogram{$x[0]} = $x[2];
			} elsif( $x[1] eq "ReportTitle" ) {
				$reporttitle{$x[0]} = $x[2];
			}
		}
		foreach ( sort keys %time ) {
			my(@x);
			push @x,$_;
			push @x,$severity{$_};
			push @x,$time{$_};
			push @x,$container{$_};
			push @x,$production{$_};
			push @x,$showprogram{$_};
			push @x,$progname{$_};
			push @x,$reporttitle{$_};
			push @outarray, \@x;
		}
		return \@outarray;
	} elsif( $args{-screen_name} eq "Ignore List" ) {
		my(@x)=qw(Program_LU System Subsystem Both);
		push @outarray,\@x;
		my($report_query)=
		"select 	monitor_program,system,subsystem
			from IgnoreList where monitor_program is not null order by monitor_program,system,subsystem";
		print $report_query,"\n" if defined $args{-debug};
		foreach ( dbi_query(-db=>$db,-query=>$report_query, -connection=>$connection, -die_on_error=>0 )) {
			my(@x) = dbi_decode_row($_);
			push @outarray, \@x;
		}
		return \@outarray;
	} elsif( $args{-screen_name} eq "Blackout Report" ) {
		my(@x)=qw(List Program_LU System Subsystem Enddate);
		push @outarray,\@x;
		my($report_query)=
		"select System=system,IgnoreType='IgnoreList',Program=monitor_program,SubSystem=subsystem,EndDate=null from IgnoreList
			union
			select system,'Blackout',null,null,enddate from Blackout
			order by system";
		print $report_query,"\n" if defined $args{-debug};
		foreach ( dbi_query(-db=>$db,-query=>$report_query, -connection=>$connection, -die_on_error=>0 )) {
			my(@x) = dbi_decode_row($_);
			push @outarray, \@x;
		}
		return \@outarray;
	} elsif( $args{-screen_name} eq "Operator Schedule" ) {
		my(@x)=qw(User_LU WD_Start_LU WD_End_LU Sat_Start_LU Sat_End_LU Sun_Start_LU Sun_End_LU Hol_Start_LU Hol_End_LU Update);
		push @outarray,\@x;
		my($report_query)=
		"select 	user_name,
   			isnull(weekday_start_time,0)   , isnull(weekday_end_time,24)     ,
			isnull(saturday_start_time,0)  , isnull(saturday_end_time,24)    ,
			isnull(sunday_start_time,0) ,    isnull(sunday_end_time,24)      ,
   			isnull(holiday_start_time,0)   , isnull(holiday_end_time,24)
			from Operator order by user_name";
		print $report_query,"\n" if defined $args{-debug};
		foreach ( dbi_query(-db=>$db,-query=>$report_query, -connection=>$connection, -die_on_error=>0 )) {
			my(@x) = dbi_decode_row($_);
			push @outarray, \@x;
		}
		return \@outarray;
	} elsif( $args{-screen_name} eq "Agents" ) {

#		my($report_query)="select monitor_program,perl_program,batch_name from ProgramLookup";
#		my(%perlp,%batchp);
#		print $report_query,"\n" if defined $args{-debug};
#		foreach ( dbi_query(-db=>$db,-query=>$report_query, -connection=>$connection, -die_on_error=>0 )) {
#			my(@x) = dbi_decode_row($_);
#			$perlp{$x[0]} = $x[1];
#			$batchp{$x[0]}= $x[2];
#		}
my(@x)=qw(MonitorProgram System Sched_Host Frequency Last_Run Text Minutes);
		#my(@x)=qw(State MonitorProgram System Date Scheduled_On);
		push @outarray,\@x;
		my($report_query)=
#"select distinct system, subsystem,  max(monitor_time) ,
#               datediff(hh,max(monitor_time),getdate())
#from Heartbeat
#where system in ('CheckForBlocks', 'CheckUsers')
#and   monitor_program='Monitor'
#group by system,subsystem
#having monitor_program in ('CheckForBlocks', 'CheckUsers')
#union

"select p.monitor_program, system, subsystem, rec_frequency_str, monitor_time=convert(varchar,monitor_time,0), message_text,
timedif=datediff(mi,max(monitor_time),getdate())
into #tmp
from ProgramLookup p,Heartbeat h
where p.monitor_program = h.monitor_program
and batchjob='AGENT'
group by p.monitor_program
having p.monitor_program = h.monitor_program
and batchjob='AGENT'

insert #tmp
select monitor_program, '', '', rec_frequency_str, 'Never', 'No Heartbeats Found / Is this job scheduled?', -1
from ProgramLookup
where monitor_program not in
(
	select monitor_program from Heartbeat where batchjob='AGENT'
)

select *,'' from #tmp";

$report_query.="
where ( (rec_frequency_str = 'frequently' and timedif >2*15)
or  	(rec_frequency_str = 'hourly' and timedif >2*60)
or  	(rec_frequency_str = 'daily' and timedif >2*24*60)
or  	( timedif >2*7*24*60) -- weekly
or	timedif = -1
)" if $args{-severity} eq "Errors"
		or $args{-severity} eq "Fatals"
		or $args{-severity} eq "Emergency";

$report_query.="
order by timedif desc, monitor_program

drop table #tmp";

#"select  state, monitor_program, system, time=convert(varchar,monitor_time,0), subsystem, diff=datediff(hh,monitor_time,getdate())
#from Heartbeat
#where batchjob='AGENT'";

# group by system
# order by system";
# where monitor_program='Monitor'

		print $report_query,"\n" if defined $args{-debug};
		foreach ( dbi_query(-db=>$db,-query=>$report_query, -connection=>$connection, -die_on_error=>0 )) {
			my(@x) = dbi_decode_row($_);
			push @outarray, \@x;
		}
		return \@outarray;

#		foreach ( dbi_query(-db=>$db,-query=>$report_query, -connection=>$connection, -die_on_error=>0 )) {
#			my(@x) = dbi_decode_row($_);
#			my($t)=pop @x;
#
#			if( defined $perlp{ $x[0] } ){
#				push @x,$perlp{$x[0]};
#			} else {
#				push @x,"N.A.";
#			}
#			if( defined $batchp{ $x[0] } ){
#				push @x,$batchp{$x[0]};
#			} else {
#				push @x,"N.A.";
#			}
#			push @x,$t;
#
#			push @outarray, \@x;
#			##print ">>> ",join("|",@x),"\n";
#		}
#		return \@outarray;
	}

	print "DBG: routine report - starting".$NL if defined $args{-debug};

#	# we have container, screen_name, and html
#	# convert this to set of servers and screen_defn
#	my($query)= "SELECT screen_defn, container_name from Screen where 1=1\n";
#	$query.=" and owner = \"".$args{-user}."\""
#		if defined $args{-user};
#	$query.=" and screen_name = \"".$args{-screen_name}."\""
#		if defined $args{-screen_name};
#
#	my($screen_defn, $container_name);
#	print "DBG: (Setup QUERY)=",$query,$NL if defined $args{-debug};
#	foreach ( dbi_query(-db=>$db,-query=>$query, -connection=>$connection, -die_on_error=>0 )) {
#		($screen_defn, $container_name) = dbi_decode_row($_);
#	}
	my($container_name)=$args{-container} if defined $args{-container};

	if( ! defined $args{-system}  and ! defined $args{-program} ) {
		if( ! defined $container_name or $container_name eq "" ) {
	   		print _warn($args{-html},"Error - No Container Passed".$NL);
	   		$! = _warn($args{-html},"Error - No Container Passed".$NL);
	   		foreach (keys %args) { print "MlpRunScreen: $_ => $args{$_}<br>" unless $_ eq "-NL"; }
	   		return undef;
		}
	}

	if( ! defined $args{-maxtimehours}	or $args{-maxtimehours} eq "" ) {
   		print _warn($args{-html},"Error - No Time Passed".$NL);
   		$! = _warn($args{-html},"Error - No Time Passed".$NL);
   		return undef;
	}

#	if( ! defined $screen_defn	or $screen_defn eq ""
#   	or  ! defined $container_name 	or  $container_name eq "" ) {
#   		$! = _warn($args{-html},"Error - Bad Screen Definition ($screen_defn) or Container ($container_name)".$NL);
#   		return undef;
#	}

	my($screen_defn)=$args{-screen_name};
	print "DBG: Screen=$screen_defn Container=$container_name".$NL if defined $args{-debug};

	#################################
   #  BUILD THE QUERY              #
	#################################
	my($report_query)="select * from ";
	my($order_by)="";
	my($whereclause);

	if( defined $args{-production} and $args{-production} ne "0" ) {
		$whereclause .= " ,ProductionServers p";
	}

	if( defined $args{-system} ) {
		$whereclause.="\n where t.system='".$args{-system}."'";
	} elsif( defined $args{-program} ) {
		if( $args{-program} eq "BACKUPS") {
			$whereclause.="\n where monitor_program in ('backup.pl', 'BackupFull', 'BackupTran', 'LoadAllTranlogs', 'FixLogship')";
		} else {
			$whereclause.="\n where monitor_program='".$args{-program}."'";
		}
 	} elsif( $container_name eq "All" ) {
 		$whereclause.=" where\n 1=1 ";
	} else {
		$whereclause.=" ,Container_full f\n where f.name='".$container_name."' and t.system=f.element\n ";
	}

	$whereclause .= " and subsystem like 'Job:%'"
		if $args{-screen_name} eq "Backups";

	$whereclause .= " and subsystem = '".$args{-subsystem}."' " if $args{-subsystem};
	#print "DBG: Whereclause=>$whereclause".$NL if defined $args{-debug};

	#####################
	# Get Report Type Code and Max Age for rows
	#####################
	my($report_code)=MlpGetReportCode(-screen_name=>$args{-screen_name});
	my($max_row_age_secs)=60*60;
	$max_row_age_secs *= $args{-maxtimehours} if $args{-maxtimehours};

	#print "DBG: ReportTypeCode=$report_code MaxRowAge=$max_row_age_secs seconds.".$NL if defined $args{-debug};
	my($state_sev_col);
	if( $report_code eq "e" ) {
		if( ! defined $args{-showok} ) {
			#$whereclause.=" and ( reviewed_time is null or datediff(dd,reviewed_time,getdate())>=1 )\n ";
			$whereclause.=" and reviewed_time is null\n ";
		}
		$state_sev_col='severity';
		$order_by=" order by event_time desc";
		$report_query.= "Event t $whereclause and event_time>\"".do_time(-time=>(time - $max_row_age_secs), -fmt=>"mm/dd/yyyy hh:mi:ss")."\"";
	} elsif( $report_code eq "h" ) {
		if( ! defined $args{-showok} ) {
			#$whereclause.=" and ( reviewed_time is null or datediff(dd,reviewed_time,getdate())>=1 )\n ";
			$whereclause.=" and ( reviewed_time is null or reviewed_until<getdate() )\n ";
		}
		$state_sev_col='state';
			$report_query="select monitor_program,t.system,subsystem,state,
		monitor_time=datediff(ss,monitor_time,getdate()),
		lastok=datediff(ss,last_ok_time,getdate()),
		message_text,document_url,reviewed_by,reviewed_time,
		datediff(mi,reviewed_until,getdate()),
		reviewed_severity
	from Heartbeat t $whereclause";
			$report_query .= " and monitor_time>\"".do_time(-time=>(time - $max_row_age_secs), -fmt=>"mm/dd/yyyy hh:mi:ss")."\"" if defined $args{-useheartbeattime} and $args{-useheartbeattime}==1;
			$order_by=" order by t.system,subsystem";
	} elsif( $report_code eq "p" ) {
		$state_sev_col='state';
		$order_by="";
		if( ! defined $max_row_age_secs or $max_row_age_secs =~ /^\s*$/ ) {
			$report_query.="Performance $whereclause";
		} else {
			$report_query.= "Performance t $whereclause and monitor_time>\"".do_time(-time=>(time - $max_row_age_secs), -fmt=>"mm/dd/yyyy hh:mi:ss")."\"";
		}
	} else {
		$! = _warn($args{-html},"Invalid Source Table For $screen_defn - $report_code must be e,h,p".$NL);
		return undef;
	}

	if( ! defined $report_code ) {
		$! = _warn($args{-html}, "Report Internal errror.  No Report in ScreenDefn for $screen_defn" );
		return undef;
	}

	$report_query .= " and internal_state!='D'"
		unless $args{-screen_name} eq "Agents";
	$report_query .= " and batchjob is not null"
		if $args{-screen_name} =~ /Batch/i;
	#$report_query .= " and monitor_program = 'Monitor'"
	$report_query .= " and batchjob = 'AGENT'"
		if $args{-screen_name} eq "Agents";
	#$report_query .= " and batchjob is null and monitor_program != 'Monitor'"
	$report_query .= " and batchjob is null "
		if $args{-screen_name} =~ /eartbeat/i and defined $container_name;

	if( defined $args{-severity} and $args{-severity} ne "All" ) {
		# $report_query .= " and last_ok_time is not null and batchjob is null and monitor_program != 'Monitor'"
		if( $args{-severity} =~ /Warning/i ) {
			$report_query .= " and ( $state_sev_col in ('WARNING','ERROR','ALERT','CRITICAL','EMERGENCY')";
			$report_query .= " or datediff(hh,last_ok_time,getdate())>4"
				if $args{-screen_name} =~ /Agents/i;# or $args{-screen_name} =~ /Heartbeats/i;
			$report_query .= " )"
		} elsif( $args{-severity} =~ /Emergency/i ) {
			$report_query .= " and ( $state_sev_col = 'EMERGENCY'";
			$report_query .= " or datediff(hh,last_ok_time,getdate())>4"
				if $args{-screen_name} =~ /Agents/i;# or $args{-screen_name} =~ /Heartbeats/i;
			$report_query .= " )"
		} elsif( $args{-severity} =~ /Error/i ) {
			$report_query .= " and ( $state_sev_col in ('ERROR','ALERT','CRITICAL','EMERGENCY')";
			$report_query .= " or datediff(hh,last_ok_time,getdate())>4"
				if $args{-screen_name} =~ /Agents/i;# or $args{-screen_name} =~ /Heartbeats/i;
			$report_query .= " )"
		} elsif( $args{-severity} =~ /Fatal/i ) {
			$report_query .= " and ( $state_sev_col in ('ALERT','CRITICAL','EMERGENCY')";
			$report_query .= " or datediff(hh,last_ok_time,getdate())>4"
				if $args{-screen_name} =~ /Agents/i;# or $args{-screen_name} =~ /Heartbeats/i;
			$report_query .= " )"
		} elsif( ! defined $args{-severity} ) {
			die "OOPS  severity is undefined???\n";
		} else {
			die "OOPS  what kind of severity is \"$args{-severity} ???\n";
		}
	}
	if( defined $args{-production} and $args{-production} ne "0" ) {
		$report_query .= " and t.system = p.system and is_production=1";
	}

	$report_query.=$order_by;
	print "DBG (FINAL QUERY): {".join($NL,split(/\n/,$report_query))."}".$NL if defined $args{-debug};

	if( $report_code eq "h" ) {
		my(@rc)=dbi_query(-db=>$db,-query=>$report_query, -connection=>$connection, -die_on_error=>0 );
		my(@x)=qw(State System Subsystem Program LastTime LastOk Text OkDelete);
		push @outarray,\@x;
		foreach ( @rc ) {
			my($d_monitor_program,$d_system,$d_subsystem,$d_state,$d_monitor_time,
				$d_lastok,$d_message_text,$d_document_url,$d_reviewed_by,
				$d_reviewed_time,$d_reviewed_until,$d_reviewed_severity)=dbi_decode_row($_);

			$d_state="(OK)-".$d_state if $d_reviewed_by ne "" and $d_reviewed_until<0;
			# $d_state="(OK)-".$d_reviewed_until if $d_reviewed_by ne "";
			# $d_monitor_time=do_diff($d_monitor_time);
			if( $d_monitor_time  =~ /^\s*$/ ) {
				$d_monitor_time="";
			} elsif( $d_monitor_time > 24*60*60 ) {
				$d_monitor_time=do_diff($d_monitor_time);
			} else {
				$d_monitor_time=do_time(-fmt=>"hh:mi", -time=>(time-$d_monitor_time));
			}
			if( $d_lastok =~ /^\s*$/ ) {
				$d_lastok="Never";
			} elsif( $d_lastok > 24*60*60 ) {
				$d_lastok=do_diff($d_lastok);
			} else {
				$d_lastok=do_time(-fmt=>"hh:mi", -time=>(time-$d_lastok));
			}

			my(@x)= ( $d_state,$d_system,$d_subsystem,$d_monitor_program,$d_monitor_time,
				$d_lastok,$d_message_text );
			push @outarray, \@x;
		}
	} elsif( $report_code eq "e" ) {
		$report_query =~ s/and state in/and severity in/;
		my(@x)=qw(Source EventTime System Subsystem Severity Text);
		push @outarray,\@x;
		my($d_monitor_program,$d_monitor_time,$d_system,$d_subsystem,$d_event_time,$d_severity,
				$d_event_id, $message_text, $message_value, $document_url,
				$reviewed_by, $reviewed_time, $internal_state );
		foreach ( dbi_query(-db=>$db,-query=>$report_query, -connection=>$connection, -die_on_error=>0 )) {
			($d_monitor_program,$d_monitor_time,$d_system,$d_subsystem,$d_event_time,$d_severity,
				$d_event_id, $message_text, $message_value, $document_url,
				$reviewed_by, $reviewed_time, $internal_state )= dbi_decode_row($_);
			my(@x)=($d_monitor_program,$d_event_time,$d_system,$d_subsystem,$d_severity,$message_text );
			push @outarray, \@x;
		}
	} else {
		my(@x)=qw(N.A. N.A. N.A. N.A. N.A. N.A.);
		push @outarray,\@x;
		foreach ( dbi_query(-db=>$db,-query=>$report_query, -connection=>$connection, -die_on_error=>0 )) {
			my(@x)= dbi_decode_row($_);
			push @outarray, \@x;
		}
	}
	print "DBG MlpRunScreen Completed: Returning $#outarray Rows".$NL if defined $args{-debug};
	return \@outarray;
}

sub MlpGetReportCode
{
	my(%args1)=@_;
	initialize(%args1) unless $i_am_initialized eq "TRUE";
	if($i_am_initialized eq "FAILED" ) {
		print "Cant Get Alarm Info: Connection Was Marked Failed\n";
		return 0;
	};

	my(%args)= validate_params("MlpGetReportCode",%args1);

	my($rc)='h';
	$rc="e" if $args{-screen_name} =~ /event/i;

	return $rc;
}

sub MlpGetAlarms {
	my(%args1)=@_;
	initialize(%args1) unless $i_am_initialized eq "TRUE";
	if($i_am_initialized eq "FAILED" ) {
		print "Cant Get Alarm Info: Connection Was Marked Failed\n";
		return 0;
	};

	my($query);
	if( defined $args1{-test} ) {
		# this query will get all alarms WITHOUT regard to internal_state
#	$query="select h.monitor_program,h.system,h.subsystem,h.state,h.message_text,n.container,h.monitor_time
#		from Heartbeat h, Notification n
#		where n.monitor_program = h.monitor_program
#		and   isnull(n.system,isnull(h.system,'XXX')) = isnull(h.system,'XXX')
#		and   (n.container is null
#			or  h.system in ( select element from Container_full where name=n.container))";
		$query="exec get_possible_alarms \@test_mode='Y'";
	} else {
		$query="exec get_possible_alarms";
	}

	my(@rc);
	foreach ( dbi_query(-db=>$db,-query=>$query, -connection=>$connection, -die_on_error=>0)) {
		my(@r) = dbi_decode_row($_);
		push @rc,\@r;
	}
	return @rc;
}

sub MlpClearAlarms {
	my(%args1)=@_;
	initialize(%args1) unless $i_am_initialized eq "TRUE";
	if($i_am_initialized eq "FAILED" ) {
		print "Cant Get Alarm Info: Connection Was Marked Failed\n";
		return 0;
	};
	my(@rc);
	my($query)="exec clear_possible_alarms";
	dbi_query(-db=>$db,-query=>$query,-no_results=>1,-connection=>$connection, -die_on_error=>0);
}

sub MlpSaveNotification
{
    my(%args)= validate_params("MlpSaveNotification",@_);
    $args{-EmergencyMin} 	= 0 unless $args{-EmergencyMin};
    $args{-FatalMin} 	= 60 unless $args{-FatalMin};
    $args{-ErrorMin} 	= 120 unless $args{-ErrorMin};
    $args{-WarnMin}	= 300 unless $args{-WarnMin};

    my($query)="insert SentNotification
    values ('".
      	$args{-monitor_program}. "','".
        $args{-system}. "','".
        $args{-subsystem}. "','".
        $args{-monitor_time}. "','".
	$args{-severity}. "','".
      	$args{-user_name} 	. "',getdate(),'".
      	$args{-notify_type} 	. "','".
      	$args{-header} 		. "','".
      	$args{-message} 		. "'".
      	" )";

	#print $query,"\n";
   	dbi_query(-db=>$db,-query=>$query,-no_results=>1,-connection=>$connection, -die_on_error=>0);
   	#print "--\n";
}

sub MlpGetSeverity {
	my(%args1)=@_;
	initialize(%args1) unless $i_am_initialized eq "TRUE";
	if($i_am_initialized eq "FAILED" ) {
		print "Cant Get Alarm Info: Connection Was Marked Failed\n";
		return 0;
	};
	my(@severityvalues)=('Emergency','Fatals','Errors','Warnings','All');
	return @severityvalues;

	# my(@rc);
	# my($query)="SELECT severity from Severity order by value";
	# foreach ( dbi_query(-db=>$db,-query=>$query, -connection=>$connection, -die_on_error=>0)) {
		# push @rc, dbi_decode_row($_);
	# }
	# return @rc;
}

sub MlpGetOperator {
	my(%args1)=@_;
	initialize(%args1) unless $i_am_initialized eq "TRUE";
	if($i_am_initialized eq "FAILED" ) {
		print "Cant Get Alarm Info: Connection Was Marked Failed\n";
		return 0;
	};
	if( defined $args1{-showall} ) {
		my($query)="selectuser_name,user_type,enabled,
			pager_email,email,netsend,
			last_pager,last_email,last_netsend,
			weekday_start_time,weekday_end_time,saturday_start_time,
			saturday_end_time,sunday_start_time,sunday_end_time,
			holiday_start_time,holiday_end_time
 		from Operator";
		return dbi_query( -db=>$db,
				-query=>$query,
				-connection=>$connection,
				-die_on_error=>0);
	}

	my(@rc);
	my($query)="select distinct user_name from Operator";
	foreach ( dbi_query(-db=>$db,-query=>$query, -connection=>$connection, -die_on_error=>0)) {
		push @rc, dbi_decode_row($_);
	}
	return @rc;
}

sub MlpGetProgram {
	my(%args1)=@_;
	initialize(%args1) unless $i_am_initialized eq "TRUE";
	if($i_am_initialized eq "FAILED" ) {
		print "Cant Get Alarm Info: Connection Was Marked Failed\n";
		return 0;
	};
	my(@rc);
	my($query)="select distinct monitor_program from Heartbeat where batchjob is null
union
select monitor_program  from Event
order by monitor_program";
	foreach ( dbi_query(-db=>$db,-query=>$query, -connection=>$connection, -die_on_error=>0)) {
		push @rc, dbi_decode_row($_);
	}
	return @rc;
}

sub MlpGetContainer
{
	my(%args1)=@_;
	initialize(%args1) unless $i_am_initialized eq "TRUE";
	if($i_am_initialized eq "FAILED" ) {
		print "Cant Get Alarm Info: Connection Was Marked Failed\n";
		return 0;
	};
	my(%args)= validate_params("MlpGetContainer",%args1);

	$args{-type} = "s" unless defined $args{-type};

	# if screen_name passed then you must get the container name for that screen
	if( $args{-screen_name} ) {
		my($query)="SELECT container_name from Screen where screen_name=\"".$args{-screen_name}."\"";
		$query.=" and owner = \"".$args{-user}."\"" if defined $args{-user};
		foreach ( dbi_query(-db=>$db,-query=>$query, -connection=>$connection, -die_on_error=>0)) {
			($args{-name}) = dbi_decode_row($_);
		}
	}

	my(@rc);
	push @rc,"All";
	if( $args{-type} eq "s" ) {
		my($query)= "SELECT distinct element from Container_full where 1=1 \n";
		$query.=" and owner = \"".$args{-user}."\"" if defined $args{-user};
		$query.=" and name = \"".$args{-name}."\"" if defined $args{-name};
		print $query if defined $args{-debug};
		foreach ( dbi_query(-db=>$db,-query=>$query, -debug=>$args{-debug}, -connection=>$connection, -die_on_error=>0 )) {
			my($el,$ty)=dbi_decode_row($_);
			# if( $ty eq "s" ) {
				push @rc,$el;
			# } else {
				# push @rc,MlpGetContainer(-name=>$el,-type=>"s",-user=>$args{-user});
			# }
		}
		return sort @rc;
	} elsif( $args{-type} eq "c" ) {
		my($query)= "SELECT distinct name from Container where 1=1\n";
		$query.=" and owner = \"".$args{-user}."\"" if defined $args{-user};
		$query.=" and name = \"".$args{-name}."\"" if defined $args{-name};
		print $query if defined $args{-debug};
		foreach ( dbi_query(-db=>$db,-query=>$query, -debug=>$args{-debug}, -connection=>$connection, -die_on_error=>0 )) {
			push @rc,dbi_decode_row($_);
		}
		return sort @rc;
	} else {
		die "Invalid Type Arg Passed - must be c or s";
	}
}

sub MlpGetDbh
{
	initialize() unless $i_am_initialized eq "TRUE";
	if($i_am_initialized eq "FAILED" ) {
		print "Cant Get Alarm Info: Connection Was Marked Failed\n";
		return 0;
	};
	return $connection;
	# return dbi_getdbh();
}

sub MlpManageData
{
	my(%args)=@_;
	my(@rc);
	my($query);
	initialize(%args) unless $i_am_initialized eq "TRUE";
	if($i_am_initialized eq "FAILED" ) {
		print "Cant Get Alarm Info: Connection Was Marked Failed\n";
		return 0;
	};

	if( defined $args{-debug} ) {
		print "MlpManageData ( time=",localtime(),") \n";
		foreach( keys %args ) {
			print "MlpManageData( $_ => $args{$_} ) \n" unless $_ eq "-NL";
		}
	}

	my($username) = $ENV{REMOTE_ADDR} || $ENV{LOGNAME} || $ENV{USERNAME};
	if( $username eq $DEVELOPER_IPADDR ) {
		push @rc, "<FONT SIZE=-2>MlpManageData - Called From The Developers Workstation</FONT>\n";
		foreach ( sort keys %args ) {
			push @rc, "<FONT SIZE=-2>MlpManageData: $_ => $args{$_} </FONT>\n";
		}
	}

	# OkId and ! Admin and Operation=del = delete row
	# OkId and ! Admin and Operation!=del = approve for 1 day
	if( defined $args{OkId}
	and (! defined $args{Admin} or $args{Admin}=~/^\s*$/)) {
			if( ! defined $args{Program} ) {
				push @rc, "ERROR: Parameter Admin Called But Program Not Set - No Calling Screen ";
				foreach ( keys %args ) { push @rc, "Key=$_ Arg=$args{$_} "; }
				return @rc;
			}

			if( $args{Operation} eq "del" ) {
				$query="Delete Heartbeat
					where monitor_program='".$args{Program}."'
			and system =    '".$args{System}."'
			and isnull(subsystem,'')=  '".$args{Subsystem}."'
			and state=      '".$args{State}."'";
			} elsif( $args{Operation} eq "delserver" ) {
				$query="Delete Event where system = '".$args{System}."'";
				push @rc,$query;
				foreach ( dbi_query(-db=>$db,-query=>$query, -connection=>$connection, -die_on_error=>0 )) {
					push @rc,"ERROR:".dbi_decode_row($_);
					push @rc,"$_ $DBI::err $DBI::errstr";
				}
				$query="Delete Heartbeat where system = '".$args{System}."'";

			} else {
				$args{Hours}=24 unless $args{Hours};
				$query="Update Heartbeat Set
			reviewed_by	=	'".$username."',
			reviewed_time	=	getdate(),
			reviewed_until	=	dateadd(hh,$args{Hours},getdate()),
			reviewed_severity= null
	where monitor_program='".$args{Program}."'
			and system =    '".$args{System}."'
			and isnull(subsystem,'')=  '".$args{Subsystem}."'
			and state=      '".$args{State}."'";
			}
	}  elsif( $args{Admin} eq "Delete By Monitor") {
		if( $args{Operation} eq "del" ) {
			$query="Delete Event where monitor_program='".$args{Monitor}."'";
			push @rc,$query;
			foreach ( dbi_query(-db=>$db,-query=>$query, -connection=>$connection, -die_on_error=>0 )) {
				push @rc,"ERROR:".dbi_decode_row($_);
				push @rc,"$_ $DBI::err $DBI::errstr";
			}
			$query="Delete Heartbeat where monitor_program='".$args{Monitor}."'";
			#push @rc,$query;
		} else {
			push @rc,"FILE=".__FILE__." LINE=".__LINE__."DBG DBG NOTE - OPERATION=$args{Operation}";
		}
	}  elsif( $args{Admin} eq "Delete By Monitor_Type") {
		if( $args{Operation} eq "del" ) {
			$query="Delete $args{Type} where monitor_program='".$args{Monitor}."'";
			#push @rc,$query;
		} else {
			push @rc,"FILE=".__FILE__." LINE=".__LINE__."DBG DBG NOTE - OPERATION=$args{Operation}";
		}
	} elsif( $args{Admin} eq "Production" ) {
			# because of the way the initial query worked, we are ensured
			# that the server is in ProductionServers, but are not ensured
			# that a row exists in IgnoreList
			if( $args{Operation} eq "setignore" ) {
				if( $args{Value} == 1 ) {
					push @rc,"Placing Server $args{Server} On Ignore List\n";
					$query="insert IgnoreList values(null,'".$args{Server}."',null)";
				} else {
					push @rc,"Removing Server $args{Server} From Ignore List\n";
					$query="DELETE IgnoreList where monitor_program is null and subsystem is null and system='".$args{Server}."'";
				}
			} elsif( $args{Operation} eq "setproduction" ) {
				$query="Insert ServerChanges values( \"".$args{Server}."\",getdate(),\"is_production\",\"".$args{Value}."\")";
				foreach ( dbi_query(-db=>$db,-query=>$query, -connection=>$connection, -die_on_error=>0 )) {
					push @rc,"ERROR:".dbi_decode_row($_);
					push @rc,"$_ $DBI::err $DBI::errstr";
				}
				if( $args{Value} == 1 ) {
					push @rc,"Server $args{Server} Is Now Considered PRODUCTION\n";
					$query="UPDATE ProductionServers set is_production=1 where system='".$args{Server}."'";
									} else {
					push @rc,"Server $args{Server} Is No Longer Considered PRODUCTION\n";
					$query="UPDATE ProductionServers set is_production=0 where system='".$args{Server}."'";
				}
			} elsif( $args{Operation} eq "setblackout7days" ) {
				$args{Days}=7 unless $args{Days};
				if( $args{Value} == 1 ) {
					push @rc,"Server $args{Server} Is Now Blacked Out\n";
					$query="Insert Blackout select '".$args{Server}."', dateadd(dd,$args{Days},getdate())\n";
					$query.="Update Heartbeat Set
			reviewed_by	=	'".$username."',
			reviewed_time	=	getdate(),
			reviewed_until	=	dateadd(dd,$args{Days},getdate()),
			reviewed_severity= null
			where system='".$args{Server}."'\n";
				} else {
					push @rc,"Server $args{Server} Is No Longer Blacked Out\n";
					$query="Delete Blackout where system='".$args{Server}."'\n";

				}
			} elsif( $args{Operation} eq "setblackout" ) {
				if( $args{Value} == 1 ) {
					my($hours)=0;
					$hours+=$args{Hours} 	if $args{Hours};
					$hours+=24*$args{Days} 	if $args{Days};
					push @rc,"Server $args{Server} Is Now Blacked Out\n";
					$query="Delete Blackout where system='".$args{Server}."'\n ";
					$query.="Insert Blackout select '".$args{Server}."', dateadd(hh,$hours,getdate())\n";
					$query.="Update Heartbeat Set
			reviewed_by	=	'".$username."',
			reviewed_time	=	getdate(),
			reviewed_until	=	dateadd(hh,$hours,getdate()),
			reviewed_severity= 	null
			where system='".$args{Server}."'\n";
				} else {
					push @rc,"Server $args{Server} Is No Longer Blacked Out\n";
					$query="Delete Blackout where system='".$args{Server}."'\n";
				}
			} elsif( $args{Operation} eq "del" ) {
				push @rc,"Server $args{Server} Deleted\n";
				$query.="Delete Blackout where system='".$args{Server}."'\n";
				$query.="Delete ProductionServers where system='".$args{Server}."'\n";
				$query.="Delete IgnoreList where system='".$args{Server}."'\n";
				$query.="Delete Heartbeat where system='".$args{Server}."'\n";
				$query.="Delete Event where system='".$args{Server}."'\n";
			} else {
					push @rc,"ERROR: Invalid Args To Production Screen Handler";
					foreach (keys %args) { push @rc,"Key=$_ val=$args{$_}"; }
					return @rc;
			}
	} elsif( $args{Admin} eq "Rebuild Containers" ) {
			$query="exec build_container";
	} elsif( $args{Admin} eq "System->Container" ) {
		if( $args{Container} =~ /^\s*$/
				or $args{System_LU} =~ /^\s*$/ ) {
			push @rc,"ERROR: Not All Required Args Defined";
			return @rc;
		}

		if( $args{Operation} eq "del" ) {
			$query="Delete ContainerOverride where element='".
					$args{System_LU}."' and name='".
					$args{Container}."'";
		} else {
			$query="if exists ( select * from Container where name='".$args{System_LU}."' )
	Insert ContainerOverride Values ('".
					$args{Container}."','c','".
					$args{System_LU}."',null)
else
	Insert ContainerOverride Values ('".
					$args{Container}."','s','".
					$args{System_LU}."',null)";
		}
		push @rc, $query;
		foreach ( dbi_query(-db=>$db,-query=>$query, -connection=>$connection, -die_on_error=>0 )) {
			push @rc,"ERROR:".dbi_decode_row($_);
			push @rc,"$_ $DBI::err $DBI::errstr";
		}
		$query="exec build_container";
	} elsif( $args{Admin} eq "Program->Container" ) {
		if( $args{Program_LU} =~ /^\s*$/ or $args{Container_LU} =~ /^\s*$/ ) {
			push @rc,"ERROR: Not All Required Args Defined";
			return @rc;
		}

		if( $args{Operation} eq "del" ) {
			$query="Delete ContainerMap where monitor_program='".
					$args{Program_LU}."' and container='".
					$args{Container_LU}."'";
		} else {
			$query="Insert ContainerMap Values ('".
					$args{Program_LU}."','".
					$args{Container_LU}."')";
		}
		push @rc, $query;
		foreach ( dbi_query(-db=>$db,-query=>$query, -connection=>$connection, -die_on_error=>0 )) {
			push @rc,"ERROR:".dbi_decode_row($_);
			push @rc,"$_ $DBI::err $DBI::errstr";
		}
		$query="exec build_container";
	} elsif( $args{Admin} eq "Delete By Row" ) {
		if( $args{Monitor} =~ /^\s*$/
				or $args{System} =~ /^\s*$/
				or $args{Type} =~ /^\s*$/ ) {
			push @rc,"ERROR: Not All Required Args Defined";
			return @rc;
		}

		$query="Delete $args{Type} where monitor_program='".
				$args{Monitor}."' and system='".
				$args{System}."'";
	} elsif( $args{Admin} eq "Alarm History" ) {
				push @rc,"ERROR: I havent Got Around To Coding This Yet";
				return @rc;
	} elsif( $args{Admin} eq "Report Setup" ) {
		if( $args{Operation} eq "del" ) {
			$query="DELETE ReportArgs where reportname='".  $args{Report_Name}."'";
		} elsif( $args{Operation} eq "add" ) {
			$query="Delete ReportArgs where reportname='". $args{Report_Name}."'";
			push @rc,$query;
			foreach ( dbi_query(-db=>$db,-query=>$query, -connection=>$connection, -no_results=>1,-die_on_error=>0 )) {
				push @rc,"ERROR:".join(" ",dbi_decode_row($_));
			}

			if( $args{Program} !~ /^\s*$/ ) {
				$query="insert ReportArgs ( reportname,keyname,value ) values ('".
						$args{Report_Name}."','progname','".$args{Program}."')";
				push @rc,$query;
				foreach ( dbi_query(-db=>$db,-query=>$query, -connection=>$connection, -no_results=>1,-die_on_error=>0 )) {
					push @rc,"ERROR:".join(" ",dbi_decode_row($_));
				}
			}

			$query="insert ReportArgs ( reportname,keyname,value ) values ('".
					$args{Report_Name}."','filter_severity','".$args{Min_Severity_LU}."')";
			push @rc,$query;
			foreach ( dbi_query(-db=>$db,-query=>$query, -connection=>$connection, -no_results=>1,-die_on_error=>0 )) {
				push @rc,"ERROR:".join(" ",dbi_decode_row($_));
			}

			$query="insert ReportArgs ( reportname,keyname,value ) values ('".
					$args{Report_Name}."','ProdChkBox','".$args{Production_YN_LU}."')";
			push @rc,$query;
			foreach ( dbi_query(-db=>$db,-query=>$query, -connection=>$connection, -no_results=>1,-die_on_error=>0 )) {
				push @rc,"ERROR:".join(" ",dbi_decode_row($_));
			}

			$query="insert ReportArgs ( reportname,keyname,value ) values ('".
					$args{Report_Name}."','filter_time','".$args{Time_LU}."')";
			push @rc,$query;
			foreach ( dbi_query(-db=>$db,-query=>$query, -connection=>$connection, -no_results=>1,-die_on_error=>0 )) {
				push @rc,"ERROR:".join(" ",dbi_decode_row($_));
			}

			$query="insert ReportArgs ( reportname,keyname,value ) values ('".
					$args{Report_Name}."','ShowProgram','".$args{Show_Program_YN_LU}."')";
			push @rc,$query;
			foreach ( dbi_query(-db=>$db,-query=>$query, -connection=>$connection, -no_results=>1,-die_on_error=>0 )) {
				push @rc,"ERROR:".join(" ",dbi_decode_row($_));
			}

			$query="insert ReportArgs ( reportname,keyname,value ) values ('".
					$args{Report_Name}."','ReportTitle','".$args{Title}."')";
			push @rc,$query;
			foreach ( dbi_query(-db=>$db,-query=>$query, -connection=>$connection, -no_results=>1,-die_on_error=>0 )) {
				push @rc,"ERROR:".join(" ",dbi_decode_row($_));
			}

			$query="insert ReportArgs ( reportname,keyname,value ) values ('".
					$args{Report_Name}."','filter_container','".$args{Container_LU}."')";
		} else {
			push @rc,"FILE=".__FILE__." LINE=".__LINE__."DBG DBG NOTE - OPERATION=$args{Operation}";
		}
		# push @rc,"ERROR: I havent Got Around To Coding This Yet";
		# "select 	reportname, keyname, value
		# from ReportArgs order by reportname";
		# return @rc;
	} elsif( $args{Admin} eq "Ignore List" ) {
		if( $args{Operation} eq "del" ) {
			$query="Delete IgnoreList where monitor_program";
			if( $args{Program_LU} =~ /^\s*$/ ) {
				$query .= " is null";
			} else {
				$query .= "='".$args{Program_LU}."'";
			}
			if( $args{System} =~ /^\s*$/ ) {
				$query.=" and system is null";
			} else {
				$query.=" and system = '".$args{System}."'";
			}
			if( $args{Subsystem} =~ /^\s*$/ ) {
				$query.=" and subsystem is null";
			} else {
				$query.=" and subsystem = '".$args{Subsystem}."'";
			}
		} else {
			$query="insert IgnoreList ( monitor_program,system,subsystem
				) values ('".  $args{Program_LU}."',";

			if( $args{System} =~ /^\s*$/ ) {
				$query.="null,";
			} else {
				$query.="'".$args{System}."',";
			}

			if( $args{Subsystem} =~ /^\s*$/ ) {
				$query.="null)";
			} else {
				$query.="'".$args{Subsystem}."')";
			}
			push @rc,$query;
			foreach ( dbi_query(-db=>$db,-query=>$query, -connection=>$connection, -die_on_error=>0 )) {
				push @rc,"ERROR:".dbi_decode_row($_);
				push @rc,"$_ $DBI::err $DBI::errstr";
			}
			$query="DELETE Heartbeat from IgnoreList i, Heartbeat h where i.monitor_program = h.monitor_program and isnull(i.system,h.system)=h.system and isnull(i.subsystem,h.subsystem)=h.subsystem";
			push @rc,$query;
			foreach ( dbi_query(-db=>$db,-query=>$query, -connection=>$connection, -die_on_error=>0 )) {
				push @rc,"ERROR:".dbi_decode_row($_);
				push @rc,"$_ $DBI::err $DBI::errstr";
			}
			$query="DELETE Event from IgnoreList i, Event h where i.monitor_program = h.monitor_program and isnull(i.system,h.system)=h.system and isnull(i.subsystem,h.subsystem)=h.subsystem";
		}
	} elsif( $args{Admin} eq "Alarm Routing" ) {

		if( $args{Operation} eq "del" ) {
				$query="Delete Notification Where
			user_name = '".$args{User_LU}."' and
			monitor_program = '".$args{Program_LU}."' and
			min_severity = '".$args{Min_Severity_LU}."' and
			max_severity = '".$args{Max_Severity_LU}."'";

			if( ! defined $args{Container} or $args{Container} eq "" ) {
				$query.= " and container is null";
			} else {
				$query.= " and container = '".$args{Container}."'";
			}
			if( ! defined $args{System} or $args{System} eq "" ) {
				$query.= " and system is null";
			} else {
				$query.= " and system = '".$args{System}."'";
			}
			if( ! defined $args{Subsystem} or $args{Subsystem} eq "" ) {
				$query.= " and subsystem is null";
			} else {
				$query.= " and subsystem = '".$args{Subsystem}."'";
			}
		} else {
			my($cnt)=$#rc;
			push @rc,"ERROR: Missing Argument User"
					if $args{User_LU}					=~ /^\s*$/;
			push @rc,"ERROR: Missing Argument Program"
					if $args{Program_LU}			=~ /^\s*$/;
			push @rc,"ERROR: Missing Argument Min Severity"
					if $args{Min_Severity_LU}	=~ /^\s*$/;
			push @rc,"ERROR: Missing Argument Max Severity"
					if $args{Max_Severity_LU}	=~ /^\s*$/;
			push @rc,"ERROR: Missing Argument Pager"
					if $args{Use_Pager_LU}		=~ /^\s*$/;
			push @rc,"ERROR: Missing Argument Email"
					if $args{Use_Email_LU}		=~ /^\s*$/;
			push @rc,"ERROR: Missing Argument Netsend"
					if $args{Use_Netsend_LU}	=~ /^\s*$/;
			return @rc if $#rc ne $cnt;
			my($sys)="null";
			my($subsys)="null";
			my($contr)="null";
			$contr="'".$args{Container}."'" if defined $args{Container} and $args{Container}!~/^\s*$/;
			$sys="'".$args{System}."'" if defined $args{System} and $args{System}!~/^\s*$/;
			$subsys="'".$args{Subsystem}."'" if defined $args{Subsystem} and $args{Subsystem}!~/^\s*$/;

			$query="Delete Notification where user_name=\"".$args{User_LU}."\"
				and monitor_program = \"".$args{Program_LU} ."\" ";
			if( $sys eq "null" ) {
				$query.="and system	is null ";
			} else {
				$query.="and system	= \"$sys\" ";
			}

			if( $subsys eq "null" ) {
				$query.="and subsystem	is null ";
			} else {
				$query.="and subsystem	= \"$subsys\" ";
			}
			push @rc,$query;
			foreach ( dbi_query(-db=>$db,-query=>$query, -connection=>$connection, -die_on_error=>0 )) {
				push @rc,"ERROR:".dbi_decode_row($_);
				push @rc,"$_ $DBI::err $DBI::errstr";
			}

			$query="insert Notification ( user_name,monitor_program,system,subsystem,min_severity,
				max_severity,use_pager,use_email,use_netsend,container,
				emergency_minutes,fatal_minutes,error_minutes,warning_minutes
				) values ('".
					$args{User_LU}."','".
					$args{Program_LU}."',".
					$sys.",".
					$subsys.",'".
					$args{Min_Severity_LU}."','".
					$args{Max_Severity_LU}."','".
					$args{Use_Pager_LU}."','".
					$args{Use_Email_LU}."','".
					$args{Use_Netsend_LU}."',".
					$contr.",0,60,120,300)";
		}
	} elsif( $args{Admin} eq "Filter Repeated Messages" ) {
		$args{-EmergencyMin} 	= 0 unless $args{-EmergencyMin} 	=~ /^\d+$/;
	    	$args{-FatalMin} 	= 60 unless $args{-FatalMin} 	=~ /^\d+$/;
    		$args{-ErrorMin} 	= 120 unless $args{-ErrorMin} 	=~ /^\d+$/;
    		$args{-WarnMin}	= 300 unless $args{-WarnMin} 		=~ /^\d+$/;

		$query="Update Notification SET
			emergency_minutes 	= $args{EmergencyMin},
			fatal_minutes 		= $args{-FatalMin},
			error_minutes 	        = $args{-ErrorMin},
			warning_minutes 	= $args{-WarnMin}
		Where user_name = '".$args{User}."'
		and   monitor_program    = '".$args{Program}."'
		and   isnull(container,'')    = '".$args{Container}."'
		and   isnull(system,'')    = '".$args{System}."'
		and   isnull(subsystem,'') = '".$args{Subsystem}."'";

	} elsif( $args{Admin} eq "Operator" ) {
		if( $args{Operation} eq "del" ) {
			$query="Delete Operator where user_name='".$args{User}."'";
		} else {
			if( 	$args{User}					=~ /^\s*$/ or
					$args{Enabled_LU}			=~ /^\s*$/ ) {
				push @rc,"ERROR: Not All Required Args Defined";
				return @rc;
			}
			# $args{Pager} =~ s/\@/\\\@/;
			# $args{Email} =~ s/\@/\\\@/;
			# $args{Netsend} =~ s/\@/\\\@/;
			$query="insert Operator ( user_name,user_type,enabled,pager_email,email,netsend
				) values ('".
					$args{User}."','".
					"Admin"."','".
					$args{Enabled_LU}."','".
					$args{Pager}."','".
					$args{Email}."','".
					$args{Netsend}."')";
		}

	} elsif( $args{Admin} eq "Operator Schedule" ) {
		$args{WD_End_LU} 	= 0 unless defined $args{WD_End_LU}  or defined $args{WD_Start_LU};
		$args{Sat_End_LU}  	= 0 unless defined $args{Sat_End_LU} or defined $args{Sat_Start_LU};
		$args{Sun_End_LU}  	= 0 unless defined $args{Sun_End_LU} or defined $args{Sun_Start_LU};
		$args{Hol_End_LU}  	= 0 unless defined $args{Hol_End_LU} or defined $args{Hol_Start_LU};

		$args{WD_Start_LU} 	= 0 unless defined $args{WD_Start_LU};
		$args{Sat_Start_LU} 	= 0 unless defined $args{Sat_Start_LU};
		$args{Sun_Start_LU}     = 0 unless defined $args{Sun_Start_LU};
		$args{Hol_Start_LU}     = 0 unless defined $args{Hol_Start_LU};

		$args{WD_End_LU} 	= 24 unless defined $args{WD_End_LU};
		$args{Sat_End_LU}  	= 24 unless defined $args{Sat_End_LU};
		$args{Sun_End_LU}  	= 24 unless defined $args{Sun_End_LU};
		$args{Hol_End_LU}  	= 24 unless defined $args{Hol_End_LU};

		if( $args{WD_Start_LU} == $args{WD_End_LU} ){
			$args{WD_Start_LU} 	= "null" ;
			$args{WD_End_LU} 	= "null";
		}

		if( $args{Sat_Start_LU} == $args{Sat_End_LU} ){
			$args{Sat_Start_LU} 	= "null";
			$args{Sat_End_LU}  	= "null";
		}

		if( $args{Sun_Start_LU} == $args{Sun_End_LU} ){
			$args{Sun_Start_LU}     = "null";
			$args{Sun_End_LU}  	= "null";
		}

		if( $args{Hol_Start_LU} == $args{Hol_End_LU} ){
			$args{Hol_End_LU}  	= "null";
			$args{Hol_Start_LU}     = "null";
		}

		$query="Update Operator SET
			weekday_start_time 	= $args{WD_Start_LU},
			weekday_end_time 	= $args{WD_End_LU},
			saturday_start_time 	= $args{Sat_Start_LU},
			saturday_end_time 	= $args{Sat_End_LU},
			sunday_start_time 	= $args{Sun_Start_LU},
			sunday_end_time 	= $args{Sun_End_LU},
			holiday_start_time	= $args{Hol_Start_LU},
			holiday_end_time 	= $args{Hol_End_LU}
		Where user_name = '".$args{User_LU}."'";
	} else {
		push @rc, "ERROR: Unknown Calling Screen ($args{Admin}) ";
		foreach ( keys %args ) { push @rc, "Key=$_ Arg=$args{$_} "; }
		return @rc;
	}
	if( defined $query ) {
		push @rc, $query;
		#push @rc,"<b>Query < $query > was Run</b><br>conn=$connection<br>";
		foreach ( dbi_query(-db=>$db,-query=>$query, -no_results=>1, -connection=>$connection, -die_on_error=>0 )) {
			push @rc,"ERROR:".dbi_decode_row($_);
			push @rc,"ERROR: $_ $DBI::err $DBI::errstr";
		}
		push @rc,"ERROR: $_ $DBI::err $DBI::errstr" if defined $DBI::err;
	}
	if( defined $args{-debug} ) {
		print "MlpManageData ( completed=",localtime(),") \n";
	}

	return @rc;
}

sub MlpCleanup {
	my(%args1)=@_;
	print "Caling MlpCleanup()\n";
	initialize(%args1) unless $i_am_initialized eq "TRUE";
	if($i_am_initialized eq "FAILED" ) {
		print "Cant Get Alarm Info: Connection Was Marked Failed\n";
		return 0;
	};
	my(%args)= validate_params("MlpCleanup",%args1);
	$args{-evdays} = $args{-days} unless defined $args{-evdays};
	$args{-hbdays} = $args{-days} unless defined $args{-hbdays};

	my($query)= "DELETE Event where datediff(dd,event_time,getdate()) > $args{-evdays}\n";
	print $query;# if defined $args{-debug};
	my(@rc);
	foreach ( dbi_query(-db=>$db,-query=>$query, -debug=>$args{-debug}, -no_results=>1, -connection=>$connection, -die_on_error=>0 )) {
		push @rc, dbi_decode_row($_);
	}

	$query= "DELETE Heartbeat where datediff(dd,monitor_time,getdate()) > $args{-hbdays} and monitor_program not in (select monitor_program from HeartbeatThresh)\n";
	print $query;	# if defined $args{-debug};
	foreach ( dbi_query(-db=>$db,-query=>$query, -debug=>$args{-debug}, -no_results=>1, -connection=>$connection, -die_on_error=>0 )) {
		push @rc, dbi_decode_row($_);
	}

	$query= "DELETE Heartbeat where datediff(dd,monitor_time,getdate()) > 3*$args{-hbdays}\n";
	print $query;	# if defined $args{-debug};
	foreach ( dbi_query(-db=>$db,-query=>$query, -debug=>$args{-debug}, -no_results=>1, -connection=>$connection, -die_on_error=>0 )) {
		push @rc, dbi_decode_row($_);
	}

	$query= "DELETE AgentHistory where datediff(dd,start_time,getdate())>14\n";
	print $query;	# if defined $args{-debug};	
	foreach ( dbi_query(-db=>$db,-query=>$query, -debug=>$args{-debug}, -no_results=>1, -connection=>$connection, -die_on_error=>0 )) {
		push @rc, dbi_decode_row($_);
	}

	$query="UPDATE Heartbeat
	set state=\"WARNING\", message_text=\"(No Recent Heartbeat) \"+ message_text
	from HeartbeatThresh t, Heartbeat h
	where t.monitor_program=h.monitor_program
	and   h.state in (\"OK\",\"COMPLETED\",\"RUNNING\")
	and   ( t.system is null or t.system=h.system)
	and   ( t.subsystem is null or t.subsystem=h.subsystem)
	and datediff(mi,h.last_ok_time,getdate())>threshold_min\n";

	print $query;	# if defined $args{-debug};
	foreach ( dbi_query(-db=>$db,-query=>$query, -debug=>$args{-debug},-no_results=>1, -connection=>$connection, -die_on_error=>0 )) {
		push @rc, dbi_decode_row($_);
	}

	return @rc;
}

sub MlpBatchStep
{
	my(%args)=@_;
	my(%x);

	%args= validate_params("MlpBatchStep",%args);
	$job_curstep 		= $args{-stepnum};
	$job_curstepname 	= $args{-stepname};

	foreach (keys %args ) {
		if( $_ eq "-stepname" ) {
			$x{-subsystem}=$args{$_};
		} else {
			$x{$_}=$args{$_};
		}
	}
	foreach (keys %mlp_batch_args ) {
		#print "BATCH ARGS $_ => $mlp_batch_args{$_}\n";
		$x{$_}=$mlp_batch_args{$_} unless defined $x{$_};
	}
	$x{-message}="Job=$x{-system} Step=$job_curstep Name=$job_curstepname Has Been Started" unless defined $x{-message};

	MlpHeartbeat(		-monitor_program=>$x{-monitor_program},
				-system=>$x{-system},
				-subsystem=>$x{-subsystem},
				-state=>"RUNNING",
				-debug=>$x{-debug},
				-batchjob=>$x{-batchjob},
				-message_text=>$x{-message});
}

sub MlpMonitorStart
{
	my(%args)=@_;
	$args{-monitorjob}=1;
	MlpBatchStart(%args);
}

# sync the config files with the server
sub MlpSyncConfigFiles
{
	my(%args)=@_;
	print "Calling MlpSyncConfigFiles to sync configuration files with the database\n";
	initialize() unless $i_am_initialized eq "TRUE";
	if($i_am_initialized eq "FAILED" ) {
		print "Cant Run : Connection Was Marked Failed!\n(this should not abort your program)\n"
			unless defined $quiet;
		return 0;
	};
	# extract the table
	use Repository;
	my($config_directory)=get_gem_root_dir()."/conf";

	my($query)="select servername, chvalue from ServerChanges where chkey=\"is_production\" order by tstamp\n";
	print $query if $args{-debug};
	my(%is_prod_changes);
	foreach ( dbi_query( 	-db=>$db,
		-query=>$query,
		-debug=>$args{-debug},
		-connection=>$connection,
		-die_on_error=>0 )
	) {
		my(@dat)=dbi_decode_row($_);
		$is_prod_changes{$dat[0]} = $dat[1];
		print "Setting $dat[1] to |".$dat[0]."| \n";
	}

	# ok lets read the configuration file
	foreach my $filetype ( get_passtype() ) {
		next if $filetype eq "documenter";
		next if $filetype eq "win32servers";

		print "Working on $filetype\n";
		my(%comm_method,%win32_comm_method, %server_type, %hostname, %type, %backupservername, %is_production);		
		my($query)="select servername, comm_method, win32_comm_method, server_type, hostname, type,
			backupservername, is_production
         	from ServerInfo i, ProductionServers p
         	where p.system =* i.servername
         	and filetype=\"".$filetype."\"\n";

		print $query if $args{-debug};
		my($count)=0;
		foreach ( dbi_query( 	-db=>$db,
					-query=>$query,
					-debug=>$args{-debug},
					-connection=>$connection,
					-die_on_error=>0 )
				) {

				my(@dat)=dbi_decode_row($_);
				#print "Found: ".join("\t",@dat)."\n";
				#print "$dat[0] ";

				$dat[1]=~s/\s//g;
				$dat[2]=~s/\s//g;
				$dat[3]=~s/\s//g;
				$dat[4]=~s/\s//g;
				$dat[5]=~s/\s//g;
				$dat[6]=~s/\s//g;
				$count++;

				$comm_method{$dat[0]} 		= $dat[1];
				$win32_comm_method{$dat[0]} 	= $dat[2];
				#print "win32 Comm Method={".$win32_comm_method{$dat[0]}."}\n";
				$server_type{$dat[0]} 		= $dat[3];
				#print "setting server type $dat[0] to $dat[3]\n";
				$hostname{$dat[0]} 		= $dat[4];
				$type{$dat[0]} 			= $dat[5];
				$backupservername{$dat[0]} 	= $dat[6];
				$is_production{$dat[0]} 	= $dat[7];
		}
		print "$count servers found of type $filetype\n";

		my(@hosts)=get_password(-type=>$filetype);
		my(%file_overrides);
		my($file_changes)=0;
		foreach my $host (@hosts) {
			my(%args)=get_password_info(-type=>$filetype,-name=>$host);
			my(%newargs);
			$newargs{comm_method}="";
			$newargs{win32_comm_method}="";
			$newargs{server_type}="";
			$newargs{hostname}="";
			$newargs{type}="";
			$newargs{backupservername}="";
			foreach (keys %args) { 
				#	print "$_ => $args{$_}\n"; 
					$newargs{lc($_)} = $args{$_}; 
				}
			$newargs{server_type}="DEVELOPMENT" unless $newargs{server_type};			
			$newargs{comm_method} = $newargs{unix_comm_method};

			my($query);
			if( $server_type{$host} ) {
				#print "Existing host $host!\n";
				my($changes_found)=0;
				# compare the results
				if( $comm_method{$host} ne $newargs{comm_method} ) {
					$changes_found++;
					print "comm_method: You have updated {".$comm_method{$host}."} to {".$newargs{comm_method}."}\n";
				}
				if( $win32_comm_method{$host} ne $newargs{win32_comm_method} ) {
					$changes_found++;
					print "win32_comm_method: You have updated {".$win32_comm_method{$host}."} to {".$newargs{win32_comm_method}."}\n";
				}

				#if($is_prod_changes{$host}
				#
				if( defined $is_prod_changes{$host} ) {
					print "DBG DBG is_prod_changes $host = $is_prod_changes{$host} \n";
					if( $is_prod_changes{$host} ) {
						$file_overrides{$host."|SERVER_TYPE"}="PRODUCTION";
						$file_changes++;
						$changes_found++ if $newargs{server_type} ne "PRODUCTION";
						$newargs{server_type} = "PRODUCTION"
					} else {
						$file_overrides{$host."|SERVER_TYPE"}="DEVELOPMENT";
						$file_changes++;
						$changes_found++ if $newargs{server_type} ne "DEVELOPMENT";
						$newargs{server_type} = "DEVELOPMENT";
					}
					print "DBG DBG $host was set via web page to value $is_prod_changes{$host}\n";
				} elsif( $server_type{$host} ne $newargs{server_type} ) {
					$changes_found++;
					print "server_type: Updating Database For $host ".$server_type{$host}." to ".$newargs{server_type}."\n"
					 	if $args{-debug};

					my($p)=0;
					$p=1 if $newargs{server_type} eq "PRODUCTION";
					$query="UPDATE ProductionServers set is_production=$p where system=\"".$host."\"\n";
					print $query;
					foreach ( dbi_query(-db=>$db,-query=>$query,
						-connection=>$connection, -die_on_error=>0 )) {
						die join( dbi_decode_row($_) );
	   				}
				} elsif( $is_production{$host} eq "1" ) {
					if( $newargs{server_type} ne "PRODUCTION" ) {
						print "Setting Host $host To Be Development\n"  if $args{-debug};
						$query="UPDATE ProductionServers set is_production=0	where system=\"".$host."\"\n";
						print $query;
						foreach ( dbi_query(-db=>$db,-query=>$query, -debug=>$args{-debug},
							-connection=>$connection, -die_on_error=>0 )) {
							die join( "~",dbi_decode_row($_) );
		   				}

		   			#$query="select * from ProductionServers where system=\"".$host."\"\n";
						#print  $query if $args{-debug};
						#foreach ( dbi_query(-db=>$db,-query=>$query,-connection=>$connection, -die_on_error=>0 )) {
						#	print "DBG DBG",join( "~",dbi_decode_row($_) )."\n";		   			
	   				#}
	   			}
	   		} elsif( $is_production{$host} eq "0" ) {
					if( $newargs{server_type} eq "PRODUCTION" ) {
						print "Setting Host $host in Database to be Production\n"  if $args{-debug};
						$query="UPDATE ProductionServers set is_production=1 where system=\"".$host."\"\n";
						print $query;
						foreach ( dbi_query(-db=>$db,-query=>$query, -debug=>$args{-debug},
							-connection=>$connection, -die_on_error=>0 )) {
							die join( "~",dbi_decode_row($_) );
		   				}
		   			#$query="select * from ProductionServers\n
						#	where system=\"".$host."\"\n";
						#print $query  if $args{-debug};
						#foreach ( dbi_query(-db=>$db,-query=>$query, -debug=>0,
						#	-connection=>$connection, -die_on_error=>0 )) {
						#	print "DBG DBG",join( "~",dbi_decode_row($_) )."\n";
		   			#	}
	   				}
				} elsif( $is_production{$host} eq "" ) {
					# oops not found - so set it
					my($p)=0;
					$p=1 if $server_type{$host} eq "PRODUCTION";				
						
					$query="insert ProductionServers values ('".$host."',$p)\n";
					print $query;
					foreach ( dbi_query(-db=>$db,-query=>$query, -debug=>$args{-debug},
						-connection=>$connection, -die_on_error=>0 )) {
						die join( "~",dbi_decode_row($_) );
	   				}
				}

				if( $hostname{$host} ne $newargs{hostname} ) {
					$changes_found++;
					print "hostname: You have updated ".$hostname{$host}." to ".$newargs{hostname}."\n";
				}
				if( $type{$host}  ne $newargs{type} ) {
					$changes_found++;
					print "host: You have updated ".$type{$host}." to ".$newargs{type}."\n";
				}
				if( $backupservername{$host}  ne $newargs{backupservername} ) {
					$changes_found++;
					print "backupservername: You have updated ".$backupservername{$host}." to ".$newargs{backupservername}."\n";
				}

				next unless $changes_found;
				print "Server Values In Config Files Have Been Changed\n";

				# UPDATE the server
				$query="UPDATE ServerInfo set
			comm_method 		= \"".$newargs{comm_method}."\",
			win32_comm_method 	= \"".$newargs{win32_comm_method}."\",
			server_type 		= \"".$newargs{server_type}."\",
			hostname 		= \"".$newargs{hostname}."\",
			type 			= \"".$newargs{type}."\",
			backupservername 	= \"".$newargs{backupservername}."\"
				where filetype = \"".$filetype."\"
				and servername=\"".$host."\"\n";

			} else {
				print "New host $host\n";
				$query="insert ServerInfo values (
			\"".	hostname()."\",
			\"".	$filetype."\",
			\"".	$config_directory."\",
			\"".	$host."\",
			\"".$newargs{comm_method}."\",
			\"".$newargs{win32_comm_method}."\",
			\"".$newargs{server_type}."\",
			\"".$newargs{hostname}."\",
			\"".$newargs{type}."\",
			\"".$newargs{backupservername}."\")\n";

			}
			print $query;
			#from ServerInfo where mon_hostname = \"".hostname()."\" and filetype = \"".$filetype."\"";

			foreach ( dbi_query(-db=>$db,-query=>$query, -debug=>$args{-debug},
				-connection=>$connection, -die_on_error=>0 )) {
						print "SERVER=$host\nQUERY=$query\n";
						die join( dbi_decode_row($_) );	   		
	   	}
	   	add_edit_item(-type=>$filetype, -values=>\%file_overrides, -op=>"update") if $file_changes;
   	}
   }
	
   $query="DELETE ServerChanges where chkey=\"is_production\"";
	print $query if $args{-debug};	
	foreach ( dbi_query( 	-db=>$db,
		-query=>$query,
		-debug=>$args{-debug},
		-connection=>$connection,
		-die_on_error=>1 )
	) {
			die "ERROR ".dbi_decode_row($_);
	}
	print "Sync of Config Files Completed\n";
  # exit(0);
}

sub MlpAgentStart
{
	my(%args)=@_;
	initialize(%args) unless $i_am_initialized eq "TRUE";
	if($i_am_initialized eq "FAILED" ) {
		print "Cant Save Heartbeat: Connection Was Marked Failed!\n(this should not abort your program)\n"
			unless defined $quiet;
		return 0;
	};
	%args= validate_params("MlpAgentStart",%args);
	if( ! defined $args{-message} ) {
		$args{-message}="Job $args{-system} Started" if defined $args{-system};
		$args{-message}.=" on host $args{-subsystem}" if defined $args{-subsystem};
		$args{-message}="Job Started" if ! defined $args{-message};
	}
	chomp $args{-message} if $args{-message};
	$args{-system}="" unless $args{-system};
	$args{-subsystem}=$this_hostname unless $args{-subsystem};
	print "Registering agent $args{-monitor_program}\n" if $args{-debug};

	$mlp_batch_args{-monitor_program} 	= $args{-monitor_program};
	$mlp_batch_args{-system} 		= $args{-system};
	$mlp_batch_args{-subsystem} 		= $args{-subsystem};
	$mlp_batch_args{-batchjob} 		= "AGENT";
	$mlp_batch_args{-debug} 		= $args{-debug};
	$job_started="MONITOR";

	my($query)='insert AgentHistory
	values ( getdate(), null, null, @@SPID, \''.$args{-monitor_program}."','".$args{-system}.
		"','".	$args{-subsystem}.
		"','RUNNING','".	$args{-message}."')";
	print $query,"\n" if $DBGDBG;
	foreach ( dbi_query(-db=>$db,-no_results=>1,
		-query=>$query, -debug=>$args{-debug}, -connection=>$connection, -die_on_error=>0 )) {
			die join( dbi_decode_row($_) );
	}
		
	MlpHeartbeat(
		-monitor_program=>$args{-monitor_program},
		-debug=>$args{-debug},
		-system=>$args{-system},
		-state=>"RUNNING",
		-subsystem=>$args{-subsystem},
		-batchjob=>"AGENT",
		-message_text=>$args{-message});
	print "Done Registring agent $args{-monitor_program}\n" if $args{-debug};
}

sub MlpAgentDone {
	my(%args)=@_;
	if( ! $args{-message} ) {
		$args{-message} ="Completed Job $mlp_batch_args{-monitor_program}";
		$args{-message}.=" ( $mlp_batch_args{-system} )"
			if defined $mlp_batch_args{-system} and $mlp_batch_args{-system} ne "";
		$args{-message}.=" on host $mlp_batch_args{-subsystem}"
			if defined $mlp_batch_args{-subsystem};
	}
	MlpBatchDone(%args);
}

sub MlpBatchStart
{
	my(%args)=@_;
	initialize(%args) unless $i_am_initialized eq "TRUE";
	if($i_am_initialized eq "FAILED" ) {
		print "Cant Save Heartbeat: Connection Was Marked Failed!\n(this should not abort your program)\n"
			unless defined $quiet;
		return 0;
	};

	$args{-system}=$args{-name}
		if defined $args{-name} and ! defined $args{-system};

	#print "Setting\n";
	if( defined $args{-monitorjob} ) {
		if( $args{-reset_status} ) {
			die "Error - can not pass -reset_status and not pass -batchjob\n";
		}
		$args{-batchjob}="AGENT";

		# promote system/subsystem and make host the subsystem
		if( ! defined $args{-monitor_program} ) {
			$args{-monitor_program} = $args{-system};
			$args{-system}= $args{-subsystem};
		} elsif( ! defined $args{-system} ) {
			$args{-system}= $args{-subsystem};
		}

		$args{-system}= $args{-monitor_program} unless $args{-system};
		$args{-subsystem}= $this_hostname;
		$job_started="MONITOR";
	} elsif( defined $args{-batchjob} ) {
		# ok... if its a batch job and you have no monitor_program, promote system
		# it may be duplicated but who cares
		if( ! defined $args{-monitor_program} ) {
			$args{-monitor_program}=$args{-system} || $args{-name};
		}
		$job_started="BATCH";
	} else {
		die "ERROR must pass -batchjob or -monitorjob to MlpBatchStart\n";
	}
	%args= validate_params("MlpBatchStart",%args);
	$mlp_batch_args{-monitor_program} 	= $args{-monitor_program};
	$mlp_batch_args{-system} 		= $args{-system};
	$mlp_batch_args{-subsystem} 		= $args{-subsystem};
	$mlp_batch_args{-monitorjob} 		= $args{-monitorjob};
	$mlp_batch_args{-batchjob} 		= $args{-batchjob};
	$mlp_batch_args{-debug} 		= $args{-debug};

	if( ! defined $args{-message} ) {
		$args{-message}="Job $args{-system}";
		$args{-message}.=" / $args{-subsystem}" if defined $args{-subsystem};
		$args{-message}.=" Started";
	}
	chomp $args{-message};

	my($query)='insert AgentHistory
	values ( getdate(), null, null, @@SPID, \''.$args{-monitor_program}."','".$args{-system}.
		"','".	$args{-subsystem}.
		"','RUNNING','".	$args{-message}."')";
	print $query,"\n" if $DBGDBG;
	foreach ( dbi_query(-db=>$db,-no_results=>1,
		-query=>$query, -debug=>$args{-debug}, -connection=>$connection, -die_on_error=>0 )) {
			die join( dbi_decode_row($_) );
	}

	MlpHeartbeat(
		-monitor_program=>$args{-monitor_program},
		-debug=>$args{-debug},
		-system=>$args{-system},
		-state=>"RUNNING",
		-subsystem=>$args{-subsystem},
		-batchjob=>$args{-batchjob},
		-reset_status=>$args{-reset_status},
		-message_text=>$args{-message});
}

sub MlpBatchDone
{
	my(%args)=@_;
	$args{-state}="OK" unless $args{-state};
	die "dbg(): MlpBatchDone - Job not started\n" if defined $args{-debug} and ! defined $job_started;
	return unless defined $job_started;
	$job_started=undef unless defined $args{-stepnum};

	$args{-system}=$args{-name}
		if defined $args{-name} and ! defined $args{-system};

	foreach (keys %mlp_batch_args) {
		print "Copying Val For Arg $_ From MlpBatchArgs ($mlp_batch_args{$_})\n"
			if defined $args{-debug};
		$args{$_}=$mlp_batch_args{$_} unless defined $args{$_};
	}

	%args= validate_params("MlpBatchDone",%args);

	$args{-subsystem} = $job_curstepname
		if defined $args{-stepnum} and $args{-stepnum} eq $job_curstep;

	if( ! defined $args{-message} ) {
		$args{-message}="Completed $args{-system}";
		$args{-message}.=" / $args{-subsystem}" if defined $args{-subsystem};
	}

	chomp $args{-message};

	my($query)='UPDATE AgentHistory
	set end_time=getdate(),
	duration_secs=datediff(ss,start_time,getdate()),
	 message_text="'.
	 $args{-message}.
	 '", state="'.
	 $args{-state}.
	 '"
	where spid=@@SPID
	and end_time is null
	and monitor_program= \''.$args{-monitor_program}."'
	and system='".$args{-system}."'
	and subsystem='".$args{-subsystem}."'";
	print $query,"\n" if $DBGDBG;
	foreach ( dbi_query(-db=>$db,-no_results=>1,
		-query=>$query, -debug=>$args{-debug}, -connection=>$connection, -die_on_error=>0 )) {
			die $query.join( dbi_decode_row($_) );
	}

	MlpHeartbeat(
		-monitor_program=>$args{-monitor_program},
		-system=>$args{-system},
		-subsystem=>$args{-subsystem},
		-state=> $args{-state},
		-debug=>$args{-debug},
		-batchjob=>$args{-batchjob},
		-message_text=>$args{-message});
}

sub MlpGetBackupState
{
	my(%args1)=@_;
	initialize(%args1) unless $i_am_initialized eq "TRUE";
	if($i_am_initialized eq "FAILED" ) {
		print "Cant Get Alarm Info: Connection Was Marked Failed\n";
		return 0;
	};
	my(%args)= validate_params("MlpGetBackupState",%args1);
			#datediff(ss,"Jan 1 1970",last_fulldump_time),
			#datediff(ss,"Jan 1 1970",last_trandump_time),
			#datediff(ss,"Jan 1 1970",last_truncation_time),
			#datediff(ss,"Jan 1 1970",last_tranload_time),

	my($query)= 'SELECT
			source_system,
			dbname,
			convert(varchar,last_fulldump_time,110)+\' \'+convert(varchar,last_fulldump_time,108),
			last_fulldump_file,
			last_fulldump_lsn,
			convert(varchar,last_trandump_time,110)+\' \'+convert(varchar,last_trandump_time,108),
			last_trandump_file,
			last_trandump_lsn,
			last_truncation_time,
			convert(varchar,last_fullload_time,110)+\' \'+convert(varchar,last_fullload_time,108),
			last_fullload_file,
			last_fullload_lsn,
			convert(varchar,last_tranload_time,110)+\' \'+convert(varchar,last_tranload_time,108),
			last_tranload_file,
			last_tranload_lsn,
			last_tranload_filetime,
			is_tran_truncated,
			is_db_usable,
			db_server_name  from BackupState';
	
	if( defined $args{-production} and $args{-production} ne "0" ) {
		$query .= " ,ProductionServers p";
	}
	
	$query.=" where 1=1 ";
	$query.=" and source_system = \'".$args{-system}."\'" if defined $args{-system};
	
	if( defined $args{-production} and $args{-production} ne "0" ) {
		$query .= " and BackupState.source_system = p.system and is_production=1";
	}
		
	$query.= " ".$args{-orderby} if defined $args{-orderby};
	print $query if defined $args{-debug};

	my(@rc);
	@rc = dbi_query(-db=>$db,-query=>$query, -debug=>$args{-debug}, -connection=>$connection, -die_on_error=>0 );
	#foreach ( dbi_query(-db=>$db,-query=>$query, -debug=>$args{-debug}, -connection=>$connection, -die_on_error=>0 )) {
	#	push @rc, dbi_decode_row($_);
	#}
	return @rc;
}

#sub MlpGetMssqlDumpLoadInfo
#{
#	my(%args1)=@_;
#	initialize(%args1) unless $i_am_initialized eq "TRUE";
#	my($query)="select event_time, system, severity, message_text
#	from Event
#	where monitor_program='PcEventlog'
#	and   subsystem='MSSQLSERVER'
#	and   message_text like '182%'
#	order by event_time";
#
#	return dbi_query(-db=>$db,-query=>$query, -debug=>$args1{-debug}, -connection=>$connection, -die_on_error=>0 );
#}
sub MlpSetBackupState
{
	my(%args1)=@_;
	initialize(%args1) unless $i_am_initialized eq "TRUE";
	if($i_am_initialized eq "FAILED" ) {
		print "Cant Get Alarm Info: Connection Was Marked Failed\n";
		return 0;
	};
	my(%args)= validate_params("MlpSetBackupState",%args1);
	if( defined $args{-debug} ) {
		print "MlpSetBackupState\n";
		foreach ( keys %args) { print "\t$_ => $args{$_}\n" if defined $args{$_}; }
	}

    my($query)="Exec Backup_proc ".
    	$connection->quote($args{-system}).",".
        $connection->quote($args{-dbname}).",".
    	$connection->quote($args{-last_fulldump_time}).",".
        $connection->quote($args{-last_fulldump_file}).",".
        $connection->quote($args{-last_fulldump_lsn}).",".

        $connection->quote($args{-last_trandump_time}).",".
        $connection->quote($args{-last_trandump_file}).",".
        $connection->quote($args{-last_trandump_lsn}).",".

        $connection->quote($args{-last_truncation_time}).",".

        $connection->quote($args{-last_fullload_time}).",".
        $connection->quote($args{-last_fullload_file}).",".
        $connection->quote($args{-last_fullload_lsn}).",".

        $connection->quote($args{-last_tranload_time}).",".
        $connection->quote($args{-last_tranload_file}).",".
        $connection->quote($args{-last_tranload_lsn}).",".
        $connection->quote($args{-last_tranload_filetime}).",".
        $connection->quote($args{-is_tran_truncated}).",".
	$connection->quote($args{-is_db_usable}).",".
	$connection->quote($args{-db_server_name}) ;

	print $query,"\n" if $args{-debug};
   	return dbi_query(-db=>$db,-query=>$query,-no_results=>1,-connection=>$connection, -die_on_error=>0);
}

sub MlpBatchRunning
{
	return unless defined $job_started;

	my(%args)=@_;

	$args{-system}=$args{-name}
		if defined $args{-name} and ! defined $args{-system};

	foreach (keys %mlp_batch_args) {
		#next if $_ eq "-monitorjob" or $_ eq "-debug";
		print "Copying Val For Arg $_ From MlpBatchArgs ($mlp_batch_args{$_})\n"
			if defined $args{-debug};
		$args{$_}=$mlp_batch_args{$_} unless defined $args{$_};
	}

	%args= validate_params("MlpBatchRunning",%args);

	$args{-subsystem} = $job_curstepname
		if defined $args{-stepnum} and $args{-stepnum} eq $job_curstep;

	if( ! defined $args{-message} ) {
		$args{-message}="Running $args{-system}";
		$args{-message}.=" / $args{-subsystem}" if defined $args{-subsystem};
	}
	chomp $args{-message};

	if( defined $args{-debug} ) {
		foreach ( keys %args ) {
			print "MlpBatchRunning - Arg($_) = $args{$_} \n";
		}
	}
	MlpHeartbeat(
					-monitor_program=>$args{-monitor_program},
					-system=>$args{-system},
					-subsystem=>$args{-subsystem},
					-debug=>$args{-debug},
					-state=>"RUNNING",
					-batchjob=>$args{-batchjob},
					-message_text=>"(Running) $args{-message}");
}

sub MlpBatchErr
{
	return unless defined $job_started;

	my(%args)=@_;
	$args{-state}="ERROR" unless $args{-state};

	$args{-system}=$args{-name}
		if defined $args{-name} and ! defined $args{-system};

	foreach (keys %mlp_batch_args) {
		#next if $_ eq "-monitorjob"
		#			or $_ eq "-debug"
		#			or $_ eq "-batchjob";
		#			# or $_ eq "-subsystem";
		$args{$_}=$mlp_batch_args{$_} unless defined $args{$_};
	}

	%args= validate_params("MlpBatchErr",%args);

	$args{-subsystem} = $job_curstepname
		if defined $args{-stepnum} and $args{-stepnum} eq $job_curstep;

	if( ! defined $args{-message} ) {
		$args{-message}="Error $args{-system}";
		$args{-message}.=" / $args{-subsystem}" if defined $args{-subsystem};
	}
	chomp $args{-message};
	my($m)=substr($args{-message},0,250);
	$m=~s/\"//g;
	
	my($query)='UPDATE AgentHistory
	set end_time=getdate(),
		duration_secs=datediff(ss,start_time,getdate()),
		 message_text="'.
	 $m.
	 '", state="'.
	 $args{-state}.
	 '"
	where spid=@@SPID
	and end_time is null
	and monitor_program= \''.$args{-monitor_program}."'
	and system='".$args{-system}."'
	and subsystem='".$args{-subsystem}."'";
	
	print $query,"\n" if $DBGDBG;
	foreach ( dbi_query(-db=>$db,-no_results=>1,
		-query=>$query, -debug=>$args{-debug}, -connection=>$connection, -die_on_error=>0 )) {
			die $query.join( dbi_decode_row($_) );
	}

	$quiet=$args{-quiet};
	MlpHeartbeat(	-monitor_program	=>	$args{-monitor_program},
			-system			=>	$args{-system},
			-subsystem		=>	$args{-subsystem},
			-state			=>	$args{-state},
			-message_text		=>	$args{-message});
}

1;

__END__

=head1 NAME

MlpAlarm.pm - Alarming And Monitoring

=head2 DESCRIPTION

This perl module provides a generic mechanism to manage alarms and monitoring. The tool is distributed as a perl module with several associated programs (a web based GUI, an alarm router, and some monitoring programs).   The library contains several simple functions to monitor your systems and some back end functions used by the reporting user interface.  Alarm data is stored in a database (Sybase or SQL Server).

=head2 SUMMARY

MlpAlarm provides a simple mechanism to manage alarming and reporting.  Functions use a by name interface with consistent parameters for simplicity and ease of use.  It has the following features

The system supports the following types of "data"

=over 4

=item * Heartbeat Data

Heartbeats are frequent up/down type messages regarding state.  Examples of programs that generate heartbeats include ping and a database disk space monitor which alarms based on space used.  Heartbeats are saved with a -severity argument which can be EMERGENCY, CRITICAL, ERROR, WARNING, or OK.  Because we expect heartbeats frequently, it is considered a problem if a heartbeat is not generated in a timely manner. Heartbeats are stored using the MlpHeartbeat() function. The system does not keep track of historical Heartbeat information.

=item * Batch Job Data

Batch jobs typically run infrequently but regularly.  They can thus be treated as Heartbeats with a low frequency.  The system identifies batch jobs by the -batchjob argment to MlpHeartbeat.  Because some batch jobs have a frequency as low as once per month, it is not considered a warning when a Heartbeat message is not generated in a timely manner.

Because the system does not care how long it is between batch job heartbeats, heartbeat messages for batch jobs may include three states in adition to the normal Heartbeat states.   These are "RUNNING", "NOT RUN", and "COMPLETED".  Batch jobs, when started, should set their state to RUNNING, and when finished should set their state to COMPLETED (which is the same as OK) or one of the error states.

There are several convenience functions starting with MlpBatch...() that can be used to store Batch Job data.  These routines are simple wrappers on MlpHeartbeat().

=item * Event Data

Events are incidents on our systems for which we care about the time they occurred.  Event logs for your servers are an example.  Because we care what time they occurred, The system keeps a history of all the events you have saved and a cleanup program must be run to purge this data.  Events are stored using the MlpEvent() function.

=item * Performance Data

Performance data can be stored in free form data for use  by graphics functions.  The use of performance data is not implemented yet. Performance data will be stored using the MlpPerformance() function

=back

Fundamentally, from the perspective of a user of this module, the system consists of 3 functions named MlpHeartbeat, MlpEvent, and MlpPerformance.  These functions can be considered Black Boxes to transmit, store, and route your messages appropriately.

=head2 THE GORY DETAILS

=head2 System/Subsystem Values

Everything monitored is keyed by monitoring_program, system and subsystem.  A Systems is a hardware or software component.  Subsystems refer to individual parts of those systems (disks, logs etc).   If you are monitoring batch jobs, the system refers to a logical group of batch jobs, while the subsystem refers to the step.

=head2 Containers

When you view the data, you view it by Containers, which are groups of systems.  The user interface supports a mechanism to create and manage your "Containers".  An example of containers are PRODUCTION, DATABASES, or UNIX.  This allows different users to see  only the subset of systems they are interested in.

=head2 State/Severity Values

Key to the system is the state and severity levels.  Message routing will be based on a combination of the key (monitoring_program/system/subsystem) and this value.

The following are legitimate heartbeat states

=over 4

=item * EMERGENCY = system down / critical failure

=item * CRITICAL = serious problem.

=item * ERROR = non fatal error possibly requiring administrator attention

=item * WARNING = non fatal warning.

=item * INFORMATION = a simple message.  Synonym for OK.

=item * DEBUG = messages only of interest to developers

=back


Additionally, batch jobs may also be

=over 4

=item * STARTED = the job has started

=item * COMPLETED = the job has completed normally

=item * NOT_RUN = the job has not started (default state)

=back


If a batch job aborts/fails, the status should not be COMPLETED (it should be one of the other states from above).  Note that you can have the batch job submit many RUNNING heartbeats, with different message texts, to identifying the exact position in the batch.

=head1 Monitoring API

=head2 MlpHeartbeat()

Heartbeat message.  A heartbeat message may require attention but the system does not keep heartbeat history.  Ping is an example of a heartbeat message.

Requried Arguments: -state, -monitor_program -system

 -state=>[STARTED, COMPLETED, RUNNING, EMERGENCY, CRITICAL, ERROR, WARNING, INFORMATION, DEBUG]

Optional Arguments: -debug -subsystem -message_text -document_url -batchjob

 -batchjob =>	this is a batch job - so value is not going to be refreshed frequently
 -event_time => valid sybase date time format - usually can be ignored

=head2 MlpPerformance()

All arguments to this call, which records system performance data, are optional. Obviously, at least one value should be passed into the routine to make any logical business sense.

Required Arguments: -monitor_program, -system

Standard Optional Arguments: -debug -subsystem

Optional Arguments: -debug -monitor_program -system -subsystem -value1 -value2 -value3 -value4 -value5 -value6 -value7

=head2 MlpEvent()

MlpEvent saves Events, which are monitioring messages where history may be of interest.  Application errorlogs are examples of events.

Required arguments;

 -message_text=>[text string]
 -severity=>[EMERGENCY,CRITICAL,ERROR,WARNING,INFORMATION,DEBUG]
 -system=>[system being monitored]
 -monitor_program=>[program name.  Default is basename($0)]
 -severity

Optional arguments:

 -subsystem=>[subsystem being monitored]
 -debug
 -event_id=> 0
 -message_value=> 0
 -message_text
 -document_url=> 0
 -event_time=>[timestamp of event in sybase format.  Default to now]
 -save_syslog= 0|1	- save event using syslogd if on unix
 -save_dbms = 0|1

=head2 Batch Job Convenience Functions

Batch jobs in the system are treated as a special case of Heartbeats.  You *could* write your manager using a heartbeat fore each batch, or you can use these convenience functions.

4 functions are provided as simple wrappers to the MlpHeartbeat function for use by batch jobs.  These functions are MlpBatchStart, MlpBatchRunning, MlpBatchDone, and MlpBatchErr.   Funcamentally, when your batch or monitoring job starts, you call MlpBatchStart and you finish with a call to MlpBatchDone or MlpBatchErr.  If your job runs for a while (or continuously), you can use MlpBatchRunning to indicate that the batch is still alive.

=over 4

=item * MlpBatchStart()

This function identifies that a program has started.  The program should be passed either -name or -system (they are synonyms) and the -batchjob flag.   If you are using multiple occurances of this program to check multiple systems, you should identify the system monitored with the -subsystem parameter. If you are running a batchjob, you should also pass a name for the batch job with -monitor_program.  Finally you can pass in an optional -message.

Additionally, if this is a -batchjob, you may specify -step to indicate which step this subsystem is numbered and -reset_status to clear times.

example
	MlpBatchStart(-monitor_program=>"NightlyCopy", -name="IMAGSYB1", -batchjob=>1);

=item * MlpBatchStep

This function takes a required -stepname and -stepnum and optional -message. The
step name and number are saved so the next call to MlpBatchDone/MlpBatchErr will know where you are at.  The -stepname is considered a -subsystem and a heartbeat is saved with a state of RUNNING

=item * MlpBatchRunning()

This function just saves a heartbeat in the system so you can see that your batch or monitoring job is working.

example
	MlpBatchRunning()
	MlpBatchRunning(-message=>"at step number 3")

=item * MlpBatchDone()

The batch has completed.  Required -monitor_program and -system.  Optional -name -debug -subsystem -message -stepnum.

=item * MlpBatchErr()

These two functions indicate the completion of your batch or monitoring job.  They also use the same arguments as above, although you probably also want to pass a -message if the job is in error.

example
	MlpBatchDone()
	MlpBatchErr(-message=>"Cant Connect To The Server")

=back

=head1 Monitoring Reporting

The reporting functions are pretty for developer internal use and are simply listed here for completeness:

=over 3

=item * MlpGetCategory()

Args:  -user => username, -debug

Get report category code

=item * MlpGetScreens()

Args:  -user=>username, -category=name, -debug

Get screens viewable by a user

=item * MlpGetReportCode()

Args:  -user=>username, -screen_name=name

Get Report type code

=item * MlpRunScreen()

Args: -screen_name=name, [-container=name], -system, -program, -debug

MlpRunScreen() returns an array of pointer to arrays.  You can
override your predefined containers or just use the normally set up ones.  You can pass -NL to define newline characters for debug messages

=item * MlpGetContainer()

Args:  -name=name, [-type=(s)ystem|(c)container], -user=>username, -debug=>[1|0], -screen_name=>name

Fetch a list of systems and containers

=item * MlpGetDbh()

Returns the DBprocess handle currently in use (shouldnt be needed)

=item * MlpGetProgram()

Gets a list of monitoring program information

=item * MlpGetSeverity()

Retrieves a list of severities

=item * MlpManageData()

Administrative interface to the system

=item * MlpGetOperator()

Returns operator information

=back

=head1 Monitoring Coding

=head2 Example 1.  Write Event Log

The following example writes an event message.  Note that the monitor program name will be determined from the current program name, and the system the event is for will be determined by hostname():

 #!C:\Perl\bin\perl.exe
 use lib 'G:/ADMIN_SCRIPTS/lib';
 use strict;
 use MlpAlarm;
 MlpEvent( -severity=>"INFORMATION",
           -message_text=>"Test Message",
           -system=>"test");


=head2 Example 2. A Simple Monitor

The following example monitors something...

 #!C:\Perl\bin\perl.exe
 use lib 'G:/ADMIN_SCRIPTS/lib';
 use MlpAlarm;
 MlpBatchStart( -system=>"TaQ",-subsystem=>'Nyse',-monitorjob => 1 );
 if( ! connect($SERVER) ) {
	MlpBatchErr( -message=>"Can Not Connect To DB $SERVER: $text" );
	MlpHeartbeat( 	-system	=> 'TaQ', -subsystem => 'Nyse',
				-state=>"ERROR", -monitor_program=>"TaqMonitor",
				-message_text=>"Can Not Connect to Taq/Nyse");
	die "ERROR - Can Not Connect to Taq/Nyse";
 }
 while( 1 ) {
	my($rc,$text)=monitor_taq($SERVER);
	if( $rc  eq  "ERROR" ) {
		MlpHeartbeat( -state=>"ERROR", -debug=>$opt_d, -system=>"TaQ", -subsystem=>"Nyse",
			-monitor_program=>"TaqMonitor", -message_text=>$text);
	} else {
		MlpHeartbeat( -state=>"OK", -debug=>$opt_d, -system=>"TaQ", -subsystem=>"Nyse",
			-monitor_program=>"TaqMonitor", -message_text=>"Server Ok: $text");
	}
	sleep(300);
   MlpBatchRunning(-message=>'still working')
 }
 MlpBatchDone(-message=>'completed normally');

=head2 Example 3. A Complex Multistep Batch

Let us assume that we have a batch job called Nightly that is composed of 6 named steps.  We would like to see the state of this batch in the appropriate order and would like to reset the state of the subbatches when we start.  This way we can see progress.  So... we should *always* see 6 substeps plus an overall step when we look a the Heartbeat table.  When the batch starts, everything gets marked "not run".  When steps get run their status gets updated.

The way we do this is to use our convenience functions. Technically, the master heartbeat differes from that of the steps as it has no -subsystem while the steps do.  But they are connected - they share the same other information.  The Master row state will be set to RUNNING at the start.  This row will have its timestamp updated when any MlpBatchStep row is run.  It will also have its message updated to be the last message saved by any of the -step functons.  Note that this means that failed substeps where the program continues will result in a progression like (state=RUNNING msg="Running Step 1 : Init") to (state=RUNNING msg="Step 1 Failed") to (state=RUNNING msg="Running Step 2 : AlphaSetup") in short order.  You must track final state and write that with MlpBatchDone/MlpBatchErr as normal.

 # start up our program, clear the states of all the jobs
 my($finalstate,$finalmsg)=("OK","Job Complted Successfully";

 # start our batch named Nightly and reset all associated rows
 MlpBatchStart( -system=>"Nightly",-batchjob => 1, -reset_status=>1 );
 $rc=connect();
 if( $rc eq "FAIL" ) { 		# Fail the whole batch...
 	MlpBatchErr( -message=>"Can not Connect");
 	die "Done - Error Can Not Connect";
 }

 # Step 1 - changed to RUNNING and then to COMPLETED or ERROR
 MlpBatchStep( -stepname=>"Init",-stepnum=>1);
 $rc=init();
 if( $rc eq "FAIL" ) {
 	MlpBatchErr( -message=>"Step 1 Failed $!", -stepnum=>1 );
 	($finalstate,$finalmsg)=("ERROR","Step 1 Failed $!");
 } else {
 	MlpBatchDone( -message=>"Step 1 Completed Normally", -stepnum=>1 );
 }

 # Step 2
 if( $finalstate eq "OK" ) {
   MlpBatchStep( -stepname=>"AlphaSetup", -stepnum=>2 );
   $rc=AlphaSetup();
   if( $rc eq "FAIL" ) {
 	  MlpBatchErr( -message=>"Step 2 Failed $!", -stepnum=>2 );
 	  ($finalstate,$finalmsg)=("ERROR","Step 2 Failed $!");
   } else {
 	  MlpBatchDone( -message=>"Step 2 Completed Normally", -stepnum=>2 );
   }
 }

 # Step 3 - A long Running step
 # changed to RUNNING and then the timestamp is updated every 100 second
 # loop and finally to COMPLETED or ERROR
 if( $finalstate eq "OK" ) {
   MlpBatchStep( -stepname=>"AlphaRun", -stepnum=>3 );
   while($i<100) {
 	  sleep(100);
 	  $rc = itterate();
 	  last if $rc eq 'FAIL";
 	  MlpBatchRunning(-message=>"step 3: completed $i/100", -stepnum=>3);
 	  $i++;
   }
 	if( $rc eq "FAIL" ) {
 		MlpBatchErr( -message=>"Step 3 Failed at part $i: $!", -stepnum=>3 );
 		($finalstate,$finalmsg)=("ERROR","Step 2 Failed at part $i $!");
 	} else {
 		MlpBatchDone( -message=>"Step 3 Completed Normally", -stepnum=>3 );
 	}
 }

 if( $finalstate eq "ERROR" ) {
 	MlpBatchErr( -message=>$finalmsg );
 } else {
 	MlpBatchDone( -message=>$finalmsg );
 }

=head1 Monitoring Notes

The following are paths and perl versions that are known to work with this lib.

=over 4

=item * NT

With G: mapped to /samba/sybmon

	#!//adcluster300/tibco/perl/bin/perl.exe
	use lib 'G:/ADMIN_SCRIPTS/lib

=item * Solaris

	#!/usr/local/bin/perl-5.6.1
	use lib '/apps/sybmon/ADMIN_SCRIPTS/lib';

=item * Linux

	#!/usr/local/bin/perl-5.8.2
	use lib "/apps/sybmon/ADMIN_SCRIPTS/lib";

=back

=head2 MONITORJOB / BATCHJOB

In addition to Heartbeats and Events, the system will handle batch jobs and agents.  Agents are the
programs that collect Heartbeats and Events.  Agents are stored with by setting -monitor_program=>1
in the functions.  This saves heartbeat data with batchjob=AGENT.  Batchjobs are considered all
data with batchjob!=AGENT but not null. The functions MlpBatchStart, MlpBatchDone, MlpBatchErr,
and MlpBatchRunning and MlpBatchStep.

=cut



