# Copyright (c) 1999 by Edward Barlow. All rights reserved.
# This program is free software; you can redistribute it and/or modify
# it under the same terms as Perl itself.

package UpdFunc;

use Exporter;
use File::Find;
use File::Basename;

$VERSION= 1.00;
@ISA      = ('Exporter');
@EXPORT = ( 'dirupdate','dircopy','dircmp' );

use strict;

my(%CONFIG_ARGS);
my(%FILE_COUNT,%FILE_DIR,%FILE_DAYS, %FILE_SIZE,%FILE_DIFFS);
my(%NEWEST_DAYS,%NEWEST_SIZE,%NEWEST_DIR);
my(@dirlist);
my(%ignore_dirs);

sub dircopy {
        $|=1;
        my(%ARGS)=@_;
        $ARGS{'COPY'} = 1;
        $ARGS{'UPDATE_MISSING'}=1;
        $ARGS{"EQUIVALENTS_ONLY"}=1;
        print "ANALYZING...\n" if defined $ARGS{"DEBUG"};
        diranalyze(%ARGS);
}

sub printobj {
        my($obj)=@_;
        my($str)="# ==================================\n";
        $str .= "# Object $obj\n";
        $str .= "# ==================================\n";
        $str .= "#   Newest Directory =".$NEWEST_DIR{$obj}."\n";
        $str .= "#   Newest Days      =".$NEWEST_DAYS{$obj}."\n";
        $str .= "#   Newest Size      =".$NEWEST_SIZE{$obj}."\n";

        my(@count)=split( /\s+/, $FILE_COUNT{$obj} );
        my(@sizes)=split( /\s+/, $FILE_SIZE{$obj} );
        my(@days) =split( /\s+/, $FILE_DAYS{$obj} );
        my(@dirs )=split( /\s+/, $FILE_DIR{$obj} );
        my(@diffs)=split( /\s+/, $FILE_DIFFS{$obj} );

        my($cnt)=$#dirs;
        while($cnt-- >= 0) {
                        $str.= "#   === DIR:".$dirs[$cnt]."\n";
                        $str.= "#   SIZE $sizes[$cnt]  DAYS $days[$cnt] CNT $count[$cnt] DIF $diffs[$cnt]\n";
        }
        return $str;
}

sub prettyprint {
        print "--------------------------------------\n";
        foreach my $nf (sort keys %NEWEST_DAYS) {
                printf("%24s %3s %6.6s %s\n",$nf,$FILE_DIFFS{$nf},
                                $NEWEST_SIZE{$nf},
                                $NEWEST_DIR{$nf}
                        );
                my(@sizes)=split( /\s+/, $FILE_SIZE{$nf} );
                my($cnt) = -1;
                foreach (split(/\s+/,$FILE_DIR{$nf})){
                        $cnt++;
                        next if $_ eq $NEWEST_DIR{$nf};
                        printf("                             %6.6s %s\n",$sizes[$cnt],$_);
                }
        }
}

sub dircmp
{
        $|=1;
        my(%ARGS)=@_;
        $ARGS{'DIRCMP'} = 1;
        $ARGS{'UPDATE_MISSING'}=1;
        $ARGS{"EQUIVALENTS_ONLY"}=1;
        diranalyze(%ARGS);
}

sub dirupdate
{
        $|=1;
        my(%ARGS)=@_;
        diranalyze(%ARGS);
}

# ===================================
# COMPARISION LOGIC
# ALL YOUR LOGIC SHOULD BE DONE HERE
# ===================================
sub print_results
{

        # ===================================
        # HANDLE MISSING DIRECTORIES IF NECESSARY
        # ===================================
        if( defined $CONFIG_ARGS{'UPDATE_MISSING'} ) {
                foreach my $key ( keys %FILE_COUNT ) {

                        # compare FILE_DIR to real list of directories
                        my(@file_fnd_dirs) = split( /\s+/, $FILE_DIR{$key} );
                        next if $#file_fnd_dirs == $#dirlist;

                        foreach my $d ( @dirlist ) {
                                my($found)=0;
                                foreach ( @file_fnd_dirs ) { $found=1 if $d eq $_; }
                                next if $found;

                                print "M";
                                if( defined $CONFIG_ARGS{'COPY'} ) {
                                        # only copy if source is the newest
                                        if( $NEWEST_DIR{$key} eq $dirlist[0] ) {
                                                print OUTFILE "cp ".$NEWEST_DIR{$key}."/".$key." ".$d."/$key\n";
                                        } else {
                                                print OUTFILE "rm ".$NEWEST_DIR{$key}."/".$key."\n";
                                        }
                                } elsif( defined $CONFIG_ARGS{'DIRCMP'} ) {
                                        next unless $d eq $NEWEST_DIR{$key};
                                        print OUTFILE "Only in ".$NEWEST_DIR{$key}.": ".$key."\n";
                                } else {
                                        print OUTFILE "# UPDATE MISSING FILE\n";
                                        print OUTFILE "cp -i ".$NEWEST_DIR{$key}."/".$key." ".$d."\n";
                                }
                        }
                }
        }

        print "\n";
        foreach my $diff_files ( keys %FILE_DIFFS ) {
                # Search through files marked different and copy most recent!

                # Is there a different file?
                next if $FILE_DIFFS{$diff_files} == 0;

                #
                # now print out the copy command
                #
                my(@sizes)=split( /\s+/, $FILE_SIZE{$diff_files} );
                my(@days) =split( /\s+/, $FILE_DAYS{$diff_files} );
                my(@dirs )=split( /\s+/, $FILE_DIR{$diff_files} );

                my($cnt)=0;
                foreach (@dirs) {
                        #next if $NEWEST_DIR{$diff_files} eq $_;
                        if( defined $CONFIG_ARGS{'COPY'} ) {
                                if( $NEWEST_DIR{$diff_files} eq $dirlist[0] ) {
                                        print "X";
                                        print OUTFILE "cp ".$NEWEST_DIR{$diff_files}."/".$diff_files." ".$_."/$diff_files\n" if $sizes[$cnt] != $NEWEST_SIZE{$diff_files};
                                } else {
                                        print OUTFILE "# THE FILE $diff_files is confused\n";
                                        print OUTFILE "# THE NEWEST DIRECTORY IS $NEWEST_DIR{$diff_files}\n";
                                        print OUTFILE "# THE WORKING DIRECTORY IS $_\n";
                                        print OUTFILE "# THE OLD SIZE=$sizes[$cnt] and the NEW SIZE=$NEWEST_SIZE{$diff_files}\n";

                                        print OUTFILE "#".__LINE__." cp ".$NEWEST_DIR{$diff_files}."/".$diff_files." ".$_."/$diff_files\n" if $sizes[$cnt] != $NEWEST_SIZE{$diff_files};
                                }
                        } elsif( defined $CONFIG_ARGS{'DIRCMP'} ) {
                                print "X";
                                print OUTFILE "# Sizes => ",$sizes[$cnt],$NEWEST_SIZE{$diff_files},"\n" if defined $CONFIG_ARGS{'DEBUG'};
                                print OUTFILE "Files ".$NEWEST_DIR{$diff_files}."/".$diff_files." and ".$_."/$diff_files differ\n" if $sizes[$cnt] != $NEWEST_SIZE{$diff_files};
                        } else {
                                if($sizes[$cnt] != $NEWEST_SIZE{$diff_files} ) {
                                        print "X";
                                        print OUTFILE printobj($diff_files) if defined $CONFIG_ARGS{'DEBUG'};
                                        print OUTFILE "cp ".$NEWEST_DIR{$diff_files}."/".$diff_files." ".$_."/$diff_files\n";
                                }
                        }
                        $cnt++;
                }
        }

        close OUTFILE;
        if( ! defined $CONFIG_ARGS{"HIDE OUTPUT FILE"} ) {
                print "\n#=====================\n";
                print "# OUTPUT in ".$CONFIG_ARGS{"OUTPUT_FILE"};
                print " (appending)" if defined $CONFIG_ARGS{"APPEND"};
                print "\n#=====================\n";
        }
}

# ==============================
# READ DIRECTORY INFORMATION INTO:
#               FILE_COUNT      is number of times a file is found
#               FILE_DIR                space separated list of directories for each file
#               FILE_DAYS       space separated list of modification times
#               FILE_SIZE       space separated list of sizes of files
#               FILE_DIFFS      is number of different files found
#               NEWEST_DAYS     newest file info
#               NEWEST_SIZE     newest file info
#               NEWEST_DIR      newest file info
#               DIRLIST         list of directories (from ARG{DIRLIST})
# ==============================
sub diranalyze
{
        %CONFIG_ARGS=@_;

        my($indir)=$CONFIG_ARGS{"DIRLIST"};
        die "MUST PASS ARRAY REFERENCE FOR DIRLIST\n" unless ref($indir) eq "ARRAY";
        @dirlist=@$indir;

        my($ignore)=$CONFIG_ARGS{"IGNORE_DIRS"};
        die "MUST PASS ARRAY REFERENCE FOR IGNORE_DIRS\n"
                if defined $ignore and ref($ignore) ne "ARRAY";
        my(@extlist)=@$ignore if defined $ignore;
        undef %ignore_dirs;
        foreach (@extlist) {
                $ignore_dirs{$_}=1;
        }

        # ===================================
        # HANDLE EQUALVELENTS_ONLY TAG
        #  - to do this recursively with ignore_subdirs on
        # ===================================
        if( defined $CONFIG_ARGS{"EQUIVALENTS_ONLY"} ) {

                print "RECURSIVE COMPARE STARTED\n";

                $CONFIG_ARGS{"HIDE OUTPUT FILE"}=1;
                $CONFIG_ARGS{'IGNORE_SUBDIRS'}=1;
                delete $CONFIG_ARGS{"EQUIVALENTS_ONLY"};

                die "Can only pass 2 directories if doing equivalent compare\n"
                        if $#dirlist>1;

                # ===================================
                # compare highest level for starters
                # ===================================
                print "=> Top Level Directories Being Compared\n";
                foreach (@dirlist) {
                        print "=> $_ (Top Level)\n";
                }

                dirupdate(%CONFIG_ARGS);
                $CONFIG_ARGS{"APPEND"}=1;                                               # now we start appending

                # ===================================
                # NOW THE RECURSIVE PART
                # ===================================
                my(%subdirs);
                foreach (subdirlist($dirlist[0],"",1)) { $subdirs{$_}=1; }
                foreach (subdirlist($dirlist[1],"",1)) { $subdirs{$_}=1; }

                SUBDIR: foreach my $sdir (keys %subdirs) {

                        if( -d $dirlist[1].$sdir and -d $dirlist[0].$sdir) {

                                # =========================
                                # FOUND BOTH DIRECTORIES! (analyze them)
                                # =========================

                                print " => $sdir (Found Pair)\n";
                                my(@cmpdir)=($dirlist[0].$sdir,$dirlist[1].$sdir);
                                $CONFIG_ARGS{"DIRLIST"} = \@cmpdir;
                                my(@svdirlist)=@dirlist;
                                dirupdate(%CONFIG_ARGS);
                                @dirlist=@svdirlist;

                        } else {

                                print " => $sdir (No Pair)\n";

                                # =========================
                                # FOUND ONLY 1 DIRECTORY!
                                # =========================

                                my($srcdir,$tgtdir);

                                if( -d $dirlist[0].$sdir ) {
                                        $tgtdir     = $dirlist[1].$sdir;
                                        $srcdir                 = $dirlist[0].$sdir;
                                } else {
                                        $tgtdir     = $dirlist[0].$sdir;
                                        $srcdir                 = $dirlist[1].$sdir;
                                }

                                open(OUTFILE,">>".$CONFIG_ARGS{"OUTPUT_FILE"})
                                        or die "Cant open ".$CONFIG_ARGS{"OUTPUT_FILE"}."\n";

                                print "D";

                                if( defined $CONFIG_ARGS{'COPY'} ) {
                                        if( $srcdir =~ /$dirlist[0]/ ) {
                                                print OUTFILE "cp -R $srcdir $tgtdir\n";
                                        } else {
                                                print OUTFILE "rm -R $srcdir\n";
                                        }
                                } elsif( defined $CONFIG_ARGS{'DIRCMP'} ) {
                                        print OUTFILE "Only In ",dirname($srcdir),": ",basename($srcdir),"\n";
                                } else {
                                        print OUTFILE "cp -R $srcdir $tgtdir\n";
                                }

                                close OUTFILE;
                        }
                }

                #print_results(%ARGS);

                print "\n#=====================\n";
                print "# OUTPUT in ".$CONFIG_ARGS{"OUTPUT_FILE"};
                print "\n#=====================\n";

                return;
        }

        %NEWEST_DAYS=();
        %NEWEST_SIZE=();
        %NEWEST_DIR=();
        %FILE_COUNT=();
        %FILE_DIR=();
        %FILE_DAYS=();
        %FILE_SIZE=();
        %FILE_DIFFS=();

        die "Must Pass OUTPUT_FILE\n" unless defined $CONFIG_ARGS{"OUTPUT_FILE"};
        die "Must Pass DIRLIST\n"     unless defined $CONFIG_ARGS{"DIRLIST"};

        my($indir)=$CONFIG_ARGS{"DIRLIST"};
        die "MUST PASS ARRAY REFERENCE FOR INDIR\n" unless ref($indir) eq "ARRAY";
        my(@dirlist)=@$indir;

        if( defined $CONFIG_ARGS{"APPEND"} or ! -e $CONFIG_ARGS{"OUTPUT_FILE"} ) {
                open(OUTFILE,">>".$CONFIG_ARGS{"OUTPUT_FILE"})
                        or die "Cant open ".$CONFIG_ARGS{"OUTPUT_FILE"}."\n";
        } else {
                print "OPENING ".$CONFIG_ARGS{"OUTPUT_FILE"} ." FOR WRITING\n";
                open(OUTFILE,">".$CONFIG_ARGS{"OUTPUT_FILE"})
                        or die "Cant open ".$CONFIG_ARGS{"OUTPUT_FILE"}."\n";
        }

        # ===================================
        # FETCH FILE INFO FOR DIRECTORIES
        # what we do here is find all files in their trees and populate their hashes
        # ===================================
        foreach my $curdir ( @dirlist ) {
                die "Cant Find Directory $curdir\n" unless -d "$curdir";
                find ( \&process_file, $curdir );
        }

        print_results(%CONFIG_ARGS);
        #prettyprint() if $CONFIG_ARGS{"DEBUG"};
}

sub process_file {
        if( -d ) {
                $File::Find::prune=1 if defined $CONFIG_ARGS{"IGNORE_SUBDIRS"} and $_ ne ".";
                $File::Find::prune=1 if defined $ignore_dirs{$_};
                return;
        }

        print ".";

        my($ignore)=$CONFIG_ARGS{"IGNORE_EXTENSIONS"};
        die "MUST PASS ARRAY REFERENCE FOR IGNORE_EXTENSIONS\n"
                if defined $ignore and ref($ignore) ne "ARRAY";
        my(@extlist)=@$ignore
                if defined $ignore;
        foreach my $ext (@extlist) {
                $ext="\.".$ext."\$";
                print "Ignoring $_ "
                        if defined $CONFIG_ARGS{"DEBUG"} and /$ext/;
                return if /$ext/;
        }

        ($ignore)=$CONFIG_ARGS{"IGNORE_FILES"};
        die "MUST PASS ARRAY REFERENCE FOR IGNORE_FILES\n"
                if defined $ignore and ref($ignore) ne "ARRAY";
        (@extlist)=@$ignore
                if defined $ignore;
        foreach my $file (@extlist) {
                $file="\^".$file."\$";
                print "Ignoring $_ " if $_ =~ /$file/i
                        and defined $CONFIG_ARGS{"DEBUG"};
                return if $_ =~ /$file/i;
        }

        # if file name has allready been found
        if( defined $FILE_COUNT{$_} ) {
                $FILE_COUNT{$_}++;
                $FILE_SIZE{$_}.= " ".-s;
                $FILE_DAYS{$_}.= " ".-M;
                $FILE_DIR{$_} .= " ".$File::Find::dir;

                # THIS IS NEWEST FILE
                if( -M $_ < $NEWEST_DAYS{$_} ) {
                        $FILE_DIFFS{$_}++ if $NEWEST_SIZE{$_} != -s;
                        $NEWEST_SIZE{$_}= -s;
                        $NEWEST_DAYS{$_}= -M;
                        $NEWEST_DIR{$_} = $File::Find::dir;
                } else {
                        $FILE_DIFFS{$_}++ if $NEWEST_SIZE{$_} != -s;
                }

        } else {
                # new file
                $FILE_COUNT{$_} = 0 ;
                $FILE_SIZE{$_}  = -s;
                $FILE_DAYS{$_}  = -M;
                $FILE_DIR{$_}   = $File::Find::dir;
                $FILE_DIFFS{$_} = 0;
                $NEWEST_SIZE{$_}= -s;
                $NEWEST_DAYS{$_}= -M;
                $NEWEST_DIR{$_} = $File::Find::dir;
        }
}

# find dir & subdirs
sub subdirlist
{
        my($dir,$prefix,$recurse) = @_;

        opendir(DIR,$dir) || die("Can't open directory $dir for reading\n");
        my(@dirlist) = grep((!/^\./ and -d $dir."/".$_ and ! defined $ignore_dirs{$_}),readdir(DIR));
        closedir(DIR);

        my(@out)=();
        return @out if $#dirlist<0;
        foreach (@dirlist) {
                push @out,$prefix."/".$_;
                push @out,subdirlist($dir."/".$_,$prefix."/".$_,1)
                        if $recurse==1;
        }
        return @out;
}

1;


__END__

=head1 NAME

UpdFunc.pm - Functions to update directory trees

=head2 DESCRIPTION

FUNCTIONS TO UPDATE AND MAINTAIN DIRECTORIES

=head2 SUMMARY

This library consists of three functions.  The dirupdate() function will
update common files in directories to the most recent version.  The dircmp()
routine will compare 2 directories in the same manner as does dircmp(1).  The
dircopy() makes a target directory EXACTLY like a source directory.


=head2 FUNCTIONS

=over 4

=item * dirupdate()

Update files between directories to the most recent file.  An
Output file of copy commands is created.  This is useful to maintain
releases when development is being handled in more than one subtree.

=over 4

=item * UPDATE_MISSING

=item * OUTPUT FILE

=item * DEBUG

=item * DIRLIST

(array reference)

=item * IGNORE_SUBDIRS

=item * IGNORE_EXTENSIONS

(array references)

=item * HIDE OUTPUT FILE

=item * EQUIVALENTS_ONLY

Update equivalent directories only.  Ie. if your dir 1 has subdirs a and b
and dir 2 has subdirs b and c then files in dir1 is compared to dir2 and
files in dir1/b to dir2/b and that is it.

=item * APPEND

(append to file)

=item * IGNORE_FILES

(array references)

=back

=item * dircmp()

Compares directories.  Works with the same parameters as the above routine.

=item * direxact()

Copy Files between different directories from a source directory.  An
Output file of copy commands is created.

SRC_DIRECTORY - only set on call
DIR
FNAME
EXT
DEST_DIRECTORY - destination directories
DEBUG

=back

=cut


