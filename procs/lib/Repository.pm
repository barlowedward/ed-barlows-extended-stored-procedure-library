# Copyright (c) 1999-2006 by Edward Barlow. All rights reserved.
# This program is free software; you can redistribute it and/or modify
# it under the same terms as Perl itself.

package Repository;

use Exporter;
use Carp;

$VERSION= 1.0;
@ISA      = ('Exporter');
@EXPORT = ( 'get_password','get_password_info','read_threshfile','set_installation_state','get_installation_state',
	'get_passtype','get_conf_dir','get_gem_root_dir','get_root_dir','get_passfile_info', 'add_edit_item' );

##################################################

use strict;

my( %filemap ) = (
	sybase 		=> "sybase_passwords.dat",
	unix		=> "unix_passwords.dat",
	sqlsvr 	   	=> "sqlsvr_password.dat",
	oracle 	   	=> "oracle_password.dat",
	win32servers 	=> "pc_password.dat",
	documenter 	=> "console.dat",
);

my(%login);			# key type.name
my(%password);		# key type.name
my(%conntype); 	# key type.name
my(%server_data);	# key type.name.key
my(%file_read);	# key filename
my(%server_type); # key servername

sub get_installation_state
{	
	my($dir)=get_conf_dir();	
	return "UNINSTALLED" unless -r $dir."/InstallState";
	open(IS,$dir."/InstallState") or die "Cant read $dir/InstallState";
	my(%vals);
	while(<IS>) {
		chomp;
		my($k,$v)=split(/\s+/);
		$vals{$k} = $v;
	}	
	close(IS);		
	return wantarray ? ($vals{STATE}, %vals) : $vals{STATE};
}

sub set_installation_state
{
	my($state)=@_;	
	# REGISTERED INSTALLED
	my($cs, %vals) = get_installation_state();
	$vals{$state} = time;
	
	$cs = $state if ( $cs eq "UNINSTALLED" or $cs eq "REGISTERED" ) or $state eq "INSTALLED";
	my($dir)=get_conf_dir();
	
	open(IS2, "> ".$dir."/InstallState") or die "Cant write $dir/InstallState";
	print IS2 "STATE\t",$cs,"\n";
	foreach ( keys %vals ) {
		print IS2 "$_ $vals{$_}\n" unless /^STATE/;
	}	
	close(IS2);
}

sub get_root_dir
{
	my($dir)=get_gem_root_dir();
	return "$dir/ADMIN_SCRIPTS";
}

my($root_dir);
sub get_gem_root_dir {
	my($finding_cfg_files)=@_;
	return $root_dir if defined $root_dir;

	my(%WHY_NOT);
	foreach my $d (@INC) {
		$WHY_NOT{$d}="Not a lib"  unless $d =~ /lib$/i;
		#print "Not a lib $d\n" unless $d =~ /lib$/i;
		next unless $d =~ /lib$/i;
		$WHY_NOT{$d}="Not a directory"  unless -d $d;
		next unless -d $d;

		# ok its a lib directory ... lets see if there are conf and admin
		# directories as its peers

		my($rd)=$d;
		$rd =~ s/.lib$//;
		if( -d "$rd/conf" and -d "$rd/debugging_tools" and -d "$rd/bin" ) {
			die "WOAH... gem root dir is '.' - seek help ($d).  YOur include path should not have relative paths in ita.\n" if $rd =~ /^\./;
			$root_dir=$rd;
			return $rd;
		} elsif( ! -d "$rd/debugging_tools" ) {
			$WHY_NOT{$d}="No SubDirectory $rd/debugging_tools";
		} elsif( ! -d "$rd/bin" ) {
			$WHY_NOT{$d}="No SubDirectory $rd/bin";
		} elsif( ! -d "$rd/conf" ) {
			$WHY_NOT{$d}="No SubDirectory $rd/conf";
		}
	}

	if( $finding_cfg_files) {	# exception case - not using gem looking for cfg files
		#use Cwd;
		#print "."." cwd=".getcwd()."\n" if -r "./configure.cfg"
		#	or -r "./sybase_passwords.dat"  or -r "./sqlsvr_password.dat";
		#print ".."." cwd=".getcwd()."\n" if -r "../configure.cfg"
		#	or -r "../sybase_passwords.dat"  or -r "../sqlsvr_password.dat";
		#print "../conf"." cwd=".getcwd()."\n" if -r "../conf/configure.cfg"
		#	or -r "../conf/sybase_passwords.dat"
		#	or -r "../conf/sqlsvr_password.dat";

		if( -r "./configure.cfg" or -r "./sybase_passwords.dat"  or -r "./sqlsvr_password.dat" ) {
			$root_dir=".";			
			return "." ;
		}
		if( -r "../configure.cfg" or -r "../sybase_passwords.dat"  or -r "../sqlsvr_password.dat" ) {
			$root_dir="..";			
			return ".." ;
		}
		if( -r "../conf/configure.cfg" or -r "../conf/sybase_passwords.dat" or -r "../conf/sqlsvr_password.dat" ) {
			$root_dir="../conf";			
			return "../conf" ;
		}
	}

	# figure out why not above

#		next unless /ADMIN_SCRIPTS\/lib$/i;
#		my($dir)=$_;
#		$dir=~s/\/lib$//i;
#		$dir=~s/\/ADMIN_SCRIPTS$//i;
#		next # die "Gem Root Directory $dir - Does Not Exist\n"
#			unless -d $dir;
#		return $dir;

	# CAN NOT FIND APPROPRIATE THING IN THE INCLUDE PATH...

	warn "get_gem_root_dir() failed\n";
	warn "Can Not Locate Gem Root Directirectory based on your \@INC Perl Path.\n";
	warn "GEM searches your include path for a directory with conf, debugging_tools, and bin subdirectories.\n";
	warn "Your PERL Include path is \n";
	foreach (@INC) { printf STDERR "%-30s => %s\n",$_,$WHY_NOT{$_}; }
	return undef;
}

sub get_conf_dir {
	my($dir)=get_gem_root_dir(1);
	return $dir if $dir eq "." or $dir eq ".." or $dir eq "../conf";
	return "$dir/conf";
}

sub get_passtype
{
	return keys %filemap;
}

sub get_password
{
	my(%args)=@_;
	$args{-type} = $server_type{$args{-name}}
		if defined $args{-name} and ! defined $args{-type};
	die "Must Pass -type to get_password"
			unless defined $args{-type};
	unless( defined $filemap{$args{-type}} ) { $args{-type}=~s/\s//g; }
	die "Invalid type ".$args{-type}."\nValid Types Are: ".join(keys(%filemap))
			unless defined $filemap{$args{-type}};
	print "get_password( type=>$args{-type} name=>$args{-name} )\n" if defined $args{-debug};
	read_passfile( -type=>$args{-type}, -debug=>$args{-debug} )
			unless defined $file_read{$args{-type}};

	if( defined $args{-name} ) {
		foreach (sort keys %login) { print "debug:",$_," ",$login{$_}," ",$password{$_},"\n" if $args{-debug} and /$args{-name}/; }
		#print "returning login/pass/type","\n" if defined $args{-debug};
		$conntype{$args{-type}.".".$args{-name}} = ucfirst($args{-type})
			if defined $conntype{$args{-type}.".".$args{-name}}
			and $conntype{$args{-type}.".".$args{-name}} eq "ODBC" and
			! defined $ENV{PROCESSOR_LEVEL};
		$conntype{$args{-type}.".".$args{-name}} = "ODBC"
			unless defined $conntype{$args{-type}.".".$args{-name}};

		print "returning(login=".  $login{$args{-type}.".".$args{-name}}," pass=",
					$password{$args{-type}.".".$args{-name}}," ctype=",
					$conntype{$args{-type}.".".$args{-name}}," )\n" if $args{-debug};
		return(  $login{$args{-type}.".".$args{-name}},
					$password{$args{-type}.".".$args{-name}},
					$conntype{$args{-type}.".".$args{-name}} );
	} else {
		# return array of values
		my(@rc);
		foreach ( keys %login ) {
			next unless /^$args{-type}/;
			s/^$args{-type}\.//;
			push @rc, $_;
		}
		print "returning ",join(" ",@rc),"\n" if defined $args{-debug};
		return sort @rc;
	}
}

# returns a array config_options
sub get_passfile_info
{
	my(%args)=@_;
	if( ! defined $filemap{$args{-type}} ) {
		print "get_passfile_info() \n";
		foreach ( keys %args ) {
			print " key=$_ val=$args{$_} \n";
		}
		die "Invalid type ".$args{-type}." Passed to get_passfile_info in Repostiory.pm";
	}
	if( $args{-type} eq "sybase" ){
		return( qw(server login password conntype -SERVER_TYPE -backupservername ) );
	} elsif( $args{-type} eq "oracle" ){
		return( qw(server login password conntype -SERVER_TYPE) );
	} elsif( $args{-type} eq "unix"   ){
		return( qw(server login password -SYBASE -SYBASE_CLIENT -IGNORE_RUNFILE -UNIX_COMM_METHOD -WIN32_COMM_METHOD -SERVER_TYPE) );
	} elsif( $args{-type} eq "sqlsvr" ){
		return( qw(server login password -hostname -SERVER_TYPE) );
	} elsif( $args{-type} eq "win32servers" ){
		return( qw(server DISKS) );
	} elsif( $args{-type} eq "documenter" ){
		return( qw(console.dat) );
	}
}

sub read_passfile
{
	my(%args)=@_;
	my($type)= $args{-type};
	my($filenm) = $filemap{$type};

	$filenm = get_conf_dir()."/".$filenm unless -r $filenm;
	die "Cant find file $filenm" unless -r $filenm;

	my(@dat);
	my($server);
	if( $type eq "sybase" or $type eq "unix" or $type eq "sqlsvr" or $type eq "oracle" ) {
		print "Reading Password File $filenm\n" if defined $args{-debug};
		open(CONFIGFILE,$filenm)
			|| croak("Can't open file: $filenm: $!");
		while ( <CONFIGFILE> ) {
			next if /^\s*$/ or /^\s*#/ ;
			s/\s+$//;
			chomp;
			#print "debug: Read Line $_\n" if defined $args{-debug};
			if( /^\s/ ) {
				s/^\s+//;
				my($var,$val) = split("=",$_,2);
				if( $var eq "RSH_OK" ) {	# RSH_OK depricated
					if( $val eq "Y" ) {
						$server_data{$type.".".$server.".UNIX_COMM_METHOD"} = "RSH";
					} else {
						$server_data{$type.".".$server.".UNIX_COMM_METHOD"} = "FTP";
					}
				} else {
					#print "RESETTING $type.$server.$var = $val \n";
					$server_data{$type.".".$server.".".$var} = $val;
				}
			} else {
				my(@d)=split;
				$server=$d[0];
				$login{$type.".".$server}	 	=	$d[1];
				$password{$type.".".$server}	=	$d[2];
				$conntype{$type.".".$server}	=	$d[3];
				$server_type{$server} = $type;
				if( $type eq "unix" ) {
					$server_data{$type.".".$server.".UNIX_COMM_METHOD"}  = "FTP";	# defaults
					$server_data{$type.".".$server.".WIN32_COMM_METHOD"} = "FTP";	# defaults					
				}
			}
		}
		close(CONFIGFILE);
		#use Data::Dumper;
		#print Dumper \%server_data;
		#die "done";
	} elsif( $type eq "documenter" ) {
		my(%INFO)=read_custom_defined($filenm);
		foreach my $k ( keys %INFO ) {
			$login{$type.".".$k}	 	=	"";
			# print "$k = $INFO{$k} \n";
			my(%xxx)= %{$INFO{$k}};
			foreach my $j ( keys %xxx ) {
				$server_data{$type.".".$k.".".$j} = $xxx{$j};
			}
		}
	} elsif( $type eq "win32servers" ) {
		open(CONFIGFILE,$filenm)
			|| croak("Can't open file: $filenm: $!");
		while ( <CONFIGFILE> ) {
			chomp;
			chomp;
			next if /^\s*$/ or /^\s*#/ ;
			if( /^\t/ ) {
				s/^\s+//;
				my($var,$val) = split("=",$_,2);
				$server_data{$type.".".$server.".".$var}	 	=	$val;
			} else {
				my(@d)=split;
				$server=shift @d;
				$login{$type.".".$server}	 	=	"";
				$server_data{$type.".".$server.".DISKS"} = join(" ",@d);
			}
		}
		close(CONFIGFILE);
	} else {
		die "Invalid Password File Type=$type\n";
	}
	$file_read{$type}=1;
}

# -type
# -name
sub get_password_info
{
	my(%args)=@_;
	croak "Must pass -name to get_password_info" unless defined $args{-name};
	$args{-type} = $server_type{$args{-name}} unless defined $args{-type};
	croak "Must pass -type to get_password_info()" unless defined $args{-type};

	read_passfile( -type=>$args{-type}, -debug=>$args{-debug} )
			unless defined $file_read{$args{-type}};

	my($key)=$args{-type}."\\.".$args{-name}."\\.";
	my(%INFO);
	my($found)=0;
	foreach ( keys %server_data ) {
		next unless /^$key/;
		my($v)=$_;		
		s/^$key//;
		$INFO{$_} = $server_data{$v};
		$found++;
	}
	$INFO{NOT_FOUND}=1 unless $found;
	if( ! $found and $args{-debug} ) {		
		print "Warning: No info Found for $key\n";
		foreach ( keys %server_data ) { print "ok key=$_ \n"; }
	}
	return %INFO;
}

sub read_custom_defined
{
	my($filenm)=@_;

	# the output hashes
	my(%custom_report_name);	# key=number (from $count)
	my(%custom_ignore_msgs );	# ignore messages
	my(%custom_report_type, %custom_report_server, %custom_report_query, %custom_report_database);
	my(%next,%last);
	my(%used_names);				# used names

	my($count)=0;
	my($tmplast)="";

	open(USERDEF,$filenm) or return;
	while ( <USERDEF> ) {
		next if /^\s*$/ or /^\s*#/;
		chomp;
		chomp;
		my(@fields)=split(/\s+/,$_,4);

		if( $fields[0] eq "IGNORE" ) {
			$custom_ignore_msgs{$fields[1]." ".$fields[2]}=$fields[3];
			next;
		};

		if( defined $used_names{$fields[0]} ) {
			print "Warning: Custom Report Previously Defined By Same Name\n";
			print "This Report Shall Be Ignored\n";
			next;
		} else {
			$used_names{$fields[0]}=1;
		}

		$custom_report_name{$count} = $fields[0];
		if( $fields[1] 	=~ /^cmd/i
			or $fields[1] =~ /^file/i
			or $fields[1] =~ /^html/i
			or $fields[1] =~ /^sql/i ) {

			$custom_report_type{$count} = $fields[1];
			$custom_report_server{$count} = "";
			$custom_report_database{$count} = "";

			if( $fields[1] =~ /^file/i ) {
				$custom_report_server{$count} = $fields[2];
				shift @fields;
			}
			shift @fields;
			shift @fields;
			$custom_report_query{$count} = join(" ",@fields);

			# ok... set up %next and %last
			if( $tmplast != "" ) {
				$next{$tmplast} = $fields[0].".html" ;
				$last{$fields[0].".html"} = $tmplast;
			}
			$tmplast = $fields[0].".html";

		} else {
			# key number 1 is server

			$custom_report_server{$count} = $fields[1];
			if( $fields[2] =~ /null/i ) {
				$custom_report_database{$count} = "";
			} else {
				$custom_report_database{$count} = $fields[2];
			}
			$custom_report_query{$count} = $fields[3];

		}
		$count++;
	}
	close USERDEF;

	my(%info);
	$info{custom_report_name} = \%custom_report_name;
	$info{custom_ignore_msgs } = \%custom_ignore_msgs;
	$info{custom_report_type} = \%custom_report_type;
	$info{custom_report_server} = \%custom_report_server;
	$info{custom_report_query} = \%custom_report_query;
	$info{custom_report_database} = \%custom_report_database;
	$info{next} = \%next;
	$info{last} = \%last;
	return(%info);
}

# required args -op -type -name opt -login -password %args
#if 0name
#	-name and -del will delete server
#	-name and -conntype will set conntype
#	print -name/-login/-password
 # 		-keystuff
# if -op = add will add or update as needed
# if -op = update then will update based on -value

sub add_edit_item {
	my(%args)=@_;
	my($type)	= $args{-type};
	my($filenm)	= $filemap{$type};

	if( defined $args{-debug} ) {
		print "Repository.pm add_edit_item() called in Debug Mode\n";
		foreach ( keys %args ) {
			print " add_edit_item( key=$_ value=$args{$_} )\n";
		}
	}

	$filenm = get_conf_dir()."/".$filenm unless -r $filenm;
	die "Cant find file $filenm" unless -r $filenm;

	my(@dat);
	my($server);
	if( $type eq "sybase" or $type eq "unix" or $type eq "sqlsvr" or $type eq "oracle" ) {
		print "Reading Password File $filenm\n" if defined $args{-debug};
		open(CONFIGFILE,$filenm)
			|| croak("Can't open file: $filenm: $!");
		my(@x)= <CONFIGFILE>;
		close(CONFIGFILE);
		if( $args{-op} eq "update" ) {
			my($hash_override)=$args{-values};	# key= server.val
			if( ! $hash_override ) {
				die "Must pass -values";
			}
			if( defined $args{-debug} ) {
				foreach ( keys %$hash_override ) {
					print "add_edit_items($_ val=$$hash_override{$_})\n";
				}
			}
			print "Writing Password File $filenm\n";# if defined $args{-debug};
			open(WRITEIT,">".$filenm)
				|| croak("Can't write file: $filenm: $!");
			my($cursvrname)="";
			foreach (@x) {
				chomp;chomp;
	 			if( !/^\s/ ) {
	 				my(@dat)=split(/\s+/,$_);
	 				$cursvrname=$dat[0];

	 			} elsif( /\=/ ) {
	 				my($k,$v)=split(/=/,$_,2);
	 				$k=~s/^\s+//;
	 				#print ">",$cursvrname,"|",$k,"\n";
	 				if( defined $$hash_override{$cursvrname."|".$k} ) {
	 					# overriden
	 					print WRITEIT "\t$k=".$$hash_override{$cursvrname."|".$k},"\n";
	 					next;
	 				}
	 			}
	 			print WRITEIT $_,"\n";
	 		}
	 		close(WRITEIT);
	 		print "... Finished modification of $filenm \n"	if defined $args{-debug};
		} else {
		print "Writing Password File $filenm\n" if defined $args{-debug};
		open(WRITEIT,">".$filenm)
			|| croak("Can't write file: $filenm: $!");
		my($foundname)="FALSE";
		print "... Searching for $args{-name} \n" if defined $args{-debug};
		foreach (@x) {
			chomp; chomp;

			# FOUND THE LINE WITH THE SERVER ON IT
			if( $args{-name} and /^$args{-name}\s/i) {
				$foundname="TRUE";
				if( $args{-op} eq "del" ) {
					print "-- $_ \n" if defined $args{-debug};
					next;
				}
				print "-- $_ \n" 		if defined $args{-debug};

				if( ! defined $args{-conntype} ){
					my(@d)=split;
					$args{-conntype}=$d[3];
				}

				if( defined $args{-debug} ) {
					print "++ ",$args{-name}."\t".$args{-login}."\t".$args{-password}."\t".$args{-conntype}."\n";
					foreach ( keys %args ) {
						print  "++\t",$_,"=",$args{$_},"\n" unless $_ =~ /^-/;
					}
				}
				print WRITEIT $args{-name}."\t".$args{-login}."\t".$args{-password}."\t".$args{-conntype}."\n";
				foreach ( keys %args ) {
					print WRITEIT "\t",$_,"=",$args{$_},"\n" unless $_ =~ /^-/;
				}
				next;
			}

			# erase args for the found one
			if($foundname eq "TRUE"){
				print "-- $_\n" if /^\s/ and defined $args{-debug};
				next if /^\s/;
				$foundname = "DONE";
			}
			print WRITEIT $_,"\n";
		}
		# ok we didnt find it so we write it now
		if( $foundname eq "FALSE" and $args{-op} eq "add" ) {
			print " -- FOUND $args{-name} FOR ADD\n" if defined $args{-debug};
			print WRITEIT $args{-name}."\t".$args{-login}."\t".$args{-password}."\n";
			print $args{-name}."\t".$args{-login}."\t".$args{-password}."\n"
				if defined $args{-debug};
			foreach ( keys %args ) {
				if( defined $args{-debug} ) {
					print "\t",uc($_),"=",$args{$_},"\n" unless $_ =~ /^-/;
				}
				print WRITEIT "\t",uc($_),"=",$args{$_},"\n" unless $_ =~ /^-/;
			}
		}
		close(WRITEIT);
		print "... Finished modification of $filenm \n"	if defined $args{-debug};
		}
	} elsif( $type eq "documenter" ) {
		my(%INFO)=read_custom_defined($filenm);
		foreach my $k ( keys %INFO ) {
			$login{$type.".".$k}	 	=	"";
			# print "$k = $INFO{$k} \n";
			my(%xxx)= %{$INFO{$k}};
			foreach my $j ( keys %xxx ) {
				$server_data{$type.".".$k.".".$j} = $xxx{$j};
			}
		}
	} elsif( $type eq "win32servers" ) {
		print "Opening $filenm\n" if defined $args{-debug};
		open(CONFIGFILE,$filenm)
			|| croak("Can't open file: $filenm: $!");
		my(@x)= <CONFIGFILE>;
		close(CONFIGFILE);
		print "Writing Password File $filenm\n" if defined $args{-debug};
		open(WRITEIT,">".$filenm)
			|| croak("Can't write file: $filenm: $!");

		foreach ( @x ) {
			chomp;
			chomp;
			next if /^\s*$/ or /^\s*#/ ;
			my(@d)=split;
			$server=shift @d;
			next if  $args{-name} eq $server;
			print WRITEIT $_,"\n";
			$login{$type.".".$server}	 	=	"";
			$server_data{$type.".".$server.".DISKS"} = join(" ",@d);
		}
		print WRITEIT $args{-name}." ".$args{DISKS}."\n"
				unless $args{-op} eq "del";
		$login{$type.".".$args{-name}}	 	=	"";
		$server_data{$type.".".$args{-name}.".DISKS"} = $args{DISKS};
		close(WRITEIT);
	} else {
		die "Invalid Password File Type=$type\n";
	}

	print "... Marking $type information unread - it will be refetched.\n" if defined $args{-debug};
	undef $file_read{$type};
}

sub read_threshfile {
	my($THRESHFILE,$program)=@_;

	if( ! $THRESHFILE ) {
		$THRESHFILE = get_conf_dir()."/threshold_overrides.dat"
			if -r get_conf_dir()."/threshold_overrides.dat";
		return unless $THRESHFILE;
	}

	my(%warnthresh, %alarmthresh, %critthresh);
	
	if( ! -r $THRESHFILE and -r "$THRESHFILE.sample" ) {
		use File::Copy;
		output("Copying Sample File To $THRESHFILE\n");
		copy "$THRESHFILE.sample",$THRESHFILE;
	}
		
        if( -r $THRESHFILE ) {
      		#output("Reading $THRESHFILE\n");
         	open (TR, $THRESHFILE ) or die "Cant open $THRESHFILE\n";
                while (<TR>) {
                   chomp;
                   chomp;
                    #SERVERNM,ThresholdMonitor,CRITICAL,99,dbname
                   next if /^#/ or /^\s*$/;
                   next unless /,$program,/;
                   
                   my($svr,$prog,$state,$pct,$subsys)=split(/,/);
                   $svr=~s/\s//g;
                   $state=~s/\s//g;
                   undef $subsys if $subsys =~ /^\s*$/;
                   #output("   Setting $state threshold for system $svr to $pct \n");
                   if( ! $subsys ) {
	                $warnthresh{$svr}=$pct 	if $state=~/WARNING/i;
                   	$alarmthresh{$svr}=$pct 	if $state=~/ERROR/i;
                   	$critthresh{$svr}=$pct 	if $state=~/CRITICAL/i;
                   } else {
                        $warnthresh{$svr.":".$subsys}=$pct 	if $state=~/WARNING/i;
                   	$alarmthresh{$svr.":".$subsys}=$pct 	if $state=~/ERROR/i;
                   	$critthresh{$svr.":".$subsys}=$pct 	if $state=~/CRITICAL/i;
               	   }
                }
         	close(TR);
      }
      return(\%warnthresh, \%alarmthresh, \%critthresh);
	
}

1;

__END__

=head1 NAME

Repository.pm - Customizable Server/Password Repository

=head2 SYNOPSIS

	# Get Password For Particular Server
	($login,$password)=get_password(-type=>"sybase", -name=>"SYBPROD")

	# Work on all Sybase Servers
	use Repository;
	@servers=get_password(-type=>xxx,-name=>undef)
	foreach (@servers) {
		($login,$password)=get_password(-type=>xxx, -name=>$_)
		...
	}

	# Get Unix Servers
	use Repository;
	@hosts=get_password(-type=>"unix", -name=>undef)

	# Get information on particular server
	%info_about_servers=get_password_info($server)

=head2 DESCRIPTION

This basic module should be extended to allow encryption.  For now it
allows you to get passwords from a file for your application through a standard
interface that can be enhanced to meet your security concerns.

Two files are required for this module to work.  The first file is a
configuration file that identifies the password information.  This file
must exist in the current directory (checked first),  or in one of the
directories in your perl library include path.  The file name is
sybase_password.cfg by default.

The password file is user definable but needs to follow the following rules:

	lines starting with # are ignored
	lines starting with any white space are informational and can be
		returned with the get_password_info function.

Normally, the password file should be named password.cfg in the current
directory and have the format

SERVER<spaces>PASSWORD<spaces>LOGIN
<spaces>KEY=VALUE
<spaces>KEY=VALUE
SERVER<spaces>PASSWORD<spaces>LOGIN
<spaces>KEY=VALUE
<spaces>KEY=VALUE

If your password file is of a different format, or you are using a differently
named password file, you need to define a configuration file (default
sybase_password.cfg).  This file should be of the format:

	PASSFILE=passwords
	SERVER_COL=0
	LOGIN_COL=1
	PASS_COL=2

get_password_info returns lines that start with spaces

=cut

