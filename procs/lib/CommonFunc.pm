# Copyright (c) 2003-6 by Edward Barlow. All rights reserved.
# This program is free software; you can redistribute it and/or modify
# it under the same terms as Perl itself.

package CommonFunc;

use vars qw($VERSION @ISA @EXPORT @EXPORT_OK);
use Exporter;
use strict;
use File::Basename;
use Cwd;
use Sys::Hostname;
use Repository;

BEGIN {
	if( $] < 5.008001 and defined $ENV{PROCESSOR_LEVEL} ) {
		if( defined $^V and $^V!~/\s*$/) {
			die "Error : You must have perl 5.8.1 installed under Win32.  You are running $^V \n";
		}
		my($x)=$];
		$x=~s/0+/\./g;
		$x=~s/\.\.+/\./g;
		die "Error : You must have perl 5.8.1 installed.  You are running $x ($])\n";
	}
}

$VERSION= 1.0;

@ISA      = (	'Exporter'	);
@EXPORT   = ( 'get_version','is_nt','cd_home','today','get_tempdir','get_job_list','read_configfile', 'get_tempfile', 'is_up', 'reword',
	'mark_up','is_interactive','I_Am_Up','Am_I_Up', "I_Am_Down","GetLockfile");

sub reword {
	my($txt,$wantlen,$maxlen)=@_;

	if( length($txt)>=$maxlen ) {
	   	$txt=~s/\s+/ /g;
		my(@words)=split(/ /,$txt);
	   	my($curlen)=0;
   		$txt="";
   		foreach(@words) {
      			if( $curlen+length() > $wantlen ) {
   				if( $curlen+length()>$maxlen ) {
   					$txt.="\n".$_;
   					$curlen=length();
	   			} else {
   					$txt.=" ".$_."\n";
   					$curlen=0;
   				}
   			} else {
   				$curlen+=length()+1;
   				$txt.=" ".$_;
   			}
   		}
   		$txt=~s/\n\s+/\n/g;
		$txt=~s/^\s+//;
		$txt=~s/\s+$//;
   	}
   	return $txt;
}

sub get_version {
	my($file)=@_;
	my($version_file, $VERSION);
	$version_file=$file if defined $file and -r $file;
	$version_file="version" if -e "version" and ! defined $version_file;
	$version_file="VERSION" if -e "VERSION" and ! defined $version_file;
	$version_file="../version" if ! defined $version_file and -e "../version";
	$version_file="../VERSION" if ! defined $version_file and -e "../VERSION";
	my($codedir)=dirname($0);
	$version_file=$codedir."/VERSION" if ! defined $version_file and -e $codedir."/VERSION";
	$version_file=$codedir."/version" if ! defined $version_file and -e $codedir."/version";
	die "Cant find a file named VERSION anywhere\nThis file should be located in . or .. or $codedir directories\n" unless defined $version_file;
	open (VER,$version_file) or die "Cant read file $version_file...\n";
	while (<VER>) { chomp; $VERSION=$_; }
	close VER;

	my($sht_version)=$VERSION;
	$sht_version=~s/\(.+$//;
	$sht_version=~s/\s+$//;
	$sht_version=~s/^[\D\s]+//g;
	return($VERSION, $sht_version);
}

my($sv_is_nt);
sub is_nt {
	# set something if you are on NT
	return $sv_is_nt if defined $sv_is_nt;
	$sv_is_nt=0;
	$sv_is_nt=1 if defined $ENV{PROCESSOR_LEVEL};
	return $sv_is_nt;
}

sub cd_home {
	# change to this directory if run from somewhere else
	my  $curdir=dirname($0);
	chdir($curdir) or die "Cant cd to $curdir: $!\n";
	return cwd();
}

sub today
{
	my(@t)=localtime(time);
	$t[4]++;
	$t[5]+=1900;
	substr($t[3],0,0)="0" if length($t[3])==1;
	substr($t[4],0,0)="0" if length($t[4])==1;
	substr($t[5],0,0)="0" if length($t[5])==1;
	substr($t[2],0,0)="0" if length($t[2])==1;
	substr($t[1],0,0)="0" if length($t[1])==1;
	substr($t[0],0,0)="0" if length($t[0])==1;
	return $t[5].$t[4].$t[3].".".$t[2].$t[1].$t[0];
}

# Check for Lock File and Dont Restart unless it exists
# Args: -threshtime , -filename
sub is_up { 	return Am_I_Up(@_); }
sub mark_up { 	return I_Am_Up(@_); }

sub get_job_list
{
	my(%config)=@_;
	die "Must pass %config" unless %config;

	my(@keys)=qw(
	DSQUERY REMOTE_DIR IGNORE_SERVER NUM_DAYS_TO_KEEP NUM_BACKUPS_TO_KEEP
	NUMBER_OF_STRIPES DUMP_FILES_PER_SUBDIR COMPRESSION_LEVEL COMPRESS UNCOMPRESS IGNORE_LIST DBCC_IGNORE_DB
	MAIL_TO DO_COMPRESS COMPRESS_DEST COMPRESS_LATEST DO_UPDSTATS DO_AUDIT AUDIT_PURGE_DAYS SUCCESS_MAIL_TO
	DO_DBCC DO_BCP BCP_COMMAND BCP_TABLES DO_PURGE DO_INDEXES DO_REORG REORG_ARGS UPD_STATS_FLAGS
	IS_REMOTE REMOTE_HOST REMOTE_ACCOUNT REMOTE_PASSWORD DO_DUMP DO_CLEARLOGSBEFOREDUMP
	DO_LOAD XFER_TO_HOST XFER_SCRATCH_DIR XFER_TO_SERVER XFER_TO_DB
	XFER_FROM_DB XFER_TO_DIR XFER_BY_FTP LOCAL_DIR MAIL_HOST REMOTE_DUMP_DIR REMOTE_LOG_DIR
	LOCAL_DUMP_DIR LOCAL_LOG_DIR);

	my(%keyhash);
	my(%jobhash);
	foreach (@keys) { $keyhash{$_}=1; }

	foreach my $k ( keys %config ) {

		next if defined $keyhash{$k};
		next if $k !~ /\_/;
		my($job,@parts)=split(/\_/,$k);
		my(@permuted);
		while(@parts) {
			my($v)=join("_",@parts);
			if( defined $keyhash{ $v } ){
				#print "Job $job found From $k\n" unless defined $jobhash{$job};
				$jobhash{$job}=1;
				last;
			}
			$job.="_".shift @parts;
		}
	}
	return(keys %jobhash);
}

# returns a temporary file name
sub get_tempdir {
	if(  -d "C:/tmp" and -w "C:/tmp" ) {
		return "C:/tmp";
	} elsif( -d "C:/temp" and -w "C:/temp") {
		return "C:/temp";
	} elsif( -d "/tmp" ) {
		return "/tmp";
	} else {
		die "Cant find temporary file to return\n";
	}
}

# returns a temporary file name
sub get_tempfile {
	my($base)=@_;
	if( defined $base ) {
		return $base.".".$$;
	} elsif(  -d "C:/tmp" and -w "C:/tmp" ) {
		return "C:/tmp/".basename($0).".".$$;
	} elsif( -d "C:/temp" and -w "C:/temp") {
		return "C:/temp/".basename($0).".".$$;
	} elsif( -d "/tmp" ) {
		return "/tmp/".basename($0).".".$$;
	} elsif( -w dirname($0) ) {
		return dirname($0)."/".basename($0).".".$$;
	} else {
		die "Cant find temporary file to return\n";
	}
}

sub read_configfile
{
	my(%args)=@_;
	my($opt_J)= $args{-job} || "UNKNOWN";
	my($opt_S)= $args{-srv};
	# my($help_maxlength)= $args{-maxlength} || 200;
	#my(%variable_help);
	my(@errors);

	print "CommonFunc.pm: read_configfile() at ".scalar(localtime(time))."\n" if defined $args{-debug};
#	if( defined $args{-helpref} ) {
#		%variable_help = %{$args{-helpref}}
#	}

	my(%OUTPUT);
	my $filename="configure.cfg"
		if -r "configure.cfg";
	$filename="../configure.cfg"
		if ! defined $filename and -r "../configure.cfg";
	# $filename="conf/configure.cfg"
		#if ! defined $filename and -r "conf/configure.cfg";
	$filename=get_conf_dir()."/configure.cfg"
		if ! defined $filename and -r get_conf_dir()."/configure.cfg";

	die("No Configuration File configure.cfg Found.  Searched . .. and ".get_conf_dir()) unless defined $filename;
	my($errstr)="Error In Configuration File $filename\n";
	my(%C);
	print "CommonFunc.pm: file=$filename at ".scalar(localtime(time))."\n" if defined $args{-debug};

	#
	# read the config file
	#
	open(CFGFILE,$filename) or die("Cant Read $filename: $!\n");
	#my($cur_vbl)="";
	while (<CFGFILE>) {
		next if /^\s*$/;
		chop;
		next if /^\s*#/;
#		if( /^#/ ) { 	# comment
#			if( $cur_vbl eq "" ) {
#				if( /\=\[/ ) {
#					# DUPLICATED BELOW
#					s/^#\s+//;
#					my($kkk)=$_;
#					s/\=\[.*$//;
#					$cur_vbl=$_;
#					$variable_help{$cur_vbl}=$kkk."\n";
#				}
#			} else {
#				if( /\=\[/ ) {
#					# DUPLICATED FROM ABOVE
#					s/^#\s+//;
#					my($kkk)=$_;
#					s/\=\[.*$//;
#					$cur_vbl=$_;
#					$variable_help{$cur_vbl}=$kkk."\n";
#				} elsif( /^#\s/ ) {
#					s/^#\s*//;
#					if( length($_) > $help_maxlength ) {
#						print "Splicing Newline Into Help Line For $cur_vbl \n";
#						$_ = substr($_,0,$help_maxlength)."\n".substr($_,($help_maxlength+1));
#						print "--> $_";
#					}
#					$variable_help{$cur_vbl}.=$_."\n";
#				} else {
#					$cur_vbl="";
#				}
#			}
#			next;
#		}

		#$cur_vbl="";
   		s/^\s+//;
   		s/\s+$//;

		my($key,$value)=split(/=/,$_,2);

		if( defined $value and $value !~ /^\s*$/) {
			$C{$key}=$value;
		} else {
			if( $key=~/\_COMPRESSION\_LEVEL/ ) {
				s/_COMPRESSION_LEVEL//;
				my $msg=$errstr."COMPRESSION_LEVEL must be defined for server $_ - it is set to empty string - either remove this variable or set it to level 0 (no compression)\n";
				if( defined $args{-nodie} ) {
					push @errors,$msg;
				} else {
					die $msg;
				}
			}
		}
	}
	close CFGFILE;
	print "CommonFunc.pm: validating data at ".scalar(localtime(time))."\n" if defined $args{-debug};

	#if( ! defined $C{INSTALL_SYDUMP_BACKUP_SCRIPTS} ) {
	#	print "Hmmm We have a serious error reading $filename\n";
	#	print "We are unable to  find INSTALL_SYDUMP_BACKUP_SCRIPTS in this file\n";
	#	foreach (sort keys %C ) { print "KEY: $_ VAL: $C{$_} \n"; }
	#	exit(0);
	#}

	#if( $C{INSTALL_SYDUMP_BACKUP_SCRIPTS} ne "Y"
	#and $C{INSTALL_SYDUMP_BACKUP_SCRIPTS} ne "y"
	#if(  defined $args{-job} ) {
	#	print "Configuration Variable Report\n\n";
	#	print "The file read was $filename\n";
	#	print "The Job is $opt_J\n";
	#	print "You can Not Run Backup Scripts As INSTALL_SYDUMP_BACKUP_SCRIPTS!=y\n";
	#	print "INSTALL_SYDUMP_BACKUP_SCRIPTS=\|",
	#				$C{INSTALL_SYDUMP_BACKUP_SCRIPTS},"\|\n";
	#	#foreach (sort keys %C ) { print "KEY: $_ VAL: $C{$_} \n"; }
	#	exit(0);
	#}

	$OUTPUT{JOBNAME}= $opt_J || $opt_S;
	$OUTPUT{SYBASE} = $C{"SYBASE"} || $ENV{SYBASE};
	$OUTPUT{DSQUERY}= $C{$opt_J."_DSQUERY"} || $opt_J;
	$OUTPUT{BASE_BACKUP_DIR} =  $C{$opt_J."_BASE_BACKUP_DIR"} || $C{BASE_BACKUP_DIR};
	$OUTPUT{LOCAL_DIR}
		=	$C{$opt_J."_LOCAL_DIR"} || $C{LOCAL_DIR} || $OUTPUT{BASE_BACKUP_DIR};
	$OUTPUT{REMOTE_DIR}
		=	$C{$opt_J."_REMOTE_DIR"} || $C{REMOTE_DIR} || $OUTPUT{LOCAL_DIR} || $OUTPUT{BASE_BACKUP_DIR};
	$OUTPUT{REMOTE_DUMP_DIR} = $C{$opt_J."_REMOTE_DUMP_DIR"};
	$OUTPUT{REMOTE_LOG_DIR}  = $C{$opt_J."_REMOTE_LOG_DIR"};
	$OUTPUT{LOCAL_DUMP_DIR} = $C{$opt_J."_LOCAL_DUMP_DIR"};
	$OUTPUT{LOCAL_LOG_DIR}  = $C{$opt_J."_LOCAL_LOG_DIR"};
	$OUTPUT{IGNORE_SERVER}
		=	$C{$opt_J."_IGNORE_SERVER"} || $C{IGNORE_SERVER};
	$OUTPUT{NUM_DAYS_TO_KEEP}
		= $C{$opt_J."_NUM_DAYS_TO_KEEP"} || $C{NUM_DAYS_TO_KEEP};
	if( ! defined $C{$opt_J."_NUM_BACKUPS_TO_KEEP"} or length($C{$opt_J."_NUM_BACKUPS_TO_KEEP"})==0 ) {
		$OUTPUT{NUM_BACKUPS_TO_KEEP} = $C{NUM_BACKUPS_TO_KEEP};
	} else {
		$OUTPUT{NUM_BACKUPS_TO_KEEP} =  $C{$opt_J."_NUM_BACKUPS_TO_KEEP"};
		#= length($C{$opt_J."_NUM_BACKUPS_TO_KEEP"}) ? $C{$opt_J."_NUM_BACKUPS_TO_KEEP"} : $C{NUM_BACKUPS_TO_KEEP};
	}
	$OUTPUT{NUMBER_OF_STRIPES}
		= defined($C{$opt_J."_NUMBER_OF_STRIPES"}) ? $C{$opt_J."_NUMBER_OF_STRIPES"} : $C{NUMBER_OF_STRIPES};
	$OUTPUT{DUMP_FILES_PER_SUBDIR}
		= defined($C{$opt_J."_DUMP_FILES_PER_SUBDIR"}) ? $C{$opt_J."_DUMP_FILES_PER_SUBDIR"} : $C{DUMP_FILES_PER_SUBDIR};

	# pretty lame... must do this if value is 0 doh...
	$OUTPUT{COMPRESSION_LEVEL}
		= 	length($C{$opt_J."_COMPRESSION_LEVEL"}) ? $C{$opt_J."_COMPRESSION_LEVEL"} : $C{COMPRESSION_LEVEL};

	$OUTPUT{COMPRESS}
		= 	$C{$opt_J."_COMPRESS"} || $C{COMPRESS};
	$OUTPUT{COMPRESS_DEST}
		= 	$C{$opt_J."_COMPRESS_DEST"} || $C{COMPRESS_DEST};
	$OUTPUT{UNCOMPRESS }
		= 	$C{$opt_J."_UNCOMPRESS"} || $C{UNCOMPRESS};
	$OUTPUT{IGNORE_LIST}
		=	$C{$opt_J."_IGNORE_LIST"} || $C{IGNORE_LIST};
	$OUTPUT{DBCC_IGNORE_DB}
		=	$C{$opt_J."_DBCC_IGNORE_DB"} || $C{DBCC_IGNORE_DB};
	$OUTPUT{SUCCESS_MAIL_TO}
		=	$C{$opt_J."_SUCCESS_MAIL_TO"} || $C{SUCCESS_MAIL_TO};
	$OUTPUT{MAIL_TO}
		=	$C{$opt_J."_MAIL_TO"} || $C{MAIL_TO};
	$OUTPUT{MAIL_HOST} =	 $C{MAIL_HOST};
	$OUTPUT{DO_COMPRESS}
		=	lc($C{$opt_J."_DO_COMPRESS"}||$C{DO_COMPRESS})||"n";
	$OUTPUT{DO_ONLINEDB}
		=	lc($C{$opt_J."_DO_ONLINEDB"}||$C{DO_ONLINEDB})||"n";
	$OUTPUT{COMPRESS_LATEST}
		=	lc($C{$opt_J."_COMPRESS_LATEST"}||$C{COMPRESS_LATEST})||"n";
	$OUTPUT{UPD_STATS_FLAGS}
		=	$C{$opt_J."_UPD_STATS_FLAGS"} || $C{UPD_STATS_FLAGS} || "";
	$OUTPUT{DO_UPDSTATS}
		=	lc($C{$opt_J."_DO_UPDSTATS"} || $C{DO_UPDSTATS}) || "n";
	$OUTPUT{DO_AUDIT}
		=	lc($C{$opt_J."_DO_AUDIT"} || $C{DO_AUDIT}) || "n";
	$OUTPUT{AUDIT_PURGE_DAYS}
		=	lc($C{$opt_J."_AUDIT_PURGE_DAYS"} || $C{AUDIT_PURGE_DAYS}) || "30";
	$OUTPUT{DO_DBCC}
		=	lc($C{$opt_J."_DO_DBCC"} || $C{DO_DBCC}) || "n";
	$OUTPUT{DO_BCP}
		=	lc($C{$opt_J."_DO_BCP"} || $C{DO_BCP}) || "n";
	$OUTPUT{BCP_COMMAND}
		=	$C{$opt_J."_BCP_COMMAND"} || $C{BCP_COMMAND} || "";
	$OUTPUT{BCP_TABLES}
		=	$C{$opt_J."_BCP_TABLES"} || $C{BCP_TABLES};
	$OUTPUT{DO_PURGE}
		=	lc($C{$opt_J."_DO_PURGE"}||$C{DO_PURGE})||"n";

	$OUTPUT{DO_INDEXES}
		=  lc($C{$opt_J."_DO_INDEXES"}||$C{DO_INDEXES})||"n";
	$OUTPUT{DO_REORG}
		=  lc($C{$opt_J."_DO_REORG"} || $C{DO_REORG}) || "n";
	$OUTPUT{REORG_ARGS}
		=  $C{$opt_J."_REORG_ARGS"}||$C{REORG_ARGS}||"--REORG_COMPACT";

	$OUTPUT{IS_REMOTE}
		=	lc($C{$opt_J."_IS_REMOTE"} || $C{IS_REMOTE}) || "n";

	$OUTPUT{REMOTE_HOST} =	$C{$opt_J."_REMOTE_HOST"}
						|| $C{$opt_J."_RSH_HOST"}
						|| $C{REMOTE_HOST}
						|| $C{RSH_HOST};

	$OUTPUT{REMOTE_PASSWORD} =	$C{$opt_J."_REMOTE_PASSWORD"}
						|| $C{REMOTE_PASSWORD};

	$OUTPUT{REMOTE_ACCOUNT} =	$C{$opt_J."_REMOTE_ACCOUNT"}
						|| $C{$opt_J."_RSH_ACCOUNT"}
						|| $C{REMOTE_ACCOUNT}
						|| $C{RSH_ACCOUNT};

	$OUTPUT{DO_DUMP}
		=	lc($C{$opt_J."_DO_DUMP"}||$C{DO_DUMP})||"n";
	$OUTPUT{DO_LOAD}
		=	lc($C{$opt_J."_DO_LOAD"} || $C{DO_LOAD}) || "n";
	$OUTPUT{DO_CLEARLOGSBEFOREDUMP}
		=	lc($C{$opt_J."_DO_CLEARLOGSBEFOREDUMP"}||$C{DO_CLEARLOGSBEFOREDUMP})||"y";

	$OUTPUT{XFER_TO_SERVER} 	= $C{$opt_J."_XFER_TO_SERVER"};
	$OUTPUT{XFER_TO_DB} 		= $C{$opt_J."_XFER_TO_DB"};
	$OUTPUT{XFER_FROM_DB} 		= $C{$opt_J."_XFER_FROM_DB"};
	$OUTPUT{XFER_TO_DIR} 		= $C{$opt_J."_XFER_TO_DIR"};
	$OUTPUT{XFER_TO_DIR_BY_TARGET} 	= $C{$opt_J."_XFER_TO_DIR_BY_TARGET"};
	$OUTPUT{XFER_BY_FTP} 		= lc($C{$opt_J."_XFER_BY_FTP"});
	$OUTPUT{XFER_TO_HOST} 		= $C{$opt_J."_XFER_TO_HOST"};
	$OUTPUT{XFER_SCRATCH_DIR} 	= $C{$opt_J."_XFER_SCRATCH_DIR"};

	$OUTPUT{SRVR_DIR_BY_BKSVR} = $OUTPUT{REMOTE_DIR}."/".$OUTPUT{DSQUERY};
	$OUTPUT{DUMP_DIR_BY_BKSVR} = $OUTPUT{REMOTE_DIR}."/".$OUTPUT{DSQUERY}."/dbdumps";
	$OUTPUT{TRAN_DIR_BY_BKSVR} = $OUTPUT{REMOTE_DIR}."/".$OUTPUT{DSQUERY}."/logdumps";

	$OUTPUT{NT_PERL_LOCATION}	= 	$C{NT_PERL_LOCATION};
	$OUTPUT{NT_CODE_LOCATION}	= 	$C{NT_CODE_LOCATION};
	$OUTPUT{UNIX_PERL_LOCATION}	=	$C{UNIX_PERL_LOCATION};
	$OUTPUT{UNIX_CODE_LOCATION} 	=	$C{UNIX_CODE_LOCATION};
	$OUTPUT{CODE_LOCATION_ALT1} 	=	$C{CODE_LOCATION_ALT1};
	$OUTPUT{CODE_LOCATION_ALT2} 	=	$C{CODE_LOCATION_ALT2};

	print "CommonFunc.pm: copying data at ".scalar(localtime(time))."\n" if defined $args{-debug};
	if( ! defined $args{-job} and ! defined $opt_S ) {
		foreach( keys %C ) {
			$OUTPUT{$_}=$C{$_} unless defined $OUTPUT{$_};
		}
	}

	print "CommonFunc.pm: final validation at ".scalar(localtime(time))."\n" if defined $args{-debug};
	if( $OUTPUT{XFER_BY_FTP} eq "y" ) {
		unless( defined $OUTPUT{XFER_TO_HOST}
			and 	$OUTPUT{XFER_TO_HOST} !~ /^\s*$/ ){
				my $msg=$errstr."IF XFER_BY_FTP is defined for $opt_J, then XFER_TO_HOST must also be\n";
				if( defined $args{-nodie} ) {
					push @errors,$msg;
				} else {
					die $msg;
				}
		}
		unless ( defined $OUTPUT{XFER_SCRATCH_DIR} and $OUTPUT{XFER_SCRATCH_DIR} !~ /^\s*$/ ){
			my $msg=$errstr."IF XFER_BY_FTP is defined for $opt_J, then XFER_SCRATCH_DIR must also be";
			if( defined $args{-nodie} ) {
				push @errors,$msg;
			} else {
					die $msg;
			}
		}
	}

	unless ( $OUTPUT{NUM_BACKUPS_TO_KEEP}>=1 ) {
			my $msg=$errstr."NUM BACKUPS TO KEEP MUST BE >=1";
			if( defined $args{-nodie} ) {
				push @errors,$msg;
			} else {
				die $msg;
			}
	}
	if( defined $OUTPUT{SYBASE} and ! -d $OUTPUT{SYBASE} ){
			my $msg=$errstr."SYBASE ($OUTPUT{SYBASE}) MUST BE A DIRECTORY";
			if( defined $args{-nodie} ) {
				push @errors,$msg;
			} else {
				warn $msg;
			}
	}


	$OUTPUT{NT_PERL_LOCATION}	=~ s.\\.\/.g;
	$OUTPUT{NT_CODE_LOCATION}	=~ s.\\.\/.g;
	$OUTPUT{UNIX_PERL_LOCATION}	=~ s.\\.\/.g;
	$OUTPUT{UNIX_CODE_LOCATION} 	=~ s.\\.\/.g;
	$OUTPUT{CODE_LOCATION_ALT1} 	=~ s.\\.\/.g if defined $OUTPUT{CODE_LOCATION_ALT1};
	$OUTPUT{CODE_LOCATION_ALT2} 	=~ s.\\.\/.g if defined $OUTPUT{CODE_LOCATION_ALT2};
	$OUTPUT{BASE_BACKUP_DIR} 	=~ s.\\.\/.g;

	# first we try to replace base backup directory
	print "CommonFunc.pm: checking base backup dir at ".scalar(localtime(time))."\n"
		if defined $args{-debug};

	unless ( -d $OUTPUT{BASE_BACKUP_DIR} ){

		# ok now do some stuff to alter paths as needed...
		# print "UNIX_CODE_LOCATION 	$OUTPUT{UNIX_CODE_LOCATION}\n";
		# print "NT_CODE_LOCATION 	$OUTPUT{NT_CODE_LOCATION}\n";
		# print "CODE_LOCATION_ALT1 	$OUTPUT{CODE_LOCATION_ALT1}\n";
		# print "CODE_LOCATION_ALT2 	$OUTPUT{CODE_LOCATION_ALT2}\n";
		# print "BASE_BACKUP_DIR 		$OUTPUT{BASE_BACKUP_DIR}\n";

		my($found)="TRUE";
		my($dir)=$OUTPUT{BASE_BACKUP_DIR};
		if( $OUTPUT{UNIX_CODE_LOCATION} !~ /^\s*$/
			and $OUTPUT{BASE_BACKUP_DIR} =~/$OUTPUT{UNIX_CODE_LOCATION}/ ) {
				$dir=~s/$OUTPUT{UNIX_CODE_LOCATION}//;
		} elsif(  $OUTPUT{NT_CODE_LOCATION} !~ /^\s*$/
			and $OUTPUT{BASE_BACKUP_DIR} =~/$OUTPUT{NT_CODE_LOCATION}/ ) {
				$dir=~s/$OUTPUT{NT_CODE_LOCATION}//;
		} elsif(  $OUTPUT{CODE_LOCATION_ALT1} !~ /^\s*$/
			and $OUTPUT{BASE_BACKUP_DIR} =~/$OUTPUT{CODE_LOCATION_ALT1}/ ) {
				$dir=~s/$OUTPUT{CODE_LOCATION_ALT1}//;
		} elsif(  $OUTPUT{CODE_LOCATION_ALT2} !~ /^\s*$/
			and $OUTPUT{BASE_BACKUP_DIR} =~/$OUTPUT{CODE_LOCATION_ALT2}/ ) {
				$dir=~s/$OUTPUT{CODE_LOCATION_ALT2}//;
		} else {
			$found="FALSE";
		}
		if( $found eq "TRUE" ) {
			$dir =~ s/^\///;
			if( -d "$OUTPUT{UNIX_CODE_LOCATION}/$dir" ) {
				# print "Setting to $dir in UNIX $OUTPUT{UNIX_CODE_LOCATION} \n";
				$OUTPUT{BASE_BACKUP_DIR}= "$OUTPUT{UNIX_CODE_LOCATION}/$dir";
			} elsif( -d "$OUTPUT{NT_CODE_LOCATION}/$dir" ) {
				# print "Setting to $dir in NT $OUTPUT{NT_CODE_LOCATION} \n";
				$OUTPUT{BASE_BACKUP_DIR}= "$OUTPUT{NT_CODE_LOCATION}/$dir";
			} elsif( -d "$OUTPUT{CODE_LOCATION_ALT1}/$dir" ) {
				# print "Setting to $dir in ALT1 $OUTPUT{CODE_LOCATION_ALT1} \n";
				$OUTPUT{BASE_BACKUP_DIR}= "$OUTPUT{CODE_LOCATION_ALT1}/$dir";
			} elsif( -d "$OUTPUT{CODE_LOCATION_ALT2}/$dir" ) {
				# print "Setting to $dir in ALT2 $OUTPUT{CODE_LOCATION_ALT2} \n";
				$OUTPUT{BASE_BACKUP_DIR}= "$OUTPUT{CODE_LOCATION_ALT2}/$dir";
			}
			print "CommonFunc.pm: BASE_BACKUP_DIR is now $OUTPUT{BASE_BACKUP_DIR}\n"
				if -d $OUTPUT{BASE_BACKUP_DIR} and defined $args{-debug};
		}

		unless( defined $OUTPUT{BASE_BACKUP_DIR} ){
			my $msg=$errstr."BASE_BACKUP_DIR MUST BE DEFINED\nBackup Software Has Not Been Initialized.";
			if( defined $args{-nodie} ) {
				push @errors,$msg;
			} else {
				die $msg;
			}
		}
		if( defined $OUTPUT{BASE_BACKUP_DIR} and ! -d $OUTPUT{BASE_BACKUP_DIR} ){
			my $msg=$errstr."BASE_BACKUP_DIR  $OUTPUT{BASE_BACKUP_DIR} IS NOT A DIRECTORY.\n\nThe BASE_BACKUP_DIR is the location of all the logs and output files\nfrom the backup scripts.\n";
			$msg=$errstr."BASE_BACKUP_DIR IS NOT DEFINED.\nBackup Software Has Not Been Initialized.\n" if ! defined $OUTPUT{BASE_BACKUP_DIR} ;
			$msg.="Alternate Location FROM UNIX_CODE_LOCATION = $OUTPUT{UNIX_CODE_LOCATION}\n"
				if $OUTPUT{UNIX_CODE_LOCATION};
			$msg.="Alternate Location FROM NT_CODE_LOCATION =   $OUTPUT{NT_CODE_LOCATION}\n"
				if $OUTPUT{NT_CODE_LOCATION};
			$msg.="Alternate Location FROM CODE_LOCATION_ALT1 = $OUTPUT{CODE_LOCATION_ALT1}\n"
				if $OUTPUT{CODE_LOCATION_ALT1};
			$msg.="Alternate Location FROM CODE_LOCATION_ALT2 = $OUTPUT{CODE_LOCATION_ALT2}\n"
				if $OUTPUT{CODE_LOCATION_ALT2};

			if( defined $args{-nodie} ) {
				push @errors,$msg;
			} else {
				die $msg;
			}
		}
	}

	if( $OUTPUT{IS_REMOTE} eq "y" and defined $opt_J ) {
		unless ( defined $OUTPUT{REMOTE_HOST} ) {
			my $msg=$errstr."Configuration Error: IS_REMOTE is set in $filename but REMOTE_HOST is not.  Please seek help.  If the dump directory for server ".$OUTPUT{DSQUERY}." can not be viewed as a local filesystem (via nfs or windows networking), you must define the configuration value for REMOTE_HOST.  ";
			if( defined $args{-nodie} ) {
				push @errors,$msg;
			} else {
				die $msg;
			}
		}

		unless ( defined $OUTPUT{REMOTE_DIR} ) {
			my $msg=$errstr."Configuration Error: IS_REMOTE is set in $filename but REMOTE_DIR is not.  Please seek help.  If the dump directory for server ".$OUTPUT{DSQUERY}." can not be viewed as a local filesystem (via nfs or windows networking), you must define the configuration value for REMOTE_DIR.  ";
			if( defined $args{-nodie} ) {
				push @errors,$msg;
			} else {
				die $msg;
			}
		}


		if( ! defined $OUTPUT{REMOTE_HOST} or $OUTPUT{REMOTE_HOST} =~ /^\s*$/ ) {
			my $msg=$errstr."REMOTE_HOST Variable Must Be Set For this Job (in configure.cfg)\n";
			if( defined $args{-nodie} ) {
				push @errors,$msg;
			} else {
				die $msg;
			}
		}
	}
	$OUTPUT{ERRORS}=\@errors if $#errors>=0;

	print "CommonFunc.pm: returning configuration info at ".scalar(localtime(time))."\n" if defined $args{-debug};
#print "BASE_BACKUP_DIR is $OUTPUT{BASE_BACKUP_DIR}\n";

	return(%OUTPUT);
}

sub is_interactive {
	# returns 1/0 if you are interactive
	return -t STDIN && -t STDOUT;
}

# Check for Lock File and Dont Restart unless it exists
# Args: -threshtime , -filename
sub Am_I_Up
{
	my(%args)=@_;

	print "Am_I_Up(@_) called\n" if defined $args{-debug};
	my($filename,$filetimesecs,$threshtime)=GetLockfile(%args);
	print "File=$filename Ping=$filetimesecs Thres=$threshtime\n" if $args{-debug};
	my($rc)=0;
	if( defined $filetimesecs ) {
		if( $filetimesecs > $threshtime ) {
			$rc=0;
		} else {
			$rc=1;
			print "Am_I_Up() returning $rc (not touching file)\n" if defined $args{-debug};
			return $rc;
		}
	} else {
		print "Am_I_Up() file does not exists\n" if defined $args{-debug};
	}
#	if( defined $args{-filename} ) {
#		$filename = $args{-filename};
#	} elsif( defined $args{-dir} ) {
#		$filename= $args{-dir}."/".hostname()."_".basename($0).".lck";
#	} else {
#		my($root)=get_gem_root_dir();
#		if( -d "$root/data/lockfiles" ) {
#			$filename=$root."/data/lockfiles/".hostname()."_".basename($0).".lck";
#		} else {
#			print "Am_I_Up():  no $root/data/lockfiles directory\n" if defined $args{-debug};
#			$filename=hostname()."_".basename($0).".lck";
#		}
#	}
#	print "Am_I_Up() filename=$filename\n" if defined $args{-debug};
#
#	my($rc);
#	if( -e $filename ) {
#		print "Am_I_Up() file exists\n" if defined $args{-debug};
#		die "Lock File Is UnWritable ($filename)"
#			unless -w $filename;
#
#		if( $args{-threshtime} ) {
#			$threshtime = $args{-threshtime};
#		} else {
#			$threshtime=1200;	# twenty minutes
#		}
#
#		my($filetimesecs)= (-M $filename)*24*60*60;
#		if( $filetimesecs > $threshtime ) {
#			$rc=0;
#		} else {
#			$rc=1;
#			print "Am_I_Up() returning $rc (not touching file)\n" if defined $args{-debug};
#			return $rc;
#		}
#	} else {
#		print "Am_I_Up() file does not exists\n" if defined $args{-debug};
#		$rc=0;
#	}
	$args{-filename}=$filename;
	I_Am_Up(%args);
	return $rc;
}
sub I_Am_Down
{
	my(%args)=@_;

	print "I_Am_Down(@_) called\n" if defined $args{-debug};
	my($filename,$filetimesecs,$threshtime)=GetLockfile(%args);

	#if( defined $args{-filename} ) {
	#	$filename = $args{-filename};
	#} elsif( defined $args{-dir} ) {
	#	$filename= $args{-dir}."/".hostname()."_".basename($0).".lck";
	#} else {
	#	my($root)=get_gem_root_dir();
#		if( -d "$root/data/lockfiles" ) {
	#		$filename=$root."/data/lockfiles/".hostname()."_".basename($0).".lck";
	#	} else {
	#		print "Am_I_Up():  o $root/data/lockfiles directory\n" if defined $args{-debug};
	#		$filename=hostname()."_".basename($0).".lck";
	#	}
	#}

	unlink $filename if -r $filename;
}

sub GetLockfile {
	my(%args)=@_;
	my($filename,$threshtime);

	my($prefix);
	if( defined $args{-prefix} ) {
		$prefix = $args{-prefix}."_";
	} else {
		$prefix="";
	};

#foreach (keys %args ) { print "$_ = $args{$_} \n"; }
	if( defined $args{-filename} ) {
		$filename = $args{-filename};
	} elsif( defined $args{-dir} ) {
		$filename= $args{-dir}."/".$prefix.hostname()."_".basename($0).".lck";
	} else {
		my($root)=get_gem_root_dir();
		if( -d "$root/data/lockfiles" ) {
			$filename=$root."/data/lockfiles/".$prefix.hostname()."_".basename($0).".lck";
		} else {
			$filename=$prefix.hostname()."_".basename($0).".lck";
		}
	}

	my($rc);
	if( -e $filename ) {
		die "Lock File Exists But Is UnWritable ($filename) host=".hostname()
			unless -w $filename;
		my($filetimesecs)= (-M $filename)*24*60*60;
		if( $args{-threshtime} ) {
			$threshtime = $args{-threshtime};
		} else {
			$threshtime=1200;	# twenty minutes
		}
		return $filename, $filetimesecs, $threshtime;
	} else {
		return $filename,undef, undef;
	}
}

sub I_Am_Up
{
	my(%args)=@_;

	print "I_Am_Up(@_) called\n" if defined $args{-debug};
	my($filename,$filetimesecs,$threshtime)=GetLockfile(%args);
	print "File=$filename File=$filetimesecs Thres=$threshtime\n" if $args{-debug};
	my($rc)=0;
	if( defined $filetimesecs ) {
		if( $filetimesecs > $threshtime ) {
			$rc=0;
		} else {
			$rc=1;
		}
	}
#	if( defined $args{-filename} ) {
#		$filename = $args{-filename};
#	} elsif( defined $args{-dir} ) {
#		$filename= $args{-dir}."/".hostname()."_".basename($0).".lck";
#	} else {
#		my($root)=get_gem_root_dir();
#		if( -d "$root/data/lockfiles" ) {
#			$filename=$root."/data/lockfiles/".hostname()."_".basename($0).".lck";
#		} else {
#			print "I_Am_Up() No $root/data/lockfiles directory\n" if defined $args{-debug};
#			$filename=hostname()."_".basename($0).".lck";
#		}
#	}
#	print "I_Am_Up() filename=$filename\n" if defined $args{-debug};
#
#	my($rc);
#	if( -e $filename ) {
#		print "I_Am_Up() file exists\n" if defined $args{-debug};
#		die "Lock File Is UnWritable ($filename)"
#			unless -w $filename;
#
#		if( $args{-threshtime} ) {
#			$threshtime = $args{-threshtime};
#		} else {
#			$threshtime=1200;	# twenty minutes
#		}
#
#		my($filetimesecs)= (-M $filename)*24*60*60;
#		if( $filetimesecs > $threshtime ) {
#			$rc=0;
#		} else {
#			$rc=1;
#		}
#	} else {
#		print "I_Am_Up() file ($filename) does not exist\n" if defined $args{-debug};
#		$rc=0;
#	}
#
	print "I_Am_Up() creating $filename\n" if defined $args{-debug};
	open(OUT,">$filename") or die "Cant write $filename: $!";
	print OUT "Lock File Written\n";
	close(OUT);

	return $rc;
}

1;

__END__

=head1 NAME

CommonFunc.pm - Common Function Library

=head2 DESCRIPTION

Standard/Miscellaneous functions

=head2 SUMMARY

 get_version() - returns version of the gem ($version,$sht_version)
 is_nt()       - return 1/0 if you are on NT/Windows
 cd_home()     - cd to the directory that contains the main code and return that directory
 today()       - returns date string yyyymmdd.hhmiss
 is_up()       - writes lock file useful to determine if program is allready running

