# copyright (c) 2005-2006 by SQL Technologies.  All Rights Reserved.

package GemData;

use strict;
use vars qw($VERSION @ISA @EXPORT @EXPORT_OK);
use Exporter;
use strict;
use CommonFunc;
use Repository;
use XML::Simple;
use Data::Dumper;
use Carp qw(confess cluck);
use File::Basename;
use Carp;
use Net::myFTP;
use DBIFunc;
use Do_Time;
use RosettaStone;
use File::Compare;

$VERSION= 1.0;

@ISA      = (	'Exporter'	);
@EXPORT   = ( 	'dump', 'save', 'get_set_data', 'get_reports', 'run_reports',
			'get_install_state', 'get_install_color', 'SetInstallState' );

my($gemfile);
my($gemdata);
my($hostname);
my($printfunc,$debugfunc);
my($DATA_NEEDS_WRITE)="FALSE";

sub new {
	my($class)=shift;
	my(%args)=@_;

	$gemfile= $args{-file}  || get_gem_root_dir()."/conf/gem.xml";
	$gemfile= "conf/gem.xml" if ! -r $gemfile and -r "conf/gem.xml";
	$gemfile= "gem.xml" if ! -r $gemfile and -r "gem.xml";

	$printfunc = $args{-printfunc};
	$debugfunc = $args{-debugfunc};
	$hostname  = $args{-hostname};
	
	croak "gemdata->new() Can Not Read $gemfile File" unless -e $gemfile;
	print "XML Configuration File: $gemfile\n" if $args{-debug};

	my( @myforcearray )=("discovered_files","fileinfo","oracle","sqlsvr","unix","win32servers","sybase");
	print "Reading $gemfile\n" if $args{-debug};
	$gemdata =  XMLin( $gemfile, forcearray=>\@myforcearray );
	my $self = bless { %args }, $class;

	my($v) = $self->get_set_data( -class=>'version', -name=>'xml' );
	if( ! defined $v ) {
		print "Unknown XML Version found in the configfile.\n  This is probably an upgrade from beta.\n Setting to $VERSION\n";
		my($v) = $self->get_set_data( -class=>'version', -name=>'xml', -value=>$VERSION );
	} elsif( $v != $VERSION ) {
		print "XML Version is $v but version $VERSION is required\n";
	} else {
		print "XML Version: $v\n" if $args{-debug};
	}
	return $self;
}

# for backwards compatibility only.... dont use db_param
sub db_param {
	my($package)=shift;
	my($key)=@_;
	if( $key eq "sybase_servers" ) {
		# return space separated list of servers
		my($rc)=$package->get_set_data(-class=>'sybase',-keys=>1);
		return join(" ",@$rc);
	}
}

sub get_reports {
	my($package)=shift;
	my(%args)=@_;

	return get_schema_info(-reportname=>"objreports") if $args{-type} eq "objreports";

	return get_schema_info(-reportname=>"allreports") if $args{-type} eq "database";

     	return ('Summary', 'Configuration', 'Database', 'Storage', get_system_info(-reportname=>"gemreports"))
     			 if $args{-type} eq "server";

	return undef;

}

sub run_reports {
	my($package)=shift;
	my(%args)=@_;
	$package->survey_says(
		"[GemData] Running $args{-type} $args{-report} Report on $args{-servername} $args{-database}" );
	$args{-report}=lc($args{-report});

	my($t);
	if( $args{-type} ne "database" ) {
		if( $args{-report} eq 'summary' ) {
     			$t = main::global_data(
     						-class=>$args{-servertype},
     						-name=>$args{-servername});
     		} elsif( $args{-report} eq 'configuration' ) {
     			$t = main::global_data(
     						-class=>$args{-servertype},
     						-name=>$args{-servername},
     						-arg=>'Configuration');
     		} elsif( $args{-report} eq 'database' ) {
     			$t = main::global_data(
     						-class=>$args{-servertype},
     						-name=>$args{-servername},
     						-arg=>'Database_Info');
     		} elsif( $args{-report} eq 'storage' ) {
     			$t = main::global_data(
     						-class=>$args{-servertype},
     						-arg=>'DeviceSize',
     						-name=>$args{-servername});
     		}
     		if( defined $t) {
			$package->show_that_hash($t,
				$args{-textObject},
				"Running $args{-report} Report For $args{-server} At ".localtime(time)."\n\n",
				1,
				$args{report});
		}
	}

#     	 elsif( $args{-report} eq 'vdev' ) {
#     		} elsif( $args{-report} eq 'audit' ) {
#			$query=	"sp__auditsecurity \@srvname='".$args{-servername}."'";
#			$each_db_query="sp__auditdb \@srvname='".$args{-servername}."'";
#     		} elsif( $args{-report} eq 'space' ) {
#			$query        = "exec sp__dbspace";
#     			$each_db_query= "exec sp__dbspace";
#     		} elsif( $args{-report} eq 'layout#1' ) {
#			$query="sp__helpdbdev \@fmt=\"ND\", \@dont_format=\"Y\"";
#     		} elsif( $args{-report} eq 'layout#2' ) {
#			$query="sp__helpdbdev \@fmt=\"P\", \@dont_format=\"Y\"";
#     		} elsif( $args{-report} eq 'login' ) {
#			$query="sp__helplogin \@dont_format='Y'";
#     		} elsif( $args{-report} eq 'revdb' ) {
#			$query="sp__revdb";
#    		} elsif( $args{-report} eq 'revdev' ) {
#			$query="sp__revdevice";
#		}

	if( ! defined $t ) {
        	if( $args{-servertype} ne "sqlsvr" and $args{-servertype} ne "sybase" ) {
        		$args{-textObject}->insert('end',"Only sqlsvr and sybase currently supported\n", "t_hdr");
        		$args{-textObject}->insert('end',"You ran this report for $args{-servertype}\n", "t_hdr");
        		$args{-textObject}->insert('end',"No Data Found For Report\n", "t_hdr");
        		return;
        	}

        	#$args{window}->Label( -text=>"Insert Results of $query on $args{-servername}/$args{-servertype}/$args{-database}",
        	#	-relief=>'ridge' , -bg=>'red' )->pack( -side=>'top', -fill=>'x' );

        	# run the query
        	#$package->survey_says(  "[GemData] CONNECTING TO $args{-servername} TYPE as $args{-servertype} \n" );
        	my($c,$errmsg)=$main::connectionmanager->connect(-name=>$args{-servername}, -type=>$args{-servertype} );
        	main::colorize_tree($args{-servername});
        	if( ! $c ) {
        		$args{-label_variable}="[GemData] unable to connect to $args{-servername}";
        		$package->survey_says("[GemData] unable to connect to $args{-servername}\n");
        		$package->survey_says( "[GemData]  error: unable to connect to $args{-servername}\n" );
        		return;
        	}

 	      	$args{-label_variable}="Fetching Results for ".$args{-report}." on ". $args{-servername}."/".
 	      		$args{-servertype}."/".$args{-database};
        	$package->survey_says("[GemData] $args{-label_variable}\n");

        	my($q_starttime)=time;
        	$args{-textObject}->delete("1.0","end");

		my(@rc);
		if( $args{-type} eq "server" ) {
			@rc=get_system_info(-connection=>$c	,
				-db_type=>$args{-servertype},
				#-schema	=> $args{-database},
				-reportname=>$args{-report},
				-alldb=>main::global_data(
						-class=>$args{-servertype},
						-name=>$args{-servername},
						-arg=>"Database_String"),
				-debug=>$package->{-debug},
				-returntype=>"report"	);
		} else {
			@rc=get_schema_info(-connection=>$c	,
				-db_type=>$args{-servertype},
				-schema	=> $args{-database},
				-reportname=>$args{-report},
				-alldb=>main::global_data(
						-class=>$args{-servertype},
						-name=>$args{-servername},
						-arg=>"Database_String"),
				-debug=>$package->{-debug},
				-returntype=>"report"	);
		}
		if( $#rc<0 ) {
			croak "gemdata->run_reports() REPORT IS  NOT DEFINED";
		}

        	my($font)="t_hdr";
        	foreach (@rc) {
        		$_=uc($_) if $font eq "t_hdr";
        		$args{-textObject}->insert('end',$_."\n", $font);
        		$font="t_blue";
        	}
        	$args{-textObject}->insert('end',"\n", $font);
        }

        $args{-label_variable}="Completed: Results for ".$args{query}." on ".
        			$args{-servername}."/".$args{-servertype}."/".$args{-database};
        $package->survey_says(  "[GemData] Completed query on $args{-servername}\n" );
}

#        	foreach ( split(/\n/,$query) ) {
#                    	my(@rc) = $main::connectionmanager->query(
#                    		-query=>$_,
#                    		#-debug=>4,
#                    		-connection=>$c,
#                    		-db=>$args{-database},
#                    		-print_hdr=>1 );
#                    	$package->survey_says("[GemData] query completed : ".($#rc+1)." rows returned\n");
#
#                    	if( defined $each_db_query ) {
#                    		# db list
#                    		foreach ( split( /\s+/, main::global_data( -class=>$args{-servertype},
#								-name=>$args{-servername}, -arg=>"Database_String") )) {
#                    			print "RUNNING $each_db_query} IN DB $_!!!\n";
#                    			push @rc, $main::connectionmanager->query(
#                    				-query=>$each_db_query,
#                    				-connection=>$c,
#                    				-db=>$_	);
#                    		}
#                	}
#
#                	my( @results ) = dbi_reformat_results(@rc);
#        		my($font)="t_hdr";
#        		foreach (@results) {
#        			$_=uc($_) if $font eq "t_hdr";
#        			$args{-textObject}->insert('end',$_."\n", $font);
#        			$font="t_blue";
#        		}
#        		$args{-textObject}->insert('end',"\n", $font);

#
##################################################################################################
#   Button Name					Button 	
#							
#   AGREE_WITH_LICENSE			Welcome.AGREE_WITH_LICENSE							By Server
#   REGISTER_BUTTON				Register.REGISTER YOUR VERSION					By Server
#   MAIL_BUTTON					Mail.TestMailInstall									By Server
#	 ADD_SERVER_WIZARD			Servers.AddServerWizard								By Server
#   CONNECT_SURVEY				Connections.SurveyAll								By Server
#   CONNECT_TO_ALL				Connections.ConnectAll								By Server
#   ALARM_INSTALL_UPDATE		Alarms.Install/Upgrade								By Server
#   ALARM_VALIDATE_ALARM		Alarms.ValidateAlarmInstallation					By Server
#   FINAL_INSTALL_SOFTWARE		Install.InstallSoftware								By Server
#   FINAL_REFORMAT_CODE			Install.ReformatCode									By Server
#   FINAL_CREATE_BATCHES		Install.CreateBatchScritps							By Server
#   FINAL_INSTALL_BACKUP_MGR 	Install.BackupManager								By Server
#   FINAL_BUILD_CONSOLE 		Install.BuildConsole									By Server
#   FINAL_INSTALL_PROCS			Install.InstallProcedureLibrary					By Server
#								also CONNECT_INSTALL_PROCS	(Procs.InstalProcs)		By Server
#   SAVE_CONFIGURATION_CHANGES All														By Server
#	 Console_Setup_Step_1			PostInstalltasks.SnapshotWin32Services			By Server
#	 Console_Setup_Step_2			PostInstalltasks.PopulateWin32BackupState		By Server
#	 Console_Setup_Step_4			PostInstalltasks.CreatePortMonitor				By Server
#	 Console_Setup_Step_5			PostInstalltasks.CollectConsoleData				By Server
#	 Console_Setup_Step_6			PostInstalltasks.BuildTheConsole					By Server
#	 Console_Setup_Step_7			PostInstalltasks.ViewTheConsole					By Server
##################################################################################################
#
sub get_install_key {
	my($package,$key)=@_;
	cluck "DBG DBG: Hmmm get_install_key() key is undef???" unless defined $key;
	return $key 			if $key eq "REGISTER_BUTTON" 
								or $key eq "ALARM_INSTALL_UPDATE"
								or $key eq "ALARM_VALIDATE_ALARM";	
	return $hostname.".".$key;
}

sub get_install_color {
	my($package,$key)=@_;
	my($rc)=$package->get_install_state($key);
	return "white" if $rc;
	return "yellow";
}

sub SetInstallState {
	my($package,$key,$unsetflag)=@_;
	croak "gemdata->SetInstallState() NO HOSTNAME DEFINED TO GemData->new()" unless $hostname;
	return $package->get_set_data(-class=>"installsteps", 
					-name=>$package->get_install_key($key), -value=>undef)
		if $unsetflag;
	return $package->get_set_data(-class=>"installsteps", 
		-name=>$package->get_install_key($key), -value=>time);
}

sub get_install_state {
	my($package,$key)=@_;
	croak "gemdata->get_install_state() NO HOSTNAME DEFINED TO GemData->new()" unless $hostname;
	return $package->get_set_data(-class=>"installsteps", -name=>$package->get_install_key($key));
}

sub show_that_hash {

	my($package,$thehashref,$textObject,$title,$do_delete,$reportname)=@_;
	$textObject->delete("1.0","end") if $do_delete;
	$textObject->insert('end', $title, "t_hdr") if $title ne "";
	my($font)="t_blue";
	if( ! defined $thehashref ) {
		$textObject->insert('end',"No Data Found For Report\n", $font);
	} elsif( $reportname eq "Configuration"
	or $reportname eq "Database Layout"
	or $reportname eq "Disk/Storage Layout") {
		# these reports are pure tables

		my(@keys);
		@keys=("category","comment","default","value") if $reportname eq "Configuration";
		@keys=("key","Space_Allocated","LogSpaceByDb","DBoptions") if $reportname eq "Database Layout";
		@keys=("key","size","alloc","free","phys","growth","maxsize") if $reportname eq "Disk/Storage Layout";

		# GET THE LENGTHS
		my(%maxsize_by_key);
		foreach (@keys) {
			$maxsize_by_key{$_} = length;
		}
		foreach my $item (keys %$thehashref) {
			if( ! ref $$thehashref{$item} eq "HASH" ) {
				croak "gemdata->show_that_hash() DBG: ITS NOT A HASH REF $item / $$thehashref{$item}\n";
			}
			my(%href) = %{$$thehashref{$item}};
			foreach(@keys) {
				my($l)=length($href{$_});
				$l=length($item) if $_ eq "key";
				$maxsize_by_key{$_} = $l if $l>$maxsize_by_key{$_};
			}
		}
		foreach( keys %maxsize_by_key ) { $maxsize_by_key{$_}++; }

		# RUN THE REPORT
		my($rowid)=0;
		foreach my $item (sort keys %$thehashref) {
			my(%href) = %{$$thehashref{$item}};
			if( $rowid==0 ) {
				foreach (@keys) {
					my($fmt)="%-".$maxsize_by_key{$_}.".".$maxsize_by_key{$_}."s";
					$textObject->insert('end',sprintf($fmt, ucfirst($_)), "t_orange");
				}
				$textObject->insert('end',"\n");
			}
			foreach (@keys) {
				my($fmt)="%-".$maxsize_by_key{$_}.".".$maxsize_by_key{$_}."s";
				if( $_ eq "key" ) {
					$textObject->insert('end',sprintf($fmt, $item), "t_blue");
				} else {
					$textObject->insert('end',sprintf($fmt, $href{$_}), "t_blue");
				}
			}
			$textObject->insert('end',"\n");
			$rowid++;
		}
	} else {
		foreach (sort keys %$thehashref) {
			next if /^Configuration/ and $reportname ne "Configuration";
			next if /^DeviceSize/ and $reportname ne "Disk/Storage Layout";
			next if /^Database_Info/ and $reportname ne "Database Layout";

			if( ref $$thehashref{$_} eq "HASH" ) {
				$textObject->insert('end',sprintf("%20.20s:\n", $_), $font);
				my(%href) = %{$$thehashref{$_}};
				foreach( keys %href ) {
					if( ref $href{$_} ) {
						$textObject->insert('end',sprintf("         %20.20s: ", $_), "t_orange");
						my($str);
						my(%href2)=%{$href{$_}};
						foreach( keys %href2 ) {
							$str.="$_=>$href2{$_} ";
						}
						$textObject->insert('end', $str.":\n", "t_blue");
					} else {
						$textObject->insert('end',sprintf("         %14.14s: %s\n", $_, $href{$_}), "t_orange");
					}
				}
			} else {
				my($val)=$$thehashref{$_};
				$val=do_time(-fmt=>'yyyy/mm/dd hh:mm',-time=>$val)
					if /_time$/;
				$val=~s/\.0+$//;
				$textObject->insert('end',sprintf("%20.20s: %s\n", $_, $val), $font);
			}
		}
	}
}

sub get_data { my $package=shift; return $package->get_set_data(@_); }
sub set_data { my $package=shift; return $package->get_set_data(@_); }
sub get_set_data {
	my($package)=shift;
	my(%args)=@_;

	if( $args{-debug} ) {
		my($str);
		if( $args{-value} ) {
			$str="  set_data(";
		} else {
			$str="  get_data(";
		}

		foreach ( keys %args ) { $str .= $_."->".$args{$_}." "; }
		$str .= ")\n";
		$package->survey_says( "[get_set] ".$str );
	}

   	if( defined $args{-delete} ) {
		if( defined $args{-class} ) {
			if( defined $args{-name} ) {
				if( defined $args{-arg} ) {
					delete $$gemdata{$args{-class}}{$args{-name}}{$args{-arg}};
				} else {
					delete $$gemdata{$args{-class}}{$args{-name}};
				}
			} else {
					delete $$gemdata{$args{-class}};
			}
		} else {
			croak "gemdata->get_set_data() Bad -delete syntax";
		}
	}

	# populate stuff that doesnt exist
	if( defined $args{-class} ) {
		if( defined $args{-name} and ! defined $$gemdata{$args{-class}}  ) {
			$package->survey_says( "[GemData] NEW CLASS $args{-class} IN XML DATA\n" );
			my(%x);
			$$gemdata{$args{-class}}=\%x;
		}
		if( defined $args{-name} ) {
			if( defined $args{-arg} and ref($$gemdata{$args{-class}}) eq "ARRAY" ) {
				# hmmm want to set argument but no key value around... first time?
				my(%x);
				$$gemdata{$args{-class}}=\%x;
			}
			if( defined $args{-arg} and ! ref $$gemdata{$args{-class}}  ) {
				$package->survey_says("Found Name\n");

				croak "gemdata->get_set_data() ERROR $args{-class} is not a reference";
			}
			if( defined $args{-arg} and ! defined $$gemdata{$args{-class}}->{$args{-name}}  ) {
				$package->survey_says(  "[GemData] new xml hash $args{-name} in class $args{-class}\n" );
				my(%x);
				$$gemdata{$args{-class}}->{$args{-name}}=\%x;
			}
		}
	}

	if( defined $args{-value} ) {
		if( defined $args{-class} ) {
			#print "In -class ",__LINE__,"\n";
			if( ref $$gemdata{$args{-class}} eq "ARRAY" ) {
				my($x)=$$gemdata{$args{-class}};
				if( $#$x == 0 ) {
					my(%a);
					$$gemdata{$args{-class}}=\%a;
				} else {
					croak "gemdata->get_set_data() -class argument has $#$x elements/???\n";
				}
				#print "It has num ", $#$x," elements\n";
			}
			if( defined $args{-name} ) {
				if( defined $args{-arg} ) {
					$$gemdata{$args{-class}}{$args{-name}}{$args{-arg}}  = $args{-value};
				} else {
					
					$$gemdata{$args{-class}}{$args{-name}}  = $args{-value};
					
				}
			} else {				
				$$gemdata{$args{-class}} = $args{-value};				
			}
			
		} else {
			print "NO CLASS ARG PASSED - CAN NOT SAVE \n";
			return undef;
		}
		$DATA_NEEDS_WRITE="TRUE" unless defined $args{-arg} and $args{-arg} =~ /^last/;
	} else {
		# retrieving value

		if( defined $args{-class} ) {
			$package->survey_says( "[get_set] ". " class level => ",ref $$gemdata{$args{-class}}," \n" )
				if $args{-debug} and ref $$gemdata{$args{-class}} ne "HASH";
			if( defined $args{-name} ) {
				print "get_set_data() name level => ",ref $$gemdata{$args{-class}}{$args{-name}}," \n"
					if $args{-debug} and ref $$gemdata{$args{-class}}{$args{-name}} ne "HASH";
				if( defined $args{-arg} ) {
					my($x)=$$gemdata{$args{-class}}{$args{-name}}{$args{-arg}};
					if( defined $args{-keys} ) {
						my(@rc)=keys %$x;
						return \@rc;
					}

					# _survey_says "get_set_data() arg level => $x \n" if $args{-debug};
					#if( ! defined $x ) {
					#	_survey_says("[gem.pl] get_set_data() ERROR - get_set_data() cant retrieve data \n");
					#	print Dumper $$gemdata{$args{-class}};
					#	croak "gemdata->save()Abnormal Termination";
					#}
					# print Dumper $x,"\n" if $args{-debug};

					$package->survey_says("[get_set]  Returning $x\n") if $args{-debug};
					return $x;
				} else {
					#print "DBG: $args{-class} $$gemdata{$args{-class}}\n";
					my($x)=$$gemdata{$args{-class}}{$args{-name}};
					print Dumper $x,"\n" if $args{-debug};
					$package->survey_says("[get_set] Returning $x\n") if $args{-debug};
					return $x;
				}
			} else {
				if( defined $args{-keys} ) {
					# return an array of class keys
					#my($x)= $$gemdata{$args{-class}};
					my(@rc)=keys %{$$gemdata{$args{-class}}};
					return \@rc;
				}
				my($x)= $$gemdata{$args{-class}};
				print Dumper $x,"\n" if $args{-debug};
				print "NO NAME PASSED TO RETRIEVE - returning whole class hash (did you want -keys?).  You can skip this message with -noprint argument"
					unless defined $args{-noprint};	# if $args{-debug};
				return $x;
			}
		} else {
			print "NO CLASS ARG PASSED - THIS IS A NEW KEY\n";
			return undef;
		}
	}	
}

sub dump {
	my($package)=shift;
	my($dir)=dirname($gemfile);
	unlink "$dir/gem.dat" if -r "$dir/gem.dat";
	open(REP,">"."$dir/gem.dat") or croak "gemdata->dump() Cant Write $dir/gem.dat\n";
	print REP Dumper $gemdata;
	close(REP);
	return "$dir/gem.dat";
}

sub survey_says {
	my $package=shift;
	my($str)=join("",@_);
	&$printfunc($str) if defined $printfunc;
	return $str;
}

sub get_server_interfaces {
	my($package)=shift;	
	my($server)=shift;
	$package->build_server_interfaces() unless $package->{interfaces_ref};
	
	if( $server ) {
		$package->survey_says( "[Interfaces] getting port info for $server\n" );
		foreach ( @{$package->{interfaces_ref}} ) {
				next unless $_->[0] eq $server;
				my($s, $host,$port)=( $_->[0], $_->[1], $_->[2] );
				$host=~s/\..+$//;
				$package->survey_says( "[Interfaces] svr=$s h=$host port=$port\n" );
				return ($host,$port);
		}
		$package->survey_says( "NO interface info found from server $server\n" );
		return (undef,undef);
	}
	return @{$package->{interfaces_ref}};
}

sub build_server_interfaces {
	my($package)=shift;
	# use interfaces file to build possible host list
	my($phosts)=$package->get_set_data( -class=>"server_interfaces", -noprint=>1 );
	
	my(%hlist);	
	foreach ( @{$package->get_set_data(-class=>'sybase',-keys=>1)} ) {	$hlist{$_} = ""; 	}
	foreach ( keys %$phosts ) {		$hlist{$_} = $$phosts{$_};	}

	if( $package->{interfaces_ref} ) {		
		foreach ( @{$package->{interfaces_ref}} ) {
			$hlist{$_->[0]} = "$_->[1],$_->[2]" if $_->[1] and $_->[2];
		}
	} elsif( defined $ENV{SYBASE} and -d $ENV{SYBASE} ) {
		my(@x)=dbi_get_interfaces();
		$package->{interfaces_ref}=\@x;
		foreach ( @x ) {
			$hlist{$_->[0]} = "$_->[1],$_->[2]" if $_->[1] and $_->[2];
		}
	} else {
		print "SYBASE not defined\n";
	}

	foreach my $srv ( keys %hlist ) {
		next unless $hlist{$srv} eq "";		
		foreach my $host ( @{$package->get_set_data(-class=>'unix',-keys=>1)} ) {
			my($dat)=$package->get_set_data(-class=>'unix',-name=>$host,-arg=>'discovered_files');
			foreach (keys %{$dat}) {
				if( ref $$dat{$_} ) {
					next unless $$dat{$_}{File_Type} eq "Run File";
					$hlist{$$dat{$_}{Server_Name}} = $host;
				} else {
					# only 1 file i guess
					if( $$dat{"File_Type"} eq 'Run File' ) {
						$hlist{$$dat{Server_Name}} = $host;
					}
				}
			}
		}
	}
	
	foreach ( keys %hlist ) {
		if( $hlist{$_} eq "" ) {
			delete $hlist{$_};				 
			next;
		}
		&$debugfunc("[interfaces] $_ => ".$hlist{$_}."\n") if defined $debugfunc;
	}
	$package->get_set_data( -class=>"server_interfaces", -value=>\%hlist );
}

sub survey {
	my($package)=shift;
	my(%args)=@_;
	my(@rc);
	# print "DBG DBG ",__LINE__," ",join(" ",@_),"\n";
	push @rc, $package->survey_says("[survey] ".$args{-name}.": Started At ".localtime(time)."\n");
	
	foreach ( sort keys %args ) { 
		my($val)=$args{$_} || "";
		if( defined $debugfunc ) {
			&$debugfunc("[survey] ". $args{-name}.": $_ => $val\n");
		} elsif( $args{-debug} ) {
			$package->survey_says("[survey] ". $args{-name}.": $_ => $val\n" )
				 unless /^-password/; 	
		}		
	}			

	if( ! defined $args{-type} or ! defined $args{-name} ) {
		foreach ( keys %args ) { print "Survey ".$args{-name}." - $_ => $args{$_}\n" unless /^-password/; }
		croak "gemdata->survey() Must pass -type and -name to GemData::survey\n";
	}

	my($orig_connection)=$args{-connection};
	$args{-type}=lc($args{-type});

	my($lasttime)=$package->get_set_data( -class=>$args{-type},-name=>$args{-name}, -arg=>"Last_Survey_Time" );
	$lasttime=0 unless defined $lasttime;
	if( $args{-since} and $args{-since}<$lasttime){
		push @rc, $package->survey_says("[survey] ".$args{-name}.": Recent survey (".localtime($lasttime).") - Skipping (wait an hour to resurvey)\n" );
		return @rc;
	}

	my(%DATA);
	$DATA{Last_Survey_Time}=time;
	$DATA{Server_Type}=$args{-type};

	$args{-local_directory}=get_gem_root_dir()."/data/system_information_data"
		unless defined $args{-local_directory};

	mkdir("$args{-local_directory}/$args{-name}",0777) unless -d "$args{-local_directory}/$args{-name}";

	if( $args{-type} eq "unix" ) {
		croak "gemdata->survey() Must pass either -login/-password to GemData::survey for unix ($args{-win32method},$args{-unixmethod})\n"
			unless defined $args{-login} and defined $args{-password};

		croak "gemdata->survey() Must pass -sybase_dirs, -sybase_client_dirs, and -ignore_runfile for unix survey\n"
			unless defined $args{-sybase_dirs}
			and  defined $args{-sybase_client_dirs}
			and  defined $args{-ignore_runfile};

		croak "gemdata->survey() Must Pass -local_directory to survey - directory to collect data into\n"
			unless defined $args{-local_directory};
		croak "gemdata->survey() -local_directory arg to survey does not exist or is not a directory - directory to collect data into\n"
			unless -d $args{-local_directory};

		$DATA{sybase_dirs}			= $args{-sybase_dirs};
		$DATA{sybase_client_dirs}	= $args{-sybase_client_dirs};
		$DATA{ignore_runfile}		= $args{-ignore_runfile};

		my(%IGNORE_RUNFILE);
		foreach ( split(/\s+/,$args{-ignore_runfile})){ $IGNORE_RUNFILE{$_}=1; }

		my($ftp);
		if( $args{-connection} ) {
			$ftp=$args{-connection};
		} else {
			push @rc, $package->survey_says("[survey] ".$args{-name}.": CONNECTING BY HAND - THIS METHOD IS DEPRICATED - SEEK HELP\n" );
			print "[survey] ".$args{-name}.": CONNECTING BY HAND - THIS METHOD IS DEPRICATED - SEEK HELP\n";
			cluck "DBG DBG: Stack trace for future reference...\n";	
		
			my($method);						
			if( is_nt() ) {
				$method=$args{-win32method};
				#$method=$package->get_set_data(-class=>'install',-name=>'win32tounix');
			} else {
				$method=$args{-unixmethod};
				#$method=$package->get_set_data(-class=>'install',-name=>'unixtounix');
			}
			if( ! $method ) {
				warn "Coding Error - Method not defined for server - setting to ftp - seek help\n";
				$method="FTP";
			}				
			
			$ftp = Net::myFTP->new($args{-name},
											PRINTFUNC=>$printfunc, 
											DEBUGFUNC=>$debugfunc, 
											#-reconnect=>1,
											METHOD=>$method,
											Debug=>$args{-debug} );
			if( ! $ftp )  {
				$DATA{Last_Conn_State}="Fail";
				$DATA{Last_Conn_Time}=time;
				$package->get_set_data( -class=>$args{-type},-name=>$args{-name}, -value=>\%DATA );
				push @rc, $package->survey_says("[survey] ".$args{-name}.": Failed To Connect To \"$args{-name}\" Method=$method : $!\n" );
				return @rc;
			}
			
			if( defined $debugfunc ) {
				&$debugfunc("[survey] ". $args{-name}.": Connected via $method\n");
			} elsif( $args{-debug} ) {
				$package->survey_says("[survey] ". $args{-name}.": Connected via $method\n" ); 	
			}		
			
			my($rc,$err)=$ftp->login($args{-login},$args{-password});
			if( ! $rc ) {
				$DATA{Last_Conn_State}="Fail";
				$DATA{Last_Conn_Time}=time;
				$package->get_set_data( -class=>$args{-type},-name=>$args{-name}, -value=>\%DATA );
				push @rc, $package->survey_says("[survey] ".$args{-name}.": Failed To Login To $args{-name} : $err\n" );
				return @rc;
			}
		}		

		$ftp->ascii();

		push @rc, $package->survey_says("[survey] ".$args{-name}.": Chdir / and get list of files\n" ) if $args{-debug};
		if( ! $ftp->cwd("/") ) {
			print "Survey ".$args{-name}.": ERROR: Cant Chdir To / ??? $!\n";			
			push @rc, $package->survey_says("[survey] ".$args{-name}.": ERROR: Cant Chdir To / ??? $!\n" );
			$DATA{Last_Conn_State}="Fail";
			$DATA{Last_Conn_Time}=time;
			$package->get_set_data( -class=>$args{-type},-name=>$args{-name}, -value=>\%DATA );
			
			return @rc;
		}
		$DATA{Last_Conn_State}="Ok";
		$DATA{Last_Conn_Time}=time;
		$DATA{Last_Ok_Conn_Time}=time;

		my(@rootfiles)=$ftp->ls();
  		push @rc, $package->survey_says("[survey] ".$args{-name}.": found ",($#rootfiles+1)," files in root directory\n" );

		my(@sybase_dirs)=split(/\s+/,$args{-sybase_dirs});
		my(@client_dirs)=split(/\s+/,$args{-sybase_client_dirs});
		my(@unknown_dirs);
		foreach ( keys %args ) { 
			push @unknown_dirs, $args{$_} if /^SYBASE\d$/ and defined  $args{$_} and $args{$_}!~/^\s*$/; 
		}
		
		push @rc, $package->survey_says("[survey] ".$args{-name}.": ",($#sybase_dirs+1)," Sybase Directories To Check\n" );
		push @rc, $package->survey_says("[survey] ".$args{-name}.": ",($#client_dirs+1)," Sybase Open Client Directories To Check\n" );
		push @rc, $package->survey_says("[survey] ".$args{-name}.": ",($#unknown_dirs+1)," Potential Sybase Directories To Check\n" );

		my(@founddirs);
		my(@foundfiles);

		my(%FILES);
		my(%searched);
		foreach my $sy_dir (@sybase_dirs) {
			if( $searched{$sy_dir} ) {
				push @rc, $package->survey_says("[survey] ".$args{-name}.": Duplicate $sy_dir - Skipping\n" );
				next;
			}			
			$searched{$sy_dir}=1;
			push @rc, $package->survey_says("[survey] ".$args{-name}.": Searching Sybase Directory $sy_dir\n" );
			my($fileptr,$dirptr) = $package->recurse_directory($ftp,$sy_dir,$args{-debug},"",$args{-name});
			if( defined $fileptr ) {
				push @founddirs, @$dirptr;				
				foreach ( @$fileptr ) {
					push @foundfiles,$_ unless defined $IGNORE_RUNFILE{$_};
				}
			} else {
				push @rc, $package->survey_says("[survey] ".$args{-name}.": Directory $sy_dir May Not Exist\n" );
			}
		}

		foreach my $sy_dir (@client_dirs) {
			if( $searched{$sy_dir} ) {
				push @rc, $package->survey_says("[survey] ".$args{-name}.": Duplicate $sy_dir - Skipping\n" );
				next;
			}
			$searched{$sy_dir}=1;
			push @rc, $package->survey_says("[survey] ".$args{-name}.": Searching Sybase Client Directory $sy_dir\n" );
			my($fileptr,$dirptr) = $package->recurse_directory($ftp,$sy_dir,$args{-debug},"",$args{-name});
			if( defined $fileptr ) {			
				push @founddirs, @$dirptr;
				foreach (@$fileptr) {
					push @foundfiles,$_ unless /RUN_/ or /install/ or /\.log/;
				}
			} else {
				push @rc, $package->survey_says("[survey] ".$args{-name}.": Directory $sy_dir May Not Exist\n" );
			}		
		}

		foreach my $sy_dir (@unknown_dirs) {
			if( $searched{$sy_dir} ) {
				# push @rc, $package->survey_says("[survey] ".$args{-name}.": Duplicate $sy_dir - Skipping\n" );
				next;
			}			
			$searched{$sy_dir}=1;
			push @rc, $package->survey_says("[survey] ".$args{-name}.": Searching Unknown Sybase Directory $sy_dir\n" );
			my($fileptr,$dirptr) = $package->recurse_directory($ftp,$sy_dir,$args{-debug},"",$args{-name});
			if( defined $fileptr ) {			
				push @rc, $package->survey_says("[survey] ".$args{-name}." Directory Exists : $sy_dir\n" );
				push @founddirs, @$dirptr;
				foreach (@$fileptr) {
					push @foundfiles,$_ unless /RUN_/ or /install/ or /\.log/;
				}
			} else {
				push @rc, $package->survey_says("[survey] ".$args{-name}.": $sy_dir Does Not Exist\n" )
					if $args{-debug};
			}		
		}
		
		push @rc, $package->survey_says("[survey] ".$args{-name}.": Completed Reading Layout\n" );
		push @rc, $package->survey_says("[survey] ".$args{-name}.": Local Copies To: $args{-local_directory}/$args{-name}\n" );
		mkdir("$args{-local_directory}/$args{-name}",0777) unless -d "$args{-local_directory}/$args{-name}";

		my(%local_file_list);
		foreach my $curfile ( @foundfiles ) {
			$curfile=~s/\\/\//g;

			# ok now grab the interfaces files
			if( $curfile=~/\/interfaces/ or $curfile=~/\/ini\/sql\.ini/ ) {
				push @rc, $package->survey_says("[survey] ".$args{-name}.": Interfaces File $curfile\n" );
				my($rcode,@msgs)=$package->fetch_file(
					$ftp,
					$curfile,
					#"$args{-local_directory}/$args{-name}/interfaces",
					"$args{-name}/interfaces",
					\%local_file_list,
					undef,
					$args{-name},
					$args{-local_directory});
				foreach (@msgs) {
					#WARN#warn $_ if /^ERROR/;
					print $_;
				}
				next unless $rcode == 1;

				my(%file)=$package->db_readfile(
						-filename=>$local_file_list{USED_LOCAL_FILE},
						-debug=>$args{-debug},
						-orig_filename=>$curfile,
						-type=>"I",
						-host=>$args{-name},
						-localroot=>$args{-local_directory} );
				next if defined $file{DONT_USE};
				next unless %file;
				push @rc, $package->survey_says("[survey] ".$args{-name}.": Saved As $local_file_list{USED_LOCAL_FILE}\n" )
					if $args{-debug};
				#$interfaces_count++;
				$FILES{$curfile}=\%file;

			# shell scripts
			} elsif( $curfile=~/SYBASE.sh$/ ) {
				push @rc, $package->survey_says("[survey] ".$args{-name}.": Env File $curfile\n" );
				my($rcode,@msgs)=$package->fetch_file($ftp,
					$curfile,
					"$args{-name}/".basename($curfile),
					#"$args{-local_directory}/$args{-name}/".basename($curfile),
					\%local_file_list,
					undef,
					$args{-name},$args{-local_directory});
				foreach (@msgs) { #WARN#warn $_ if /^ERROR/;
					print $_;
				}

				next unless $rcode == 1;

				my(%file)= $package->db_readfile(
					-type=>"Y",
					-debug=>$args{-debug},
					-orig_filename=>$curfile,
					#-filename=>"$args{-local_directory}/$args{-name}/".$f,
					-filename=>$local_file_list{USED_LOCAL_FILE},
					-host=>$args{-name},
					-localroot=>$args{-local_directory}  );
				next if defined $file{DONT_USE};
				next unless %file;
				my($servernm)=$file{Server_Name};
				push @rc, $package->survey_says("[survey] ".$args{-name}.": Saved As .../".basename($local_file_list{USED_LOCAL_FILE})."\n" );
#				if( $args{-debug} ) {
#					foreach ( sort keys %file ) {
#						if( $file{$_} =~ /\n/ ) {
#							push @rc, $package->survey_says("[survey] ".$args{-name}.": ... $_ = <multiline>\n" );
#						} else {
#							push @rc, $package->survey_says("[survey] ".$args{-name}.": ... $_ = $file{$_}\n" );
#						}
#					}
#				}
				#$shfile_count++;
				$FILES{$curfile}=\%file;

			} elsif( $curfile=~/\/install\/RUN_/ ) {

				push @rc, $package->survey_says("[survey] ".$args{-name}.": Run File $curfile\n" );
				my($rcode,@msgs)=$package->fetch_file(
					$ftp,
					$curfile,
					$args{-name}."/".basename($curfile),
					#"$args{-local_directory}/$args{-name}/".basename($curfile),
					\%local_file_list,
					undef,
					$args{-name},
					$args{-local_directory});
				foreach (@msgs) { #WARN#warn $_ if /^ERROR/;
					print $_;
				}
				next unless $rcode == 1;

				my(%file)= $package->db_readfile(
					-type=>"R",
					-debug=>$args{-debug},
					-orig_filename=>$curfile,
					#-filename=>"$args{-local_directory}/$args{-name}/".basename($curfile).".$runfile_count",
					-filename=>$local_file_list{USED_LOCAL_FILE},
					-host=>$args{-name},
					-localroot=>$args{-local_directory}  );

				#foreach (keys %file ) { print "KEY=$_ VAL=$file{$_} \n"; }

				next if defined $file{DONT_USE};
				next unless %file;
				my($servernm)=$file{Server_Name};
				push @rc, $package->survey_says("[survey] ".$args{-name}.": Saved As .../".basename($local_file_list{USED_LOCAL_FILE})."\n" )
					if $args{-debug};
				push @rc, $package->survey_says("[survey] ".$args{-name}.": Run File Is For Server $servernm\n" )
					if $args{-debug};

				#if( $args{-debug} ) {
				#	foreach ( sort keys %file ) {
				#		if( $file{$_} =~ /\n/ ) {
				#			push @rc, $package->survey_says("[survey] ".$args{-name}.": ... $_ = <multiline>\n" );
				#		} else {
				#			push @rc, $package->survey_says("[survey] ".$args{-name}.": ... $_ = $file{$_}\n" );
				#		}
				#	}
				#}
				$FILES{$curfile}=\%file;
				#$runfile_count++;

				# OK RUN FULES ARE SPECIAL... WE HAVE ERROR LOGS ETC FROM THEM
				# - so create an associated error log value
				my($log_file)=$file{Error_Log};
				push @rc, $package->survey_says("[survey] ".$args{-name}.": Log File $log_file\n" );
				
				# copy some values from the RUN_FILE so we are in sync
				my(%file2);
				$file2{Server_Name} = $file{Server_Name} || "ERROR";
				$file2{Hostname}=$args{-name};
				$file2{File_Type}="Errorlog";
				$file2{Server_Type}=$file{Server_Type};
				$file2{Source_Runfile}=$curfile;

				# ok figure out what the local file for this should be
				my($tmp)="$args{-local_directory}/$args{-name}/".basename($log_file);
				$file2{Local_Filename}=$args{-name}."/".basename($log_file);

				my($count)=0;
				while ( defined $local_file_list{$tmp} ) {
					$count++;
					$tmp="$args{-local_directory}/$args{-name}/".basename($log_file).".".$count;
					$file2{Local_Filename}=$args{-name}."/".basename($log_file).".".$count;
				}
				$local_file_list{$tmp}=1;
				#	$file2{Local_Filename}=$tmp;				
				$FILES{$log_file}=\%file2;
			}
		}

#		# ok ... recurse through the log files that you found...
#		foreach  ( keys %FILES ) {
#			# ok the file name of the log file in here is
#			my $hashptr = $FILES{$_};
#			if( $$hashptr{File_Type} eq "Run File" ) {
#				my($log_file)=$$hashptr{Error_Log};
#				push @rc, $package->survey_says("[survey] ".$args{-name}.": Log File $log_file\n" );
#				my(%file);
#
#				$file{"Original_Filename"}=$log_file;
#				$file{"Local_Filename"}="$args{-local_directory}/$args{-name}/".basename($log_file).".$runfile_count";
#				$file{"Hostname"}=$args{-name};
#				$file{File_Type}="Log File";
#				$file{Log_File_Type}=$$hashptr{type};
#
#				$runfile_count++;
#			}
#		}

		if( ! $args{-connection} ) {
			$ftp->close();
			$ftp->quit();
		}
		push @rc, $package->survey_says("[survey] ".$args{-name}.": ** Unix Survey Completed\n" );
		$DATA{discovered_files}=\%FILES;

	} elsif( $args{-type} eq "win32servers" ) {
		$DATA{Last_Conn_State}="Ok";
		$DATA{Last_Ok_Conn_Time}="y";

	} elsif( $args{-type} eq "sybase" or $args{-type} eq "sqlsvr" ) {

		my $DATA_PTR =$package->get_set_data(-class=>$args{-type},-name=>$args{-name});
		if( $args{-debug} ) {
			print "--------------------- INITIAL VALUES FOR $args{-name} -------------------------\n";
			print Dumper $DATA_PTR;
			print "--------------------- DONE INIT VALUES FOR $args{-name} -------------------------\n";
		}

		croak "gemdata->survey() Must pass -login/-password or -connection to GemData::survey for sybase server $args{-name}\n"
			unless ( defined $args{-login} and defined $args{-password} ) or defined $args{-connection};
		croak "gemdata->survey() Must Pass -local_directory to survey - directory to collect data into\n"
			unless defined $args{-local_directory};
		croak "gemdata->survey() -local_directory arg to survey does not exist or is not a directory - directory to collect data into\n"
			unless -d $args{-local_directory};

		if( ! defined $args{-connection} ) {
			my($type) = "Sybase" if $args{-type} eq "sybase";
			$type="ODBC" if defined $ENV{PROCESSOR_LEVEL} or $args{-type} eq "sqlsvr";
			my($c)= dbi_connect(
					-srv=>$args{-name},
					-login=>$args{-login},
					-password=>$args{-password},
					#-type=>"Sybase",
					-type=>$type,	# will be overriden if necessary
					-connection=>1);
			if( ! $c ) {
				push @rc, $package->survey_says("[survey] ".$args{-name}.": Cant connect to $args{-name} as $args{login} - continuing\n" );
				$DATA{Last_Conn_Time}=time;
				$DATA{Last_Conn_State}="Fail";
				$package->get_set_data( -class=>$args{-type},-name=>$args{-name}, -value=>\%DATA );
				return @rc;
			}
			$args{-connection}=$c;
		}
		print "Survey ".$args{-name}.": Connected to server\n" if $args{-debug};

		# test connection to see if it really is an ase server...
		my(@d)=dbi_query(-connection=>$args{-connection},-db=>"master",-query=>"select name from sysobjects where name='sysobjects'");
		foreach (@d) {
			my($n)=dbi_decode_row($_);
			last if $n eq "sysobjects";
			# well this would be a bad install...
			delete $args{-connection};
			$DATA{Last_Conn_Time}=time;
			$DATA{Last_Conn_State}="Fail";
			$package->get_set_data( -class=>$args{-type},-name=>$args{-name}, -value=>\%DATA );
			push @rc, $package->survey_says( "Bad Connection - This may not be a sybase ASE server\n" );
			return @rc;
		}

		push @rc, $package->survey_says("[survey] ".$args{-name}.": Connected to server\n" );

		$DATA{Last_Conn_Time}=time;
		$DATA{Last_Conn_State}="Ok";
		$DATA{Last_Ok_Conn_Time}=time;

 		print "Survey ".$args{-name}.": Running get_system_info(survey)\n"  if $args{-debug};
		my($tmp)=get_system_info(	-db_type=>$args{-type},
 						-reportname=>'survey',
 						-connection=>$args{-connection},
 						-returntype=>'hash' );
 		foreach( keys %$tmp ) {	$DATA{$_} = $$tmp{$_}; 	 		} 		
 		
		print "Survey ".$args{-name}.": Running get_system_info(schema)\n"  if $args{-debug};
		$DATA{Database_Info}=get_system_info(
					-db_type=>$args{-type},
					-reportname=>'schema',
 					-connection=>$args{-connection},
 					-returntype=>'hash' );

		$DATA{Bad_Database_Info}="";
		$DATA{Database_String}="";
		$DATA{Space_Allocated}=0;
 		foreach ( keys %{$DATA{Database_Info}} ) {
 			$DATA{Database_String} .= $_." ";
 			$DATA{Bad_Database_Info} .= $_." "
 				if $DATA{Database_Info}->{$_}->{Offline} != 0;
			$DATA{Space_Allocated} += $DATA{Database_Info}->{$_}->{Space_Allocated}
				if $DATA{Database_Info}->{$_}->{Space_Allocated};
 		}
 		$DATA{Server_Start_DateTime}=$DATA{Database_Info}->{tempdb}->{CreateDate};
 		push @rc, $package->survey_says("[survey] ".$args{-name}.": Database Information Fetched\n" );

        	if( ! defined $DATA{NumPgsMb} or $DATA{NumPgsMb} == 0 ) {
        		push @rc, $package->survey_says("[survey] ".$args{-name}.": ERROR: INVALID numpgsmb For Server $args{-name}\n" );
        		return @rc;
        	}

        	print "Survey ".$args{-name}.": Running get_system_info(file)\n"  if $args{-debug};
			$DATA{DeviceSize}=get_system_info(
        					-db_type=>$args{-type},
        					-reportname=>'file',
         				-connection=>$args{-connection},
         				-returntype=>'hash' );
        	push @rc, $package->survey_says("[survey] ".$args{-name}.": Device Information Saved\n" );

        	print "Survey ".$args{-name}.": Running get_system_info(configuration)\n"  if $args{-debug};
		#$DATA{Configuration}=get_system_info(
        	#			-db_type=>$args{-type},
        	#			-reportname=>'configuration',
         	#			-connection=>$args{-connection},
         	#			-returntype=>'hash' );
        	#push @rc, $package->survey_says("[survey] ".$args{-name}.": Configuration Information Saved\n" );

#	# character sets
#	my(%charsets);
#	my(@d)=dbi_query(-connection=>$args{-connection},-db=>"master",-query=>"select id,name from master..syscharsets");
#	foreach (@d) {
#		my($lid,$lname)=dbi_decode_row($_);
#		my(%dat);
#		$dat{id}=$lid;
#		$dat{description}=$lname;
#		$charsets{$lname}=\%dat;
#	}
#	$DATA{charsets}=\%charsets;
#
#	my(%languages);
#	my(@d)=dbi_query(-connection=>$args{-connection},-db=>"master",-query=>"select langid,name from master..syslanguages");
#	my($num_langs)=0;
#	foreach (@d) {
#		my($lid,$lname)=dbi_decode_row($_);
#		$num_langs++;
#		my(%dat);
#		$dat{id}=$lid;
#		$dat{description}=$lname;
#		$languages{$lid}=\%dat;
#	}
#	$languages{num_languages}=$num_langs;
#	$DATA{languages}=\%languages;
#	push @rc, $package->survey_says("[survey] ".$args{-name}.": Language and Char Set Information Saved\n" );


	} elsif( $args{-type} eq "oracle" ) {
		my $DATA_PTR =$package->get_set_data(-class=>$args{-type},-name=>$args{-name});
		if( $args{-debug} ) {
			print "--------------------- INITIAL VALUES FOR $args{-name} -------------------------\n";
			print Dumper $DATA_PTR;
			print "--------------------- DONE INIT VALUES FOR $args{-name} -------------------------\n";
		}

		croak "gemdata->survey() Must pass -login/-password or -connection to GemData::survey for sybase server $args{-name}\n"
			unless ( defined $args{-login} and defined $args{-password} ) or defined $args{-connection};
		croak "gemdata->survey() Must Pass -local_directory to survey - directory to collect data into\n"
			unless defined $args{-local_directory};
		croak "gemdata->survey() -local_directory arg to survey does not exist or is not a directory - directory to collect data into\n"
			unless -d $args{-local_directory};

		if( ! defined $args{-connection} ) {
			print "Connecting To Oracle...\n";
			my($type) = "Oracle";
			$type="ODBC" if defined $ENV{PROCESSOR_LEVEL};
			my($c)= dbi_connect(
					-srv=>$args{-name},
					-login=>$args{-login},
					-password=>$args{-password},
					-type=>$type,
					-connection=>1);
			if( ! $c ) {
				push @rc, $package->survey_says("[survey] ".$args{-name}.": Cant connect to $args{-name} as $args{login} - continuing\n" );
				$DATA{Last_Conn_Time}=time;
				$DATA{Last_Conn_State}="Fail";
				$package->get_set_data( -class=>$args{-type},-name=>$args{-name}, -value=>\%DATA );
				return @rc;
			}
			$args{-connection}=$c;
		}
		push @rc, $package->survey_says("[survey] ".$args{-name}.": Connected to server\n" );
		$DATA{Last_Conn_Time}=time;
		$DATA{Last_Conn_State}="Ok";
		$DATA{Last_Ok_Conn_Time}=time;

		$DATA{Server_Version}    = one_line_query("",'select * from v$version',$args{-connection}) || "N.A.";
		$DATA{ServerVersionLong} = $DATA{Server_Version};

		$DATA{Server_Start_DateTime}  = one_line_query("",
			'select logon_time from sys.v_$session where sid=1',$args{-connection}
		) || "N.A.";

		sub query_to_stdout {
			my($q,$c)=@_;
			print "=================================================================\n";
			print "Query=$q\n";
			print "=================================================================\n";
			my(@rc);
			foreach (dbi_query(-connection=>$c,-db=>"",-query=>$q)) {

				my(@x)=dbi_decode_row($_);
				print join(" ",@x),"\n";
				push @rc,\@x;
			}
			return @rc;
		}

		#query_to_stdout("select * from v\$datafile",$args{-connection});
		my($q)="select NAME,CREATED,LOG_MODE,OPEN_MODE,DATABASE_ROLE from v\$database";
		foreach (dbi_query( -connection=>$args{-connection}, -db=>"", -query=>$q)) {
			my(@x)=dbi_decode_row($_);

			$DATA{name}=$x[0]	if defined $x[0];;
			$DATA{created}=$x[1]	if defined $x[1];
			$DATA{log_mode}=$x[2]	if defined $x[2];
			$DATA{open_mode}=$x[3]	if defined $x[3];
			$DATA{database}=$x[4]	if defined $x[4];
			$DATA{role}=$x[5] 	if defined $x[5];
			$DATA{Logging}=$x[6]	if defined $x[6];
		}

		$q="select * from v\$sga";
		foreach (dbi_query( -connection=>$args{-connection}, -db=>"", -query=>$q)) {
			my(@x)=dbi_decode_row($_);
			$x[0] =~ s/\s/_/g;
			$DATA{$x[0]}=$x[1];
		}

		#$q="select * from v\$option";
		#my(%config);
		#foreach (dbi_query( -connection=>$args{-connection}, -db=>"", -query=>$q)) {
		#	my(@x)=dbi_decode_row($_);
		#	$x[0] =~ s/\s/_/g;
		#	$x[0] =~ s/\//-/g;
		#	$config{$x[0]}=$x[1];
		#}
		#$DATA{Configuration}=\%config;

		#query_to_stdout("select * from v\$dbfile",$args{-connection});
		#query_to_stdout("select * from v\$instance",$args{-connection});
		#query_to_stdout("select * from v\$datafile",$args{-connection});
		#query_to_stdout("select TYPE, RECORDS_USED, RECORDS_TOTAL, RECORD_SIZE from v\$controlfile_record_section",$args{-connection});

		my(%Database_Info);
		$q="select name,ts# from v\$tablespace";
		foreach (dbi_query( -connection=>$args{-connection}, -db=>"", -query=>$q)) {
			my(@x)=dbi_decode_row($_);
			my(%dat);
			$dat{ts_num}=$x[1];
			$x[0] =~ s/\s/_/g;
			$Database_Info{$x[0]} = \%dat;
		}
		$q="select TABLESPACE_NAME,BLOCK_SIZE,INITIAL_EXTENT,NEXT_EXTENT,STATUS,CONTENTS,LOGGING from dba_tablespaces";
		foreach (dbi_query( -connection=>$args{-connection}, -db=>"", -query=>$q)) {
			my(@x)=dbi_decode_row($_);
			$x[0] =~ s/\s/_/g;
			${$Database_Info{$x[0]}}{Block_Size}=$x[1];
			${$Database_Info{$x[0]}}{Initial_Extent}=$x[2];
			${$Database_Info{$x[0]}}{Next_Extent}=$x[3];
			${$Database_Info{$x[0]}}{Status}=$x[4];
			${$Database_Info{$x[0]}}{Contents}=$x[5];
			${$Database_Info{$x[0]}}{Logging}=$x[6];
		}
		$DATA{Database_Info}=\%Database_Info;

		my(%datafiles);
		$q="select NAME,STATUS from v\$controlfile";
		foreach (dbi_query( -connection=>$args{-connection}, -db=>"", -query=>$q)) {
			my(@x)=dbi_decode_row($_);
			my(%dat);
			$dat{status}=$x[1] || "UNKNOWN";
			$dat{type}="controlfile";
			$x[0] =~ s/\s/_/g;
			$datafiles{$x[0]} = \%dat;
		}
		$DATA{DeviceSize}=\%datafiles;

		$q="select MEMBER,STATUS,TYPE,GROUP# from v\$logfile";
		foreach (dbi_query( -connection=>$args{-connection}, -db=>"", -query=>$q)) {
			my(@x)=dbi_decode_row($_);
			my(%dat);
			$dat{status}=$x[1] || "UNKNOWN";
			$dat{type}="logfile";
			$dat{log_type}=$x[2];
			$dat{group_num}=$x[3];
			$datafiles{$x[0]} = \%dat;
		}

		$q="select NAME, FILE#, bytes/1024/1024, TS#, STATUS, ENABLED  from v\$datafile";
		foreach (dbi_query( -connection=>$args{-connection}, -db=>"", -query=>$q)) {
			my(@x)=dbi_decode_row($_);
			my(%dat);
			$dat{file_num}=$x[1];
			$dat{size}=$x[2];
			$dat{ts_number}=$x[3];
			$dat{status}=$x[4];
			$dat{enabled}=$x[5];
			$dat{type}="datafile";
			$datafiles{$x[0]} = \%dat;
		}

		# get the free space by FILE_ID
		$q="select FILE_ID, sum(bytes/1024/1024) from dba_free_space group by file_id";
		foreach (dbi_query( -connection=>$args{-connection}, -db=>"", -query=>$q)) {
			my(@x)=dbi_decode_row($_);
			my($ddd)=$datafiles{$x[0]};

			$$ddd{used}=$$ddd{size}-$x[1];
			#$datafiles{$x[0]} = \%dat;
		}

		#print Dumper \%datafiles;

		$DATA{Bad_Database_Info}="";
		$DATA{Database_String}="";
	} else {
		croak "gemdata->survey() Can only survey unix, sqlserver, and sybase at this point\nYou tried to survey $args{-type}\n";
	}

	if( $args{-debug} ) {
		print "----------------------- END VALUES FOR $args{-name} -------------------------\n";
		print Dumper \%DATA;
		print "--------------------- DONE END VALUES FOR $args{-name} -------------------------\n";
	}

	$package->get_set_data( -class=>$args{-type},-name=>$args{-name}, -value=>\%DATA );
	if( $args{-debug} ) {
		print "--------------------- DONE SAVING DATA PTR FOR $args{-name} -------------------------\n";
	}
	dbi_disconnect(-connection=>$args{-connection}) unless defined $orig_connection;
	push @rc, $package->survey_says("[survey] ".$args{-name}.": Survey Completed At ".localtime(time)."\n" );

	return @rc;
}

sub one_line_query {
	my($db,$query,$conn)=@_;
	foreach (dbi_query(-connection=>$conn,-db=>$db,-query=>$query)){
		my(@d)=dbi_decode_row($_);
		return "[Not Installed]" if $d[0]=~/42000: Could not find stored procedure/
			or $d[0]=~/not found. Specify owner.object/;
		return $d[0];
	}
}

sub gem_file_collector {
	my($package)=shift;
	my(%args)=@_;
	my(@rc);
	my(%copied_files);
   my(%allowed_svrs);

	if( defined $args{-allowed_srv} ) {
  		foreach( split(/,/,$args{-allowed_srv}) ) { $allowed_svrs{$_}=1; }
  	}

	if( defined $args{-errlog_type} ){
     	croak "gemdata->gem_file_collector() Error Log Type must be BACKUP_SERVER, REP_SERVER, ASE_SERVER, HISTORICAL_SERVER, or MONITOR_SERVER"
     		unless $args{-errlog_type} eq "BACKUP_SERVER"
     		or $args{-errlog_type} eq "REP_SERVER"
     		or $args{-errlog_type} eq "ASE_SERVER"
     		or $args{-errlog_type} eq "HISTORICAL_SERVER"
     		or $args{-errlog_type} eq "MONITOR_SERVER";
   }

	if( $args{-debug} ) {
		my($str) = "gem_file_collector( ";
		foreach ( keys %args )  { $str.= "$_=>$args{$_}," if defined $args{$_};  }
		$str=~s/,$//;
	   $str.= " )\n";
	   push @rc,$str;	   
   }
   
   foreach my $hostname ( @{$package->get_set_data(-class=>'unix',-keys=>1) } ) {
    	next if defined $args{-allowed_srv} and ! defined $allowed_svrs{$hostname};
		push @rc, "gem_file_collector(Host: $hostname)\n" if $args{-debug};

      my($dat)=$package->get_set_data(-class=>'unix',-name=>$hostname,-arg=>'discovered_files');
      my($ftp);
   
		next unless defined $dat;
		my %x =%{$dat};
	   #foreach my $filename ( keys %{$dat} )
	   foreach my $filename ( keys %x ) {
	     	my $ref=$$dat{$filename};	
			next if defined $args{-file_type} and ($$ref{File_Type} ne $args{-file_type});
	     	next if defined $args{-errlog_type}
	     			and ($$ref{File_Type} ne "Errorlog" or $$ref{Server_Type} ne $args{-errlog_type});
	     				        			
	     	# if purge is set then we just look at the local file and ignore if its not > size	     			
	     	my($purge_this_file)=0;
	     	if( $args{-purge_size} ) {	     		
	     		my($lfile)=get_gem_root_dir()."/data/system_information_data/".$$ref{Local_Filename};
	     		next unless -r $lfile;
	     		next unless -s $lfile > $args{-purge_size};
	     		$purge_this_file=1;
	     		push @rc, "Purging $filename\n";		     								     		
			}
	        	
	      if( $purge_this_file or ! defined $args{-nofetch} ) {	
   			my($starttime)=time;
  	      	push @rc,"Fetching Host=$hostname File=$filename\n"  if $args{-debug};

  		    	if( ! defined $ftp ) {	
	    			my($method);				
					my(%argsx)=get_password_info(-type=>"unix",-name=>$hostname);								
					if($argsx{NOT_FOUND}) {
						push @rc, "Ignoring host $hostname - no password data found\n";
						next;
					}								
					if( is_nt() ) {
						$method=$argsx{WIN32_COMM_METHOD} if is_nt();
					} else {
						$method=$argsx{UNIX_COMM_METHOD};
					}								
					croak "COMM_METHOD NOT DEFINED for SERVER $hostname\n" unless $method;
					

					if( $method eq "NONE" ) {	# hmm cant talk
						push @rc, "host $hostname: METHOD=NONE - skipping\n";
						next;
					}															
					
  	            push @rc, "Fetch method=$method\n" if $args{-debug};
    				$ftp = Net::myFTP->new( $hostname,
		                						PRINTFUNC	=> $printfunc,
		                						DEBUGFUNC	=> $debugfunc, 
		                						Debug			=> $args{-debug},
		                						METHOD		=> $method);
       			if( ! $ftp )  {
       				push @rc, "unable to create connection to $hostname\n";
       				next;
       			}

       			my($l,$p)=get_password(-type=>"unix",-name=>$hostname);
       			my($rc,$err)=$ftp->login($l,$p);
       			if( ! $rc ) {
       				push @rc, "unable to log in to $hostname : $err\n";
       				next;
       			}

       			push @rc, "connected to $hostname\n" if $args{-debug};
       			$ftp->ascii();
       		}	

				push @rc, "Fetching $filename\n" if $args{-debug};
        	   push @rc, " .. to $$ref{Local_Filename}\n" if $args{-debug};						
				my($rc2,@msgs)=$package->fetch_file(
					$ftp,
					$filename,
					$$ref{Local_Filename},
					#get_gem_root_dir()."/data/system_information_data/".$$ref{Local_Filename},
					undef,
					$args{-debug},
					$hostname,
					get_gem_root_dir()."/data/system_information_data/" );
				
				push @rc,@msgs;
				foreach (@msgs) { #WARN#warn $_ if /^ERROR/;
					print ">>".$_; 	 
				}							
				
				my($lfile)=get_gem_root_dir()."/data/system_information_data/".$$ref{Local_Filename};
				my($lsize)=0;
				my($size)="";
				if( -r $lfile ) {
					$lsize= -s $lfile;
					$size = $lsize;
					$size /= 1024;
					if( $size>2048) { 
						$size/=1024;
						$size=int($size);
						$size.="MB";
					} else {
						$size=int($size);
						$size.="KB";
					}
				} else {
					push @rc," ERROR: Oddly Enough - No Local File $lfile Exists???\n" unless $rc2==0;
				}
				#push @rc, "fetch_file completed rc=$rc2\n" if $args{-debug};
       		
				if( $rc2==0 ) {
   				#push @rc, "**********************\n";
   				#push @rc, "*\n";
   				push @rc, "ERROR: Failed To Retrieve $hostname:$filename\n";
   				#push @rc, "* \n";
   				#push @rc, "**********************\n";
   			} else {
   				push @rc,"Fetched $size bytes in ".do_diff(time - $starttime)."\n";					
					if( $args{-purge_size} and $purge_this_file and $lsize > $args{-purge_size} ) {
						# WE WANT TO PURGE HERE!
						my($rdat)=$ftp->runcmd("ls /dev/null \n");
						if( $rdat eq "/dev/null" ) {
							my($rdat)=$ftp->runcmd("ls $filename 2>&1 \n");
							if( $rdat eq $filename ) {
								#$rdat=$ftp->runcmd("id\n");
								#push @rc, "id >> $rdat\n";
								#$rdat=$ftp->runcmd("who am i\n");
								#push @rc, "who am i >> $rdat\n";
								#$rdat=$ftp->runcmd("ls -l $filename\n");
								#push @rc, "ls -l $filename >> $rdat\n";
								
								my($d)=do_time(-fmt=>'yyyymmddhhmi');
								push @rc, "Purging Source File on $hostname\n";
								push @rc, "cp $filename $filename.$d 2>&1\n";
								$rdat=$ftp->runcmd("cp $filename $filename.$d 2>&1\n");
								push @rc, ">> $rdat\n" if $rdat;
								if( $rdat !~ /^failure:/ ) {
									push @rc, "cat /dev/null 2>&1 >$filename\n";
									$rdat=$ftp->runcmd("cat /dev/null  2>&1 >$filename\n");
									push @rc, ">> $rdat\n";
								}
							} else {
								push @rc, $filename. " Not Found - Skipping Purge\n";
							}								
						}	
						#die "-------------------",join("",@rc)."DONE";
   				}
   			}	
	   	}
        	# do here as we only want successful copies
     		$copied_files{$hostname.":".$filename} = $ref;
	   }
     	if( defined $ftp ) {
     		$ftp->close();
			$ftp->quit();
		}
  	}
 	return (\@rc, \%copied_files);
}

sub save {
	return unless $DATA_NEEDS_WRITE eq "TRUE";
	$DATA_NEEDS_WRITE="FALSE";
	my($package)=shift;
	$package->survey_says( "[get_set] ". "Writing gem.xml and archiving old gem.xml\n" );
	croak "gemdata->save() ... woah gemdata is undefined " unless defined $gemdata;

	# this is kind of a kluge wait five seconds for .tmp file to no longer exist if it does to prevent lock conditions
	my($count)=0;
	sleep(1) while $count++ < 5 and -e $gemfile.".tmp";
	
	#print "DUMPING!";
	#$package->dump();

	open(GEMREP,">".$gemfile.".tmp" ) or croak "gemdata->save()Cant Write $gemfile.tmp\n";
	print GEMREP XMLout( $gemdata );
	close(GEMREP);

	croak "gemdata->save()Unable to write $gemfile.tmp\n" unless -r $gemfile.".tmp";

	if( compare($gemfile.".tmp",$gemfile) != 0 ) {
		my($f)=basename($gemfile);
		$package->survey_says( "[get_set] Copying ",$f," to ",$f.".".do_time(-fmt=>"yyyymmddhhmi")."\n" );
		rename( $gemfile, $gemfile.".".do_time(-fmt=>"yyyymmddhhmi"));
		rename( $gemfile.".tmp",$gemfile)
			or croak "gemdata->save()Cant rename $f.tmp to $f : $!\nYou may wish to start over with a datestamped gem.xml backup file\n";

		my($dir)=dirname($gemfile);
		opendir(DIR,$dir)
			|| die("Can't open directory $dir for reading\n");
		my(@pfiles) = sort grep(/^gem.xml.\d\d\d\d\d\d\d/,readdir(DIR));
		closedir(DIR);

		if( $#pfiles>10 ) {
			$#pfiles-=10;
			foreach (sort @pfiles) {
				&$printfunc( "[gemdata] removing $dir/$_\n" )
				 	if defined $printfunc;
				unlink $dir."/".$_;
			}
		}
	} else {
		unlink $gemfile.".tmp";
	}

	return 1;
};

# look in @searchdir for @searchfile - recursive
sub recurse_directory
{
	my($package)=shift;	
	my($ftp,$dir,$opt_d,$indentlevel,$hostname)=@_;
	return(undef,undef) if length($indentlevel) > 8;	# 8 levels is enough - no need to waste too much time
	
	$indentlevel.="  ";
	$package->survey_says("[rsearch] ". $indentlevel."Searching $hostname $dir\n");
		#if defined $opt_d;

	if( ! $ftp->cwd($dir) ) {
		$package->survey_says("[rsearch] ".$indentlevel."WARNING: $hostname Cant Chdir To $dir\n");		
		return(undef,undef);
	}

	my(@files)=$ftp->ls();
	#return(undef,undef) if $files[0]
	$package->survey_says("[rsearch] ". $indentlevel."Found ",($#files+1)," Files in directory $dir\n" )
		if defined $opt_d;

	my(@foundfiles,@founddirs);
	foreach my $file (@files) {
		foreach my $sdir ("install","lib","ini","\\w+-\\d\\d\_\\d") {
			push @founddirs,$dir."/".$file if $file=~/^$sdir$/;
		}
		foreach my $sdir ("libsybdb.a","sql.ini","interfaces","RUN\\_.+",".*\\.log",".*\\.sh",".*\\.csh") {
			push @foundfiles,$dir."/".$file	if $file=~/^$sdir$/;
		}
	}

	if( defined $opt_d ) {
		$package->survey_says("[rsearch] ". $indentlevel."Found ",($#founddirs+1)," Interesting Subdirectories\n");
		$package->survey_says("[rsearch] ". $indentlevel."Found ",($#foundfiles+1)," Interesting Files\n");
		foreach ( @foundfiles ) { $package->survey_says("[rsearch] ". $indentlevel."*** Found Interesting File $_\n"); }
		foreach ( @founddirs ) { $package->survey_says("[rsearch] ". $indentlevel."*** Found Interesting Directory $_\n"); }
	}

	# recurse down the tree
	my(@rc)=@founddirs;
	foreach (@rc) {
		$package->survey_says("[rsearch] ". $indentlevel."*** Searching Interesting Subdirectory $_\n" ) 
			if defined $opt_d;
		my($fileptr,$dirptr) = $package->recurse_directory($ftp,$_,$opt_d,$indentlevel,$hostname);
		push @founddirs, @$dirptr 	 if $dirptr;
		push @foundfiles, @$fileptr if $fileptr;
	}
	return(\@foundfiles,\@founddirs);
}

sub fetch_file
{
	my($package,$ftp,$file,$localname,$filehash,$opt_d,$hostname,$localdir)=@_;	
	my(@rc);	
	if( defined $filehash) {
              	my($tmp)=$localname;
              	my($count)=0;
              	while ( defined $$filehash{$tmp} ) {
              		$count++;
              		$tmp=$localname.".".$count;
              	}
              	$$filehash{$tmp}=1;
              	$$filehash{USED_LOCAL_FILE}=$tmp;
              	$localname=$tmp;
	}
	my($path_to_local)=$localdir."/".$localname;
	if( ! -d $localdir ) {
		mkdir($localdir,0777);
	} elsif( ! -d dirname($path_to_local) ) {
		mkdir(dirname($path_to_local),0777);
	}

	my($if_dir)=dirname($file);	
	if( ! $ftp->cwd($if_dir) ) {
		my($msg)="ERROR: fetch_file($hostname)\n\tcant chdir To $if_dir\n\tto collect file ".basename($file)." $!\n";
		#warn $msg;
		push @rc,$msg;
		return(0,@rc);
	} else {
		push @rc, "$hostname cwd( $if_dir )\n" if $opt_d;
	}	
	
	push @rc, "Unlinking <gem data dir>/$localname\n" if $opt_d and -e $path_to_local;
	unlink $path_to_local if -e $path_to_local;
	if( -e $path_to_local ) {
		push @rc,"ERROR: Cant Unlink $hostname:$path_to_local\n";
		return(0,@rc);
	}
	
	if( ! $ftp->get(basename($file),$path_to_local) ) {
		if( ! -d dirname($path_to_local) ) {
			push @rc,"ERROR: $hostname No Directory ".dirname($path_to_local)." Found\n";
		} elsif( ! -w dirname($path_to_local) ) {
			push @rc,"ERROR: $hostname Cant Write Directory ".dirname($path_to_local)."\n";
		} else {
			push @rc,"ERROR: $hostname Couldnt Fetch $file\n\tto $path_to_local: $!\n";
		}
	
		return(0,@rc);
	}
	push @rc, "File Saved $path_to_local\n" if $opt_d and -e $path_to_local;
	push @rc, "ERROR File Not Saved $path_to_local\n" if $opt_d and ! -e $path_to_local;
	
	return(1,@rc);
}

sub db_readfile {
	my($package)=shift;
	my(%args)=@_;
	my(%INFO);

	if( $args{-debug} ) {
		print "Called db_readfile() in debug mode\n";
		foreach (keys %args) { print "\tArg $_ = $args{$_}\n"; }
	}

	$INFO{"Local_Filename"}=$args{-filename};
	$INFO{"Hostname"}=$args{-host};

	$args{-filename}=$args{-localroot}."/".$args{-filename}
		unless -r $args{-filename};
	if( $args{-type} eq "I" ) {
		$INFO{File_Type}="Interfaces File";
		return %INFO;

	} elsif( $args{-type} eq "Y" ) {
		if( ! open(XFILE,$args{-filename})) {
			print "Cant Open RUN FILE ".$args{-filename}."\n";
			#WARN#warn("Cant Open RUN FILE ".$args{-filename}."\n");
			$INFO{DONT_USE}=1;
			return %INFO;
		}
		$INFO{File_Type}="Environment File";

		foreach (<XFILE>) {
			next unless /=/;
			next if /^\s/;
			chop;
			my($k,$v,$x)=split(/=/,$_,3);
			next if defined $x;
			$k=~s/^\s+//;
			next if $k=~/\s/;
			next if $k eq "PATH" or $k eq "LD_LIBRARY_PATH";
			$INFO{$k}=$v;
		}

		close(XFILE);
		return %INFO;

	} elsif( $args{-type} eq "R"
		or      $args{-type} eq "B"
		or      $args{-type} eq "S" ) {

		#       R - From RUN_FILE
		#       B - From Backup Server RUN_FILE
		#       S - From Sybase Server RUN_FILE
		if( ! open(RUNFILE,$args{-filename})) {
			print "Cant Open RUN FILE ".$args{-filename}."\n";
			#WARN#warn("Cant Open RUN FILE ".$args{-filename}."\n");
			$INFO{DONT_USE}=1;
			return %INFO;
		}

		# Interpolate The Sybase Directory From Run File Name
		my($tmp)=$args{-orig_filename};
		$tmp =~ s/\/[\w\_\.]+\/RUN_.+$//;

		$INFO{File_Type}="Run File";
		$INFO{Sybase_Dir}=$tmp;
		$INFO{"Line_Count"}=0;
      		#$INFO{"Contents"}="";

		my($command)="";
		my($in_command)=0;
		my($found_cmd_line)=0;
		while(<RUNFILE>) {
			#$INFO{"Contents"}.=$_;
			if( /SQL Server Information:/ ) {
				$INFO{Server_Type}="ASE_SERVER";
			} elsif( /Replication Server/ and /rs_init/) {
				$INFO{Server_Type}="REP_SERVER";
			} elsif( /Backup Server Information:/ ) {
				$INFO{Server_Type}="BACKUP_SERVER";
			} elsif( /^#\s+([\s\w]+):\s+(\w+)$/  ) {
				# my($key,$val)=($1,$2);
				# print "===> KEY $key --- VALUE => $val \n";
				$in_command=0 unless /\\$/;	# perhaps this line can be removed... dunno
			} elsif( /^\s*#/ ) {
				# comment
			} elsif( /^\s*$/ ) {
				# blank line
				$in_command=0 unless /\\$/;
			} else {
				$INFO{"Line_Count"}++ unless /\\\s$/;
				/\s*(\S+)\s/;
				my($word1)=$1;				
				if( $word1=~/[\\\/]dataserver/ or $word1=~/[\\\/]backupserver/
					or $word1=~/[\\\/]sqlsrvr.exe/ or $word1=~/[\\\/]bcksrvr.exe/ or $word1=~/[\\\/]histserver/
					or $word1=~/[\\\/]repserver/ or $word1=~/[\\\/]monserver/){

					$INFO{"Line_Count"}++ if /\\\s$/;
					$found_cmd_line=1;
					# doublecheck server type
					if( /dataserver/ or /sqlsrvr.exe/ ) {
						$INFO{Server_Type}="ASE_SERVER" if ! defined $INFO{Server_Type};
						croak "Confused RUNSERVER type $INFO{Server_Type} in file $args{-filename}"
							if $INFO{Server_Type} ne "ASE_SERVER";
					} elsif( /repserver/ ) {
						$INFO{Server_Type}="REP_SERVER" if ! defined $INFO{Server_Type};
						croak "Confused RUNSERVER type $INFO{Server_Type} in file $args{-filename}"
							if $INFO{Server_Type} ne "REP_SERVER";
					} elsif( /histserver/ ) {
						$INFO{Server_Type}="HISTORICAL_SERVER" if ! defined $INFO{Server_Type};
						croak "Confused RUNSERVER type $INFO{Server_Type} in file $args{-filename}"
							if $INFO{Server_Type} ne "HISTORICAL_SERVER";
					} elsif( /monserver/  ) {
						$INFO{Server_Type}="MONITOR_SERVER" if ! defined $INFO{Server_Type};
						croak "Confused RUNSERVER type $INFO{Server_Type} in file $args{-filename}"
							if $INFO{Server_Type} ne "MONITOR_SERVER";
					} else {
						$INFO{Server_Type}="BACKUP_SERVER" if ! defined $INFO{Server_Type};
						croak "Confused RUNSERVER type $INFO{Server_Type} in file $args{-filename}"
							if $INFO{Server_Type} ne "BACKUP_SERVER";
					}
					$in_command=1;
					s/\\\s*$//;
					$command=$_;
				} elsif ( $in_command and !/^#/ ) {
					$command.=$_;
					$command=~s/\s*\\\s*$/ /;
					$in_command=0 unless /\\$/;
				}
			}
		}
		close(RUNFILE);
		if( ! defined $found_cmd_line ) {
			$INFO{DONT_USE}=1;
			return %INFO;
		}

		$command=~s/(\s-[a-zA-Z])\s+/$1/g;
		$command=~s/(\s-[a-zA-Z])-/$1 -/g;		# fix -P- that will occurr from -P \ on its own line

		if( $args{-debug} ) {
			print "\tdb_readfile() file parse completed - interpreting result\n";
			print "\tcommand=$command\n";
			foreach (keys %INFO) { print "\tResults $_ = $INFO{$_}\n"; }
		}
		my(@cargs)=split(/\s+/,$command);
		foreach (@cargs) {
			print "\tInterpreting $_\n" if $args{-debug};
        		if( $INFO{Server_Type} eq "ASE_SERVER" ) {
        			if( /-d/ ) {		s/^-\w//;	$INFO{"Master_Device"}=$_;
        			} elsif( /-c/ ) {	s/^-\w//;	$INFO{"Configuration_File"}=$_;
        			} elsif( /-e/ ) {	s/^-\w//;	$INFO{"Error_Log"}=$_;
        			} elsif( /-m/ ) {	s/^-\w//;	$INFO{"Single_User_Mode"}=$_;
        			} elsif( /-r/ ) {	s/^-\w//;	$INFO{"Master_Mirror_Device"}=$_;
        			} elsif( /-M/ ) {	s/^-\w//;	$INFO{"Shmem_Dir"}=$_;
        			} elsif( /-i/ ) {	s/^-\w//;	$INFO{"Interfaces_File_Dir"}=$_;
        			} elsif( /-s/i ) {	s/^-\w//;	$INFO{Server_Name}=$_;
        			} elsif( /-p/ ) {	s/^-\w//;	$INFO{"SSO"}=$_;
        			}
        		} elsif( $INFO{Server_Type} eq "BACKUP_SERVER" ) {
        			if( /-S/ ) {		s/^-\w//; 	$INFO{Server_Name}=$_;
        			} elsif( /-e/ ) {	s/^-\w//;	$INFO{"Error_Log"}=$_;
        			}
        		} elsif( $INFO{Server_Type} eq "REP_SERVER" ) {
        			if( /-C/ ) {		s/-\w//;	$INFO{Configuration_File}=$_;
        			} elsif( /-E/ ) {	s/^-\w//;	$INFO{"Error_Log"}=$_;
        			} elsif( /-I/ ) {	s/^-\w//;	$INFO{"Interfaces_File"}=$_;
        			} elsif( /-S/ ) {	s/^-\w//;	$INFO{"Server_Name"}=$_;
        			}
        		} elsif( $INFO{Server_Type} eq "MONITOR_SERVER" ) {
        			if( /-M/ ) {		s/^-\w//;	$INFO{Server_Name}=$_;
        			} elsif( /-S/ ) {	s/^-\w//;	$INFO{ASE_Server_Name}=$_;
        			} elsif( /-l/ ) {	s/^-\w//;	$INFO{"Error_Log"}=$_;
        			}
        		} elsif( $INFO{Server_Type} eq "HISTORICAL_SERVER" ) {
        			if( /-S/ ) {		s/^-\w//;	$INFO{Server_Name}=$_;
        			} elsif( /-l/ ) {	s/^-\w//;	$INFO{"Error_Log"}=$_;
        			}
        		}
		}

		if( $args{-debug} ) {
			foreach (keys %INFO) { print "\tFinal Results $_ = $INFO{$_}\n"; }
		}

		#
		# QA The info u just got before u save it
		#
		if( ! defined $INFO{"Server_Name"} ) {
			#WARN#warn "ERROR: Server_Name Not Defined in Run File ",$args{-orig_filename}," on host ",$args{-host},"\n";
			print "ERROR: Server_Name Not Defined in Run File ",$args{-orig_filename}," on host ",$args{-host},"\n";
			$INFO{DONT_USE}=1;
			return %INFO;
		}
		return %INFO;

	} elsif( $args{-type} eq "H" ) {
		#       H - From Unix Password File (via GetPassword.pm)
		croak "Invalid File Type: ".$args{-type}."\n";
	} elsif( $args{-type} eq "s" ) {
		#       s - From Sybase Password File (via GetPassword.pm)
		croak "Invalid File Type: ".$args{-type}."\n";
	} else {
		croak "Invalid File Type: ".$args{-type}."\n";
	}
}

1;

__END__

=head1 NAME

GemData.pm - XML Repository Manager

=head2 DESCRIPTION

XML Repository Manager for the GEM.  The G.E.M. uses a file
called gem.xml as one of its primary repositories.  This data
is stored in xml format - and these routines read and write that
repository.  The data, for clarity, is sometimes saved as
gem.dat.

=head2 SUMMARY

 my $rep = new GemData;
 my $rep = new GemData(-file=>Filename);
 $rep->dump
 $rep->save
 $rep->get()
 $rep->set()
 $rep->survey()

=head2 KEYS

The get() and set() subroutines work on a -class / -name / -arg heirarchy.  At any level the routines
can return a hash of values or a scalar.  You set the value by passing in -value=>val.  Use the -debug=>1
flag to print debugging messages.

=head2 DESCRIPTION

This module provides an interface to an Administrative Database.  The G.E.M. can populate this
data as can the survey() function of this package.

This database contains information from your run files, interfaces files, and
from your sql servers.  It will perhaps be expanded to contain information
from your unix servers.

=head2 AUTHOR

   Edward Barlow
   mail: edbarlow@hotmail.com
   url : http://www.edbarlow.com
   All Rights Reserved

=head2 TYPES

Data is stored by type.  The type indicates the SOURCE of the data.  The
following types are currently recognized:

=over 4

=item * G - General Information (server names etc)

=item * H - Host specific information (list of files found)

=item * B - From Backup Server RUN_FILE

=item * S - From Sybase Server RUN_FILE

=item * I - From Sybase Server Interfaces File

=item * M - Space Information

=item * s - server information (from catalogs)

=back

=head2 FUNCTIONS

=over 4

=item * $gem_data->fetch_file($ftp,$file,$localname,$filehash,$opt_d,$hostname)

    sets $$filehash{$tmp}=1; 	$$filehash{USED_LOCAL_FILE}=$tmp;
    uses the passed in ftp connection
    cwd(dirname($file))
    unlink $localname
    get(basename($file),$localname)
    return ($rc, @messages)

=item * db_open(-dbmname=>filename)

Open a New Database .  The name is the root of the dbm file (no .pag/.dir
extension. pass -clear to delete data from the dbm database.

=item * db_close()

Close The database.

=item * db_param(
   -name=>KeyName,
   -value=>KeyValue,
   -type=>WhereReadFrom,
   -server=>ServerName,
   -keys,
   -dbname=>DbName)

 -name:     KeyName
 -value:    if defined then set value, if not then return it
 -type:     See Type Section Above
 -server:   server of interest if type B/S or host if type H/I
 -keys:     if defined returns a list of keys.  The keys are returned in
            a pipe separated string - type|KeyName|Server|Dbname
 -dbname:   database name (only for certain key values)

=item * db_readfile()

Read a file and return a hash of key info.  Files may be RUN files or
INTERFACE files as per the -type parameter to db_param().

Args
   -type=>FileType,
   -filename=>FileToRead,
   -host=>Hostname,
   -orig_filename=>OriginalFilename)
   -debug
   -localroot	the data directory/system_information_data
   -type = I (interfaces), Y (environment),
	R - From RUN_FILE
	B - From Backup Server RUN_FILE
	S - From Sybase Server RUN_FILE

# returns a hash
#   Local_Filename
#   Hostname

	my(%file)= $package->db_readfile(
				-type=>"Y",
				-debug=>$args{-debug},
				-orig_filename=>$curfile,
				-filename=>$local_file_list{USED_LOCAL_FILE},
				-host=>$args{-name} );
	next if defined $file{DONT_USE};
	next unless %file;

=item * db_readserver(
   -server=>SERVERNAME,
   -login=>LOGIN,
   -password=>PASSWORD )

Get Values from a server by connecting and looking in the system catelogs.  Results are saved in the database with a key 's'.

=back

=head2 KEYS

The following are a list of keys by type of program

=over 4

=item * GENERAL (TYPE G)

 unix_discover_date
 unix_systems
 sybase_server
 backup_server

=item * PER HOST   (TYPE H)   (server should be a unix host)

 SYBASE_SRV_LIST      List of the Server on this host
 Interfaces_File_Id   The Id # of the interfaces file
 Format File:Count|File:Count...

=item * RUN_SCRIPT - ALL SERVERS (TYPE B or S)

The following are available for both sybase servers and backup servers

 Run_File        Path Name To Run File
 Sybase_Dir      Directory to Sybase (from run file name)
 Hostname        Host the servers Run File Was Found On
 Line_Count  Num Non Comment Lines in Run File
 Contents    Contents of the Run File
 type    ASE_SERVER or BACKUP_SERVER


=item * RUN_SCRIPT - BACKUPSERVER - (TYPE B)

Contains the general per server keys plus

 Server_Name
 Error_Log

=item * RUN_SCRIPT - SQL SERVER - (TYPE S)

Contains the general per server keys plus

 Master_Device
 Configuration_File
 Error_Log
 Single_User_Mode
 Master_Mirror_Device
 Shmem_Dir
 Interfaces_File_Dir
 Server_Name
 SSO

=item * INTERFACES FILE - (TYPE I)

 $SERVERNAME_##       ## is the number of the interfaces file
 values are host:port.  Duplicate entries in interfaces
 file are ignored (last one is used) although a
 warning is printed.

=item * SERVER DISCOVERY (TYPE M)

populated by space_monitor.pl

The following items require a secondary parameter to be passed using
the -dbname=>xxx argument

 TimeMonitored    - a time() value
 DbSpaceUsed      - last space used (mb)
 LogSpaceUsed     - last log space used (mb)
 DbSpaceUsedPct   - last space used (mb)
 LogSpaceUsedPct  - last log space used (mb)

=item * SERVER DISCOVERY (TYPE s)

populated by db_readserver()

The following items require a secondary parameter to be passed using
the -dbname=>xxx argument

 DBoptionsHtml  html string of the database options
 DBoptions      regular string of the database options
 DataSpaceByDb  data space allocated to this database
 LogSpaceByDb   log space allocated to this database
 DeviceInfo     sec param is a sequential number.  values are || separated list
                of phy,srvr,db,dev,size,alloc,usage
 ConfigDefault  number is config value.  Default for sp_configure value
 ConfigName     number is config value.  Name of sp_configure value
 ConfigValue    number is config value.  Value of sp_configure value

DATABASE OPTIONS (aka sp_dboption)

 Offline
 OfflineToLoad
 NoSepLog
 ForLoad
 NoChkptRcvry
 Suspect
 DboOnly
 ReadOnly
 SelectInto
 SglUser
 TruncLog

ITEMS WITHOUT DBNAME REQUIRED

 BackupServer    Name of the backup server
 ServerName      Name of the local server
 BadDatabases    space sep list of databases with problems
 CharsetDef
 Databases       space sep list of databases
 DateInstalled
 NumPgsMb        pages per MB
 Space_Allocated  sum of pages allocated from sysusages
 SpaceForServer
 Version         @@version
 Languages       space separated list of id of languages installed
 Charset         space separated list of id of character sets installed

ITEMS WITH JUST A NUMBER INSTEAD OF SERVER

 LanguageDef   Language name for language id
 CharsetDef    Character Set name for id

=back

=head1 GEM.XML DATA FORMAT

The gem.xml file contains information that the GUI will use and reuse.  For
example, it contains information regarding the servers and databases that are
contained in the system.  This information can, however, get out of date, and
will be refreshed as needed.  That is done by the surveys.

The layout of the file is xml and can be hand edited and viewed.  The GemData.pm
module manages this file using an interface that looks like:

 $package->global_data(-class=>"server", -name=>$s, -arg=>"lastconntime")

The following is a high level view of this xml tree.   THis file is more readable
than the gem.xml file.  You can create your own version of this file in the installer
by using the function convert gem.xml to dat.

THE FOLLOWING IS FOR ILLUSTRATIVE PURPOSES ONLY!!!  FORMAT MAY CHANGE!!!


 'plugin:documenter' => {
       'documenter_do_copy' => 'N',
       'copy_using_ftp' => 'N'
 },
 'install' => {
       'ENABLE_ORACLE' => 'Y',
       'admin_scripts_dir' => 'G:/dist_531/ADMIN_SCRIPTS',
       'is_mail_ok' => 'FALSE',
       'current_hostname' => 'adwte083',
       'ENABLE_SQLSERVER' => 'Y',
       'ENABLE_SYBASE' => 'Y',
       'isnt' => '1',
       'installtype' => 'SAMBA',
       'root_dir' => 'G:/dist_531',
       'cf_dir' => 'G:/dist_531/conf'
 },
 'mail' => {
       'SMTPMAILSERVER' => 'mail1.mlp.com'
 },
 'server' => {
       'SYBASEDEV2' => {
                   'state' => 'OK',
                   'proclib_version' => '6.60',
                   'database' => {
                         'tempdb' => '',
                         'client' => '',
                         'sybsystemdb' => '',
                         'imagvue' => '',
                         'test1' => '',
                         'tlm' => '',
                         'clientmlp' => '',
                         'master' => '',
                         'sybsystemprocs' => '',
                         'its_pw' => '',
                         'shared1' => '',
                         'model' => ''
                         },
                   'server_version' => 'SYBASE 12.51',
                   'version_poll_time' => '1091468717',
                   'lastconntime' => '1095353504',
                   'type' => 'sybase'
              },
       'ADSRV034' => {
             'state' => 'OK',
             'proclib_version' => '6.60',
             'database' => {
                         'Northwind' => '',
                         'tempdb' => '',
                         'TradeAnalysis' => '',
                         'pubs' => '',
                         'master' => '',
                         'msdb' => '',
                         'model' => ''
                         },
             'server_version' => 'SQL SERVER 8.00760',
             'version_poll_time' => '1091468791',
             'lastconntime' => '1091725279',
             'type' => 'sqlsvr'
             },
       'SYBASE1DR' => {
                   'state' => 'FAIL',
                   'lastconntime' => '1095353504',
                   'proclib_version' => '[Can Not Connect]',
                   'database' => {},
                   'server_version' => '[Can Not Connect]',
                   'type' => 'sybase'
             },
 },
 'helpfile' => 'gem.html',
 'installsteps' => {
             'NEXT_PROCEDURES' => '1094709562',
             'VALIDATE_FILE' => '1094709524',
             'NEXT_REGISTER' => '1094709493',
             'NEXT_SERVER' => '1094709559',
             'NEXT_BACKUPS' => '1094709560',
             'FINAL_REFORMAT_CODE' => '1095426628',
             'VALIDATE_BACKUPS' => '1094709566',
             'NEXT_DOCUMENTER' => '1094709561',
             'NEXT_WELCOME' => '1095267507',
             'NEXT_LOCATIONS' => '1095352025',
             'NEXT_PRODUCTS' => '1094709496'
 },
 'package' => {
       'servers.pm' => {},
       'numsteps' => '157',
       'name' => 'barlow_procedures.pm'
 },
 'version' => {
       'Date' => 'Sep 16 2004',
       'Build' => '531',
       'VERSION' => 'Build 531 (Sep 16 2004)',
       'sht_version' => 'Build 531'
 },
 'copyright' => {
       'notice' => 'copyright (c) 1995-2004 by Edward Barlow'
 },
 'email' => {},
 'administrator' => {
             'email' => 'barlowedward@hotmail.com',
             'phone' => '555 121234',
             'name' => 'Edward Barlow'
 }

=cut

