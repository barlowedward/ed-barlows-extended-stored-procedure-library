# copyright (c) 2005 by SQL Technologies

#
# Root Class For All Plugins
#

package Plugin;

use Carp;
use vars qw($VERSION);
$VERSION = "1.00";
my(%CONFIG);

use strict;

###  	CURTREETYPE=${$$package{-curTreeType}}
###  	CUREXPLORERITEM $$package{-curExplorerItem}
###  	-toplevel, global_data, -tree, -colapseAll, statusmsg, debugmsg wrtie_globaldata connectionmanager...
### 	foreach (keys %$package ) { print "key=>$_ val=$$package{$_} \n"; }
### 	$package->statusmsg(  "curTreeType=".${$$package{-curTreeType}}."\n" );
### 	$package->statusmsg(  "curExplorerItem=",$$package{-curExplorerItem},"\n" );

my(%server_info);

sub new {
	my($class)=shift;
	my(%args)=@_;
	my $self = bless { %args }, $class;
	foreach (keys %args) {
		$CONFIG{$_}=$args{$_};
		$self->{$_}=$args{$_};
	}
	return $self;
}

sub get_last_event_info {
	my($package)=shift;
	return  $CONFIG{-pluginmanager}->get_last_event_info();
}

sub ignore_events {
	my($package,$onoff)=@_;
	return  $CONFIG{-pluginmanager}->ignore_events($onoff);
}

# returns current_tree_type, current_entrypath
#sub current_info {
#	my($package)=shift;
#	return( ${$$package{-curTreeType}},${$$package{-curExplorerItem}} );
#}

sub initialize {
	my($self,%args)=@_;
	foreach (keys %args) {
		$CONFIG{$_}=$args{$_};
		$self->{$_}=$args{$_};
	}
}

# run after initialize is run on all plugins
sub init {}

sub clear_tab {
	my($package,$tabname)=@_;
	return $CONFIG{-clear_tab}->( $tabname );
}

sub messageBox {
	my($package)=shift;
	my(%args)=@_;
	$args{-title} = "" unless $args{-title};
	$args{-type} = "OK" unless $args{-type};
	return $CONFIG{-toplevel}->messageBox( @_ );
}

sub show_html {
	my($package)=shift;
	$package->statusmsg(  "Called Plugin.pm show_html(@_)\n" );
	return $CONFIG{-show_html}->(@_);
}
sub global_data {
	my($package)=shift;
	return $CONFIG{-global_data}->(@_);
}

sub write_XMLDATA {
	my($package)=shift;
	$package->debugmsg( "Called Plugin.pm write_XMLDATA()\n" );
	return $CONFIG{-write_XMLDATA}->(@_);
}

sub DialogBox {
	my($package)=shift;
	if( ! defined $CONFIG{-toplevel} ) {
		$package->debugmsg(  "CONFIG VALUES:\n" );
		foreach ( keys %CONFIG ) {
			$package->statusmsg(  "$_ - $CONFIG{$_} \n" );
		}
		die "ERROR TOPLEVEL NOT DEFINED FOR PLUGIN\n";
	}
	return $CONFIG{-toplevel}->DialogBox( @_ );
}

sub unimplemented {
	my($p) = shift;
	my($txt)=shift;
 	my $dialog = $p->DialogBox(-title=>"Unimplemented", -buttons=>["Ok"]);
 	$dialog->Label(-text=>"<To Do> $txt")->pack();
   	$dialog->Show;
}

sub	getBitmaps { 	return( () );	}

sub make_button {
	my($package)=shift;
	my(%args)=@_;
	my($widget)=$args{-widget};
	my(%newarg);
	foreach ( keys %args ) {
		next if /^-widget/ or /^-help/ or /^-func/;
		$newarg{$_} = $args{$_};
	}
	$newarg{-activebackground}='beige';
	$newarg{-command} = $args{-func} if defined $args{-func};

	my($ButtonObj)=$widget->Button(%newarg);
	if( defined $args{-help} ) {
		$ButtonObj->bind('<Enter>',  sub { $package->notifymsg($args{-help}); });
		$ButtonObj->bind('<Leave>',  sub { $package->notifymsg(""); });
	}
	return $ButtonObj;
}

sub rtclick
{
	my($package,$Frame, $entryPath, $clktext)=@_;
	$package->statusmsg(  __FILE__, " line=",__LINE__," Plugin rtclick() should be overriden\nArgs: Frame=$Frame Path=$entryPath Txt=$clktext\n" );
}

sub debugmsg
{
	my($self,@msg)=@_;
	if( defined $CONFIG{-debugmsg} ){
		$CONFIG{-debugmsg}->(@msg);
	} else {
		print "debugmsg: @msg";
	}
}

sub notifymsg
{
	my($self,@msg)=@_;
	if( defined $CONFIG{-statusmsg} ){
		$CONFIG{-statusmsg}->("noprint",@msg);
	} else {
		print "ERROR: \$CONFIG{-statusmsg} not defined - can not print\n";
		print "Msg=",join("",@msg);
		print "The Keys For \%CONFIG are\n";
		foreach ( keys %CONFIG ) {
			print " >> key=$_ val=$CONFIG{$_} \n";
		}
		die "Exiting...";
	}
}

sub statusmsg
{
	my($self,@msg)=@_;
	if( defined $CONFIG{-statusmsg} ){
		#print "CALING $CONFIG{-statusmsg}\n"		;
		$CONFIG{-statusmsg}->(@msg);
	} else {
		print "statusmsg undefined: @msg";
	}
}

sub menuclick {
	my($package,$Frame, $Key)=@_;
	print __FILE__, " line=",__LINE__," Plugin menuclick(should be overriden)\n";
}

sub browse {
	my($package,$Frame, $entryPath)=@_;
	print __FILE__, " line=",__LINE__," Plugin browse(should be overriden)\n";
}

sub bitmapclick {
	my($package,$Frame, $entryPath)=@_;
	print __FILE__, " line=",__LINE__," Plugin bitmapclick(should be overriden)\n";
}

sub getExplorerButtons {return ();}

# sub getExplorerBaseItems {	return ();}

# get items for explorer tree
sub getTreeColorFunc { 	return undef; }
sub getTreeItems { 	return undef; }
sub getTreeRtclk {	return undef; }
sub getPluginPanes { 	return(); }
sub getMenuItems { 	return (); }

sub refresh_tree {
	my($package,$view)=@_;
	$package->debugmsg("[plugin.pm] refresh_tree(view=$view)\n");
	#$main::pluginmanager->get_last_event_info( -context=>$view );
	die "Woah No pluginmanager found in plugin.pm" unless defined $CONFIG{-pluginmanager};
	$CONFIG{-pluginmanager}->get_last_event_info( -context=>$view );
	$CONFIG{-refresh_tree}->($view) if defined $CONFIG{-refresh_tree};
	$CONFIG{-collapseAll}->() if defined $CONFIG{-collapseAll};
	return ();
}

# THIS CAN BE REPLACED WITH %$PACKAGE...
sub get_CONFIG {
	#$CONFIG{VERSION}=$VERSION;
	return %CONFIG;
}

sub set_default_tab_to_raise {
	my($package)=shift;
	my($tabname)=@_;
	#print "DBG: SETTING DEFAULT TAB TO RAISE TO $tabname -- $main::first_tab_to_raise\n";
	$main::first_tab_to_raise=ucfirst($tabname);
	$main::notebook_f->raise(ucfirst($tabname));
}

1;

__END__

=head1 NAME

Plugin.pm - Base Class For G.E.M. Plugins

=head2 USAGE

Plugins are the objects within the generic enterprise manager that perform all the functionality.
This class provides core functionality and is inheirited by .pm files that reside in the plugins
subdirectory.  This is the documentation on how to write a plugin.

=head2 SYNOPSIS

Generic Enterprise Manager plugins are written to provide users features through the
GEM.  A plugin is a perl module (extension .pm) that inheirits from the base class Plugin. The .pm file
and a .xml file which identifies associated data (author, copyright notice...) are placed in the plugins
subdirecory of the application where they will automatically be loaded at run time.   Plugins should
conform to this specification.  If a plugin does not compile, it not loaded at run time ( you
will see error messages ).  Plugins are called by and run from the PluginManager module.

=head2 TOP OF FILE

It is recommended that the top of a plugin file named xyz follow the following template:

	package xyz;
	@ISA = qw(Plugin);
	use strict;
	use vars qw($VERSION);
	$VERSION = ".01";

=head2 BOTTOM OF FILE

As with all perl modules, the plugin must return a 1 and of
course you are encouraged to embed embed perldoc documentation after that line.

  1;
  __END__

  =head1 NAME

  eventviewer.pm -  Module for viewing events

  =head2 DESCRIPTION

  The event viewer can be viewed as an example plugin.
  etc...

=head2 OTHER FUNCTIONS

After that, the plugin should contain subroutines that override the
subroutines in its parent Plugin.pm.  These will be used to define user interface elements and to
handle functionality. You can create and paint a notebook tab if you wish to use one.
You can create explorer buttons (lower left side), bitmaps for the upper left side, can
add items to the main explorer tree, and
can create the functionality that occurs when you right click items on an element of that tree.
You can also create menu items for the menu bar.  The heirarchy is that the explorer
buttons, when clicked, will populate the explorer tree.  The tree is painted and right click items
added to elements.  When a user selects
an element on this tree, selects a menu item, or right clicks on one of the right click items, a call back to
event_handler() will be performed.

=head2 PLUGIN BUILTIN FUNCTIONS

This section lists functions that your plugin must NOT override.  These are automatically called
from within the application

=over 4

=item * new		- called by PluginManager

=item * initialize	- called by PluginManager

=back

=head2 PLUGIN UTILITY FUNCTIONS

The following functions are available to your plugin:

=over 4

=item * refresh_tree - refreshes the left side window tree

=item * messageBox   - return a messageBox(@_) using normal MessageBox arguments

=item * DialogBox    - return a DialogBox(@_) using normal DialogBox arguments

=item * make_button()

=over 4

=item * -widget=>$frame

=item * -text=>$bstr

=item * -help=>$help_string

=item * -command=>$func

=back 4

=item * clear_tab($tabname)

	$current_main_frame = $package->clear_tab($tabname);

Clears the notebook tab named by $tabname. The cleared frame is then
raised and returned to the calling program and can be used as a base frame.

=item * global_data

calls gemdata->global_data() to get/set values from the gem.xml configuration file

=item * init

This user defined function will be called to initialize the plugin during startup

=item * show_html

show a html frame

=item * unimplemented

pops up a dialog box that says ... unimplemented...

=item * write_XMLDATA

save the globaldata() if needed

=item * get_last_event_info()

=item * ignore_events()

=item * browse()

=item * get_CONFIG()

=back

=head2 PRINT FUNCTIONS

You also have 3 print functions.

=over 4

=item * statusmsg(@msg)

a normal/status message that will print to the console

=item * notifymsg(@msg)

a status message but that will not print to the console

=item * debugmsg(@msg)

a debug message that will show if --DEBUG passed

=back

The following example line will set the statusbar and print to stdout

	$package->statusmsg( "[navigator.pm] menuclick($menuitem)\n" );

=head2 PLUGIN CALLBACKS

The callbacks define the look and feel of the application.
The buttons on the lower left side determines which tree is selected in the explorer
type menu above it.  This function returns an array of buttons to place in that corner.

=over 4

=item * getExplorerButtons()

return hash of buttons to go in the lower left corner.

=item * getPluginPanes($package)

Defines the tabs the plugin wishes to create.  The tab names are based on the keys returned
in a hash. and the values indicate pointer to functions to run when the tab is raised

=item * getMenuItems

Returns an array of slash delimited menu items to be displayed in the application menu bar.  If the
path does not exist, it will be created.  Thus, if you return /a/b/c you will get 'a' on the menubar
and b on its submenu.  Placing the mouse on b will show a further submenu of c.

=item * menuclick($package,$Frame,$entryPath)

This function is called with the full path name to the menu (see getMenuItems) when a user clicks on a menu item.

=item * getTreeColorFunc($package,$curTreeType)

Return function pointer to colorize the tree

=item * getTreeItems($package,$curTreeType)

Return an array of Tree Items for the current tree type.  The tree type is related to the explorer
button that is pressed and this function is called every time you press one of these buttons.
You should obviously test for tree type and not return anything if the tree type is not of your
creation.

=item *	getBitMaps($package)

Called on initialization to get bitmaps that might want to show in upper left side of window

=item *	bitmapclick($package,$Frame,$entryPath)

Called when the bitmap is clicked.

=item * getTreeRtclk($package,$curTreeType,$treeitmptr)

Native callback that allows you to add right click items in the current tree.
Basically you get a tree type in $curTreeType and a pointer to an array of items in
the tree ($treeitmptr).  You return an array of arrays where the outer array
matches the items in $treeitmptr and the inner array is a list of the popup right
clicks.

=item * rtclick($package, $entryPath, $clktext)

This function is called when a user selects a right click item.  The entry path is the full
path to the item and will match that which was specified in getTreeRtclk. The clktext is
the subitem on that menu that was clicked.

=back

=head2 PLUGIN DOCUMENTATION

Plugin documentation is contained in an xml file in the plugins directory that has the same root name as the
.pm file.  I suggest you browse a few of them for functionality.

 <package name="MDA_tables" version=".01">
   <copyright type="gpl" key="" expires="never" notice="copyright (c) 2005 by edward barlow" />
   <help_url>http://www.edbarlow.com/plugins/MDA_tables.html</help_url>
   <long_description>Monitoring Table Viewer</long_description>
   <short_description>Monitoring Table Viewer</short_description>
   <author_name>Edward Barlow</author_name>
   <author_mail>barlowedward@hotmail.com</author_mail>
 </package>

=head2 Plugin Events

Plugins respond to events from the calling program.  This section should be moved to the
plugind design section

=head2 Event Functions

	$pluginname->send_event(%args)
	$pluginname->handle_event(%args)

=head2 Event Types

Events are categorized.  The -eventtype argument defines what type
of event is being sent.  The following list describes the options
that each eventtype can have.  Note that the one confusion here is the
difference between a tab and a page.  A tab is the high level notebook
tab (monitoring, log, welcome...).  A page is a screen within the tab.
Im not really sure how to work pages - how to navigate on them.  But
the idea is that you have a fixed number of tabs and rather than requiring
each plugin to create their own, the plugins can share the tabs.  This will
make things less crowded.

	menu
		-menuitem=>
	tabchange
		-oldtab
		-curtab
	pagechange
		-oldpage
		-newpage
	treebrowse
		-entrypath
		-curtab1
	treertclick
		-entrypath
		-clicktext
	expbutton
		-context
	imgbutton
		-text

=head2 Fixed Reactions

An expbutton click action will always redraw_tree() in addition to
anything else you want it to do.

=cut

