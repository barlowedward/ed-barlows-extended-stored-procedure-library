# Copyright (c) 1999-2006 by Edward Barlow. All rights reserved.
# This program is free software; you can redistribute it and/or modify
# it under the same terms as Perl itself.

package RosettaStone;

use Exporter;
use Carp;
use DBI;
use DBIFunc;

$VERSION= 1.0;
@ISA      = ('Exporter');
@EXPORT = ( 'get_system_info','get_schema_info','get_object_info' );

##################################################

use strict;

# Method Arguments.  Set to 1 if Required, optional if 0.  The key is function:value
my(%OK_OPTS)= (
		"get_system_info:-connection"		 	=> 1,
		"get_system_info:-db_type"		       	=> 1,
		"get_system_info:-reportname"			=> 1,
		"get_system_info:-returntype"			=> 1,
		"get_system_info:-debug"			=> 0,
		"get_system_info:-alldb"			=> 0,

		"get_schema_info:-connection"			=> 1,
		"get_schema_info:-db_type"			=> 1,
		"get_schema_info:-schema"			=> 1,
		"get_schema_info:-reportname"		    	=> 1,
		"get_schema_info:-schema"		       	=> 1,
		"get_schema_info:-returntype"			=> 1,
		"get_schema_info:-debug"			=> 0,
		"get_schema_info:-alldb"			=> 0,

		"get_object_info:-connection"			=> 1,
		"get_object_info:-db_type"			=> 1,
		"get_object_info:-schema"			=> 1,
		"get_object_info:-reportname"		    	=> 1,
		"get_object_info:-object"			=> 1,
		"get_object_info:-schema"			=> 1,
		"get_object_info:-returntype"			=> 1,
		"get_object_info:-debug"			=> 0,

);

# Get Info About The Database
sub get_system_info {
	my(%args)=@_;

	# The Schema Reports are as Follows
	#	- survey		system overall information (hash)
	#	- schema		database/tablespace information (hash)
	#	- file			storage/files (hash)
	#	- configuration:	configuration options (hash)
	#
	# The dynamic (not for survey) reports are
	#	- login
	#
	# The Real Time / Performance related reports are
	#	- blocks
	#	- connections
	#

	if( defined $args{-debug} ) {
		foreach (keys %args) { print "... $_ => $args{$_} \n"; }
	}

	return qw()
		if  $args{-reportname} eq "objreports";
	return qw(schema file configuration survey)
		if  $args{-reportname} eq "surveyreports";
	return qw(schema login file block configuration connections survey)
		if  $args{-reportname} eq "allreports";
	return qw(Audit Space Layout_1 Layout_2 Logins RevDb RevDev)
		if  $args{-reportname} eq "gemreports";

	validate_params("get_system_info",%args);
	my(%report_data) = (
		"oracle:schema:keys"	=>"select tablespace_name from sys.DBA_TABLESPACES",
		"oracle:login:keys"	=>"select username from   dba_users",
		"oracle:file:keys"	=>"",
		"oracle:block:keys"	=>"",
		"oracle:configuration:keys"	=>"",

		"oracle:schema:report"	=>"select tablespace_name,extent_management,allocation_type
				    ,initial_extent,next_extent,min_extlen,contents from   sys.DBA_TABLESPACES",
		"oracle:login:report"	=>"select username,default_tablespace,temporary_tablespace from   dba_users",
		"oracle:file:report"	=>"",
		"oracle:block:report"	=>"select sn.sid,
           sn.username,
           round(se.value/1024),
           io.block_gets,
           sn.command,
           blkr.blocks,
           blkd.blocks,
           sn.status,
           sn.machine,
           sn.osuser,
           sn.program,
           sn.process,
           sn.serial#
        from v\$session sn,
             v\$sesstat se,
             v\$statname n,
             v\$sess_io io,
             (select blocker.session_id sid,
              count(blocker.session_id) blocks
              from v\$locked_object blocker, v\$locked_object blocked
              where blocker.XIDUSN > 0
                and blocker.OBJECT_ID = blocked.OBJECT_ID
                and blocked.XIDUSN = 0
              group by blocker.session_id) blkr,
             (select blocked.session_id sid,
              count(blocked.session_id) blocks
              from v\$locked_object blocker, v\$locked_object blocked
              where blocker.XIDUSN > 0
                and blocker.OBJECT_ID = blocked.OBJECT_ID
                and blocked.XIDUSN = 0
              group by blocked.session_id) blkd
        where n.statistic# = se.statistic#
          and sn.sid = se.sid
          and n.name = 'session pga memory'
          and io.sid = sn.sid
          and sn.sid = blkr.sid(+)
          and sn.sid = blkd.sid(+)
        order by sn.sid",
		"oracle:configuration:report"	=>"",
		"oracle:connections:report"	=>"select  S.SID
			       ,S.username,S.OSuser,initcap(S.status) Status,S.terminal
       				,decode(S.lockwait,NULL,'No','Yes') Lockwait
       				,initcap(A.Name) Command
       				,S.process
       				,P.SPID 'UnixID'
			from    v\$SESSION S
       				,v\$PROCESS P
       				,sys.AUDIT_ACTIONS A
			where S.username is not null
			and   A.action = S.command
			and   P.ADDR = S.PADDR",

		"sybase:schema:keys"	=>"select name from master..sysdatabases",
		"sybase:login:keys"	=>"select name from master..syslogins",
		"sybase:file:keys"	=>"select name from master..sysdevices",
		"sybase:block:keys"	=>"",
		"sybase:configuration:keys"	=>"",

		"sybase:schema:report"	=>"exec sp__helpdb",
		"sybase:login:report"	=>"exec sp__helplogin",
		"sybase:file:report"	=>"exec sp__diskdevice",
		"sybase:block:report"	=>"exec sp__block",
		"sybase:configuration:report"	=>"exec sp__configure",
		"sybase:connections:report"	=>"exec sp__who \@dont_format='Y'",
	);
	my($key)=lc($args{-db_type}.":".$args{-reportname}.":".$args{-returntype});

	if( $key eq "sybase:survey:hash" ) {
		my(%DATA);
		$DATA{Backupserver_Name} = one_line_query("master","select srvnetname from master..sysservers where srvname='SYB_BACKUP'",$args{-connection});
		$DATA{Local_Server_Name}  = one_line_query("master","select \@\@servername",$args{-connection}) || "N.A.";
		$DATA{Server_Version} 	=join(" ",dbi_db_version(-connection=>$args{-connection}));
		$DATA{Version}  			= one_line_query("master","select \@\@VERSION",$args{-connection});

		my(@ver)=split("\/",$DATA{Version});
		$DATA{DateInstalled}=pop @ver;
		$DATA{Proc_Lib_Version}=one_line_query("master","exec sp__proclib_version",$args{-connection}) || "N.A.";
		# Find pages per megabyte
		$DATA{NumPgsMb}  = one_line_query("master"," select 1048576. / low
					from master.dbo.spt_values
					where number = 1 and type = 'E' ",$args{-connection});
		if( ! defined $DATA{NumPgsMb} or $DATA{NumPgsMb} eq "0" ) {
			warn "Warning No Pages Returned from spt_values????";
			$DATA{NumPgsMb}=512;
		}
		# Find out how much space was given to sybase
		$DATA{SpaceForServer}  = int(one_line_query("master","select sum(1+high-low)/".$DATA{NumPgsMb}." from sysdevices where status&2=2 ",$args{-connection}));
		return \%DATA;
	}

	# DateInstalled - ver[1]
	# Host_Name
	# Proc_Lib_Version
	# NumPgsMb
	
	if( $key eq "sqlsvr:survey:hash" ) {
		my(%DATA);
		$DATA{Local_Server_Name}  = one_line_query("master","select \@\@servername",$args{-connection}) || "N.A.";
		$DATA{Server_Version} =join(" ",dbi_db_version(-connection=>$args{-connection}));
		$DATA{Version}  = one_line_query("master","select \@\@VERSION",$args{-connection});

		$DATA{Host_Name}  = one_line_query("master","select serverproperty('MachineName')",$args{-connection});
		undef $DATA{Host_Name} if $DATA{Host_Name}=~/^42000:/;
		if( ! $DATA{Host_Name} ) {
			$DATA{Host_Name}  = one_line_query("master","exec xp_cmdshell 'hostname'",$args{-connection});
			undef $DATA{Host_Name} if $DATA{Host_Name}=~/^42000:/;
		}

		my(@ver)=split("\n",$DATA{Version});
		#print "DBG DBG - VERSION RETURNED\n".$DATA{Version};
		$DATA{DateInstalled}=$ver[1];
		$DATA{OSVersion}=$ver[3];
		$DATA{ServerVersionLong}=$ver[0];				
		$DATA{Proc_Lib_Version}=one_line_query("master","exec sp__proclib_version",$args{-connection}) || "N.A.";
		$DATA{NumPgsMb}  = one_line_query("master"," select 1048576. / low
									from master.dbo.spt_values
									where number = 1 and type = 'E' ",$args{-connection});
		if( ! defined $DATA{NumPgsMb} or $DATA{NumPgsMb} eq "0" ) {
			warn "Warning No Pages Returned from spt_values????";
			$DATA{NumPgsMb}=512;
		}
		$DATA{SpaceForServer}  = 0;	#int(one_line_query("master",
				#"select sum(1+high-low)/".$DATA{NumPgsMb}." from sysdevices where status&2=2 ",$args{-connection}));

		#foreach ( sort keys %DATA ) { print "DBG DBG: $_:",	$DATA{$_},	"\n"; }
		return \%DATA;
	}

	if( $key eq "sybase:schema:hash" ) {
		my(%Database_Info);
		my($q1)="select 1048576. / low from master.dbo.spt_values where number=1 and type = 'E'";
		my($NumPgsMb) = one_line_query("master",$q1,$args{-connection});
		if( ! defined $NumPgsMb or $NumPgsMb!~/^\d+\.0+$/ or $NumPgsMb == 0 ) {
			warn "Warning No Pages Returned from spt_values????\nrc=$NumPgsMb\nQuery=$q1\n";
			foreach (keys %args) { warn "... $_ => $args{$_} \n"; }
			$NumPgsMb=512;
		} else {
			$NumPgsMb=~s/\.0+$//;
		}		

		foreach (dbi_query(-connection=>$args{-connection},
				-db=>"master",
				-query=>"select name,status,status2,crdate from sysdatabases")) {

			my(%cur_db_hash);
			my($n,$s1,$s2,$cdate)=dbi_decode_row($_);
			next unless defined $n;

			$Database_Info{$n}=\%cur_db_hash;
			$cur_db_hash{status1}=$s1;
			$cur_db_hash{status2}=$s2;
			$cur_db_hash{CreateDate}=$cdate;

			my($logsame)=0;
			$logsame=1 if $s2<0;
			$s2 += 32768 if $s2<0;

			my($dboptions)="";
			my($dboptions_html)="";
			# select into
			if ($s1 & 4) { 	$dboptions.="Si "; $dboptions_html.="Si "; }

			# trunc log
			if ($s1 & 8) {	$dboptions.="Tl "; $dboptions_html.="Tl "; }

			if ($s1 & 16) {	$dboptions.="NoChkRcvry "; $dboptions_html.="<FONT COLOR=BLUE>NoChkRcvry</FONT>  ";			}
			if ($s1 & 32) {
				$dboptions.="NoChkRcvry ";
				$dboptions_html.="<FONT COLOR=BLUE>NoChkRcvry</FONT>  ";
			}
			if ($s1 & 256) {
				$dboptions.="Suspect ";
				$dboptions_html.="<FONT COLOR=BLUE>Suspect</FONT>  ";
			}
			if ($s1 & 1024) {
				$dboptions.="ReadOnly ";
				$dboptions_html.="<FONT COLOR=BLUE>ReadOnly</FONT>  ";
			}
			if ($s1 & 2048) {
				$dboptions.="DboOnly ";
				$dboptions_html.="<FONT COLOR=RED>DboOnly</FONT>  ";
			}
			if ($s1 & 4096) {
				$dboptions.="SglUser ";
				$dboptions_html.="<FONT COLOR=RED>SglUser</FONT>  ";
			}
			if ($s2 & 16) {
				$dboptions.="Offline ";
				$dboptions_html.="<FONT COLOR=RED>Offline</FONT>  ";
			}
			if ($s2 & 32) {
				$dboptions.="OfflineToLoad ";
				$dboptions_html.="<FONT COLOR=RED>OfflineToLoad</FONT>  ";
			}

			if ($logsame==1 and ! $s1 & 8) {
				$cur_db_hash{NoSepLog}=1;
				$dboptions.="NoSepLog ";
				$dboptions_html.="<FONT COLOR=RED>NoSepLog</FONT>  ";
			} else {
				$cur_db_hash{NoSepLog}=0;
			}

			if( ($s1 & 32) or ($s1 & 256 ) or ($s1  & 4096 )
				or ($s2  & 16 ) or ($s2  & 32 ) ) {
				$cur_db_hash{Offline}=1;
			} else {
				$cur_db_hash{Offline}=0;
			}

			chomp $dboptions;
			chomp $dboptions_html;

			$cur_db_hash{DBoptions}		= $dboptions;
			$cur_db_hash{DBoptionsHtml}	= $dboptions_html;

			if( $cur_db_hash{Offline} ) {
				$cur_db_hash{HasDbAccess}="No";
				next;
			} else {
				$cur_db_hash{HasDbAccess}="Yes";
			}

			$cur_db_hash{Space_Allocated} = 0;
			$cur_db_hash{LogSpaceByDb} = 0;
			my($q2)="select sum(size) from master.dbo.sysusages u where u.dbid = db_id(\"$n\") and u.segmap != 4";
			#print "DBG DBG q2=$q2\n";
			foreach ( dbi_query(-connection=>$args{-connection},
				-db=>"master",
				-query=>$q2)){
				my(@rc)=dbi_decode_row($_);
				next unless $rc[0];	# apparently we need to ignore the null row
				#print "DBG DBG: Rc=",join("~~",@rc),"\n";
				$cur_db_hash{Space_Allocated} += ($rc[0]/$NumPgsMb);
				#$DATA{Space_Allocated} += ($rc[0]/$DATA{NumPgsMb});
			}

			$q2="select sum(size) from master.dbo.sysusages u where u.dbid = db_id(\"$n\") and u.segmap = 4";
			#print "DBG DBG q2=$q2\n";
			foreach ( dbi_query(
					-connection=>$args{-connection},
					-db=>"master",
					-query=>$q2)){
				my(@rc)=dbi_decode_row($_);
				next unless $rc[0];	# apparently we need to ignore the null row
				#print "DBG DBG: Rc=",join("~~",@rc),"\n";
				#print "DBG DBG: size= $rc[0] at line ",__LINE__,"\n";
				$cur_db_hash{Space_Allocated} += $rc[0]/$NumPgsMb;
				$cur_db_hash{LogSpaceByDb} += $rc[0]/$NumPgsMb;
				#$DATA{Space_Allocated} += $rc[0]/$DATA{NumPgsMb};
			}
		}
		return \%Database_Info;
	}

	if( $key eq "sqlsvr:schema:hash" ) {
		my($NumPgsMb) = one_line_query("master"," select 1048576. / low
					from master.dbo.spt_values
					where number = 1 and type = 'E' ",$args{-connection});
		my(%Database_Info);
		foreach (dbi_query(-connection=>$args{-connection},-db=>"master",
			-query=>"select name,status,status2,crdate,cmptlevel,has_dbaccess(name) from sysdatabases")) {
			my(%dbhash);
			my($n,$s1,$s2,$cdate,$cmptlevel,$hasdbaccess)=dbi_decode_row($_);
			next unless defined $n;

			$dbhash{status1}=$s1;
			$dbhash{status2}=$s2;
			$dbhash{CreateDate}=$cdate;
			$dbhash{CmptLevel}=$cmptlevel;
			if( $hasdbaccess==0 ) {
				$dbhash{HasDbAccess}="No";
			} else {
				$dbhash{HasDbAccess}="Yes";
			}


			my($dboptions)="";
			my($dboptions_html)="";

			if ($s1 & 4) {
				$dboptions.="Si ";
				$dboptions_html.="Si ";
			}

			if ($s1 & 8) {
				$dboptions.="Tl ";
				$dboptions_html.="Tl ";
			}

			if ($s1 & 16) {
				$dboptions.="TornPage ";
				$dboptions_html.="<FONT COLOR=BLUE>TornPage</FONT>  ";
			}

			if ($s1 & 256) {
				$dboptions.="Suspect ";
				$dboptions_html.="<FONT COLOR=BLUE>Suspect</FONT>  ";
			}

			if ($s1 & 1024) {
				$dboptions.="ReadOnly ";
				$dboptions_html.="<FONT COLOR=BLUE>ReadOnly</FONT>  ";
			}

			if ($s1 & 2048) {
				$dboptions.="DboOnly ";
				$dboptions_html.="<FONT COLOR=RED>DboOnly</FONT>  ";
			}

			if ($s1 & 4096) {
				$dboptions.="SglUser ";
				$dboptions_html.="<FONT COLOR=RED>SglUser</FONT>  ";
			}

			if ($s1 & 32 or $s1 & 64 or $s1 & 128 or $s1&256 or $s1&512) {
				$dbhash{Offline}=1;
				$dboptions.="Offline ";
				$dboptions_html.="<FONT COLOR=RED>Offline</FONT>  ";
				$dbhash{Space_Allocated} =0;
				$dbhash{LogSpaceByDb} = 0;

			} else {
				foreach ( dbi_query(
						-connection=>$args{-connection},
						-db=>$n,
						-query=>"select size,status&0x40 from sysfiles")){
					my(@rc)=dbi_decode_row($_);
					$dbhash{Space_Allocated} += $rc[0]/128;
					$dbhash{LogSpaceByDb} += $rc[0]/128 if $rc[0]!=0;
				}
				$dbhash{Offline}=0;
			}

			if ($s1 & 4194304) {
				$dboptions.="AutoShrink ";
				$dboptions_html.="<FONT COLOR=RED>AutoShrink</FONT>  ";
			}
			chomp $dboptions;
			chomp $dboptions_html;
			$dbhash{DBoptions}=$dboptions;
			$dbhash{DBoptionsHtml}=$dboptions_html;

			$Database_Info{$n}=\%dbhash;
		}
		return \%Database_Info;
	}

	# CONFIGURATION REPORT - CATEGORY, OPTION, VALUE, DEFAULT
	if( $key eq "sqlsvr:configuration:hash" ) {
		my($query)="select config,s=CASE status WHEN 0 then 'Static' WHEN 1 then 'Dynamic' when 2 then 'Adv. Static'
		when 3 then 'Adv. Dynamic' end, comment,value from master..sysconfigures";
		my(%configures);
		foreach ( dbi_query(-connection=>$args{-connection},-db=>"master",-query=>$query)){
        		my($rowid,$id,$cmt,$v)=dbi_decode_row($_);
				next unless defined $rowid;
        		my(%dat);
        		$dat{category}=$id;
        		$dat{comment}=$cmt;
        		$dat{value}=$v;
        		$configures{$rowid}=\%dat;
        	}
        	return \%configures;
	}

	if( $key eq "sybase:configuration:hash" ) {
		my(%configures);
		my($query)="select distinct c.config,
        		Category  	=parent.name     ,
        		Parameter       =x.comment   ,
        		Value           =c.value     ,
        		DefValue        = isnull(c.defvalue,'0')
		from     master..sysconfigures x,               -- For the name
         		master..syscurconfigs c,                -- For the values
         		master..sysconfigures parent                -- parent
		where     c.config=x.config
		and       x.parent=parent.config
		and       x.comment != parent.name";

        	foreach ( dbi_query(-connection=>$args{-connection},-db=>"master",-query=>$query)){
        		my($rowid,$id,$cmt,$v,$d)=dbi_decode_row($_);
        		$d=~s/\s//g;
        		next unless defined $d;
        		my(%dat);
        		$dat{category}=$id;
        		$dat{comment}=$cmt;
        		$dat{value}=$v;
        		$dat{default}=$d;
        		$configures{$rowid}=\%dat;
        	}
        	return \%configures;
	}

	if( $key eq "sqlsvr:file:hash" ) {
		my(%device_info);

		my(@db)= dbi_query(-connection=>$args{-connection},-db=>"master",
			-query=>"select name from sysdatabases
				where has_dbaccess(name)=1");
		foreach ( @db ) {
			my($db)= dbi_decode_row($_);
        		foreach ( dbi_query(-connection=>$args{-connection},
        			-db=>$db,
        			-query=>"select name,filename,size,maxsize,growth,status from sysfiles" )){
        			my($d,$p,$size,$a,$g,$S)=dbi_decode_row($_);
        			next unless defined $d;	# in case cant use is not found!
        			#$dbhash{Space_Allocated} += $size if $S & 2;
        			#$dbhash{LogSpaceByDb} += $size   if $S & 4;
        			my(%device);
        			$d=~s/\s//g;
        			$p=~s/\s//g;
        			$size=~s/\s//g;
        			$a=~s/\s//g;
        			$g=~s/\s//g;

        			$device{database}=$db;
        			$device{name}=$d;
        			$device{phys}=$p;
        			$device{size}=$size;
        			$device{maxsize}=$a;
        			$device{growth}=$g;
        			$device{status}=$S;
        			$device_info{$d}=\%device;
        		}
		}
		return \%device_info;
	}

	if( $key eq "sybase:file:hash" ) {
		my(%device_size);
		foreach ( dbi_query(-connection=>$args{-connection},-db=>"master",-query=>"sp__diskdevice \@dont_format='Y'") ) {
        		my($d,$p,$s,$a,$f)=dbi_decode_row($_);
        		next unless defined $d;	# in case sp__diskdevice is not found!
        		$d=~s/\s//g;
        		$p=~s/\s//g;
        		$s=~s/\s//g;
        		$a=~s/\s//g;
        		$f=~s/\s//g;

        		my(%device);
        		$device{name}=$d;
        		$device{phys}=$p;
        		$device{size}=$s;
        		$device{alloc}=$a;
        		$device{free}=$f;
        		$device_size{$d}=\%device;
		}
		return \%device_size;
	}

	my($rd)=$report_data{$key};
	return do_a_report(%args,-query=>$rd);
}

# Get Info About The Database
sub get_schema_info {
	my(%args)=@_;

	return qw( table trigger view procedure rule system default sequence index permission)
		if  $args{-reportname} eq "objreports";
	return qw(summary operation users depend audit)
		if  $args{-reportname} eq "allreports";

	validate_params("get_schema_info",%args);
	my(%report_data) = (
		"oracle:table:keys"	=> "",

		"sybase:object:keys"	=>"select name from sysobjects",
		"sybase:user:keys"	=>"select name from sysusers",
		"sybase:table:keys"	=>"select name from sysobjects where type='U'",
		"sybase:view:keys"	=>"select name from sysobjects where type='V'",
		"sybase:trigger:keys"	=>"select name from sysobjects where type='TR'",
		"sybase:procedure:keys"	=>"select name from sysobjects where type='P'",
		"sybase:rule:keys"	=>"select name from sysobjects where type='R'",
		"sybase:system:keys"	=>"select name from sysobjects where type='S'",
		"sybase:default:keys"	=>"select name from sysobjects where type='D'",
		"sybase:sequence:keys"	=>"",

		"sybase:summary:report"			=>"sp__who",
		"sybase:operation:report" 		=>"",
		"sybase:user:report"			=>"sp__helpuser",
		"sybase:table:report"			=>"sp__helptable",
		"sybase:index:report"			=>"sp__helpindex",
		"sybase:procedure:report"		=>"sp__helpproc",
		"sybase:trigger:report"			=>"sp__trigger",
		"sybase:rule:report"			=>"sp__helprule",
		"sybase:type:report"			=>"sp__helptype",
		#"sybase:dflt:report"			=>"sp__helpdefault",
		"sybase:permission:report"		=>"sp__helprotect",
		"sybase:depend:report"			=>"sp__depends",
		"sybase:audit:report" 			=>"sp__noindex\nsp__colconflict\nsp__colnull",

		"sybase:object:report"		=>"exec sp__ls",
		#"sybase:user:report"		=>"exec sp__helpuser",
		#"sybase:table:report"		=>"exec sp__helptable",
		"sybase:view:report"		=>"exec sp__helpview",
		#"sybase:trigger:report"		=>"exec sp__trigger",
		#"sybase:procedure:report"	=>"exec sp__helpproc",
		#"sybase:rule:report"		=>"exec sp__helprule",
		"sybase:system:report"		=>"select * from sysobjects where type='S'",
		"sybase:default:report"		=>"select name from sysobjects where type='D'",
		"sybase:sequence:report"	=>"",

		"sqlsvr:object:keys"	=>"select name from sysobjects",
		"sqlsvr:user:keys"	=>"select name from sysusers",
		"sqlsvr:table:keys"	=>"select name from sysobjects where type='U'",
		"sqlsvr:view:keys"	=>"select name from sysobjects where type='V'",
		"sqlsvr:trigger:keys"	=>"select name from sysobjects where type='TR'",
		"sqlsvr:procedure:keys"	=>"select name from sysobjects where type='P'",
		"sqlsvr:rule:keys"	=>"select name from sysobjects where type='R'",
		"sqlsvr:system:keys"	=>"select name from sysobjects where type='S'",
		"sqlsvr:default:keys"	=>"select name from sysobjects where type='D'",
		"sqlsvr:sequence:keys"	=>"",

		"sqlsvr:object:report"	=>"exec sp__ls",
		#"sqlsvr:user:report"	=>"exec sp__helpuser",
		#"sqlsvr:table:report"	=>"exec sp__helptable",
		"sqlsvr:view:report"	=>"exec sp__helpview",
		#"sqlsvr:trigger:report"	=>"exec sp__trigger",
		#"sqlsvr:procedure:report"	=>"exec sp__helpproc",
		#"sqlsvr:rule:report"	=>"exec sp__helprule",
		"sqlsvr:system:report"	=>"select * from sysobjects where type='S'",
		"sqlsvr:default:report"	=>"select name from sysobjects where type='D'",
		"sqlsvr:sequence:report"=>"",

		"sqlsvr:summary:report"			=>"sp__who",
		"sqlsvr:operation:report" 		=>"",
		"sqlsvr:user:report"			=>"sp__helpuser",
		"sqlsvr:table:report"			=>"sp__helptable",
		"sqlsvr:index:report"			=>"sp__helpindex",
		"sqlsvr:procedure:report"		=>"sp__helpproc",
		"sqlsvr:trigger:report"			=>"sp__trigger",
		"sqlsvr:rule:report"			=>"sp__helprule",
		"sqlsvr:type:report"			=>"sp__helptype",
		#"sqlsvr:dflt:report"			=>"sp__helpdefault",
		"sqlsvr:permission:report"		=>"sp__helprotect",
		"sqlsvr:depend:report"			=>"sp__depends",
		"sqlsvr:audit:report" 			=>"sp__noindex\nsp__colconflict\nsp__colnull",

	);

	my($rd)=$report_data{lc($args{-db_type}.":".$args{-reportname}.":".$args{-returntype})};
	print "[RosettaStone.pm] get_schema_info() called but no translation available\n"
			if defined $args{-debug} and ! defined $rd or $rd eq "";
	return undef if ! defined $rd or $rd eq "";
	print "[RosettaStone.pm] get_schema_info() called for key=$rd\n"
			if defined $args{-debug};
	return do_a_report(%args,-query=>$rd);
	#		return run_query($args{-connection},$args{-schema},"select tablespace_name from sys.DBA_TABLESPACES")
	#		return run_query($args{-connection},$args{-schema},"select tablespace_name,extent_management,allocation_type
	#			    ,initial_extent,next_extent,min_extlen,contents from   sys.DBA_TABLESPACES")
	#		return run_query($args{-connection},$args{-schema},"select index_name from all_indexes")
	#		return run_query($args{-connection},$args{-schema},"select * from all_indexes where index_name='".$args{-object}."'")
	#		return run_query($args{-connection},$args{-schema},"select * from all_indexes")
}

sub one_line_query {
		my($db,$query,$conn,$debug)=@_;
		print "QUERY: $query\n" if $debug;
		foreach (dbi_query(-connection=>$conn,-db=>$db,-query=>$query, -debug=>$debug)){
			my(@d)=dbi_decode_row($_);
			print "returning ",@d,"\n" if $debug;
			return "[Not Installed]" if $d[0]=~/42000: Could not find stored procedure/
				or $d[0]=~/not found. Specify owner.object/;
			return $d[0];
		}
		print "DONE QUERY: $query\n" if $debug;
}

sub do_a_report {
	my(%args)=@_;
	my(@rc);
	if( defined $args{-query} and $args{-query} ne "" ) {

		print "[RosettaStone.pm] returntype=$args{-returntype} Query=$args{-query}\n"
			if defined $args{-debug};
		my($print_header)=undef;
		$print_header=1 if $args{-returntype} eq "report";

		$args{-schema}="master" if ! defined $args{-schema} and $args{-db_type}=~/sybase/i;
		$args{-schema}="master" if ! defined $args{-schema} and $args{-db_type}=~/sqlsvr/i;

		@rc=dbi_query(	-query=>$args{-query},
				-connection=>$args{-connection},
				-db=>$args{-schema}, #-debug=>1,
				-print_hdr=>$print_header );
		if( $args{-returntype} eq "keys" ) {
			my(@output);
			foreach my $dat ( @rc ) { push @output, dbi_decode_row($dat); }
			return sort @output;
		} elsif( $args{-returntype} eq "hash" ) {	# return info in hash format
			my(%DAT);
			foreach my $dat ( @rc ) {
				my(@x)=dbi_decode_row($dat);
				$DAT{$x[0]} = join(" ", @x );
			}
			return %DAT;
			# return dbi_reformat_results(@rc);
		} elsif( $args{-returntype} eq "ddl" ) {	# we dont reformat ddl
			my(@output);
			foreach ( @rc ) { push @output, join("",dbi_decode_row($_)); }
			return @output;
		} elsif( $args{-returntype} eq "report" ) {
			#my(@output);
			#my($rowid)=1;
			#print "IN REPORT\n";
			#foreach my $dat ( @rc ) {
			#	print "Row $rowid ",dbi_decode_row($dat),"\n";
			#	push @output, join(" ",dbi_decode_row($dat));
			#	$rowid++;
			#}
			#return @output;
			return dbi_reformat_results(@rc);
		}
	} elsif( $args{-query} eq "" ) {
		print "[RosettaStone.pm] Incomplete Translation Found\n"
			if defined $args{-debug};
		push @rc, "RosettaStone.pm: Incomplete Translation Found\n";
		return @rc;
	} else {
		print "[RosettaStone.pm] No Translation Found\n"
			if defined $args{-debug};
		push @rc, "RosettaStone.pm: No Translation Found\n";
		foreach (sort keys %args) { push @rc, "$_  => $args{$_}\n"; }
		return @rc;
	}
	die "Invalid Return Type\n"
}

# Get Info About Objects
sub get_object_info {
	my(%args)=@_;

	return qw( table trigger view procedure rule system default sequence)
		if  $args{-reportname} eq "objreports";
	return qw( user table trigger view procedure rule system default sequence)
		if  $args{-reportname} eq "allreports";

	validate_params("get_object_info",%args);
	my(%report_data) = (
		"oracle:table:ddl"	=> "",

		"sybase:user:ddl"	=>"exec sp__helpuser $args{-object}",
		"sybase:table:ddl"	=>"exec sp__revtable $args{-object}",
		"sybase:view:ddl"	=>"exec sp__helptext $args{-object}",
		"sybase:trigger:ddl"	=>"exec sp__helptext $args{-object}",
		"sybase:procedure:ddl"	=>"exec sp__helptext $args{-object}",
		"sybase:rule:ddl"	=>"exec sp__helptext $args{-object}",
		"sybase:system:ddl"	=>"exec sp__revtable $args{-object}",
		"sybase:default:ddl"	=>"exec sp__helptext $args{-object}",
		"sybase:sequence:ddl"	=>"",

		"sybase:user:report"	=>"exec sp__helpuser",
		"sybase:table:report"	=>"exec sp__helptable",
		"sybase:view:report"	=>"exec sp__helpview",
		"sybase:trigger:report"	=>"exec sp__trigger",
		"sybase:procedure:report"=>"exec sp__helpproc",
		"sybase:rule:report"	=>"exec sp__helprule",
		"sybase:system:report"	=>"select * from sysobjects where type='S'",
		"sybase:default:report"	=>"select name from sysobjects where type='D'",
		"sybase:sequence:report"=>"",

		"sqlsvr:user:ddl"	=>"exec sp__helpuser $args{-object}",
		"sqlsvr:table:ddl"	=>"exec sp__revtable $args{-object}",
		"sqlsvr:view:ddl"	=>"exec sp__helptext $args{-object}",
		"sqlsvr:trigger:ddl"	=>"exec sp__helptext $args{-object}",
		"sqlsvr:procedure:ddl"	=>"exec sp__helptext $args{-object}",
		"sqlsvr:rule:ddl"	=>"exec sp__helptext $args{-object}",
		"sqlsvr:system:ddl"	=>"exec sp__revtable $args{-object}",
		"sqlsvr:default:ddl"	=>"exec sp__helptext $args{-object}",
		"sqlsvr:sequence:ddl"	=>"",


		"sqlsvr:user:report"	=>"exec sp__helpuser",
		"sqlsvr:table:report"	=>"exec sp__helptable",
		"sqlsvr:view:report"	=>"exec sp__helpview",
		"sqlsvr:trigger:report"	=>"exec sp__trigger",
		"sqlsvr:procedure:report"	=>"exec sp__helpproc",
		"sqlsvr:rule:report"	=>"exec sp__helprule",
		"sqlsvr:system:report"	=>"select * from sysobjects where type='S'",
		"sqlsvr:default:report"	=>"select name from sysobjects where type='D'",
		"sqlsvr:sequence:report"=>"",
	);

	my($rd)=$report_data{lc($args{-db_type}.":".$args{-reportname}.":".$args{-returntype})};
	return do_a_report(%args,-query=>$rd);
}


sub validate_params
{
	my($func,%OPT)=@_;

	# check bad keys
	foreach (keys %OPT) {
		next if defined $OK_OPTS{$func.":".$_};

		# did you mix up name=>x with -name=>x
		croak("ERROR: Incorrect $func Arguments - Change $_ argument to -$_\n")
			if defined $OK_OPTS{$func.":-".$_};
		# foreach my $x (keys %OPT) {
			# print __LINE__," ARG ~".$x."~  = ~".$OPT{$x}."~ \n";
		# }

		croak("ERROR: Function $func Does Not Accept Parameter Named $_\nAllowed Arguments: ".  join(" ",grep(s/$func://,keys %OK_OPTS)));
	}

	# check missing required keys
	foreach (keys %OK_OPTS) {
		next unless /^$func:/;
		next unless $OK_OPTS{$_} == 1;
		s/$func://;
		next if defined $OPT{$_};
		my($str)="";
		#foreach ( keys %OPT) { 	$str.=$_." "; }
		foreach ( keys %OPT) { 	$str.=$_."=>".$OPT{$_}." "; }
		croak("ERROR: Function $func Requires Parameter Named $_\nPassed Args: $str\n");
	}

	if( defined $OPT{-debug} ) {
		my($str)="";
		foreach ( keys %OPT ) {
			$str.="$_=$OPT{$_}," unless /^-password/ or /^-debug/ or /^-connection/ or /^-alldb/;
		}
		$str=~s/,$//;
		print "[RosettaStone.pm] $func($str)\n";
	}
}

sub get_error_info {
}

1;

__END__

=head1 NAME

RosettaStone.pm - A Rosetta Stone For Database Administration Commands

=head2 DESCRIPTION

The Rosetta Stone provided translations between ancient languages.  This
Rosetta Ston provides translations between database administration commands.
There are a variety of modules that deal with application sql, this module
concentrates on system level sql.  Because concepts in system design vary
greatly between databases, not all reports will look identical, and some
reports may not even be available or relevant on some target database systems.

This module uses DBI for connectivity.  It uses a combination of internal DBI
function calls and its own direct SQL calls to provide a uniform interface.

The user is responsible for providing a valid DBI connection to these routines.

=head2 FUNCTIONS

This module has 3 functions.

=over 4

=item * get_system_info()

returns information about
a system, server, database or whatever the terminology to call the highest level database "thing"
might be.  Typical reports might include "options", listing system options, "devices", listing files
or partitions the system uses, "schemas", which will describe databases/schemas in the system,
and "logins", listing system users.

=item * get_schema_info()

returns information about a tablespace, database, or whatever
the primary unit of partitioning of the "thing" might be.  Typical reports might include
"tables", "procedures", "properties".

=item * get_object_info()

returns information about objects within a schema.  Typical reports might include "ddl", which would
return DDL create statements and "help" which would summarize the object.

=back

Because of the differences in terminology, the high level "thing" will
henceforth be referred to as a system, the partitions will be schemas, and objects will of course be
referred to as objects.

=head2 FUNCTION OPTIONS

The following arguments will be provided to all functions unless noted.

=over 4

=item * -connection

This required argument is DBI connection.  Queries will be run on this
connection

=item * -db_type

This represents the DBI type of the connection.  Possible values are ODBC,
Sybase, Oracle
or any other DBI server type.

=item * -reportname

This represents the name of the report you wish to run.

The special reports "allreports" and "objreports" returns an array of allowed reports.  if a -db_type
is passed, it returns only reports allowed for that -db_type.  if it is not passed,
it returns an array of all reports.  If the report is special, an array will
always be returned.

=item * -debug

The -debug item allows optional printing of diagnostics.  It is a pointer to a function, not a
true false argument.  Thus, use -debug=>\&print, to get prints to work.

=item * -schema

The schema, database, or tablespace of interest.  Not allowed on get_system_info()

=item * -object

The object of interest. Only allowed for get_object_info()

=item * -returntype

The -returntype value may be the string "hash", "keys", "ddl", or "report".  if
"hash", a hash is returned.  This
hash will be compatible with XML::Simple.  if report or ddl, then the return will
be a single string, with
appropriate new lines, columns justified, and a header.  This report is
suitable for printing to standard output.
if "keys", then the report will be an array of keys.

ddl is, of course, only appropriate if a specific object is selected.

=back

=head2 Return Values

The return value for the function is dependant on the -returntype argument.  If
-returntype is "hash", a hash is returned, if "keys", an array, and if "report", a
scalar.

The return value will be undef if an error is encountered, and the function get_error_info()
can be called with no arguments to return the error string.
