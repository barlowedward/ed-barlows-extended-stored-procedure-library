# Copyright (c) 2003 by Edward Barlow. All rights reserved.
# This program is free software; you can redistribute it and/or modify
# it under the same terms as Perl itself.

package PluginManager;

use strict;
use vars qw($VERSION @ISA @EXPORT @EXPORT_OK);
use Exporter;
use strict;
use Repository;
use XML::Simple;
use Tk;
use LWP::Simple;
use URI::Escape;
use	Carp qw(cluck);

use Do_Time;

$VERSION= 1.0;
my $opt_d;

@ISA  = ( 'Exporter' );
my %package_imports;
my %xml_by_plugin;
my $catalog;
my %package_filename;
my($IGNORE_EVENTS)=1;
my($HOME_BASE_URL)='http://www.edbarlow.com/gem_store/server.pl';

my(@root_rtclick_items)=("Welcome","Logout");

# Event Arguments
my(%last_event_args);
$last_event_args{-newtab}="welcome";

sub new {
	my( $class,%args) = @_;

	opendir( DIR, "plugins" ) or die "Cant open plugins directory : $!\n";
   	my(@dirlist)=grep(/\.pm$/,readdir(DIR));
   	closedir(DIR);

	my $self  = bless { %args }, $class;
	#foreach (keys %args) { print "OK $_ $args{$_} \n"; }

	if( defined $args{-modulename} ) {
		my $packagename =  $args{-modulename};
		$packagename =~ s/.pm$//;
		if( -r "lib/$packagename.pm" ) {
           		$package_filename{$packagename} = "lib/$packagename.pm";
           	} elsif( -r "plugins/$packagename.pm" ) {
           		$package_filename{$packagename} = "plugins/$packagename.pm";
           		# import servers.pm if its in the plugins directory
#           		my($packagename)="servers";
#           		if( eval "require $packagename" ) {
#        			$packagename->import();
#        			$package_imports{ $packagename }= new $packagename(
#        				-debug 			=> $args{-debug},
#        				-developer_mode		=> $args{-developer_mode},
#        				-connectionmanager	=> $args{-connectionmanager},
#        				-package_imports 	=> \%package_imports,
#        				-modulename		=> $args{-modulename},
#        				-filename 		=> "lib/$packagename.pm"
#        			);
#        		} else {
#	        		die "*  Error - Cant Load Package $packagename\n",join("\n",$@),"\n";
#        		}
           	} else {
           		die "Cant find module $packagename in lib or plugins subdirectory.\n";
           	}

           	main::_statusmsg( "[plgmgr.pm] Importing package $packagename\n");
        	if( eval "require $packagename" ) {
        		$packagename->import();
        		$package_imports{ $packagename }= new $packagename(
        			-debug 			=> $args{-debug},
        			-developer_mode		=> $args{-developer_mode},
        			-connectionmanager	=> $args{-connectionmanager},
        			-package_imports 	=> \%package_imports,
        			-modulename		=> $args{-modulename},
        			-filename 		=> "lib/$packagename.pm"
        			);
        	} else {
        		die "*  Error - Cant Load Package $packagename\n",join("\n",$@),"\n";
        	}
        	main::_debugmsg( "[plgmgr.pm] Done Importing package $packagename\n");

	} else {
        	# if you havnt used the package, do a use on the file and a new() and store
        	foreach my $packagename ( @dirlist ) {
        		$packagename=~s/.pm$//;
           		$package_filename{$packagename} = "plugins/$packagename.pm";
           		main::_statusmsg( "[plgmgr.pm] Importing package $packagename\n");
        		if( eval "require $packagename" ) {
        			$packagename->import();
        			$package_imports{ $packagename }= new $packagename(
        				#-pluginmanager	 	=> \$self,		# doesnt work
        				#-plugindata	 	=> \&plugin_data,	# doesnt work
        				-debug 			=> $args{-debug},
        				-developer_mode		=> $args{-developer_mode},
        				-connectionmanager	=> $args{-connectionmanager},
        				-package_imports 	=> \%package_imports,
        				-filename 		=> "plugins/$packagename.pm"
        				);
        		} else {
        			die "*  Error - Cant Load Package $packagename\n",join("\n",$@),"\n";
        		}
        		main::_debugmsg( "[plgmgr.pm] Done Importing package $packagename\n");
        	}
	}
	main::_debugmsg( "[plgmgr.pm] Done Importing packages\n");

	opendir( DIR, "plugins" ) or die "Cant open plugins directory : $!\n";
   	my(@xmllist)=grep(/\.xml$/,readdir(DIR));
   	closedir(DIR);

	foreach my $xfile ( @xmllist ) {

		next if defined $args{-modulename} and $xfile ne $args{-modulename}.".pm";
		#die "Loading $xfile $args{-modulename}\n";

		main::_debugmsg( "[plgmgr.pm] reading file $xfile\n" );
		my($repository) = XMLin( "plugins/".$xfile );

		if( $xfile eq "catalog.xml" ) {
			$catalog=$repository;
			next;
		}

		foreach ( qw/help_url name version copyright long_description short_description/ ) {
			main::_statusmsg(  "WARNING: plugins/$xfile Contains no key $_\n" )
				unless defined $$repository{$_};
		}
		foreach ( qw/type key expires notice/ ) {
			main::_statusmsg(  "WARNING: plugins/$xfile Copyright Notice contains no key $_\n" )
				unless defined $$repository{copyright}->{$_};
		}
		# validate the repository
		#use Data::Dumper;
		#print "#############################\n";
		#print Dumper($repository),"\n";
		#print "#############################\n";

   		$xfile=~s/.xml$//;
   		$xml_by_plugin{$xfile}=$repository;
   	}
   	main::_debugmsg( "[plgmgr.pm] Plugin Manager new()\n");

   	return $self;
}

sub plugin_names { return keys %package_imports; }
sub get_last_event_info {
	my($package,$key,$val)=@_;
	if( defined $key and defined $val ) {
		$package->{-debugmsg}->(  "[plgmgr.pm] get_last_event_info() overriding event $key to $val\n" );
		$last_event_args{$key}=$val;
	}
	return %last_event_args;
}

sub ignore_events {
	my($package,$onoff)=@_;
	# $package->{-debugmsg}->( "[plgmgr.pm] SETTING IGNORE EVENTS TO $onoff\n" );
	$IGNORE_EVENTS=$onoff;
}

sub get_catalog { return $$catalog{package}; }
sub fetch_plugin_from_web {
	my($package,$plugin)=@_;
	my($dl_rul)=$HOME_BASE_URL."?op=fetch&file=$plugin";
	#print "DL URL = $dl_rul\n";

   	my($content)=get( $dl_rul );
   	if( ! defined $content ) {
   		$main::XMLDATA{mainWindow}->messageBox(-message=>"Unable to fetch plugin $plugin.  Check Proxy Settings?",
   				-title=>"ERROR");
		return;
	}

	if( $content =~ /^Error: No File/ ) {
		$main::XMLDATA{mainWindow}->messageBox(-message=>"Unable to fetch plugin $plugin.  File Not Found?",
   				-title=>"ERROR");
		return;
	}

	my($file)="$main::XMLDATA{gem_root_directory}/plugins/$plugin.pm";
	main::archive_file($file,10) if -r $file;
	$content=~s/\r\n/\n/g;	# doesnt seem to help sigh
	open (CAT,"> $file");
	print CAT  $content;
	close CAT;

	$main::XMLDATA{mainWindow}->messageBox(-message=>"Successful Plugin Download\nYou must restart the application to use this plugin.", -title=>"OK");
}


sub register_on_the_web  {
	my($package,$admin_name,$admin_email,$phone_number,$company ) = @_;

	my($dl_rul)=$HOME_BASE_URL;
	$dl_rul.="?op=register&name=".uri_escape($admin_name);
	$dl_rul.="&email=".uri_escape($admin_email);
	$dl_rul.="&phone=".uri_escape($phone_number);
	$dl_rul.="&company=".uri_escape($company);
	#print "DL=$dl_rul\n";

	my($content)=get( $dl_rul );
   	if( ! defined $content ) {
   		$main::XMLDATA{mainWindow}->messageBox(
   				-message=>"Unable to register with the Store.\nThis is probably due to firewall issues.\nPlease send a registration confirmation mail to barlowedward\@hotmail.com.\nThis will not impact your current installation.",
   				-title=>"ERROR");
		return;
	}

	$main::XMLDATA{mainWindow}->messageBox(-message=>"Registration Successful: $content", -title=>"OK");
	return $content;
}

sub fetch_catalog_from_web {
	my($package,$admin_name,$admin_email,$phone_number,$company ) = @_;
	my($dl_rul)=$HOME_BASE_URL."?op=catalog";
   	my($content)=get( $dl_rul );
   	if( ! defined $content ) {
   		$main::XMLDATA{mainWindow}->messageBox(-message=>"Unable to fetch catalog.  Check Proxy Settings?",
   				-title=>"ERROR");
		return;
	}

	my($file)="$main::XMLDATA{gem_root_directory}/plugins/catalog.xml";
	unlink $file if -r $file;
	open (CAT,"> $file");
	print CAT  $content;
	close CAT;

	$catalog=XMLin($content);
	$main::XMLDATA{mainWindow}->messageBox(-message=>"Successful Catalog Fetch", -title=>"OK");
}

#my($oldpage)="welcome";
#my($oldtab, $curpage, $curtab);
sub send_event{
	my $package = shift;
	return if $IGNORE_EVENTS;
	die "Package must be defined\n" unless defined $package;

	my %args = @_;

	die "Cant Pass -oldpage or -oldtab to send_event"
		if defined $args{-oldpage} or defined $args{-oldtab};

	if( $args{-eventtype} eq "pagechange" ) {
		# ok for pagechange we get -newpage and -eventtyp
		$last_event_args{-oldpage}=$last_event_args{-newpage};
	}
	if( $args{-eventtype} eq "tabchange" ) {
		$last_event_args{-oldtab}=$last_event_args{-curtab};
	}

	foreach ( keys %args ) {
		$last_event_args{$_}=$args{$_};
	}

	my($str)="";
	foreach ( sort keys %last_event_args ) {
		$str .= $_."=>".$last_event_args{$_}."\n\t\t" unless $_ eq "-eventtype";
	}
	$str =~s/\n\t\t$//;

	$package->{-debugmsg}->( "[plgmgr.pm] send_event( -eventtype => $args{-eventtype} )\n\t\t$str\n" )
		if defined $package->{-debugmsg};

	foreach ( keys %package_imports ) {
		$package_imports{ $_ }->event_handler( %last_event_args );
	};

	$package->{-debugmsg}->( "[plgmgr.pm] completed send_event()\n" )
		if defined $package->{-debugmsg};
	#print "DBG: STATUSMSG=",$package->{-statusmsg},"\n";
	#cluck("HERE IS MY CONFESSION");
}

sub menuclick {
	my($package,$plugin,$txt) = @_;
	$package->{-debugmsg}->(  "[plgmgr.pm] menuclick($plugin,$txt)\n" );
	$package->send_event( -eventtype=>'menu',-menuitem=>$txt );
	$package->{-debugmsg}->(  "[plgmgr.pm] menuclick() completed\n" );
}

sub bitmapclick {
	my($package,$plugin,$name) = @_;
	$package->{-debugmsg}->(  "[plgmgr.pm] bitmapclick($name)\n" );
	$package->send_event( -eventtype=>'imgbutton',-menuitem=>$name );
	$package->{-debugmsg}->(  "[plgmgr.pm] bitmapclick($name) completed\n" );
	#$package_imports{ $plugin }->bitmapclick( $name );
	#$package_imports{ $plugin }->send_event( -eventtype=>'imgbutton',-menuitem=>$name );
	#$package_imports{ $plugin }->menuclick( $uframe, $txt );
	#$package->{-statusmsg}->(  "[plgmgr.pm] menuclick() completed\n" );
}

sub initialize {
	my($self, %args)=@_;

	$args{-debugmsg}->(  "[plgmgr.pm] initialize($args{-pluginmanager})\n" );
	foreach ( keys %args ) {
		$$self{$_} = $args{$_};
	}

	foreach ( keys %package_imports ) {
		$package_imports{ $_ }->initialize( %args );
	};

	foreach ( keys %package_imports ) {
		$package_imports{ $_ }->init( %args );
	};
	$args{-debugmsg}->(  "[plgmgr.pm] initialize() completed\n" );

	$IGNORE_EVENTS=0;
	$args{-debugmsg}->( "[plgmgr.pm] Plugin Manager Initialize() done\n");
}

sub getExplorerButtons {
	my($package)=shift;
	my(%items);
	foreach ( keys %package_imports ) {
		my %x = $package_imports{ $_ }->getExplorerButtons();
		foreach( keys %x ) {
			$items{$_} = $x{$_};
		}
	}
	return %items;
}

sub getPluginPanes {
	my($package)=shift;
	my(@rc);
	foreach my $pkg ( keys %package_imports ) {
		push @rc,$package_imports{ $pkg }->getPluginPanes(@_);
	}
	return \@rc;
}

#sub config_screen {
#	my($package,$frame,$server,$stype)=@_;
#	my( %rc );
#   	foreach my $pkg ( keys %package_imports ) {
#		my(%d)=$package_imports{ $pkg }->config_screen($frame,$server,$stype);
#		foreach (keys %d ) { $rc{$_} = $d{$_}; }
#	}
#	return %rc;
#}

sub getMenuItems {
	my($package)=shift;
	my( %Items );
   	foreach my $pkg ( keys %package_imports ) {
		my(@tmp)=$package_imports{ $pkg }->getMenuItems();
		#$package->{-statusmsg}->(  "-- Menu Items For Pkg $pkg = ".join(" ",@tmp) );
		foreach(@tmp) {
			s/^\///;
			$Items{$_}    = $pkg;
		}
	}
	#$package->{-statusmsg}->(  "-- Done Menu Items " );
	return %Items;
}

sub getTreeColorFunc {
	my($package,$curTreeType)=@_;
	my(@rc);
  	foreach my $pkg ( keys %package_imports ) {
		my($cfuncref) = $package_imports{ $pkg }->getTreeColorFunc($curTreeType);
		push @rc, $cfuncref if defined $cfuncref;
	}
	return @rc;
}

# Get Data For The Tree
# 		You Pass in a Tree Type
# 		returns pointer to array of text, and ptr to arrayref of
# 	right click items
sub getTreeItems {
	my($package,$curTreeType)=@_;
	die "Must pass package to getTreeItems in Pluginmanager\n" unless defined $package;
	$package->{-statusmsg}->(  "[plgmgr.pm]   getTreeItems(curTreeType=$curTreeType)\n" );

	my(@treeitems,@treertclk);

	# if its the first time through save the context argument
	$last_event_args{-context}=$curTreeType unless defined $last_event_args{-context};

	# Ok create item /
	push @treeitems,"/";
	push @treertclk, \@root_rtclick_items;

  	#my(%plugin_by_treeitem_rank);

  	# Get Unique list of treeitems
  	my(%found);
  	foreach my $pkg ( keys %package_imports ) {
		$package->{-debugmsg}->(  "[plgmgr.pm]   -- ".$pkg."->getTreeItems() (type=$curTreeType)\n" );
		my($treeref) = $package_imports{ $pkg }->getTreeItems($curTreeType);
		next unless defined $treeref;
		foreach ( @$treeref ) {
			next if $found{$_};
			$found{$_}=1;
			#$plugin_by_treeitem{$_}=$pkg;
			push @treeitems,$_;
			my @x;
			push @treertclk,\@x;
		}
	}
	$package->{-debugmsg}->(  "[plgmgr.pm]   importing mouse right clicks\n" );

	# Get The Right Clicks For Tree Items
  	foreach my $pkg ( keys %package_imports ) {
		$package->{-debugmsg}->(  "[plgmgr.pm]   -- ".$pkg."->getTreeRtclk() (type=$curTreeType)\n" );
		my($rtclk)=$package_imports{ $pkg }->getTreeRtclk($curTreeType,\@treeitems);
		# foreach (@$rtclk ) { print "Fetched $_\n"; }
		next unless $rtclk;

		my($cnt)=0;
		foreach my $treeitm ( @treeitems ) {
			#print "TREE=$treeitm\n";
			my($new_aryptr)=$$rtclk[$cnt];
			my($real_aryptr)= $treertclk[$cnt];
			$cnt++;
			next unless defined $new_aryptr;

			foreach (@$new_aryptr) {
				#print " -- Adding \"$_\" which comes from $pkg\n";
				my($svitm)=$_;
				push @$real_aryptr, $_;
				# find rank of item
				#my($rnk)=$ranks{lc($_)};
				#if( ! $rnk ) {
				#	s/\[//;s/\]//;
				#	my(@d)=split(/\s+/,lc($_));
				#	$rnk=$ranks{$d[0]} || $ranks{$d[1]}  || $ranks{$d[2]} ||
				#		$ranks{$d[0]." ".$d[1]} || $ranks{$d[1]." ".$d[2]};

				#}
				#main::_statusmsg(  "WARNING: NO RANK DEFINED FOR $_\n" ) unless defined $rnk;
				#next unless defined $rnk;

				# pick the lowest rank...
				#if( ! defined $plugin_by_treeitem{$treeitm}
				#or $plugin_by_treeitem_rank{$treeitm} > $rnk ) {
					#$plugin_by_treeitem{$treeitm}=$pkg;
					#$plugin_by_treeitem_rank{$treeitm}=$rnk;
					#$browse_clktxt_by_treeitem{$treeitm}=$svitm;
				#}
				#$plugin_by_item_rtclick{$treeitm.":".$_}=$pkg;
			}
			#$package->{-debugmsg}->("browse($treeitm) will result in $treeitm $plugin_by_treeitem{$treeitm} -> rank $browse_clktxt_by_treeitem{$treeitm}\n" );
		}
	}


	$package->{-debugmsg}->(  "[plgmgr.pm]   -- Completed Fetch of Tree Data\n");

	return \@treeitems,\@treertclk;
}

sub browse {
    	my($package,$entryPath,$tab)= @_;
    	$tab=lc($tab);

 	# OK HANDLE DUPLICATE BROWSE EVENTS BY IGNORING THEM
    	if( 	$last_event_args{-eventtype} 	eq 'treebrowse' and
    		$last_event_args{-entrypath} 	eq $entryPath 	and
    		$last_event_args{-curtab} 	eq $tab ) {

    		$package->{-statusmsg}->( "[plgmgr.pm] ignoring duplicate click\n" )
			if defined $package->{-statusmsg};
		return;
	}
	$package->send_event( -eventtype=>'treebrowse',-entrypath=>$entryPath, -curtab=>$tab );
 	$package->{-debugmsg}->(  "[plgmgr.pm]   -- treebrowse completed\n");
}

sub rtclick {
    	my($package,$entryPath,$clktext)= @_;
	$package->{-debugmsg}->(  "[plgmgr.pm]   rtclick started\n" );
    	if( $entryPath eq "/" ) {
    		if( $clktext eq "Welcome" ) {
    			$package->ignore_events(1);
    			$main::notebook_f->raise("Welcome");
    			$package->ignore_events(0);
    			return;
    		} elsif( $clktext eq "Logout" ) {
    			print "Goodbye\n";
    			exit(0);
    		}
	}

    	$package->send_event(-eventtype=>"treertclick",-entrypath=>$entryPath, -clicktext=>$clktext);
    	$package->{-debugmsg}->(  "[plgmgr.pm]   rtclick completed\n" );
}

sub not_installed_plugins {
	my(@plugin);
	foreach ( keys %xml_by_plugin ) {
		push @plugin,$_ unless defined $package_imports{$_};
	}
	return @plugin;
}

# If called on an individual plugin, returns hash of k/v pairs.  If no plugin, returns array of hashes
sub plugin_data {
	my($self,$pluginname)=@_;

	# show generic plugin data
	if( ! defined $pluginname ) {
		my(%dat);
		$self->{-debugmsg}->( "[plgmgr.pm] plugin_data(all plugins)" );
		foreach my $pkg ( keys %package_imports ) {
			my(%pd)	= $self->plugin_data($pkg);
			$pd{state}="Installed";
			$dat{$pkg} = \%pd;

			#$package_imports{ $_ }->run( $uframe, $txt );
			#$my($plugin) = $package_imports{$pkg};
			#$print "Plugin is $plugin \n";
			# foreach (keys %$plugin) { print "key = $_ val=$$plugin{$_} \n"; }
			#$print "VERSION $pkg=",$pkg->VERSION," \n";
		}

		foreach my $pkg ( $self->not_installed_plugins() ) {
			my(%pd)	= $self->plugin_data($pkg);
			$pd{state}="Not Installed";
			$dat{$pkg} = \%pd;
		}
		return %dat;
	}
	$self->{-debugmsg}->( "[plgmgr.pm] plugin_data(pluginname=$pluginname)" );

	my(%dat);
	if( defined $xml_by_plugin{$pluginname} ) {
		my(%tmprep) = %{$xml_by_plugin{$pluginname}};
		foreach ( keys %tmprep ) { $dat{$_} = $tmprep{$_}; }
	}

	if( -r $package_filename{$pluginname} ) {
		my(@info)      = stat $package_filename{$pluginname};
		$dat{"file_name"} = $package_filename{$pluginname};
		$dat{"file_date"} = scalar( localtime($info[7]));
		$dat{"file_size"} = $info[7];
	} else {
		$dat{"file_name"} = "N.A.";
		$dat{"file_date"} = "N.A.";
		$dat{"file_size"} = "N.A.";
	}
	return %dat;
}

sub getBitmaps
{
   	my($package)=shift;
   	my(@items);
   	foreach ( keys %package_imports ) {
   		push @items,  $package_imports{ $_ }->getBitmaps();
	}
	return @items;
}

1;

__END__

=head1 NAME

PluginManager.pm - plugin manager

=head2 USAGE

PluginManager manages plugins.  It is the interface between the GEM and the plugins.


=head2 FUNCTIONS

new( %args )

  reads all plugins from plugins subdirectory and import()'s them and then runs
  new() on each.  Also reads .xml files in this directory and saves data.

plugin_names() - returns names of the plugins loaded

menuclick($pluginname,$frame,$txt)
	runs plugin->run($frame,$txt) where frame is the appropriate
		frame to run in and $txt is the text of the menuitem

initialize(%args)
	foreach (plugin) { run plugin->initialize(%args) }

GetExplorerButtons()
	foreach (plugin) { push @x,plugin->GetExplorerButtons() };
	return @x;

GetMenuItems(%Items)
	foreach (plugin) { @x=plugin->GetMenuItems(%args);
		foreach (@x) { $Items{$_} = $plugin_name
	}

getExplorerBaseItems()
	foreach (plugin) { push @x,plugin->GetExplorerButtons() };
	return @x;

getTreeItems($tree)
	push @treeitems,"/"; push @treertclk,\@dummy;
	foreach (plugin) {
		($treeref,$treertclref)=plugin->getTreeItems(%args);
		push @treeitems,@$treeref; push @treertclk,@$treertclkref;
	}
	return \@treeitems,\@treertclk

browse($frame,$entrypath)
	get plugin from saved values for $entrypath
	$plugin->browse(frame,$entrypath)
plugin_data($pluginname)
	return	$VERSION, $shortdescription, $longdescription from plugin
		plus { $xml_by_plugin{$pluginname} }
		plus	filename,filedate,filesize
	in a hash

get_bitmaps()
	return( \@ids, \%bitmaps, \%bitmaptext, \%baloontext)

=cut
