# copyright (c) 2005 by SQL Technologies

# THIS IS A FAIRLY GENERIC PLUGIN AND CAN BE USED FOR EXAMPLE PURPOSES
# IT IS OVER - COMMENTED IN AN ATTEMPT TO FULFIL THAT ROLE

# Standard Top Of File
package eventviewer;
@ISA = qw(Plugin);

use strict;
use MlpAlarm;
use Tk;
use Tk::ErrorDialog;
use Do_Time;
use Tk::Table;
use Tk::After;
use Tk::JComboBox;
use CommonFunc;
use Carp;

use vars qw($VERSION);
$VERSION = ".01";

my($current_main_frame);		# the frame you are working in
my($last_event_ptr);			# perhaps you care what the last event was (probably not)
my($currentRefRate)=120;		# for freezing and repainting
my($is_frozen)=0;
my($freeze_btn);
my($afterid);
# Done - Standard Top Of File

# Standard Variables - Customize These
my(@hb_values)=('Blackout3','Blackout6','Blackout24','Blackout72','IgnoreSvr',
	'MsgOk3hr','MsgOk','MsgOk3dy','MsgDelete','NotProd','DeleteAll','DeleteMon');

my(%hb_operations)=(
	'Blackout3'	=>"3 Hr Srvr Blackout",
	'Blackout6'	=>"6 Hr Srvr Blackout",
	'Blackout24'	=>"24Hr Srvr Blackout",
	'Blackout72'	=>"72Hr Srvr Blackout",
	'IgnoreSvr'	=>"Ignore Srvr",
	'MsgOk3hr'	=>"3 Hour Msg Ok",
	'MsgOk'		=>"1 Day Msg Ok",
	'MsgOk3dy'		=>"3 Day Msg Ok",
	'NotProd'	=>"Srvr Not Production",
	'MsgDelete'	=>"Delete Message",
	'DeleteAll'	=>"Del All Srvr Msgs",
	'DeleteMon'	=>"Del All Monitor Msgs");

my($modulename)="eventviewer.pm";	# Self Evident - The File Name
my(@menuitems) = ( );			# Any Menu items - use / as heirarchy delimeter
my($page_type)="Heartbeats";		# The Default "Page" label... the module will need to alter this if it has
					# multiple pages it can display.  This module uses Heartbeats as the default
					# for viewing alarms, and uses one page per option if you click on the Alarm Setup
my($notebook_tab_label)="eventviewer";	# This is the tab name in the event viewer
my(%explorer_labels)= (			# explorer buttons for the lower left hand corner.  The Key = label text,
					# value = the internal tag the event will have
	"Alarm Filters"	=>"Options",
	"Alarm Setup"	=>"Setup"
);
my($internal_debug)=1;			# set this if you want to see print messages just for this module
					# this is useful for development
# Done - Standard Variables

#
# Non Standard Variable Definitions - Your code will use these
#
my($font_normal, $font_buttons, $font_menuitems, $font_optionmenu) = ("small","small","small","tiny");
my(@groups, @severities, @programs, @systems);		# set these in init
my(@timevalues)=("2 Hours","4 Hours","8 Hours","Since 4PM","1 Day","3 Days", "5 Days");
my(@screens)=(
	"Events",
	"Heartbeats",
	"Agents",
	"Batch Jobs"
);
my(%is_page_a_data_screen);	# a hash with value 1 for each of @screens
my($textObject);  	# the text object (main part of the screen) for the display
my(%admin_screens)=(
		"/Define Production Servers"		=> "Production",
		"/Define Servers on Ignore List"	=> "Production",
		"/Blackout Servers"			=> "Production",
		"/Ignore Server & Program" 		=> "Ignore List",
		"/Rebuild Server Groups"		=> "Rebuild Containers",
		"/Show Systems by Groups"		=> "Containers",
		"/Show System to Group Mapping"		=> "System->Container",
		"/Show Program to Group Mapping"	=> "Program->Container",
		"/Delete Data by Monitor"		=> "Delete By Monitor",
		"/Delete Data by Monitor & Type"	=> "Delete By Monitor_Type",
		"/Delete Data by Monitor, System, & Type"	=> "Delete By Row",
		"/Report Setup"				=> "Report Setup",
		"/View Alarm History"			=> "Alarm History",
		"/Setup Alarm Routing"			=> "Alarm Routing",
		"/Define Operators"			=> "Operator",
		"/Define Operator Schedules"		=> "Operator Schedule",
		"/Filter Repeated Messages"		=> "Filter Repeated Messages"
		);


my(%severities) = (
	"EMERGENCY" 	=> 6,
	"CRITICAL" 		=> 5,
	"ALERT" 			=> 4,
	"ERROR" 			=> 3,
	"WARNING" 		=> 2,
	"INFORMATION" 	=> 1,
	"OK" 				=> 1,
	"RUNNING" 		=> 2,
	"NOT_RUN" 		=> 3,
	"COMPLETED" 	=> 1,
	"STARTED" 		=> 2,
	"DEBUG" 			=> 0,
	"(OK)-EMERGENCY" 	=> 1,
	"(OK)-CRITICAL" 		=> 1,
	"(OK)-ALERT" 			=> 1,
	"(OK)-ERROR" 			=> 1,
	"(OK)-WARNING" 		=> 1,
	"(OK)-INFORMATION" 	=> 1,
	"(OK)-OK" 				=> 1,
	"(OK)-RUNNING" 		=> 1,
	"(OK)-NOT_RUN" 		=> 1,
	"(OK)-COMPLETED" 		=> 1,
	"(OK)-STARTED" 		=> 1,
	"(OK)-DEBUG" 			=> 1,
);

# The controls for the display
my(	$show_production_only,
	$show_ok_rows,
	$current_severity,
	$current_time,
	$current_group,
	$current_program,
	$current_system,
) = ( 'No', "Ignore","Errors" , "4 Hours", "All", undef, undef );
my(@hide_on_demand_items); # items that might go invisible
my(%add_row_variables);	# these are the variables for add row / special

# Done - Non Standard Variable Definitions - Your code will use these

sub paint_hide_on_demand {
	my(%floatspots)=(  -column=>3, -row=>0 );
	$hide_on_demand_items[5]->configure(-width=>20);

	$hide_on_demand_items[0]->grid(-column=>1, -row=>0, -padx=>4, -sticky=>'ew');
	$hide_on_demand_items[1]->grid(-column=>2, -row=>0, -padx=>4, -sticky=>'ew');
	if( $current_group eq "All") {
		$hide_on_demand_items[2]->gridForget();
	} else {
		$hide_on_demand_items[2]->grid( %floatspots, -padx=>4, -sticky=>'ew');
		$floatspots{-column}=3;$floatspots{-row}=1;
	}
	$hide_on_demand_items[3]->grid(-column=>1, -row=>1, -padx=>4, -sticky=>'ew');
	$hide_on_demand_items[4]->grid(-column=>2, -row=>1, -padx=>4, -sticky=>'ew');
	$hide_on_demand_items[6]->grid(-column=>0, -row=>1, -padx=>4, -sticky=>'ew');

	if( defined $current_program ) {
		$hide_on_demand_items[7]->grid(%floatspots, -padx=>4, -sticky=>'ew');
		if( $floatspots{-row} == 1 ) {
			$floatspots{-column}=4;$floatspots{-row}=0;
		} else {
			$floatspots{-column}=3;$floatspots{-row}=1;
		}
	} else {
		$hide_on_demand_items[7]->gridForget;
	}

	if( defined $current_system ) {
		$hide_on_demand_items[8]->grid( %floatspots, -padx=>4, -sticky=>'ew' );
	} else {
		$hide_on_demand_items[8]->gridForget;
	}

	$freeze_btn->pack( -side=>'left', -padx=>3, -pady=>8 );
}

sub forget_hide_on_demand {
	$hide_on_demand_items[5]->configure(-width=>40);
	$hide_on_demand_items[0]->gridForget();
	$hide_on_demand_items[1]->gridForget();
	$hide_on_demand_items[2]->gridForget();
	$hide_on_demand_items[3]->gridForget();
	$hide_on_demand_items[4]->gridForget();
	$hide_on_demand_items[6]->gridForget();
	$hide_on_demand_items[7]->gridForget();
	$hide_on_demand_items[8]->gridForget();
	$freeze_btn->packForget();
}

# What do you wish to do when you click on a menu item
sub do_menu_item
{
	my($package)=shift;
	my(%args)=@_;

	$package->statusmsg( "[$modulename] do_menu_item($args{-menuitem})\n" );
	$page_type="Heartbeats" unless $is_page_a_data_screen{$page_type};
	if(      $args{-menuitem} =~ /^Filter\/Show Production\/Show Production Servers Only/ ) {
				$show_production_only='Yes';
	} elsif( $args{-menuitem} =~ /^Filter\/Show Production\/Show All Servers/ ) {
				$show_production_only='No';
	} elsif(      $args{-menuitem} =~ /^Filter\/Reviewed-Ok\/Show Rows/ ) {
				$show_ok_rows='Shown';
	} elsif( $args{-menuitem} =~ /^Filter\/Reviewed-Ok\/Ignore Rows/ ) {
				$show_ok_rows='Ignore';
	} elsif( $args{-menuitem} =~ /^Time\/(.+)/ ) {
				$current_time = $1;
	} elsif( $args{-menuitem} =~ /^Screen\/(.+)/ ) {
				$page_type = $1;
	} elsif( $args{-menuitem} =~ /^Severity\/(.+)/ ) {
				$current_severity = $1;
				#$current_severity =~ s/ and g
	} elsif( $args{-menuitem} =~ /^Filter\/Clear (.+)/ ) {
		if( $args{-menuitem} =~ "Group Filter" ) {
			$current_group = "All";
		} elsif( $args{-menuitem} =~ "Programs Filter" ) {
			$current_program = undef;
		} elsif( $args{-menuitem} =~ "Systems Filter" ) {
			$current_system = undef;
		} else { die "OOPS"; }
	} elsif( $args{-menuitem} =~ /^Filter\/Group\/(.+)/ ) {
				$current_group = $1;
	} elsif( $args{-menuitem} =~ /^Filter\/Programs\/(.+)/ ) {
				$current_program = $1;
	} elsif( $args{-menuitem} =~ /^Filter\/Systems\/(.+)/ ) {
				$current_system = $1;
	} elsif( $args{-menuitem} =~ /^Time/ ) {
		$current_time = $1;
	} elsif( $args{-menuitem} =~ /^Configuration\/(.+)/ ) {
		$page_type=$1;
	}

	if( $is_page_a_data_screen{$page_type} ) {
		paint_hide_on_demand();
	} else {
		forget_hide_on_demand();
	}
	$package->redraw();
}

sub getMenuItems {
	# for this plugin... lets add some of the data we collected in init();

	push @menuitems,"Filter/Show Production/Show Production Servers Only";
	push @menuitems,"Filter/Show Production/Show All Servers";
	push @menuitems,"Filter/Reviewed-Ok/Show Rows";
	push @menuitems,"Filter/Reviewed-Ok/Ignore Rows";

	foreach ( @timevalues) { push @menuitems,"Time/".$_; 	}
	foreach ( @screens ) {	 push @menuitems, "Screen/".$_; }
	foreach (  @severities ) { push @menuitems,"Severity/".$_;	}

	push @menuitems, "Filter/Clear Group Filter";
	foreach (  @groups) {  push @menuitems,"Filter/Group/$_" unless $_ eq "All" or $_ =~ /\[ All\s/; }

	push @menuitems, "Filter/Clear Programs Filter";
	foreach (  @programs) {	push @menuitems,"Filter/Programs/$_" 	unless $_ eq "All" or $_ =~ /\[ All\s/; }

	push @menuitems, "Filter/Clear Systems Filter";
	foreach (  @systems) {	push @menuitems,"Filter/Systems/$_" 	unless $_ eq "All" or $_ =~ /\[ All\s/; }

	foreach( sort keys %admin_screens) { push @menuitems,"Configuration".$_; }
	return @menuitems;
}

sub getTreeItems {
	my($package,$curTreeType)=@_;
	my(@treeitems_to_return);
	return \@treeitems_to_return;

#	$package->statusmsg( "[$modulename] getTreeItems($curTreeType)\n" );
#
#	# $curTreeType are the values in %explorer_labels by the way
#	if( $curTreeType eq "Options" or $curTreeType eq "Unknown") {
#		if( $show_production_only eq "Yes" ) {
#			push @treeitems_to_return,"/Show Only Production: on";
#		} else {
#			push @treeitems_to_return,"/Show Only Production: off";
#		}
#
#		if( $show_ok_rows eq "Shown" ) {
#			push @treeitems_to_return,"/Show Reviewed-Ok Rows: on";
#		} else {
#			push @treeitems_to_return,"/Show Reviewed-Ok Rows: off";
#		}
#
#		foreach ( @timevalues) {
#			if( $_ eq $current_time ) {
#				push @treeitems_to_return,"/Time: ".$_." : on";
#			} else {
#				push @treeitems_to_return,"/Time: ".$_." : off";
#			}
#		}
#
#		foreach ( @screens ) {
#			if( $_ eq $page_type ) {
#				push @treeitems_to_return,"/Screen: ".$_." : on";
#			} else {
#				push @treeitems_to_return,"/Screen: ".$_." : off";
#			}
#		}
#
#		foreach ( @severities ) {
#			if( $_ eq $current_severity ) {
#				push @treeitems_to_return,"/Level: ".$_." : on";
#			} else {
#				push @treeitems_to_return,"/Level: ".$_." : off";
#			}
#		}
#
#		my($base)="/Group:";
#		#push @treeitems_to_return,$base;
#		foreach (@groups) {
#			if( defined $current_group and $current_group eq $_ ) {
#				push @treeitems_to_return,"$base $_ : on";
#			} else {
#				push @treeitems_to_return,"$base $_ : off";
#			}
#		}
#
#
#	} elsif( $curTreeType eq "Setup" ) {
#		return \@admin_screens;
#	}
#	return \@treeitems_to_return;
}

#
# DONE WHAT YOU MIGHT WANT TO CHANGE
#
# OK PAINT THE SCREEN
sub get_tree_colors {
	my($path)=@_;
	return 2 if $path=~/ on$/;
	return 0;
}

sub getTreeColorFunc {
	return \&get_tree_colors;
}

sub event_handler
{
	my($package)=shift;
	my(%args)=@_;
	$last_event_ptr=\%args;

	# freeze if you are tabbing out of the page
	$package->toggle_freeze() if $args{-oldtab} eq $notebook_tab_label and $is_frozen==0;

	if( $args{-eventtype} eq "menu" ) {
		#
		# we do the following block for compatibility - remember that other plugins have menuitems
		#
		my($found)="FALSE";
		foreach ( @menuitems ) { $found="TRUE" if $args{-menuitem} eq $_; }
		return unless $found eq "TRUE";

		$package->ignore_events(1);
		if( $args{-curtab} ne $notebook_tab_label ) {
        		$package->statusmsg( "[$modulename] event_handler() raising tab $notebook_tab_label\n" );
			$main::notebook_f->raise(ucfirst($notebook_tab_label));
		}
		$package->do_menu_item( %args );
		$package->ignore_events(undef);
		$package->refresh_tree();
	} elsif( $args{-eventtype} eq "tabchange" ) {
#
#		return if $args{-curtab} ne $notebook_tab_label;
#		$package->statusmsg( "[$modulename] event_handler() called\n" );
#		$package->statusmsg(  "[$modulename] monitor tab has been raised\n" );
#
#		my($ok);
#		foreach( main::get_server_types() ) { next if $args{-context} ne $_; $ok=1; last; }
#		if( ! $ok ) {
#			$package->refresh_tree('sqlsvr');
#			$args{-context}='sqlsvr';	# the refresh_tree sets global variable but we are not
#							# triggering additional event so we do this.
#		}
#
#		my($dummy,$typ,$server,$db)=split(/\//,$args{-entrypath});
#		$currentServer=$server;
#		$currentType=$args{-context};
#		$package->redraw();
#
	} elsif( $args{-eventtype} eq "tab_pagechange" ) {
	} elsif( $args{-eventtype} eq "treebrowse" ) {
#		# an item was selected
#		return if $args{-curtab} ne $notebook_tab_label;
#		$package->debugmsg( "[$modulename] event_handler() called\n" );
#		if( $args{-context} eq "Setup" ) {
#			my($dummy);
#			($dummy,$page_type)=split(/\//,$args{-entrypath});
#			$package->redraw();
#		} else {
#			$page_type="Heartbeats" unless $is_page_a_data_screen{$page_type};
#			# set the option as appropriate
#			if( $args{-entrypath} =~ /^\/Show Only Production: off/ ) {
#				$show_production_only="Yes";
#			} elsif( $args{-entrypath} =~ /^\/Show Only Production: on/ ) {
#				$show_production_only="No";
#			} elsif( $args{-entrypath} =~ /^\/Show Reviewed-Ok Rows: off/ ) {
#				$show_ok_rows="Shown";
#			} elsif( $args{-entrypath} =~ /^\/Ignore Reviewed-Ok Rows: on/ ) {
#				$show_ok_rows="Ignore";
#			} elsif( $args{-entrypath} =~ /^\/Time: (.+) : \w+$/ ) {
#				$current_time = $1;
#			} elsif( $args{-entrypath} =~ /^\/Screen: (.+) : \w+$/ ) {
#				$page_type = $1;
#			} elsif( $args{-entrypath} =~ /^\/Level: (.+) : \w+$/ ) {
#				$current_severity = $1;
#			} elsif( $args{-entrypath} =~ /^\/Group: (.+) : o\w+$/ ) {
#				my($x)=$1;
#				if( $x eq "All" ) {
#					$current_group = "All";
#				} else {
#					$current_group = $x;
#				}
#			} elsif( $args{-entrypath} =~ /^\/Programs\/(.+) : o\w+$/ ) {
#				my($x)=$1;
#				if( $x eq "All" or $x eq "[ All Programs ]" ) {
#					$current_program = undef;
#				} else {
#					$current_program = $x;
#				}
#			} elsif( $args{-entrypath} =~ /^\/Systems\/(.+) : o\w+$/ ) {
#				my($x)=$1;
#				if( $x eq "All" or $x eq "[ All Systems ]" ) {
#					$current_system = undef;
#				} else {
#					$current_system = $x;
#				}
#			} else {
#				die "CANT PARSE $args{-entrypath}\n";
#			}
#			$package->redraw();
#			$package->refresh_tree();
#			#main::colorize_tree_generic( \&get_tree_colors );
#		}

	} elsif( $args{-eventtype} eq "treertclick" ) {
#
#		return if $args{-curtab} ne $notebook_tab_label;
#		$package->statusmsg( "[$modulename] event_handler() called\n" );
#		$package->statusmsg( "[$modulename] event_handler() called\n" );
#		my($dummy,$typ,$server,$db)=split(/\//,$args{-entrypath});
#		$currentServer=$server;
#		$currentType=$args{-context};
#		$package->redraw();
#
#		#my($entrypath, $clktext)=($args{-entrypath},$args{-clicktext});
#		#$package->statusmsg(  "[$modulename] rtclick(Path=$entrypath Txt=$clktext)\n" );
#		#$package->browse($entrypath);
#
	} elsif( $args{-eventtype} eq "expbutton" ) {
		if( $args{-context} eq "Setup" ) {
			$page_type="Setup" if $is_page_a_data_screen{$page_type};

			$package->messageBox( -title => 'Data', -message => "Stopping Automatic Refreshes", -type => "OK" );
			$package->toggle_freeze() unless $is_frozen;
			forget_hide_on_demand();
		} else {
			$page_type="Heartbeats" unless $is_page_a_data_screen{$page_type};
			paint_hide_on_demand();
		}
	} elsif( $args{-eventtype} eq "imgbutton" ) {
	}
}

#
# THE FOLLOWING FUNCTIONS SHOULD PROBABLY NOT NEED TO BE REFRESHED
#
sub getExplorerButtons {
	my($package)=shift;
	return( %explorer_labels );
}

sub getPluginPanes { return $notebook_tab_label; }

sub getTreeRtclk {
	my($package,$curTreeType,$treeitmptr)=@_;
	return( undef ) unless $curTreeType eq "sybase" or $curTreeType eq "sqlsvr";
	my(@rightclicks);
	foreach my $itm ( @$treeitmptr ) {
		my($dummy,$typ,$server,$db)=split(/\//,$itm);
		if( defined $server ) {
			my( @db_rclk )= ("Monitor $server");
			push @rightclicks, \@db_rclk;
		} else {
			push @rightclicks, undef;
		}
	}
	return \@rightclicks;
}

sub toggle_freeze {
	my($package)=@_;
	if( $is_frozen ) {
		$is_frozen=0;
		$freeze_btn->configure(-text=>'Freeze  ') if defined $freeze_btn;
		$package->statusmsg( "[$modulename] un freezing monitor\n" );
		$package->redraw();

	}else {
		$is_frozen=1;
		$freeze_btn->configure(-text=>'UnFreeze') if defined $freeze_btn;
		$package->statusmsg( "[$modulename] freezing monitor\n" );

		$afterid->cancel if defined $afterid;
		$afterid=undef;
	}
}

sub init {
	my($package)=@_;
	$package->statusmsg( "[$modulename] init($page_type)\n" );
	$afterid->cancel if defined $afterid;
	$afterid=undef;

	$internal_debug=1 if defined $$package{-developer_mode};
	#foreach (keys %$package) { print "KEY=$_ Val = $$package{$_} \n"; }

	$current_main_frame = $package->clear_tab($notebook_tab_label);

	@groups	=MlpGetContainer( -type=>'c');

	@severities	=MlpGetSeverity();
	@programs	=MlpGetProgram();
	unshift @programs,"[ All Programs ]";

	@systems = grep(!/^\s*$/,MlpGetContainer(-screen_name=>'All'));
	unshift @systems,"[ All Systems ]";

	foreach( @screens ) { $is_page_a_data_screen{$_} = 1; }

	# lets make our tab the top one
	if( defined $$package{-modulename} ) {
		$package->set_default_tab_to_raise($notebook_tab_label);
		$main::notebook_f->raise(ucfirst($notebook_tab_label));
	}

	my($butfrm)=$current_main_frame->Frame( -relief=>'groove' )->pack( -fill=>'x', -side=>'top');

	# make a label_frm which will be gridded
	my($label_frm)=$butfrm->Frame()->pack(-side=>'left',  		-padx=>3, -expand=>1, -fill=>'x');

	sub show_item {
		my($tfrm,$variableref, $label, $width, $bg)=@_;
		my($frm)=$tfrm->Frame();	#->grid(-column=>$col, -row=>$row, -padx=>4, -sticky=>'ew');

		$frm->Label(-textvariable=>$variableref, -width=>$width, -relief=>'sunken', -background=>$bg, -justify=>'left')->pack(-side=>'right');
		$frm->Label(-text=>$label, -justify=>'left')->pack(-side=>'right');
		return $frm;
	}

	my($frm)=$label_frm->Frame()->grid(-column=>0, -row=>0, -padx=>4, -sticky=>'ew');
	$hide_on_demand_items[5]=$frm->Label(-textvariable=>\$page_type, -width=>20, -relief=>'sunken',
		-background=>'beige', -justify=>'left')->pack(-side=>'right');
	$frm->Label(-text=>"Screen", -justify=>'left')->pack(-side=>'right');

	$hide_on_demand_items[0]=show_item($label_frm,\$current_severity,	"Sev"	, 	, 9,  "white");  #,  0, 1 ) ;
	$hide_on_demand_items[1]=show_item($label_frm,\$current_time,		"Time"		, 9,  "white" ); #, 0, 2 ) ;
	$hide_on_demand_items[2]=show_item($label_frm,\$current_group,		"Group"		, 18, "pink"); #, 1, 0 ) ;
	$hide_on_demand_items[3]=show_item($label_frm,\$show_production_only,	"Production"	, 4,  "white" ); #,  1, 1 ) ;
	$hide_on_demand_items[4]=show_item($label_frm,\$currentRefRate,		"Refresh"	, 3,  "white");  #,  1, 2 ) ;
	$hide_on_demand_items[6]=show_item($label_frm,\$show_ok_rows,		"Reviewed"	, 8,  "white");  #,  0, 3 ) ;
	$hide_on_demand_items[7]=show_item($label_frm,\$current_program,	"Program"	, 18, "pink"); #,  0, 4 ) ;
	$hide_on_demand_items[8]=show_item($label_frm,\$current_system,		"System"	, 18, "pink"); #,  1, 4 ) ;

	$freeze_btn = $package->make_button(-widget=>$butfrm,
    			-help=>'Freeze Auto Redraw',
    			-text=>"Freeze", -width=>9,
    			-command=> sub  { $package->toggle_freeze(); } )->pack( -side=>'left', -padx=>3, -pady=>8 );
	$package->make_button(-widget=>$butfrm,
    			-help=>'Run A Report',
    			-text=>"Run Report",
    			-command=> sub  { $package->redraw(); } )->pack( -side=>'left', -padx=>3, -pady=>8 );

#	$textObject = $current_main_frame->Scrolled("ROText",
#		-relief => "sunken", -wrap=>'none',
#                -bg=>'white',-font=>'code',
#                -scrollbars => "osoe")->pack(-side   => "top",
#                                                -anchor => 'w',
#                                                -expand => 1,
#                                                -fill   => 'both');
#
#        $textObject->tagConfigure("t_row_header",   -foreground=>'black', -relief=>'raised', -background=>'beige');
#        $textObject->tagConfigure("t_out_of_date",  -foreground=>'blue');
#        $textObject->tagConfigure("t_red",   -foreground=>'red');
#        $textObject->tagConfigure("t_grey", -foreground=>'grey');
#        $textObject->tagConfigure("t_orange", -foreground=>'orange');

	#main::colorize_tree_generic( \&get_tree_colors );

	if( $is_page_a_data_screen{$page_type} ) {
		paint_hide_on_demand();
	}

	$package->redraw();
}

my(@cell_array);	# an array of arrays
my(@cell_colors);	# ditto
my($maxcolnum)=0;
sub tableclear {
	my($package)=shift;
	@cell_array=();
	@cell_colors=();
	$package->statusmsg( "[$modulename] Clearing Table!\n" );
	my($DBGTM1)=time;
	$textObject->destroy() if defined $textObject and Tk::Exists($textObject);
	$textObject = $current_main_frame->Table(
					#-rows=>$#cell_array, -columns=>$maxcolnum,
					-scrollbars=>"osoe", -fixedrows=>1 );
        $package->statusmsg( "[$modulename] Table Cleared in ",time-$DBGTM1, " Seconds \n");

}
sub tableput {
	my($package,$row,$col,$itm, $color)=@_;

	if( ! defined $cell_array[$row] ) {
		my(@newrow, @newcolor);
		$cell_array[$row]=\@newrow;
		$cell_colors[$row]=\@newcolor;
	}
	$maxcolnum=$col if $maxcolnum<$col;
	if( defined $cell_array[$row]->[$col] ) {
		$cell_array[$row]->[$col]	.= "\n".$itm;
	} else {
		$cell_array[$row]->[$col]=$itm;
		$cell_colors[$row]->[$col]=$color;
	}
}

my(%colormap)=(
	t_row_header=>   'black beige',
	t_black=> 'black white',
        t_out_of_date=>  'blue white',
        t_red=>   'blue red',
        t_normal=>'black white',
        t_grey=>  'blue grey',
        t_orange=>'blue orange' );

sub tableshow {
	my($package)=@_;

	$textObject->configure( -rows=>$#cell_array, -columns=>($maxcolnum+1));
	$package->statusmsg( "[$modulename] Displaying Table With $#cell_array rows\n" );

 	#my($font)="tiny";
 	#$font="small" unless defined $is_page_a_data_screen{$page_type};
	my($rownum)=0;
	foreach my $row ( @cell_array ) {
		my($colnum)=0;
		foreach ( @$row ) {
			my($cmap)=$colormap{$cell_colors[$rownum]->[$colnum]};
			my($fg,$bg)=split(/\s/,$cmap);
			if( ref $_ ) {
				$textObject->put($rownum,$colnum++,$_);
			} else {
				$textObject->put($rownum,$colnum++,
					$textObject->Label(-text=>$_, -font=>$font_normal,
						-justify=>'left', -relief=>'sunken', -anchor=>'w',
						-background=>$bg, -foreground=>$fg));
			}
		}
		$rownum++;
	}

	$textObject->configure( -scrollbars=>'osoe');
	$textObject->pack(-side   => "top", -anchor => 'nw', -expand => 1, -fill   => 'both');
	$textObject->update();
}

sub process_button {
	my($package)=shift;
	my(%args)=@_;
	$package->statusmsg( "[$modulename] process_button( -buttonid=>$args{-buttonid},-rowid=>$args{-rowid})" );
	if( $args{-buttonid} == 1 ) {
		if( defined $internal_debug ) {
			$package->statusmsg( "[DEBUG] Updating A Row\n" );
			foreach ( keys %add_row_variables ) {
				$package->statusmsg( "[DEBUG] ... item key $_ val=$add_row_variables{$_}\n" );
			}
		}
		$package->statusmsg( "[DEBUG]",join("\n",MlpManageData( -debug=>$internal_debug, Admin=>get_report_from_page($page_type),
						-debug=>$internal_debug, Operation=>"add", %add_row_variables)),"\n" );
	} elsif( $args{-buttonid} == 2 ) {
		if( defined $internal_debug ) {
			$package->statusmsg( "[DEBUG] Adding New Item\n" );
			foreach ( keys %add_row_variables ) {
				$package->statusmsg( "[DEBUG] ... item key $_ val=$add_row_variables{$_}\n" );
			}
		}

		$package->statusmsg( "[DEBUG]",join("\n",MlpManageData( -debug=>$internal_debug, Admin=>get_report_from_page($page_type),
							-debug=>$internal_debug, Operation=>"add", %add_row_variables)),"\n" );
	} elsif( $args{-buttonid} == 3 ) {
		$package->statusmsg( "[DEBUG] Approving A Row\n" ) if defined $internal_debug;
		my($i)=0;
		my(%args_to_pass);
		foreach (@{$args{-data}}) {
			$package->statusmsg( "   DBG: ${$args{-hdr}}[$i]  ... ${$args{-data}}[$i] \n" )
				if defined $internal_debug;

			# woo hoo... i just wrote the following line of perl and it is completely indecipherable...
			# ive hit that point...
			$args_to_pass{${$args{-hdr}}[$i]} = ${$args{-data}}[$i];
			$i++;
		}
		$package->statusmsg( "[DEBUG]",join("\n",MlpManageData( -debug=>$internal_debug, OkId=>1, Operation=>"Ok", %args_to_pass)),"\n" );
#		my(%keys);
#		foreach (@{$args{-data}}) {
#			next unless /^sv_${ok_id}_/;
#			next if /^Last/;
#			my($k)=$_;
#			s/sv_${ok_id}_//;
#			# print "KEY=$k VAL=",param($k),br;
#			debugmsg( "KEY=$k converted to $_ VAL=".param($k) );
#			$keys{$_} = param($k);
#		}
#
#		# bug fix - wont pass as a parameter
#		$keys{Admin}=param("sv_admin");
#		# $admin=param("sv_admin");
#		# debugmsg( "ADMIN=$admin" );
#		$package->statusmsg( "[DEBUG]",join(br,MlpManageData( -debug=>$internal_debug, OkId=>$ok_id,
#					Operation=>"Ok",
#					# Admin=>param("sv_admin"),
#					%keys)),br;
	} elsif( $args{-buttonid} == 4 ) {
		$package->statusmsg( "[DEBUG] Deleting A Row\n\t".join(",",@{$args{-data}}),"\n" ) if defined $internal_debug;
		my($i)=0;
		my(%args_to_pass);
		foreach (@{$args{-data}}) {
			$args_to_pass{${$args{-hdr}}[$i]} = ${$args{-data}}[$i];
			$i++;
		}
		$package->statusmsg( "[DEBUG] ",join("\n",MlpManageData( -debug=>$internal_debug, Admin=>get_report_from_page($page_type),
						OkId=>1, -debug=>$internal_debug, Operation=>"del", %args_to_pass)),"\n" );
	} else {
		die "Program Error - Button Id Out Of Range\n";
	}
	$package->redraw();
}

sub get_report_from_page {
	my($reportname)=$page_type;
	if( defined $admin_screens{$reportname} ) {
		$reportname=$admin_screens{$reportname} if $admin_screens{$reportname} ne "";

	} elsif( defined $admin_screens{"/".$reportname} ) {
		$reportname=$admin_screens{"/".$reportname} if $admin_screens{"/".$reportname} ne "";


#	if( $reportname eq "Set Production/Ignore Servers" ){			$reportname="Production" ;
#	} elsif( $reportname eq "Ignore Server Program" ){			$reportname="Ignore List";
#	} elsif( $reportname eq "Rebuild Containers" ){
#	} elsif( $reportname eq "Show Systems by Groups" ){		$reportname="Containers";
#	} elsif( $reportname eq "Show System to Group Mapping" ){	$reportname="System->Container";
#	} elsif( $reportname eq "Show Program to Group Mapping" ){	$reportname="Program->Container";
#	} elsif( $reportname eq "Delete Data by Program" ){		$reportname="Delete By Monitor";
#	} elsif( $reportname eq "Delete Data by Program & Type" ){	$reportname="Delete By Monitor_Type";
#	} elsif( $reportname eq "Delete Data by Program, System, & Type" ){$reportname="Delete By Row";
#	} elsif( $reportname eq "Report Setup" ){			# ok
#	} elsif( $reportname eq "View Alarm History" ){			$reportname="Alarm History";
#	} elsif( $reportname eq "Setup Alarm Routing" ){		$reportname="Alarm Routing";
#	} elsif( $reportname eq "Operator List" ){			$reportname="Operator";
#	} elsif( $reportname eq "Operator Schedule" ){			#ok
#	} elsif( $reportname eq "Filter Repeated Messages" ){			#ok
	} elsif( $is_page_a_data_screen{$page_type} ) {
		return undef;
	} else {
		die "Un mapped screen type $reportname\n";
	}
	$reportname=~s.^\/..;
	return $reportname;
}

my($tmp_count)=0;
sub redraw {
	my( $package) = @_;

	$package->statusmsg( "[$modulename] ==================== redraw started ====================\n" );
	main::busy();	# set cursor to an hourglass
	$package->statusmsg( "[$modulename] Refreshing Screen $page_type\n" );

	#my($type,$entrypath)=$package->current_info();
	#my($type,$entrypath) = ($$last_event_ptr{-context},$$last_event_ptr{-entrypath});
	#my($entrypath) = $$last_event_ptr{-entrypath};
	#my($dummy,$typ,$server,$db)=split(/\//,$entrypath);

	$package->statusmsg( "[DEBUG] Canceling pending refresh $afterid\n" ) if defined $afterid  and defined $internal_debug;
	$afterid->cancel if defined $afterid;
	$afterid=undef;

	# CLEAR THE PAGE
	#return if $is_frozen;

	if( ${main::notebook_f}->raised() ne ucfirst($notebook_tab_label) ) {
		$package->statusmsg( "[$modulename] Refresh Stopped: Raised Tab has changed to ",${main::notebook_f}->raised(),"\n" );
		main::unbusy();	# set it to a normal cursor
		return;
	}

	# schedule it again
	if( $currentRefRate > 0 and  defined $is_page_a_data_screen{$page_type} ) {
		$currentRefRate=120 if $currentRefRate =~ /\D/;
		$afterid=$current_main_frame->after( $currentRefRate*1000,
			sub {
				$package->statusmsg( "[$modulename]  Refreshed Normal After Id = $afterid\n" );
				$package->redraw();
			}
		);
	}

	my(%sys_hyperlink);
	my(%prg_hyperlink);

	$package->tableclear();

	# PUT A HEADER UP

	my($admin,$report_type_code);
	if( defined $is_page_a_data_screen{$page_type} ) {
		$report_type_code = MlpGetReportCode(-screen_name=>$page_type);
	} else {
		$admin=1;
	}

	my($t);
	if($current_time =~ /Day/ ) {
			$t=$current_time;
			$t=~s/Days//;
			$t=~s/Day//;
			$t=~s/\s//g;
			$t*=24;
	} elsif($current_time =~ /Hour/ ) {
			$t=$current_time;
			$t=~s/Hours//;
			$t=~s/Hour//;
			$t=~s/\s//g;
	} elsif($current_time =~ /Since 4PM/ ) {
			$t=do_time(-fmt=>"hh");
			$t=substr($t,1,0) if substr($t,0,1) eq "0";
			$t+=8;
	} else {
		die "ERROR INVALID TIME FILTER ($current_time) - CAN NOT PARSE";
	}
	$package->debugmsg( "[$modulename] Time Filter is $t Hours");
	my($prodchkbox)=0;
	$prodchkbox=1 if $show_production_only eq "Yes";

	my($okchkbox)=1 if $show_ok_rows eq "Shown";

	my($rc);
	$package->statusmsg("[$modulename] Fetching Data For Page $page_type\n" );
	if( $page_type eq "Rebuild Containers" ){
		MlpManageData( -debug=>$internal_debug, Admin=>"Rebuild Containers");
		main::unbusy();	# set it to a normal cursor
		$package->messageBox(	-message => "Containers Rebuild" );
		return;
	}

	if( defined $admin) {
		my($reportname) = get_report_from_page($page_type);
		$package->statusmsg( "[$modulename] Running Admin Report\n" );
		$rc=MlpRunScreen( -screen_name=>$reportname, -html=>0, -debug=>$internal_debug );
		$package->messageBox(	-message => "Undefined Result Set: $!" ) unless defined $rc;
	} elsif( defined $current_system and $current_system ne "") {
		$package->statusmsg( "[$modulename]  Running system report\n" );
		$package->debugmsg( "[$modulename] Running Screen ($page_type) on System ($current_system)");
		$rc=MlpRunScreen( -screen_name=>$page_type,
				-system=>$current_system,,
				#-showok=>$okchkbox, -production=>$prodchkbox,
				-NL=>"\n",
				-debug=>$internal_debug,
				-html=>0,
				-maxtimehours=>$t,
				-severity=>$current_severity );
		$package->messageBox(	-message => "$!" ) unless defined $rc;
	} elsif( defined $current_program and $current_program ne "" ) {

		$package->statusmsg( "[$modulename] Running Normal Report\n" );
		$package->debugmsg( "[$modulename] Running Screen ($page_type) on Program ($current_program)");
		$rc=MlpRunScreen( -screen_name=>$page_type,
								-program=>$current_program,
								-NL=>"\n",
								-debug=>$internal_debug,
								-html=>0,
								-maxtimehours=>$t,
								-severity=>$current_severity,
								-production=>$prodchkbox,
								-showok=>$okchkbox );
		$package->messageBox(	-message => "$!" ) unless defined $rc;
	} else {
		$package->statusmsg( "[$modulename] Running Container Report\n" );
		$package->debugmsg( "[$modulename] Running Screen ($page_type) on Container ($current_group)");
		my($usehb)=0;
		#$usehb=1 if defined param("UseHeartbeatTime");
		my($showok);#	set to 1 if you want to show ok items
		$rc=MlpRunScreen(
			-screen_name=>$page_type,
			-container=>$current_group,
			-NL=>"\n",
			-severity=>$current_severity,
			-useheartbeattime=>$usehb,
			-showok=>0,
			-html=>0,
			-maxtimehours=>$t,
			-debug=>$internal_debug,
			-showok=>$okchkbox,
			-production=>$prodchkbox );

		$package->messageBox(	-message => "$!" ) unless defined $rc;
		if( $#$rc < 1 ) {
			# $package->messageBox(	-message => "NO ROWS FOUND" );
			$package->statusmsg( "[$modulename] No Rows Found\n" );
			main::unbusy();	# set it to a normal cursor
			return;
		}

		#foreach (@$rc) { $package->statusmsg( "[DEBUG] ",join(",",@$_),"\n"); }
	}
	$package->statusmsg( "[$modulename] Data Fetch Completed\n" );

	if( ! defined $rc ) {
		$package->messageBox( -message => "Screen ($page_type) returned undefined" );
		$package->statusmsg( "[$modulename] Screen ($page_type) returned undefined\n" );
		main::unbusy();	# set it to a normal cursor
		return;
	}

	$package->statusmsg("[$modulename] Fetched $#$rc Rows Of Data\n" );
	my( @data_rows )= @$rc;
	my( $hdr_row )=shift(@data_rows);

	if( $#data_rows<0 and ! defined $admin ) { 			# filtered to n rows
		$package->messageBox(	-message => "NO DATA RETURNED FOR SECTION ($page_type)" );
		$package->statusmsg( "[$modulename] NO DATA RETURNED FOR SECTION ($page_type)\n" );
		main::unbusy();	# set it to a normal cursor
		return;
	}
	my($operation)= $$hdr_row[$#$hdr_row];
	if( $operation eq "Both" or $operation eq "Update" or $operation eq "Delete" or $operation eq "Add" ){
		pop @$hdr_row;
		push @$hdr_row,"Operation";
	}

	$package->debugmsg( "[$modulename] Page=[$page_type] Header is ".join(",",@$hdr_row) );

	my(%chkbox_mask);
	my($count)=0;

#
# Print The Header Row
#
	$package->statusmsg( "[$modulename] Printing Header\n" );
	my($i)=0;
	my(@textcol);
	foreach ( @$hdr_row ) {

		my($x)=$_;
		if( /^Chkbox_/ ) {
			$x=~s/^Chkbox_//;
			$chkbox_mask{$count}=$x;
			$operation="Chkbox";
		}
		$x=~s/_YN_LU$//;
		$x=~s/_LU$//;
		$x=~s/\_/ /g;
		$x=~s/Min$/\nMin/g;
		$x=~s/^Use Pager/Pager/g;
		$x=~s/^Use Email/Email/g;
		$x=~s/^Use Netsend/Netsend/g;
		#$textObject->insert('end', $x.",","t_row_header");
		push @textcol,$i if $x=~/Text$/ or $x=~/Alarm Subject/ or $x=~/Alarm Message/;

		if( $x eq "OkDelete" ) {
			$package->tableput(0,$count++, "", "t_row_header");
			$package->tableput(0,$count++, "", "t_row_header");
		} else {
			# for heartbeats
			$package->tableput(0,$count, $x, "t_row_header");
				#if defined $admin or $report_type_code ne "h" or $i!=0;
			$count++ unless ((($page_type eq "Heartbeats" or $page_type eq "Batch Jobs")
						and ($i==0 or $i==2 or $i==4))
					or ($page_type eq "Events" and ( $i==0 or $i==2)));
		}
		$i++;
	}

#
# Reword any columns thar are text to be more normal by adding line feeds
#
	$package->statusmsg( "[$modulename] Rewording\n" );
	foreach my $tc (@textcol) {
		my($maxtextlen);
		foreach my $row ( @data_rows ) {
			$$row[$tc] = reword( $$row[$tc],40,45);
			$$row[$tc] =~ s/\s+$//;
		}
	}

#
# if the last item on the header row is "Operation" then include add line if needed
#
	my($colcount)=0;
	my($has_header_row)=0;
	if( defined $admin and
	(   $operation eq "Both" or $operation eq "Update" or 	$operation eq "Add" )){
		$has_header_row=1;
		$package->statusmsg( "[$modulename] Processing Header\n" );
		foreach my $hdr_itm ( @$hdr_row ) {
			#print "   DBG: Header Item [$hdr_itm]\n";
			if( $hdr_itm eq "Operation" ) {
				if( $operation eq "Update" ) {
					my($w) = $package->make_button(-widget=>$textObject,-font=>$font_buttons,
					-pady=>-1,-highlightthickness=>0,-borderwidth=>1,
					-help=>'Update this item',
		    			-text=>"Update Row", -width=>11,
		    			-command=> sub  { $package->process_button(-buttonid=>1,-rowid=>"Special" ); } );
					$package->tableput(1,$colcount, $w, "t_row_header");
				} else {
					my($w) = $package->make_button(-widget=>$textObject,-font=>$font_buttons,
					-pady=>-1,-highlightthickness=>0,-borderwidth=>1,
					-help=>'Add This item',
		    			-text=>"Add Row", -width=>7,
		    			-command=> sub  { $package->process_button(-buttonid=>2,-rowid=>"Special" ); } );

					$package->tableput(1,$colcount, $w, "t_row_header");
				}
			} elsif( $hdr_itm=~/_LU$/ and ($hdr_itm!~/System_LU/ or $#systems<30 )) {
				my(@x);
				if( $hdr_itm=~/Start/ or $hdr_itm=~/End/ ) {
					for (0..24) { push @x, $_; }
				} elsif( $hdr_itm=~/User_LU/ ) {
					@x=MlpGetOperator();
				} elsif( $hdr_itm=~/Program_LU/ ) {
					@x=MlpGetProgram();
					unshift @x, "All Programs" if $admin eq "Alarm Routing";
				} elsif( $hdr_itm=~/System_LU/ ) {
					push @x,@systems;
				} elsif( $hdr_itm=~/Severity_LU/ ) {
					@x=MlpGetSeverity();
				} elsif( $hdr_itm=~/^Use/ or $hdr_itm=~/YN_LU/ ) {
					@x = ("YES","NO");
				} elsif( $hdr_itm=~/^Container_LU/ ) {
					@x = @groups;
				} elsif( $hdr_itm=~/^Time_LU/ ) {
					@x = @timevalues;
				} elsif( $hdr_itm=~/^Enabled/ ) {
					@x = ("YES","NO");
				} else {
					@x = ("Invalid Lookup $hdr_itm","Please Seek Help");
				}
				my($w)=$textObject->Optionmenu(
						-font=>$font_optionmenu,
						-background=>'yellow',
						-options=>\@x,
						-variable=>\$add_row_variables{$hdr_itm}
					);

				$package->tableput(1,$colcount, $w, "t_black");
			} else {

				my($length)=10;
				$length = 2 if /Use_/;
				my($w) = $textObject->Entry(
					-background=>'yellow',
					-textvariable=>\$add_row_variables{$hdr_itm},
					-width=>$length );
				$package->tableput(1,$colcount, $w, "t_row_header");
			}
			$colcount++;
		}
	}

#
# Save the actual data in a format suitable for printing in the next step
#
	$package->statusmsg( "[$modulename]  Operation=$operation admin=$admin report_type_code=$report_type_code page=$page_type\n" );
	my($rowcount)= $has_header_row;
	foreach my $row ( @data_rows ) {
		$rowcount++;

		#
		# SET THE COLOR
		#
		my($color)="t_grey";
		$color = "t_normal" if $severities{$$row[0]} == 1;
		if( ! defined $admin and $report_type_code eq "h" ) {
			if( $page_type eq "Agents" ) {
				$color = "t_out_of_date" if $$row[4]>=24;
				pop @$row;
			} else {
				$color = "t_red" if $severities{$$row[0]} >= 3;
				$color = "t_orange" if ! defined $severities{$$row[0]};
				$color = "t_out_of_date" if ( $$row[4] =~ /Days/ or $$row[4] =~ /Hr/ ) and $page_type ne "Batch";
			}
		} elsif( ! defined $admin and $report_type_code eq "e" ) {
			# filter rows if type=e
			next if $current_severity eq  'Fatals' and $severities{$$row[4]} <5;
			next if $current_severity eq  'Errors' and $severities{$$row[4]} <3;
			next if $current_severity eq  'Warnings' and $severities{$$row[4]} <2;

			$color="t_red" if $current_severity eq "Fatals" and $severities{$$row[4]} >= 5;
			$color="t_red" if $current_severity eq "Errors" and $severities{$$row[4]} >= 4;
			$color="t_red" if $current_severity eq "Warnings" and $severities{$$row[4]} >= 3;
			$color="t_red" if $current_severity eq "All"    and $severities{$$row[4]} >= 3;
			# print "($current_severity) $$row[4] => $severities{$$row[4]} => $color \n";
		} elsif( ! defined $admin ) {
			$color="t_red";
		}

		if( 	$operation eq "Both"
		or 	$operation eq "Ok"
		or 	$operation eq "OkDelete"
		or	$operation eq "Chkbox"
		or 	$operation eq "Delete" ) {
			# button name del_butncnt
			# hidden row values are sv_butncnt_key = value
			my($i)=0;

			# add a hyperlink if needed
			#$$row[1]  = $sys_hyperlink{$$row[1]}
				# if $report_type_code eq "h" and defined $sys_hyperlink{$$row[1]};
			#$$row[2]  = $sys_hyperlink{$$row[2]}
				# if $report_type_code eq "e" and defined $sys_hyperlink{$$row[2]};

			my($colcount)=0;
			foreach my $cell ( @$row ) {
				# must delete or the value from the previous form will overwrite
				# the default we are now considering
				#my($pr_val)=$cell;
				#$pr_val= $sys_hyperlink{$cell} if $report_type_code eq "h" and $i==1 and defined $sys_hyperlink{$cell};
				#$pr_val= $prg_hyperlink{$cell} if $report_type_code eq "h" and $i==3 and defined $prg_hyperlink{$cell};
				#$pr_val= $sys_hyperlink{$cell} if $report_type_code eq "e" and $i==2 and defined $sys_hyperlink{$cell};
				#$pr_val= $prg_hyperlink{$cell} if $report_type_code eq "e" and $i==0 and defined $prg_hyperlink{$cell};

				if( defined $chkbox_mask{$i} ) {
					my($col)=$chkbox_mask{$i};
					my($w)=$textObject->Frame( -relief=>'sunken', -background=>'grey' );
					my($g)=$w->Checkbutton(
						-text=>"",	#"checkbox - $cell - $$row[0] - $col",
						-variable=>\$cell,-background=>'grey',
						-command => sub {
							#print "Admin => ",$package->get_report_from_page(),"\n";
							#print "Server => $$row[0]\n";
							#print "Value = $cell\n";
							if( $col eq "Ignore" ) {
								$package->statusmsg( "[DEBUG]",join("\n",MlpManageData( -debug=>$internal_debug,
									Admin=>get_report_from_page($page_type),
									Server => $$row[0],
									Value => $cell,
									-debug=>$internal_debug,
									Operation=>"setignore")),"\n");
							} elsif( $col eq "Is_Production" ) {
								$package->statusmsg( "[DEBUG]",join("\n",MlpManageData( -debug=>$internal_debug,
									Admin=>get_report_from_page($page_type),
									Server => $$row[0],
									Value => $cell,
									-debug=>$internal_debug,
									Operation=>"setproduction")),"\n");
							} elsif( $col eq "Blackout" ) {
								$package->statusmsg( "[DEBUG]",join("\n",MlpManageData( -debug=>$internal_debug,
									Admin=>get_report_from_page($page_type),
									Server => $$row[0],
									Value => $cell,
									-debug=>$internal_debug,
									Operation=>"setblackout")),"\n");
							} else {
								die "Unknown Column Name $col\n";
							}
						}
						)->pack();
					$package->tableput($rowcount,$colcount++,$w, $color);

					#if( $cell == 1 ) {
					#	$package->tableput($rowcount,$colcount++,"checkbox - $cell", $color);
					#} else {
					#	$package->tableput($rowcount,$colcount++,"checkbox - $cell", $color);
					#}
				} else {
					$package->tableput($rowcount,$colcount,$cell, $color);

					$colcount++ unless ((($page_type eq "Heartbeats" or $page_type eq "Batch Jobs")
						and ($i==0 or $i==2 or $i==4))
					or ($page_type eq "Events" and ( $i==0 or $i==2)));
				}
				$i++;
			}

			my($rid)=$rowcount - $has_header_row;
			if( $operation eq "OkDelete" ) {
				my($w) = $package->make_button(-widget=>$textObject,-font=>$font_buttons,
						-pady=>-1,-highlightthickness=>0,-borderwidth=>1,
    						-help=>'Mark this row as reviewed-ok for 1 day',
			    			-text=>"Review/OK", -width=>11,
			    			-command=> sub  { $package->process_button(-buttonid=>3,-rowid=>$rid,
			    						-hdr=>$hdr_row,
			    						-data=>$data_rows[$rid-1] ); } );
				$package->tableput($rowcount,$colcount++,$w, $color);
				my($w2) = $package->make_button(-widget=>$textObject,-font=>$font_buttons,
						-pady=>-1,-highlightthickness=>0,-borderwidth=>1,
    						-help=>'Permanantly Delete This Row',
			    			-text=>"Delete", -width=>7,
			    			-command=> sub  { $package->process_button(-buttonid=>4,-rowid=>$rid,
			    					-hdr=>$hdr_row,
			    					-data=>$data_rows[$rid-1] ); } );
			   $package->tableput($rowcount,$colcount++,$w2, $color); 					
#    			my($y);			  
#    			print "DBG DBG: Creation\n";  					
#				my($w3)=$current_main_frame->JComboBox(
#			   	-choices=> \("a","b","x"),
#			   #	-relief=>'sunken',
#			   #	-background=>'white',   	
#			   #	-mode => "readonly",
#			   	###-entrywidth=>25,
#			   	-textvariable=>\$y,
#			   	#-maxrows=>4
#			   );	     
#			   print "DBG DBG: DOne Creation\n";
			   
				#$package->tableput($rowcount,$colcount++,$w3, $color); 					
    			
				
			} elsif( $operation eq "Ok" ) {
				my($w) = $package->make_button(-widget=>$textObject,-font=>$font_buttons,
						-pady=>-1,-highlightthickness=>0,-borderwidth=>1,
    						-help=>'Ok This Item',
			    			-text=>"Ok", -width=>2,
			    			-command=> sub  { $package->process_button(-buttonid=>3,-rowid=>$rid,
			    					-hdr=>$hdr_row,
			    					-data=>$data_rows[$rid-1] ); } );
				$package->tableput($rowcount,$colcount++,$w, $color);
			} elsif( $operation ne "Chkbox") {
				my($w) = $package->make_button(-widget=>$textObject,-font=>$font_buttons,
						-pady=>-1,-highlightthickness=>0,-borderwidth=>1,
    						-help=>'Delete this item',
			    			-text=>"Delete", -width=>7,
			    			-command=> sub  { $package->process_button(-buttonid=>4,-rowid=>$rid,
			    					-hdr=>$hdr_row,
			    					-data=>$data_rows[$rid-1] ); } );
				$package->tableput($rowcount,$colcount++,$w, $color);
			};
		} else {
			# add a hyperlink if needed
			#$$row[1]  = $sys_hyperlink{$$row[1]} if $report_type_code eq "h" and defined $sys_hyperlink{$$row[1]};
			#$$row[2]  = $sys_hyperlink{$$row[2]} if $report_type_code eq "e" and defined $sys_hyperlink{$$row[2]};
			#$$row[5]=~s/<//g;
			#$$row[5]=~s/>//g;
			my($colcount)=0;
			my($i)=0;
			#print "Rc=$rowcount ",join("|",@$row),"\n" if $page_type eq "Agents";
			foreach my $x (@$row) {
				$package->tableput($rowcount,$colcount,$x, $color);
				$colcount++ unless ((($page_type eq "Heartbeats" or $page_type eq "Batch Jobs")
						and ($i==0 or $i==2 or $i==4))
					or ($page_type eq "Events" and ( $i==0 or $i==2)));
				$i++;
			}
		}
	}

#
# Show the data
#
	$package->statusmsg( "[$modulename] tableshow()\n" );
	$package->tableshow();

#
# We are Done
#
	$package->statusmsg( "[$modulename] Refresh Completed: $rowcount Rows Have Been Displayed\n" );
	main::unbusy();	# set it to a normal cursor
	$package->statusmsg( "[$modulename] Ready at ".localtime(time)." : $rowcount rows ====================\n" );
	return;
}


# Standard Return Code of 1
1;

__END__

=head1 NAME

eventviewer.pl -  Module for viewing events

=head2 DESCRIPTION

The event viewer is an example plugin that is normally run in stand alone mode.  It
provides a complete interface to your event manangement system.

This plugin is normally called using eventviewer.pl in the root directory.  You can also
call it using the standard gem syntax:

 perl gem.pl --MODULENAME=eventviewer

 print must use -showok if you wish to show ok items in the events

=head2 TO DO

	
	print "\t<TD ALIGN=CENTER>",$hfont,
						submit(-name=>"actionid_$butncnt",-value=>"Submit"),
						popup_menu(	-name=>"actionval_$butncnt",
								-values=>\@hb_values,
								-default=>'MsgDelete',
								-labels=>\%hb_operations ),
						$ehfont,"</TD>\n"
								unless defined param("REPORTNAME");
								
								{
		my($actionval)=param("actionval_".$action_id);
		my($actiontext)=$hb_operations{param("actionval_".$action_id)};
		debugmsg("Button Pushed: ".$actiontext);
		print drawbanner( b("Processing $actiontext"),"", "Row Key=$action_id" );
		my(%keys);
		foreach (param()) {
			next unless /^sv_${action_id}_/;
			my($k)=$_;
			s/sv_${action_id}_//;
			debugmsg( "KEY=$_ VAL=".param($k) );
			$keys{$_} = param($k);
		}

		if( defined param('sv_admin') ) {
			debugmsg( "Admin=".param("sv_admin") );
			$keys{Admin}=param("sv_admin");
			$admin=param("sv_admin");
		}

		if( $actionval eq "MsgDelete") {
			print join(br,MlpManageData(
					OkId=>$action_id,
					Operation=>"del",
					Admin=>"",
					Server		=> $keys{System},
					System	=> $keys{System},
					Program		=> $keys{Program},
					Subsystem	=> $keys{Subsystem},
					State		=> $keys{State},
					)),br;
		} elsif( $actionval eq "DeleteMon") {
			print join(br,MlpManageData(
					OkId=>$action_id,
					Operation=>"del",
					Admin=>"Delete By Monitor",
					Server		=> $keys{System},
					System	=> $keys{System},
					Monitor		=> $keys{Program},
					Program		=> $keys{Program},
					Subsystem	=> $keys{Subsystem},
					State		=> $keys{State},
					)),br;
		} elsif( $actionval =~ /MsgOk/) {
			my($hours)=24;
			$hours=3 if $actionval=~/3hr$/;
			$hours=72 if $actionval=~/3dy$/;

			print join(br,MlpManageData(
					OkId=>$action_id,
					Operation=>"Ok",
					Admin=>"",
					Hours		=>$hours,
					Server		=> $keys{System},
					System	=> $keys{System},
					Program		=> $keys{Program},
					Subsystem	=> $keys{Subsystem},
					State		=> $keys{State},
					)),br;
		} elsif( $actionval eq "Blackout3") {
				print join(br,MlpManageData(
					Operation=>"setblackout",
					Hours	=> 3,
					Value	=> 1,
					Server=>$keys{System},
					Admin=>"Production" )),br;
		} elsif( $actionval eq "Blackout6") {
				print join(br,MlpManageData(
					Operation=>"setblackout",
					Hours	=> 6,
					Value	=> 1,
					Server=>$keys{System},
					Admin=>"Production" )),br;
		} elsif( $actionval eq "Blackout24") {
				print join(br,MlpManageData(
					Operation=>"setblackout",
					Days	=> 1,
					Value	=> 1,
					Server=>$keys{System},
					Admin=>"Production" )),br;
		} elsif( $actionval eq "Blackout72") {
				print join(br,MlpManageData(
					Operation=>"setblackout",
					Days	=> 3,
					Value	=> 1,
					Server=>$keys{System},
					Admin=>"Production" )),br;
		} elsif( $actionval eq "IgnoreSvr") {	# ignore server
				print join(br,MlpManageData(
					Operation=>"setignore",
					Value	=> 1,
					Server=>$keys{System},
					Admin=>"Production" )),br;
		} elsif( $actionval eq "DeleteAll") {
				print join(br,MlpManageData( OkId=>$action_id,
					Operation=>"delserver",Program		=> $keys{Program},
					System=>$keys{System})),br;
		} elsif( $actionval eq "NotProd") {	# server is not production
				print join(br,MlpManageData(
					Operation=>"setproduction",
					Value	=> 0,
					Server=>$keys{System},
					Admin=>"Production" )),br;
		}
	
