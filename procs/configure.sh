#!/bin/sh
#       to install just run
#          configure
#  or
#               configure $SERVER $SA_LOGIN $SA_PASSWORD $DATABASE
#
#       if parameters are not passed it will ask you for them

[ -n "$PROCESSOR_ARCHITECTURE" ] && NT=1

# only print these lines if not in batch mode
myecho()
{
        echo $*
}

NUMARGS=$#
[ $NUMARGS -eq 4 ] && echo "Batch Configure: $*"
if [ $NUMARGS -gt 1 -a $NUMARGS -lt 4 ]
then
        echo "Usage $0 SERVER LOGIN PASS DB"
        exit
fi

NNL=""
myecho "Testing Setup..."

if [ -z "$SYBASE" ]
then
        myecho "Sybase Environment Variable Not Set Up"
        exit
fi

if [ -z "$NT" ]
then
        # UNIX SERVER
        [ -x $SYBASE/bin/isql ] && ISQL=$SYBASE/bin/isql
        [ -x $SYBASE/$SYBASE_ASE/bin/isql ] && ISQL=$SYBASE/$SYBASE_ASE/bin/isql
        [ -x $SYBASE/$SYBASE_OCS/bin/isql ] && ISQL=$SYBASE/$SYBASE_OCS/bin/isql
else
        # NT SERVER
        [ -x $SYBASE/bin/isql.exe ] && ISQL=$SYBASE/bin/isql.exe
        [ -x $SYBASE/$SYBASE_ASA/bin/isql.exe ] && ISQL=$SYBASE/$SYBASE_ASA/bin/isql.exe
        [ -x $SYBASE/$SYBASE_OCS/bin/isql.exe ] && ISQL=$SYBASE/$SYBASE_OCS/bin/isql.exe
fi

if [ ! -x "$ISQL" ]
then
        myecho "isql ($ISQL) not executable"
        exit
fi

TMPFILE=""
[ -d /tmp -a -z "$PROCESSOR_LEVEL" ] && TMPFILE="/tmp/$0.$$"
[ -z "$TMPFILE" -a -d C:/tmp ]  && TMPFILE="C:/tmp/$0.$$"
if [ -z "$TMPFILE" -a -d C:/temp ]
then
        mkdir C:/tmp
        TMPFILE="C:/tmp/$0.$$"
fi

case "`echo 'x\c'`" in
        'x\c') # BSD
                ECHON="echo -n"
                NNL=""
                ;;
        x)      # SYS V
                ECHON="echo"
                NNL="\c"
                ;;
        *)
                echo "$0 quitting: cant setup echo"
                exit 1;;
esac

SERVER=$1
SALOGIN=$2
PASSWD=$3
DATABASE=$4
[ -n "$DATABASE" ] && BATCH="TRUE";

#  first some up front stuff:
VERSION=`head -1 VERS*`

if [ -z "$BATCH" ]
then
        #clear
        myecho "Extended Sybase Stored Procedure Package $VERSION"

        myecho "Copyright (c) 1995-2000 by Edward Barlow"
        myecho "======================================"
        myecho ""
        myecho "You are installing a FREE set of Sybase stored procedures, designed to"
        myecho "extend the Sybase system procedures. "
        myecho ""
        myecho "Details on your right to use are described in the README file."
        myecho ""
        myecho "Home Page: http://www.edbarlow.com"
        myecho ""
        $ECHON "Hit Return to Continue"${NNL}
        read ans2
        myecho ""

        myecho "This script requires that you have the sa password for the server"
        myecho " you wish to install on.  As always, when installing scripts into"
        myecho " master or sybsystemprocs, it is recommended that a dump be taken."
        myecho ""
        myecho "These procs have been fully tested on sybase versions 4.8 through 11.5"
        myecho ""

        $ECHON "Please enter SQL Server DSQUERY [$DSQUERY]: "${NNL}
        read SERVER
        [ -z "$SERVER" ] && SERVER=$DSQUERY
        [ -z "$SERVER" ] && exit 1

        myecho ""
        $ECHON "Please enter SQL Server administrator ACCOUNT (sa): "${NNL}
        read SALOGIN
        [ -z "$SALOGIN" ] && SALOGIN=sa

        myecho ""
        $ECHON "Please enter SQL Server administrator (sa) Password: "${NNL}
        stty -echo
        read PASSWD
        stty echo
fi

myecho ""
myecho "Testing Login To Server"
$ISQL  -U$SALOGIN -P$PASSWD -S$SERVER -o$TMPFILE <<EOF
select name from master..sysdatabases where name='sybsystemprocs'
go
EOF

if [ $? != 0 ]
then
        myecho "*** Unable To Login ***"
        myecho ""
        cat $TMPFILE
        exit
fi

x=`grep sybsystemprocs $TMPFILE`
rm -f $TMPFILE
if [ -z "$x" ]
then
        [ -z "$DATABASE" ] && DATABASE=master
        ISTEN=n
else
        [ -z "$DATABASE" ] && DATABASE=sybsystemprocs
        ISTEN=y
fi

if [ $ISTEN = 'n' ]
then
         cp revindex.492                  revindex.sql
         cp auditdb.492           auditdb.sql
         cp helpcolumn.492                helpcolumn.sql
         cp badindex.492                  badindex.sql
         cp helpindex.492                 helpindex.sql
         cp auditsecurity.492 auditsecurity.sql
         cp block.492             block.sql
         cp colconflict.492       colconflict.sql
         cp collist.492   collist.sql
         cp colnull.492   colnull.sql
         cp helpdb.492    helpdb.sql
         cp helplogin.492 helplogin.sql
         cp helpprotect.492       helpprotect.sql
         cp lock.492              lock.sql
         cp lockt.492             lockt.sql
         cp revtable.492          revtable.sql
         cp syntax.492    syntax.sql
         cp whodo.492     whodo.sql
         #cp quickstats.492        quickstats.sql
         #rm -f record.sql
else
         cp revindex.10           revindex.sql
         #cp record.10             record.sql
         #cp quickstats.10                 quickstats.sql
         cp auditdb.10            auditdb.sql
         cp helpindex.10                  helpindex.sql
         cp helpcolumn.10                 helpcolumn.sql
         cp badindex.10           badindex.sql
         cp auditsecurity.10 auditsecurity.sql
         cp helplogin.10          helplogin.sql
         cp lock.10                       lock.sql
         cp lockt.10              lockt.sql
         cp block.10              block.sql
         cp collist.10            collist.sql
         cp colnull.10    colnull.sql
         cp colconflict.10        colconflict.sql
         cp revtable.10   revtable.sql
         cp whodo.10      whodo.sql
         cp helpdb.10     helpdb.sql
         cp helpprotect.10        helpprotect.sql
         cp syntax.10             syntax.sql
fi

rm -f database
echo "use $DATABASE" > database
chmod 666 database
echo "dump tran $DATABASE with truncate_only" > dumpdb
chmod 666 dumpdb
myecho ""
myecho "Note: the following command will be issued prior to each install:"
cat dumpdb

myecho ""
[ $NUMARGS -ne 4 ] && $ECHON "Continue Install into database $DATABASE? (y or n): "${NNL}
[ $NUMARGS -eq 4 ] && ans='Y'
[ $NUMARGS -ne 4 ] && read ans
if [ "$ans" != "y" -a "$ans" != "Y" ]
then
         echo "Quitting As Per User Instructions"
         exit
fi

# some files must be installed after the others are all in
PASS_2_FILES="helpobject.sql helpdb.sql help.sql helpdevice.sql server.sql"
#[ "$ISTEN" = "y" ] && PASS_2_FILES="$PASS_2_FILES record.sql"
#PASS_2_FILES="$PASS_2_FILES quickstats.sql"

echo $PASS_2_FILES > $TMPFILE
for file in `ls *.sql`
do
        x=`grep $file $TMPFILE`

        # skip pass two files
        [ -n "$x" ] && continue

        if [ ! -r "$file" ]
        then
                myecho "FILE $file not found"
                exit 1
        fi

        myecho "installing $file"

        $ISQL -U$SALOGIN -S$SERVER -P$PASSWD -i$file
        if [ $? != 0 ]
        then
                myecho "ISQL failed"
                exit 1
        fi
done

for file in $PASS_2_FILES
do
        myecho "installing $file"
        $ISQL -U$SALOGIN -S$SERVER -P$PASSWD -i$file
done

rm -f auditdb.sql auditsecurity.sql helplogin.sql lock.sql lockt.sql block.sql
rm -f collist.sql revtable.sql whodo.sql helpdb.sql helpprotect.sql syntax.sql
rm -f colnull.sql colconflict.sql revindex.sql record.sql helpcolumn.sql badindex.sql
rm -f helpindex.sql quickstats.sql

rm -f $TMPFILE
myecho "Successful Installation"

if [ -z "$BATCH" ]
then
myecho ""
myecho "Automatic Registration Process"
myecho ""
myecho "The author requests that you notify him of your use of this package."
myecho "If you register, I will notify you of bugs, fixes, and enhancements."
myecho "I also have several other packages I am working on & will notify you"
myecho "of them."
myecho ""
myecho "Can I mail the author informing him of your successful install? (y or n)"

[ $NUMARGS -eq 4 ] && mailtoed='y'
[ $NUMARGS -ne 4 ] && read mailtoed
myecho ""

if [ "$mailtoed" = "y" -o "$mailtoed" = "Y" ]
then

HOST=`hostname`

mail barlowedward@hotmail.com << EOF
Subject: Install Successful
successful install of version $VERSION procedures by $USER of hostname $HOST
EOF

        myecho "Mail sent"

else

        myecho "No Mail sent"

fi
fi

myecho ""
myecho "Package installation successful.  Enjoy."

