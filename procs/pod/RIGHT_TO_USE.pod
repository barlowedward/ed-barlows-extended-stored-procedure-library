=head1   RIGHT TO USE, RESALE AND COPYRIGHT

All procedures in this package are copyright (c) 1994-2005 by Edward Barlow.
They are released under a standard <A HREF=http://www.gnu.org/copyleft/gpl.html> GPL</A>
license agreement. It will be updated to GPL v3 when that draft is finished.

You may redistribute the package at will (see below). Tell your friends.
Give me access to procedures that you have written for future versions.
Tell me about bugs. Be nice - I am making no money off this.

You are allowed to use this software so long as all copyright notices,
README, and other documentation are not altered and so long as no money is
made by the sale of this software (i.e. you cant include it in a
commercial package
without permission). If you would like to "make money" or include the code
in a commercial package, I ask that you decide on a "fair" price and create
some form of "fair" agreement. Make two copies, sign them both, and send
them to the package author (Edward Barlow). If the agreement seems fair, I will
sign both and send one copy back to you, and we will have a deal. I have
put significant effort into this code and, while my primary purpose is
to create software for people to use, I expect a fair shake from anybody
who can profit from my endeavors.

Procedures submitted by outside authors retain the authors copyright
but I (Edward Barlow and any companies I create to distribute software)
get the right to redistribute them as i see fit (this is to protect me).

