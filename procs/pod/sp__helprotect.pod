
=head1 NAME

sp__helprotect  - Protection Information for current database

=head2 AUTHOR


Edward Barlow ( SQL Technologies, inc. )

=head2 DESCRIPTION


list protection information for current database

=head2 USAGE


sp__helprotect [ @parameter = objectname | username | group ] [@do_system_tables]
[@dont_format]

if @parameter is provided then procedure will attempt to print information
about only one object (if it exists), group, or user.  If no parameter
is passed, all objects are displayed. The @do_system_tables parameter,
if not null, will include system tables in the output.

=head2 SEE ALSO


sp__groupprotect, sp__objprotect

=head2 BUGS


helprotect should really be spelled helpprotect, but im sticking with the
sybase naming convention.

=head2 SAMPLE OUTPUT


 1> sp__helprotect
 ------------------------------
 Grant Execute on ap_get_disk_layout  to public
 Grant Execute on ap_get_ind_to_rebuild  to public
