=head1 NAME

sp__auditsecurity - Audit system security

=head2 AUTHOR

Edward Barlow ( SQL Technologies, inc. )

=head2 DESCRIPTION

  Reports Users With Passwords like the Username
  Reports Users With Null Passwords
  Reports Users With Short (&lt;=4 character) Passwords
  Reports Users With Master/Model/Tempdb Database As Default (except sa)
  Reports allow updates is set
  Reports Users with stupid passwords like "sybase"....

=head2 USAGE

sp__auditsecurity [@print_only_errors,] [@srvname, @hostname ]

if @print_only_errors is not null then prints only errors. Otherwise
it will print statements about successes

Programs that collect errors can pass the parameters parameters (@srvname
&amp; @hostname) to the procedure. If these are passed, the procedure will
print a slightly different return set that includes error numbers and other
information.

=head2 SEE ALSO

sp__auditdb

=head2 ACCESS

This procedure is only runable by sa because it reveals users with weak
passwords.

=head2 SAMPLE OUTPUT

  1> sp__auditsecurity

  Security Violations
  ------------------------------------------------------
  (No Users With Null Passwords)
  User monitor Has master Database As Default
  User mon6 Has master Database As Default
  User a Has master Database As Default
  Allow Updates is Set
  (Allow Updates is Not Set)
  (No Trusted Remote Logins)
