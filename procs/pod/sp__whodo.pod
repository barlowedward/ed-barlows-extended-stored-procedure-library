=head1 NAME

sp__whodo - Check who is doing something on server

=head2 AUTHOR

Simon Walker &amp; Ed Barlow (Ed rewrote Simons command)

=head2 DESCRIPTION

sp__who that drops awaiting commands and the sa stuff

=head2 SEE ALSO

sp__whoactive, sp__isactive, sp__iostat, sp__who

=head2 USAGE

sp__whodo [ @param ]

if @param is a login, it will only print information for that login.
If it is a database, it will only print information about users in that
database.

=head2 SAMPLE OUTPUT

 1> exec sp__whodo

 pid loginame  cpu   io  mem  dbname     status   cmd      bk bktime
 --- --------- ----- --- ---- ---------- -------- -------  -- ------
 1   sa        1104  8   1    statsdb    running  INSERT   0  0
