=head1 NAME

sp__revrule - Reverse engineer rules in the database

=head2 AUTHOR

Created By  : "Chris Vilsack" <cvilsack@btmna.com>

=head2 DESCRIPTION

reverse engineers rules in your current database.

=head2 USAGE

sp__revrule

=head2 SEE ALSO

sp__revalias, sp__revdb,
sp__revdevice, sp__revgroup,
sp__revindex, sp__revlogin,
sp__revmirror, sp__revuser,
sp__revtable

WARNING: run all the reverse engineering utilities in at least 180
column mode (-w180) to prevent line wrapping.

=head2 SAMPLE OUTPUT

 exec sp_bindrule 'cc1_sd_product_group', 'sd_product_group.fx'
 exec sp_bindrule 'cc2_sd_product_group', 'sd_product_group.int'
 exec sp_bindrule 'option_yn_rule', 'acc_control.active'
 exec sp_bindrule 'option_yn_rule', 'acc_control.backup'
 exec sp_bindrule 'option_yn_rule', 'acc_control.modify'

