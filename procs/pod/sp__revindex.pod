
=head1 NAME

sp__revindex - Reverse engineer indexes in current database

=head2 AUTHOR


Edward Barlow ( SQL Technologies, inc. )

=head2 DESCRIPTION


reverse engineers indexes in your current database

=head2 USAGE


sp__revindex

=head2 SEE ALSO


sp__revalias, sp__revdb,
sp__revdevice, sp__revgroup,
sp__revindex, sp__revlogin,
sp__revmirror, sp__revuser
WARNING:

Run all the reverse engineering utilities in at least 180 column mode
(-w180) to prevent line wrapping.

=head2 SAMPLE OUTPUT


 1> sp__revindex

 Text
 -------------------------------------------------------------------
 create clustered index XPKalerts on dbo.alerts (hostname,srvname)

 create unique clustered index XPKAccounting on dbo.applications
     (dbname)
 create unique clustered index XPKaudit_trail on dbo.audit_trail
     (date,login_name)
 create unique clustered index XPKdatabase on dbo.comn_database
     (srvname,dbname)
 create unique clustered index XPKcomn_dumpdevices
     on dbo.comn_dumpdevices
