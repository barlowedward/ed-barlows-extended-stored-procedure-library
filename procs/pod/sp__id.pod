
=head1 NAME

sp__id  - Tells you who you are and in which database

=head2 AUTHOR


Edward Barlow

DESCRIPTION:

Tells you who you are and in which database you are in

=head2 USAGE


sp__id

=head2 SAMPLE OUTPUT

 1> exec sp__id

 db                   login                id   db name
 -------------------- -------------------- ---- --------------------
 statsdb              sa                   1    dbo
