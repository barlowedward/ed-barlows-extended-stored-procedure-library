
=head1 NAME

sp__badindex - List badly formed indexes or those needing statistics

=head2 AUTHOR


Edward Barlow ( SQL Technologies, inc. )

=head2 DESCRIPTION


Identifies bad indexes according to the following rules. Finds indexes
containing null, vbl lth, text, or image columns. Find indexes over 30
bytes long or indexes that have never had statistics updated on them. List
NC indexes on small tables.

=head2 USAGE


sp__badindex [ @tablename ]

=head2 SAMPLE OUTPUT


1> exec sp__badindex

 Table/Index Name               Description            Problem Found
 ------------------------------ ---------------------- --------------
 alerts.XPKalerts               Length = 60            >30 Byte Index
 alerts.XPKalerts               srvname char(30)       Allows Null
 audit_trail.XPKaudit_trail     Length = 38            >30 Byte Index
 comn_database.XPKdatabase      Length = 60            >30 Byte Index
 comn_dumpdevices.XPKcomn_dumpd Length = 60            >30 Byte Index
 comn_syscolumns.XPKcomn_syscol Length = 94            >30 Byte Index
