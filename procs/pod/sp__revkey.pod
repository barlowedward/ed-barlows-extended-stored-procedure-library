=head1 NAME

sp__revkey - Reverse engineer keys in the database

=head2 AUTHOR

Created By  : "Chris Vilsack" <cvilsack@btmna.com>

=head2 DESCRIPTION

reverse engineers keys in your current database.

=head2 USAGE

sp__revkey

=head2 SEE ALSO

sp__revalias, sp__revdb,
sp__revdevice, sp__revgroup,
sp__revindex, sp__revlogin,
sp__revmirror, sp__revuser,
sp__revtable

WARNING: run all the reverse engineering utilities in at least 180
column mode (-w180) to prevent line wrapping.

=head2 SAMPLE OUTPUT

 exec sp_primarykey 'sd_region', region_id
 go
 exec sp_primarykey 'sd_site', site_id
 go
 exec sp_primarykey 'sd_table_maint', tbl_name
 go
 exec sp_primarykey 'sd_timeband', timeband_id
 go
 exec sp_primarykey 'acc_control', db_name
 go

