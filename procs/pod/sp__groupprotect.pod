
=head1 NAME

sp__groupprotect - Synopsis of protection stuff.

=head2 AUTHOR


Edward Barlow ( SQL Technologies, inc. )

=head2 DESCRIPTION


Gives number of select / update /delete /insert / revoke / and execute
grants for each group and type of object. Useful to summarize what groups
have priviliges to do what.

=head2 SEE ALSO


sp__helprotect, sp__objprotect

=head2 SAMPLE OUTPUT


  1> sp__groupprotect
  2> go
  type grp             tot  sel    upd  del  ins  rev  exe
  ---- --------------- ---- ------ ---- ---- ---- ---- ----
  P    g_mon6          27   0      0    0    0    0    0
  P    public          27   0      0    0    0    0    9
  R    g_mon6          6    0      0    0    0    0    0
  R    g_monitor       6    0      0    0    0    0    0
  S    g_mon6          57   0      0    0    0    0    0
  S    g_monitor       57   0      0    0    0    0    0
  S    public          57   16     0    0    0    0    0
  U    g_mon6          33   0      0    0    0    0    0
  U    g_monitor       33   0      0    0    0    0    0
  U    public          33   11     0    0    0    0    0
  V    g_mon6          3    0      0    0    0    0    0
  V    g_monitor       3    0      0    0    0    0    0
  V    public          3    0      0    0    0    0    0
