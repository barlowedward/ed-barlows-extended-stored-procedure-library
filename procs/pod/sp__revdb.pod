
=head1 NAME

sp__revdb - Reverse engineer database layout of the server

=head2 AUTHOR


Edward Barlow ( SQL Technologies, inc. )

=head2 DESCRIPTION


reverse engineers layout of the databases on a server. Purposly excludes
device "master", "model", and "tempdb".

=head2 USAGE


sp__revdb

=head2 SEE ALSO


sp__revalias, sp__revdb,
sp__revdevice, sp__revgroup,
sp__revindex, sp__revlogin,
sp__revmirror, sp__revuser

WARNING: Run all the reverse engineering utilities in at least 180 column
mode (-w180) to prevent line wrapping.

=head2 SAMPLE OUTPUT


 1> sp__revdb

 Create Database statsdb on 'data3'=2
  ,'datadevice'=2
  ,'datadevice'=3
  log on 'log'=2
 go
 Create Database migrator on 'datadevice'=10
 go
 Create Database pubs2 on 'master'=2
 go
