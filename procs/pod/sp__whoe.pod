
=head1 NAME

sp__whoe - Long format Who is on System

=head2 AUTHOR

Philippe Wathelet July 1998

=head2 DESCRIPTION

This is a substitute for Sybase's standard sp_who stored procedure

The output is clearer without line wrapping.  You can help it with:

   isql -U<user> -S<server> -w150

allowing a width of up to 150 characters per line for example

=head2 USAGE

sp__whoe [ @login ]

if @login is passed, it will only print information for that login.

=head2 SAMPLE OUTPUT

 1> exec sp__whoe
