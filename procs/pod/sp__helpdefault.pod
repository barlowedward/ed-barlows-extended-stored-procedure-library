
=head1 NAME

sp__helpdefault  - list default information in current databases

=head2 AUTHOR


Edward Barlow ( SQL Technologies, inc. )

=head2 DESCRIPTION


list defaults information in current database

=head2 USAGE


sp__helpdefault [ @objectname ]

if @objectname is provided then procedure will attempt to print information
about only one object (if it exists). If not, it will print any objects
with that string fragment in them. An error occurs if no objects with the
string fragment exist. For example, if you wish to print any table with
the name tbl in them use sp__helptable "tbl". If no parameter is passed,
all object of type displayed.

=head2 SEE ALSO


sp__help, sp__helpdefault,
sp__helpobject, sp__helpproc,
sp__helprule, sp__helptable,
sp__helptrigger, sp__helpview

=head2 BUGS


These procedures only read row one of syscomments to determine useful information
regarding defaults, rules, and views. It is concievable (though unlikely)
that a very long select statement could cause necessary data to be in row
two, resulting in ugly output.

=head2 SAMPLE OUTPUT


 1> sp__helpdefault

  Default Name         Times Used Definition
  -------------------- ---------- -------------------------------------
