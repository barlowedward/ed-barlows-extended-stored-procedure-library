=head1 NAME

sp__auditdb  - Audit current database

=head2 AUTHOR

Edward Barlow ( SQL Technologies, inc. )

=head2 DESCRIPTION

   Checks Common Database Problems
   Lists users in group public if groups are used.
   Warn about lack of groups if no groups exist besides public.
   List users aliased to another non dbo user.
   List aliases without logins (login previously dropped).
   List users without logins (login previously dropped).
   List objects owned by non-dbo (maybe poor code control?).
   Find objects with access to syslogins in them. This
      procedure excludes normal objects like sp__addlogin. Use
      of this procedure will identify potential Trojan horses.
   Find any objects with public access.
   If not master db, list any objects starting with "sp_" that
      are also in master (Trojan horses).
   Database has not had transaction log dump in 24 hours.
   Checks Object / Comment mismatch (hand deleted, or rename)
   Create object permissions granted to users

=head2 USAGE

sp__auditdb [@srvname, @hostname ]

External programs that collect errors can pass the parameters parameters (@srvname &amp; @hostname) to the procedure. If these are passed, the procedure
will print a slightly different return set that includes error numbers
and other information.

=head2 SEE ALSO

sp__auditsecurity

=head2 ACCESS

This procedure can be only runable by sa because it may reveal information
that can help an intruder..

=head2 SAMPLE OUTPUT

1> sp__auditdb

   Error
   ---------------------------------------------
   Object get_comn_syslogins has access to syslogins
   Object get_comn_sysusers has access to syslogins
   Object get_comn_sysusers has access to syslogins
   User sa is a member of group public
   Group Public access to object pb_catcol type=P
   Group Public access to object pb_catedt type=P
   Group Public access to object pbcatfmt type=U
   Group Public access to object pbcattbl type=U
