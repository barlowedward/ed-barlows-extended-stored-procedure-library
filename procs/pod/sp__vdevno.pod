
=head1 NAME

sp__vdevno - Show used device numbers

=head2 AUTHOR


Simon Walker, The SQL Workshop LTD.

=head2 DESCRIPTION


Shows Used Device Numbers

=head2 SAMPLE OUTPUT


 1> exec sp__vdevno

 vdevno  device
 ------- ------------------------------
     0   master
     1   sysprocsdev
     2   sybsecurity
     3         -- free --
     4         -- free --
     5         -- free --
     6         -- free --
     7   datadev
     8   datadev3
     9   datadev2
