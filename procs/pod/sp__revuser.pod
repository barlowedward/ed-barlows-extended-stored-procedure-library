
=head1 NAME

sp__revuser - Reverse engineer users of current server / database

=head2 AUTHOR


Edward Barlow ( SQL Technologies, inc. )

=head2 DESCRIPTION


reverse engineers user layout in your current database

=head2 USAGE


sp__revuser

=head2 SEE ALSO


sp__revalias, sp__revdb,
sp__revdevice, sp__revgroup,
sp__revindex, sp__revlogin,
sp__revmirror, sp__revuser

=head2 WARNING

Run all the reverse engineering utilities in at least 180 column mode
(-w180) to prevent line wrapping.

=head2 SAMPLE OUTPUT


 1> sp__revuser
 -------------------------------------------
 exec sp_adduser 'sa','dbo','developer'
 exec sp_adduser 'xxx','xxx','user'
 exec sp_adduser 'yyy','yyy','user'
 exec sp_adduser 'ttt','ttt','developer'
