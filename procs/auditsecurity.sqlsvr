/* Procedure copyright(c) 1995 by Edward M Barlow */

/************************************************************************\
|* Procedure Name:      sp__auditsecurity
|*
|* Description:         server check security
|*
|*              Users With Paswords like %Id%
|*              Users With Null Passwords
|*              Users With Short (<=4 character) Passwords
|*              Users With Master,Model, or Tempdb Database As Default
|*              Allow Updates is set
|*              Checks stupid passwords
|*
|* Usage:               sp__auditsecurity @print_only_errors
|*
|*    if @print_only_errors is not null then prints only errors
|*
|* Modification History:
|*
\************************************************************************/
use master
go
dump tran master with no_log
go
if exists (select *
           from   sysobjects
           where  type = 'P'
           and    name = 'sp__auditsecurity')
begin
    drop proc sp__auditsecurity
end
go

create procedure sp__auditsecurity (
        @print_only_errors int = NULL,
        @srvname varchar(255) = NULL,
        @hostname varchar(255) = NULL ,
        @dont_format char(1) = NULL)
as
begin
	declare @tempdb int

        if @srvname is not null and @hostname is null
        begin
                print 'MUST PASS BOTH SERVER AND HOST IF EITHER SPECIFIED'
                return 200
        end

        create table #error
        ( error_no int not null, msg char(255) not null )

        set nocount on

        INSERT  #error
        SELECT          31004,'User '+name+' Has '+dbname+' Database As Default'
        from            master..syslogins
        where           dbname in ('master','model','tempdb')
        and             name != 'sa'
        and             name != 'probe'
        if @@rowcount=0 and @print_only_errors is null
                insert #error values (31000, '(No Users With Master/Model/Tempdb As Default)')

       INSERT  #error
        SELECT          31036,'Database '+name+' Is Unusable'
        from            master..sysdatabases
		-- removed 32 - loading - to exclude hot backup systems
        where           status & (64+128+256+512) != 0
			and name not in ('Northwind','pubs')

       INSERT  #error
        SELECT          31035,'Database '+name+' Has No Torn Page Detection'
        from            master..sysdatabases
        where           status & (32+64+128+256+512) != 0
			and name not in ('master','tempdb','Northwind','pubs')
			and has_dbaccess(name)=1

       INSERT  #error
        SELECT          31034,'Database '+name+' Is not auto-shrink'
        from            master..sysdatabases
        where           status & 4194304 = 0 and name not in ('master','tempdb','Northwind','pubs','msdb','model')
			and has_dbaccess(name)=1

        INSERT  #error
        SELECT          31005,'Allow Updates is Set'
        from            master..syscurconfigs
        where           config=102 and value=1

        if @@rowcount=0
                INSERT  #error
                SELECT          31005,'Allow Updates is Set'
                from            master..sysconfigures
                where           config=102 and value=1

	select @tempdb=sum(size)
		from tempdb..sysfiles
		where status&0x2=0x2

	if @tempdb < 100
		INSERT  #error
                SELECT          31011,'Tempdb is only '+convert(varchar,@tempdb)+'MB'

	if @srvname is null
	begin
		if @dont_format is null
			select "Security Violations"=substring(msg,1,79)
			from #error
		else
			select "Security Violations"=msg
			from #error
	end
	else if @hostname is null
	begin
		if @dont_format is null
			select srvname=@srvname,db="master","Violation"=substring(msg,1,65)
			from #error
		else
			select srvname=@srvname,db="master","Violation"=msg
			from #error
	end
	else
	begin
		if @dont_format is null
			select hostname=@hostname,srvname=@srvname,error_no,db="master",type="a",day=getdate(),"Violation"=substring(msg,1,65)
			from #error
		else
			select hostname=@hostname,srvname=@srvname,error_no,db="master",type="a",day=getdate(),"Violation"=msg
			from #error
	end
end
go

/* no priviliges to any but sa */
