: # use perl
    eval 'exec perl -S $0 "$@"'
    if $running_under_some_shell;

#
# GENERIC STORED PROCEDURE INSTALLER
#

use strict;
use Getopt::Long;
use File::Basename;
my( $curdir,$startdir);

BEGIN {
	use Cwd;
	$startdir=dirname($0);
	chdir $startdir or die "ERROR: Cant cd to $startdir: $!\n";
	$startdir = cwd();
	chdir ".." or die "ERROR: Cant cd to .. from $curdir: $!\n";
	chdir ".." or die "ERROR: Cant cd to .. from $curdir: $!\n";
	$curdir = cwd();
}

use lib "$curdir/lib";
use lib "$startdir/lib";
use CommonFunc;
use Sys::Hostname;
use DBIFunc;

my($USER,$PASSWORD,$SERVER,$FILE,$TYPE,$DEBUG,$GENERATE ,$HTML,$UPGRADE_TO_LATEST,$ALLSERVERS,$MINVERSION,$TEST_MODE);

sub usage
{
	print "usage: configure.pl --FILE=file --DEBUG --USER=sa --PASSWORD=pass --SERVER=svr --MINVERSION=ver --UPGRADE_TO_LATEST --ALLSERVERS\n";
   	return "\n";
}

$| =1;		# dont buffer standard output

die usage() unless GetOptions(
		"USER=s" 		=> \$USER,
		"PASSWORD=s" 		=> \$PASSWORD,
		"SERVER=s" 		=> \$SERVER,
		"FILE=s" 		=> \$FILE,
		"MINVERSION=s"		=> \$MINVERSION,
		"TYPE=s" 		=> \$TYPE ,
		"HTML" 			=> \$HTML ,
		"GENERATE"		=> \$GENERATE ,
		"ALLSERVERS" 		=> \$ALLSERVERS ,
		"UPGRADE_TO_LATEST" 	=> \$UPGRADE_TO_LATEST ,
		"TEST_MODE"     	=> \$TEST_MODE,
		"DEBUG"     		=> \$DEBUG );

print "Changing Directory To $startdir\n";
chdir $startdir or die "ERROR: Cant cd to $startdir: $!\n";

my($NL)="\n";
$NL="<br>\n" if defined $HTML;
$DEBUG=1 if $TEST_MODE;

my($a,$VERSION)=get_version();
#die "A is $a\nB is $b\n";
#my($VERSION);
#open(VERSION,"VERSION") or die "Cant read VERSION file - curdir is ".cwd().": $!\n";
#while (<VERSION>) { chop; $VERSION=$_; last; }
#close VERSION;

open( LREC,">> installhist.log");
print   LREC "ProcInstall\t",
	$VERSION,	"\t",
	scalar(localtime(time)),"\t",
	hostname(),"\t",$SERVER,"\n";
close(LREC);

print $NL,"Extended Stored Procedure Library Installer $VERSION $NL";
print "Copyright (c) 1995-2006 by Sql Technologies $NL";

chomp $MINVERSION;
chomp $VERSION;

if( defined $UPGRADE_TO_LATEST ) {
	die "Cant pass --UPGRADE_TO_LATEST and --MINVERSION\n"
		if defined $MINVERSION;
	$MINVERSION=$VERSION;
	$MINVERSION=~s/[a-z]//g;
}

$MINVERSION=~s/\s//g;
if( defined $MINVERSION and $MINVERSION !~ /^\d+\.\d+$/ ) {
	print "ERROR - MINVERSION ($MINVERSION) is not a floating point number\n";
	exit(0);
}

installmsg() if ! defined $USER or ! defined $SERVER or !defined $PASSWORD;

my(@data);
foreach (<DATA>) {
	chomp;
	next if /^\s*$/ or /^\s*$/;
	push @data,$_;
}

if( $GENERATE ) {
	print "GENERATING FILES\n";
	foreach my $servertype ("SYBASE_MDA","SQLSVR","SYBASE12","SYBASE15") {
		print "Generating install".$servertype.".bat\n";
		open(FILE,">install".$servertype.".bat") or die "Cant write";
	foreach my $r (@data) {
		chomp $r;
		next if $r=~/^\s*#/ or $r=~/^\s*$/;
		my($ty,$nm)=split(/\s+/,$r);
		# TY = SYBASE SYBASE_MDA SYBASE_15 SYBASE_12 SQLSVR
		if( $ty eq "SYBASE" ) {
			next if $servertype eq "SYBASE_MDA";
			next if $servertype eq "SQLSVR";
		} elsif( $ty eq "SYBASE_MDA" ){
			next unless $servertype eq "SYBASE_MDA";
		} elsif( $ty eq "SYBASE_15" ){
			next if $servertype eq "SYBASE_MDA";
			next if $servertype eq "SQLSVR";
			next if $servertype eq "SYBASE12"
		} elsif( $ty eq "SYBASE_12" ){
			next if $servertype eq "SYBASE_MDA";
			next if $servertype eq "SQLSVR";
			next if $servertype eq "SYBASE15"
		} elsif( $ty eq "SQLSVR" ){
			next if $servertype eq "SYBASE_MDA";
			next if $servertype =~ /SYBASE/;
		} else {
			die "UNKNOWN $ty $nm \n";
		}
		#my($is_mda)="FALSE";
		#$is_mda="TRUE" if $f=~/^SYBASE_MDA/;
		#next if $f=~/^#/ or $f=~/^\s*$/;
		#next if $servertype eq "SQLSVR" and $f!~/SQLSVR/;
		#next if $servertype =~ /SYBASE/ and $f!~/SYBASE/;
		#next if $is_mda eq "TRUE" and $servertype ne "SYBASE_MDA";
		#next if $is_mda eq "FALSE" and $servertype eq "SYBASE_MDA";
		#next if $servertype eq "SYBASE15" and $f=~/SYBASE_12/;
		if( $servertype eq "SQLSVR" ) {
			print FILE  "osql -E -S\$1 -n -i$nm\n";
		} else {
			print FILE "isql -Usa -S\$1 -P\$2 -i$nm\n";
		}
	}
		close(FILE);
	}
	exit();
}

if( defined $ALLSERVERS ) {
	use Repository;
	print "Installing Servers\n";
	my(@srv)=get_password(-type=>'sybase');
	foreach $SERVER (@srv) {
		($USER,$PASSWORD)=get_password(-type=>'sybase',-name=>$SERVER);
		die "SYBASE ENV VBL MUST BE DEFINED"
			if ! defined $ENV{"SYBASE"};
		print "Installing $SERVER\n";
		install_a_server($SERVER,$USER,$PASSWORD);
	}
	if( is_nt() ) {
		print "Installing Sql Servers\n";
		my(@srv)=get_password(-type=>'sqlsvr');
		foreach $SERVER (@srv) {
			($USER,$PASSWORD)=get_password(-type=>'sqlsvr',-name=>$SERVER);
			print "Installing $SERVER\n";
			install_a_server($SERVER,$USER,$PASSWORD);
		}
	}
	exit(0);
}

if( ! defined $SERVER ) {
	print "Please Enter A Server Name: ";
	$SERVER=<STDIN>;
	chomp $SERVER;
	die usage("Must pass server\n")	     unless defined $SERVER and $SERVER !~ /^\s*$/;
}

if( ! defined $USER ) {
	print "Please Enter A User Name (must have sa_role): ";
	$USER=<STDIN>;
	chomp $USER;
	die usage("Must pass user\n")	       unless defined $USER and $USER !~ /^\s*$/;
}

if( ! defined $PASSWORD ) {
	print "Please Enter A Password: ";
	$PASSWORD=<STDIN>;
	chomp $PASSWORD;
	die usage("Must pass password\n")	   unless defined $PASSWORD and $PASSWORD !~ /^\s*$/;
}


die "SYBASE ENV VBL MUST BE DEFINED"
	if ! defined $ENV{"SYBASE"} and $TYPE eq "Sybase" ;

install_a_server($SERVER,$USER,$PASSWORD);
exit(0);

sub install_a_server {
	my($SERVER,$USER,$PASSWORD)=@_;
	die "NO SERVER DEFINED\n" unless defined $SERVER;
	print "Working on Server $SERVER User=$USER PASS=$PASSWORD\n" if $DEBUG;


	# Figure out what TYPE is if it isnt passed
	# $TYPE="Sybase" if ! defined $TYPE and is_nt();
	if( ! defined $TYPE ) {
		my($found)="FALSE";
		#print "Setting --TYPE. Override with -TYPE=Sybase or -TYPE=ODBC.\n";
		#print "SYBASE=",join(" ",DBI->data_sources("Sybase")),"\n" if $DEBUG;
		if( is_nt() ) {
			print "Checking ODBC Sources: ",join(" ",DBI->data_sources("ODBC")),"\n" if $DEBUG;
			foreach ( DBI->data_sources("ODBC")) {
				if( $_ =~ /:$SERVER$/ ) {
					$found="TRUE";
					$TYPE="ODBC";
					print "\tSetting connection type to ODBC\n";
					last;
				}
			}
			if( ! defined $TYPE ) {
				print "Unable to find ODBC data source for $SERVER - defaulting to sybase\n";
			}
		}
		if( ! defined $TYPE ) {
			#print "\tSetting -TYPE to Sybase \n";
			$TYPE="Sybase";
		}
	}

	print "Testing Login To Server $SERVER as $USER",$NL if defined $DEBUG;
	my($connection)=dbi_connect( -srv=>$SERVER,
		-login=>$USER,
		-password=>$PASSWORD,
		-type=>$TYPE, #-debug=>$DEBUG,
		-connection=>1 );

	if ( ! $connection ) {
		print "LINE: ",__LINE__," NO CONNECTION\n" if $DEBUG;

		if(is_nt() and $TYPE eq "Sybase" ) {
			print "Error: Possible Bad Connection Type For $SERVER - Check ODBC Settings\n";
		}
		print "*******************************",$NL;
		print "* Cant connect to $SERVER using dbi",$NL;
		print "*******************************",$NL;
		return;
	}

	my($servertype,$DBVERSION)=dbi_db_version( -connection=>$connection );
	$servertype="SQLSVR" if $servertype eq "SQL SERVER";
	my($DATABASE)="sybsystemprocs" if $servertype eq "SYBASE";

#	my($servertype,$DATABASE);
#	foreach ( dbi_query(-db=>"master",-query=>"select name from sysdatabases where name='sybsystemprocs'", -connection=>$connection )) {
#		$DATABASE="sybsystemprocs";
#		$servertype="SYBASE";
#	}
#	print "LINE: ",__LINE__," DATABASE=$DATABASE SERVERTYPE=$servertype\n" if $DEBUG;

	my($ADD_MDA)="FALSE";
	my($IS_SYS_15)="FALSE";
	print "[$SERVER] server type=$servertype version=$DBVERSION\n" if $DEBUG;
	if( $servertype eq "SYBASE" and $DBVERSION>=12.5 ) {

		# print "DBG DBG: in sybase block Checking - Enable Monitoring Option\n" if $DEBUG;
		my($enabled)="FALSE";
		foreach( dbi_query(-db=>"master",
			-query=>"select * from sysconfigures where config=356 and value=1",
			-connection=>$connection ) ) {
			print "[$SERVER] Enable Monitoring Is 1\n";
			$enabled="TRUE";
			# well enable monitoring on... check number of tables

			foreach( dbi_query(-db=>"master",
				-query=>"select count(*) from master..sysobjects where type='U' and name like 'mon%'",
				-connection=>$connection ) ) {
				my($num)=dbi_decode_row($_);
				if( $num > 15 ) {	 # there are ~20 of these things so this will work
					$ADD_MDA="TRUE";
					print "[$SERVER] SYBASE MDA TABLES FOUND\n";
				} else {
					print "[$SERVER] ONLY $num SYBASE MDA TABLES FOUND (mon%)\n";
				}
			}
			if( $ADD_MDA eq "TRUE" ) {
				my($mode)=dbi_set_mode("INLINE");
				foreach( dbi_query(-db=>"master",
					-query=>"select count(*) from master..monOpenDatabases",
					-connection=>$connection,
					-die_on_error=>0 ) ) {
					my($msg)=dbi_decode_row($_);
					if( $msg =~ /Requested server name not found/ ) {
						print "-------------------------------------------------------------------\n";
						print "MDA TABLES EXIST BUT ARE NOT SET UP        \n";
						print "PLEASE SET UP YOUR MDA TABLES IF YOU WISH MDA PROCS TO BE INSTALLED\n";
						print "-------------------------------------------------------------------\n";
						$ADD_MDA="FALSE";
						last;
					}
					#print "RETURNED: ",$msg,"\n";
				}
				dbi_set_mode($mode);
			}
		};
		if( $DBVERSION >= 15 ) {
		#foreach( dbi_query(-db=>"master",-query=>"select * from sysconfigures where config=122 and value>=15000", -connection=>$connection ) ) {}
			# IT IS SYSTEM 15!!!
			print "[$SERVER] Seting System 15 Flag\n";
			$IS_SYS_15="TRUE";
		};
	}

#print "DBG DBG: in sql svr block\n" if $DEBUG;
	my($osql_cmd)= "osql -n -U$USER -P$PASSWORD -S$SERVER ";
	my($osql_cmd_pr)= "osql -n -U$USER -PXXX -S$SERVER ";
	if( ! defined $DATABASE )  {
		foreach ( dbi_query(-db=>"master",
			-query=>"select name from sysdatabases where name='msdb'",
			-connection=>$connection )) {
			$DATABASE="master";
			$servertype="SQLSVR";

			# double check badd pasword stuff
			open(CMD,$osql_cmd." -Q\"select 'SUCCESS'\" |")
				or die "Cant test connection\n";
			my($success)="FALSE";
			foreach (<CMD>) {
				$success="TRUE" if /SUCCESS/;
			}
			close(CMD);

			if( $success eq "FALSE" ) {
				# try trusted...
				$osql_cmd= "osql -n -E -S$SERVER ";
				$osql_cmd_pr= "osql -n -E -S$SERVER ";
				# tripple check badd pasword stuff - try -E flag
				open(CMD,$osql_cmd." -Q\"select 'SUCCESS'\" |")
					or die "Cant test connection\n";
				my($success)="FALSE";
				foreach (<CMD>) {
					$success="TRUE" if /SUCCESS/;
				}
				close(CMD);
				if( $success eq "FALSE" ) {
					print "*******************************",$NL;
					print "* Cant connect to $SERVER using osql",$NL;
					print "*******************************",$NL;
					return;
				} else {
					print "*******************************",$NL;
					print "* Bad Sql Server Password Passed to $SERVER",$NL;
					print "* Successful connection in trusted mode",$NL;
					print "* Continuing...",$NL;
					print "*******************************",$NL;
				}

			}
		}
	}

	die "UNKNOWN DATABASE - neither msdb or sybsystemprocs found\n"
		unless defined $DATABASE;

	my($proclib)="";
	my(@rc)= dbi_query(-db=>$DATABASE,
			-query=>"select 1 from sysobjects where name='sp__proclib_version'",
			-connection=>$connection );
	if($#rc>=0) {
		my($spVERSION);
		foreach( dbi_query(-db=>$DATABASE,-query=>"exec sp__proclib_version", -connection=>$connection ) ) {
			($spVERSION)=dbi_decode_row($_);
		};

		if( defined $MINVERSION ) {
			if( $MINVERSION > $spVERSION ) {
				print "Current Stored Procedure At Version $spVERSION (installing) $NL";
			} else {
				if( $MINVERSION == $spVERSION ) {
					print "Skipping installation: version is current ($spVERSION) $NL $NL";
				} else {
					print "Skipping installation: current version ($spVERSION) >= $MINVERSION $NL $NL";
				}
				return;
			}
		} else {
			print "Current Stored Procedure At Version $spVERSION $NL";
		}
	} else {
		print "Procedure Library is Not Currently installed...\n";
	}

	my($useline,$dbline);

	unlink "database" if -r "database";
	open(WR,">database") or die "cant write file database";
	$useline="use $DATABASE";
	print WR $useline,"\n";
	close(WR);
	chmod 0666, "database";

	unlink "dumpdb" if -r "dumpdb";
	open(WR2,">dumpdb") or die "cant write file dumpdb";
	$dbline="dump tran $DATABASE with no_log\n";
	print WR2 $dbline,"\n";
	close(WR2);
	chmod 0666, "dumpdb";

	# test sybase install if needed
	my($SYB_IS_ISQL_OK)="FALSE";
	if( $servertype ne "SQLSVR" ) {
		open(CMD,"isql -U$USER -P$PASSWORD -S$SERVER -iget_syb_crdate.sql |")
			or die "Cant run isql -U$USER -P<pass> -S$SERVER -iget_syb_crdate.sql\n$!\n";
		foreach (<CMD>) {
			$SYB_IS_ISQL_OK="TRUE" if /crdate/;
		}
		dbi_set_mode("INLINE");
	}

	print "Install Information for $SERVER
		>> Target Db = $DATABASE
		>> SERVERTYPE=$servertype
		>> IS_SYS_15=$IS_SYS_15
		>> SYB_IS_ISQL_OK=$SYB_IS_ISQL_OK
		>> ADD_MDA=$ADD_MDA
		>> NUM PROCS = $#data\n";# if defined $DEBUG;

	foreach my $r (@data) {
		my($f)=$r;
		print "skipping $f TEST MODE IS TRUE ($TEST_MODE)\n"
			if $TEST_MODE and $f !~ /^SYBASE_MDA/ and $DEBUG;
		next if $TEST_MODE and $f !~ /^SYBASE_MDA/;
		my($is_mda)="FALSE";
		$is_mda="TRUE" if $f=~/^SYBASE_MDA/;
		die "UNKNOWN KEY VALUE $f" if ! defined $f or $f eq "";;
		#print "\tDBG DBG {".$f."} $servertype \n";
		next if $f=~/^#/ or $f=~/^\s*$/;
		next if $servertype eq "SQLSVR" and $f!~/SQLSVR/;
		next if $servertype eq "SYBASE" and $f!~/SYBASE/;
		print "DBG DBG: is_mda=$is_mda ADD_MDA=$ADD_MDA f=$f IS_SYS_15=$IS_SYS_15\n"  if defined $DEBUG;
		next if $is_mda eq "TRUE" and $ADD_MDA eq "FALSE";
		next if $IS_SYS_15 eq "TRUE" and $f=~/SYBASE_12/;
		next if $servertype eq "SYBASE" and $IS_SYS_15 eq "FALSE" and $f=~/SYBASE_15/;

		# $servertype=SYBASE|SQLSVR

		print "DBG DBG: installing $f on $servertype $SERVER\n"  if defined $DEBUG;

		#next unless $f=~/$servertype/;
		chomp $f; chomp $f;
		$f=~ s/^SYBASE_MDA\s*//;
		$f=~ s/^SYBASE_12\s*//;
		$f=~ s/^SYBASE_15\s*//;
		$f=~ s/$servertype\s*//;
		$f="../MDA_procs/$f" if $is_mda eq "TRUE" and -r "../MDA_procs/$f" ;
		$f="MDA_procs/$f"    if $is_mda eq "TRUE" and -r "MDA_procs/$f" ;

		next if $FILE and $f ne $FILE;
		#print "Running $FILE";

		print "[$SERVER] Installing ",$f,$NL;
		if( $servertype eq "SQLSVR" ) {
			print "$osql_cmd_pr -i$f\n" if $DEBUG;
			#system("$osql_cmd -i$f ");
			open(CMD,"$osql_cmd -i$f |")
				or die "Cant run $osql_cmd -i$f\n";
			foreach (<CMD>) {
				print $_
					unless /sysdepends for the current/
					or /depends on the missing object/
					or /Caution: Changing any part/
					or /The object was renamed/
					or /created./
					or /procedures./
					or /rows affected\)/
					or /THE NEXT PROCEDURE MIGHT/
					or /2007/;
			}
			close(CMD);
		} elsif( $SYB_IS_ISQL_OK eq "FALSE" ) {
			print "Calling imitate_isql_cmd()\n" if $DEBUG;
			imitate_isql_cmd($connection,$f,$DATABASE);
		} else {
			print "isql -U$USER -Pxxx -S$SERVER -i$f\n" if $DEBUG;
			open(CMD,"isql -U$USER -P$PASSWORD -S$SERVER -i$f |")
				or die "Cant run isql -U$USER -S$SERVER -i$f\n";
			foreach (<CMD>) {
				print $_
					unless /$useline/
					or /sysdepends/
					or /$dbline/
					or /Object name has been changed./
					or /return status = 0/
					or /THE NEXT PROCEDURE MIGHT/
					or /2007/
					or /^\d+>\s*$/;
			}
			close(CMD);
		}

		#next unless $f=~/\.sql$/ or  $f=~/\.492$/ or  $f=~/\.10$/;
		# next if $f=~/\.10$/  and ! defined $sys10;
		# next if $f=~/\.492$/ and   defined $sys10;
		#
		#print "Installing ",$f,$NL;
			#basename($f,(".sql",".10",".492",".sqlsvr")),$NL;
		#my($query)="";
		#open( "FILE",$f ) or die "Cant Read $f\n";
		#while( <FILE> ) {
		#	if( /^go\s*$/ ) {
		#		print ".";
		#		$query="set quoted_identifier off\n$query\n";
		#		foreach ( dbi_query(-db=>$DATABASE,-query=>$query )) {
		#			print "QUERY is ",$query;
		#			print "ERROR: ",join(" ",dbi_decode_row($_)),$NL;
		#		}
		#		print "#########################",$NL;
		#		$query="";
		#	} else {
		#		$query.=$_ unless /^:r/;
		#	}
		# }
		# close(FILE);
	}

	print "Installation Completed on $SERVER $NL $NL";
}

sub imitate_isql_cmd {
	my($connection,$file,$db)=@_;
	my($rc)=open(FILE,$file);
	if( ! $rc ) {	return "Cant read $file : $!\n"; }
	my($batchsql)="";
	my($errormsgs)="";
	my($batchid)=0;

	while(<FILE>) {
		chomp;chomp;
		if(/^go/) {
			if( $batchsql =~ /:r database/ or $batchsql=~/:r dumpdb/ or $batchsql=~/^\s*$/ ) {
				$batchsql="";
				next;
			}
			$batchid++;
			print "LINE ",__LINE__,"  Batch $batchsql\n" if $TEST_MODE;
			my(@rc) = dbi_query(
				#-debug => $DEBUG,
				-query=>$batchsql,
				-connection=>$connection,
				-db=>$db );
			my($err)="";
			foreach (@rc) {
				my(@d)=dbi_decode_row($_);
				my($rowdat)=join(" ",@d);
				next if $rowdat =~ /because it does not exist in the system catalog/;
				next if $rowdat =~ /because it doesn't exist in the system catalog/;
				chomp $rowdat;
				$err.=$rowdat."\n";
			}
			print "$file: Batch $batchid: Error $err" if $err ne "";
			$batchsql="";
		} else {
			$batchsql.=$_."\n";
		}
	}
}

sub installmsg {
		print "====================================== $NL";
		print " $NL";
		print "You are installing a FREE set of system stored procedures, designed to $NL";
		print "extend the system procedures in both Sybase ASE and Microsoft SQL Server.  $NL$NL";
		print "Details on your right to use are described in the online documentation. $NL";
		print "Home Page: http://www.edbarlow.com $NL";
		print " $NL";
}
__DATA__

SYBASE uptime.sql
SQLSVR uptime.sql
SQLSVR auditdb.sqlsvr
SQLSVR auditsecurity.sqlsvr
SQLSVR badindex.492
SQLSVR block.sqlsvr
SQLSVR colconflict.492
SQLSVR collist.492
SQLSVR colnull.492
SQLSVR helpcolumn.sqlsvr
SQLSVR helpindex.sqlsvr
SQLSVR helplogin.sqlsvr
SYBASE helpmirror.sqlsvr
SQLSVR helpprotect.sqlsvr
SQLSVR lock.sqlsvr
SQLSVR lockt.sqlsvr
SQLSVR revindex.sqlsvr
SQLSVR revtable.sqlsvr
SQLSVR syntax.492
SQLSVR whodo.sqlsvr
SQLSVR bcp.sqlsvr
SQLSVR checkkey.sql
SQLSVR date.sql
SQLSVR datediff.sql
SQLSVR dbspace.sqlsvr
SQLSVR depends.sql
SQLSVR diskdevice.sqlsvr
SQLSVR dumpdevice.sql
SQLSVR find_msg_idx.sql
SQLSVR flowchart.sql
SQLSVR get_error.sql
SQLSVR grep.sqlsvr
SQLSVR groupprotect.sql
SQLSVR helpdbdev.sqlsvr
SQLSVR helpdefault.sql
SQLSVR helpgroup.sqlsvr
SQLSVR helpproc.sql
SQLSVR helprule.sql
SQLSVR helptable.sqlsvr
SQLSVR helptext.sqlsvr
SQLSVR helptrigger.sql
SQLSVR helpuser.sqlsvr
SQLSVR helpview.sql
SQLSVR id.sqlsvr
SQLSVR indexspace.sqlsvr
SQLSVR iostat.sqlsvr
SQLSVR isactive.sql
SQLSVR ls.sql
SQLSVR noindex.sqlsvr
SQLSVR objprotect.sqlsvr
SQLSVR proclib.sql
SQLSVR qspace.sqlsvr
SQLSVR read_write.sqlsvr
SQLSVR revalias.sql
SQLSVR revdb.sql
SQLSVR revdevice.sql
SQLSVR revgroup.sql
SQLSVR revlogin.sql
SQLSVR revmirror.sqlsvr
SQLSVR revsegment.sql
SQLSVR revuser.sqlsvr
SQLSVR size.sql
SQLSVR stat.sqlsvr
SQLSVR trigger.sql
SQLSVR who.sqlsvr
SQLSVR whoactive.sqlsvr
SQLSVR revrole.sqlsvr
SQLSVR helpobject.sql
SQLSVR helpdb.sqlsvr
SQLSVR help.sqlsvr
SQLSVR helpdevice.sql
SQLSVR spconfig.sqlsvr
SQLSVR server.sql
#SQLSVR record.492
#SQLSVR quickstats.sqlsvr

SYBASE_12 auditdb.10
SYBASE_15	auditdb.15
SYBASE_12 auditsecurity.10
SYBASE_15	auditsecurity.15
SYBASE_12 badindex.10
SYBASE_15	badindex.15
SYBASE_12 bcp.sql
SYBASE_15	bcp.15
SYBASE block.10
SYBASE checkkey.sql
SYBASE colconflict.10
SYBASE collist.10
SYBASE colnull.10
SYBASE date.sql
SYBASE datediff.sql
SYBASE_12 dbspace.sql
SYBASE_15 dbspace.15
SYBASE depends.sql
SYBASE_12 diskdevice.sql
SYBASE_15	diskdevice.15
SYBASE dumpdevice.sql
SYBASE find_msg_idx.sql
SYBASE flowchart.sql
SYBASE get_error.sql
SYBASE grep.sql
SYBASE groupprotect.sql
SYBASE helpcolumn.10
SYBASE_12 helpdbdev.sql
SYBASE_15	helpdbdev.15
SYBASE helpdefault.sql
SYBASE helpgroup.sql
SYBASE helpindex.10
SYBASE helplogin.10
SYBASE helpmirror.sql
SYBASE helpproc.sql
SYBASE helpprotect.10
SYBASE helprule.sql
SYBASE_12 helpsegment.sql
SYBASE_15 helpsegment.15
SYBASE_12 helptable.sql
SYBASE_15 helptable.15
SYBASE helptext.sql
SYBASE helptrigger.sql
SYBASE helptype.sql
SYBASE helpuser.sql
SYBASE helpview.sql
SYBASE id.sql
SYBASE_12 indexspace.sql
SYBASE_15	indexspace.15
SYBASE iostat.sql
SYBASE isactive.sql
SYBASE lock.10
SYBASE lockt.10
SYBASE ls.sql
SYBASE_12 noindex.sql
SYBASE_15	noindex.15
SYBASE objprotect.sql
SYBASE proclib.sql
SYBASE_12 qspace.sql
SYBASE_15 qspace.15
SYBASE_12 read_write.sql
SYBASE_15 read_write.15
SYBASE stat.sql
#SYBASE record.10
SYBASE revalias.sql
SYBASE revbindings.sql
SYBASE_12 revdb.sql
SYBASE_15	revdb.15
SYBASE_12 revdevice.sql
SYBASE_15 revdevice.15
SYBASE revgroup.sql
SYBASE revindex.10
SYBASE revkey.10
SYBASE revlogin.sql
SYBASE revmirror.sql
SYBASE revrole.sql
SYBASE revrule.10
SYBASE_12 revsegment.sql
SYBASE_15	revsegment.15
SYBASE revtable.10
SYBASE revtype.sql
SYBASE revuser.sql
SYBASE_12 segment.sql
SYBASE_15 segment.15
SYBASE size.sql
SYBASE spconfig.sql
SYBASE syntax.10
SYBASE trigger.sql
SYBASE_12 vdevno.sql
SYBASE_15 vdevno.15
SYBASE who.sql
SYBASE whoactive.sql
SYBASE whodo.10
SYBASE whoe.sql
SYBASE helpobject.sql
SYBASE helpdb.10
SYBASE help.sql
SYBASE helpdevice.sql
SYBASE server.sql
#SYBASE quickstats.10
SYBASE_MDA sp__monbackup
SYBASE_MDA sp__moncache
SYBASE_MDA sp__mondump
SYBASE_MDA sp__monengine
SYBASE_MDA sp__monio
SYBASE_MDA sp__monlock
SYBASE_MDA sp__monlocksql
SYBASE_MDA sp__monnet
SYBASE_MDA sp__monobj
SYBASE_MDA sp__monopenobj
SYBASE_MDA sp__monpwaits
SYBASE_MDA sp__monrunning
SYBASE_MDA sp__monserver
SYBASE_MDA sp__monspid
SYBASE_MDA sp__monsql
SYBASE_MDA sp__montableusage
SYBASE_MDA sp__monwaits
SYBASE_MDA sp__monunusedindex
SYBASE_MDA sp__montopn
SYBASE_MDA sp__monusedtables
