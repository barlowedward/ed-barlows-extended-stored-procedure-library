: # use perl
	# print "Using Config File $opt_c\n";
	installmsg();
	foreach my $srvr ( get_password(-type=>"sybase", -name=>undef) ) {
		my($l,$p)=get_password(-name=>$srvr);
		install_a_server($srvr,$l,$p,"sybsystemprocs");
	}

} else {
	installmsg() if ! defined $opt_U or ! defined $opt_S or !defined $opt_P;

	if( ! defined $opt_S ) {
		print "Please Enter A Server Name: ";
		$opt_S=<STDIN>;
		chomp $opt_S;
		usage("Must pass server\n")	     unless defined $opt_S and $opt_S !~ /^\s*$/;
	}

	if( ! defined $opt_U ) {
		print "Please Enter A User Name (must have sa_role): ";
		$opt_U=<STDIN>;
		chomp $opt_U;
		usage("Must pass user\n")	       unless defined $opt_U and $opt_U !~ /^\s*$/;
	}

	if( ! defined $opt_P ) {
		print "Please Enter A Password: ";
		$opt_P=<STDIN>;
		chomp $opt_P;
		usage("Must pass password\n")	   unless defined $opt_P and $opt_P !~ /^\s*$/;
	}

	if( ! defined $opt_D ) {
		print "Please Enter A Database: (sybsystemprocs)";
		$opt_D=<STDIN>;
		chomp $opt_D;
		$opt_D='sybsystemprocs' if $opt_D =~ /^\s*$/;
		usage("Must pass Database\n")	   unless defined $opt_D and $opt_D !~ /^\s*$/;
	}
	install_a_server($opt_S,$opt_U,$opt_P,$opt_D);
}

sleep 5;

sub install_a_server {
	my($configval_S,$configval_U,$configval_P,$configval_D)=@_;

	print "Testing Login To Server $configval_S as $configval_U $NL";
	if ( ! db_connect( $configval_S, $configval_U, $configval_P ) ) {
		print "*******************************\n";
		print "* Cant connect to $configval_S\n";
		print "*******************************\n";
		return;
	}

	my($sys10);
	foreach ( db_query("master","select name from sysdatabases where name='sybsystemprocs'" )) {
		$sys10=1;
	}

	#print "System 10/11 Database Found\n" if defined $sys10;
	print "Non System 10/11 Database Found\n" unless defined $sys10;

	my($proclib)="";
	my(@rc)= db_query($configval_D,"select 1 from sysobjects where name='sp__proclib_version'" );
	if($#rc>=0) {
		@rc= db_query($configval_D,"exec sp__proclib_version" );
		if( defined $opt_v ) {
			if( $opt_v > $rc[0] ) {
				print "Current Stored Procedure At Version $rc[0] (continuing) $NL";
			} else {
				print "Skipping installation as current version ($rc[0]) >= $opt_v $NL $NL";
				return;
			}
		} else {
			print "Current Stored Procedure At Version $rc[0] $NL";
		}
	}

	my @files = dirlist();
	my @PASS_2_FILES=
				("helpobject","helpdb","help","helpdevice","server","quickstats");

	my $pass_1_objects="";
	my $pass_2_objects="";

	FILE: foreach my $f ( @files ) {

		next unless $f=~/\.sql$/ or  $f=~/\.492$/ or  $f=~/\.10$/;
		next if $f=~/\.10$/  and ! defined $sys10;
		next if $f=~/\.492$/ and   defined $sys10;

		foreach (@PASS_2_FILES) {
			if( $f =~ /^$_\./ ) {
				$pass_2_objects.=$f." ";
				next FILE;
			}
		}
		$pass_1_objects.=$f." ";
	}

	#print "PASS 1: $pass_1_objects\n";
	#print "PASS 2: $pass_2_objects\n";

	do_release(     batch_die => 1,
					username => $configval_U,
					servername => $configval_S,
					password => $configval_P,
					dbname => $configval_D,
					ctlfile => undef,
					dir => undef,
					start_at_fileno => undef,
					end_at_fileno => undef,
					batch_nodie => undef,
					noexec => undef,
					norules => undef,
					nosave => undef,
					procs_only => undef,
					new_obj_only => undef,
					objects => $pass_1_objects,
	);

	do_release(     batch_die => 1,
					username => $configval_U,
					servername => $configval_S,
					password => $configval_P,
					dbname => $configval_D,
					ctlfile => undef,
					dir => undef,
					start_at_fileno => undef,
					end_at_fileno => undef,
					batch_nodie => undef,
					noexec => undef,
					norules => undef,
					nosave => undef,
					procs_only => undef,
					new_obj_only => undef,
					objects => $pass_2_objects,
	);
}

sub dirlist
{
	my($dir) = @_;
	$dir = '.' unless defined($dir);

	# Get list of files in current directory
	opendir(DIR,$dir) || die("Can't open directory $dir for reading $NL");

	my(@dirlist) = grep(!/^\./,readdir(DIR));

	closedir(DIR);

	return @dirlist;
}

sub installmsg {
		print "====================================== $NL";
		print " $NL";
		print "You are installing a FREE set of Sybase stored procedures, designed to $NL";
		print "extend the Sybase system procedures.  $NL$NL";
		print "Details on your right to use are described in the online documentation. $NL";
		print "Home Page: http://www.edbarlow.com $NL";
		print " $NL";
}
