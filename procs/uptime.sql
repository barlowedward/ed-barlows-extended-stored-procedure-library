/* Procedure copyright(c) 1995 by Edward M Barlow */

/******************************************************************************
**
** Name        : sp__uptime.sp
**
******************************************************************************/
:r database
go
:r dumpdb
go

IF EXISTS (SELECT * FROM sysobjects
           WHERE  name = 'sp__uptime'
           AND    type = 'P')
   DROP PROC sp__uptime

go

create proc sp__uptime
AS
begin
	select @@SERVERNAME, crdate
	from master..sysdatabases where dbid=db_id('tempdb')
end
go

GRANT EXECUTE ON sp__uptime  TO public
go
