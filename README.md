# Ed Barlows Extended Stored Procedure Library

Ed Barlows Extended Stored Procedure Libarary For Sybase and Microsoft SQL Sever

THIS FILE HAS BEEN SUPERSCEDED BY HTML DOCUMENTATION THAT CAN BE FOUND
ON http://www.edbarlow.com or in the file index.htm.


There are a variety of installation programs.  It is strongly recommended
that you use the perl program configure.pl.  This requires DBI and
either DBD::Sybase or DBD::ODBC if you are on windows.  You can also
use the older - unsupported bat files that are shipped but let me
repeat - they are not supported and have not been changed in a long time
and so some of the procs they install might have been superseeded

install.bat	for sybase on NT
install_mssql.bat for sql server
install_syb.bat

Ed
